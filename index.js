import * as React from 'react';
import * as ReactDOM from 'react-dom';
import  { Provider }  from 'react-redux';
import { store } from './src/_helpers';
import { App } from './src/App';



ReactDOM.render( 
        <Provider store={store}>
            <App />
        </Provider>,
 document.getElementById('root'));