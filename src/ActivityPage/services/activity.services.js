import Config from '../../Config'

export const activityService = {
    getAll,
    getDetail,
    mailSend
};



const API_URL = Config.key.OP_API_URL


function getAll(uid, pagesize, pagenumber) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
    //return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
    return fetch(`${API_URL}/Shoporders/GetPage?udid=${uid}&pagesize=${pagesize}&pagenumber=${pagenumber}`, requestOptions)
        .then(handleResponse)
        .then(activityorderRes => {
            // localStorage.setItem('user', JSON.stringify(locationsRes.Content));
            // localStorage.setItem('UserLocations', JSON.stringify(locationsRes.Content));
            let activities = activityorderRes.Content.Records;
            return activities;

        });
}

function getDetail(ordid, uid) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
    return fetch(`${API_URL}/Shoporders/GetDetails?Udid=${uid}&OrderNumber=${ordid}`, requestOptions).then(handleResponse);
}



function mailSend(mail) {

    var Email_id = mail
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
    return fetch(`${API_URL}/send?to=${Email_id}`, requestOptions)
        .then(handleResponse)





}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
