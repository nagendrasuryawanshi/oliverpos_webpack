import React from 'react';
import { connect } from 'react-redux';
import { activityActions } from '../actions/activity.action';
import { NavbarPage, CommonHeader, LoadingModal, TaxSetting } from '../../_components';
import { ActivityFirstView } from './ActivityFirstView';
import { ActivitySecondView } from './ActivitySecondView';
import { history } from '../../_helpers';
import moment from 'moment';
import { sendMailAction } from '../../_actions/sendMail.action'
import { taxRateAction } from '../../_actions/taxRate.action'
import { CustomerPopupModal } from '../../_components/CustomerPopupModal'
import { customerActions } from '../../CustomerPage/actions/customer.action'
import { encode_UDid, get_UDid } from '../../ALL_localstorage'
import config from '../../Config';
import { checkoutActions } from '../../CheckoutPage/actions/checkout.action';
import { openDb, deleteDb } from 'idb';


class Activity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: null,
            CreatedDate: null,
            pagenumber: 2,
            getPdfdateTime: null,
            msgPOP: '',
            activeFilter: false,
            activityBuffer: new Array(),
            search: '',
            pushInactivityBuffer: true,
            loading: false,
            Emailstatus: false,
            productList:[]
            //  custActive:false

        }
        this.filterCustomerOrder = this.filterCustomerOrder.bind(this);
        this.activeClass = this.activeClass.bind(this);
        this.activityTableFilter = this.activityTableFilter.bind(this);
        this.load = this.load.bind(this);
        const { dispatch } = this.props;
        dispatch(taxRateAction.getAll());

        var udid = get_UDid('UDID');
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },
        };
       idbKeyval.get('ProductList').then(val => {
          console.log("value of productlist", val)
             this.setState({productList: TaxSetting.getTaxAllProduct(val)})
                         
         }
      );
     
        // this.state.productList = getProductList();
        // this.setState({productList:getProductList()})
    }

    componentDidMount() {
        this.reload();
        localStorage.removeItem("oliver_order_payments");
        //console.log("this.state.productList", this.state.productList)
    }



    formatDate(item) {
        var mydate = new Date(item);
        var month = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"][mydate.getMonth()];
        var CreatedOn = mydate.getDate() + ' ' + month + ' ' + mydate.getFullYear();
        return CreatedOn;
    }
    componentWillMount() {

        setTimeout(() => {
            this.setState({
                loading: true
            })

        }, 2000);

    }
    reload(pagno) {

        //  const UID = localStorage.getItem('UDID');
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID = get_UDid('UDID');
        this.setState({ UDID: UID })
        var pagesize = config.key.ACTIVITY_PAGE_SIZE
        let customer_to_activity_id = (typeof localStorage.getItem("CUSTOMER_TO_ACTVITY") !== 'undefined' && localStorage.getItem("CUSTOMER_TO_ACTVITY") !== null) ? localStorage.getItem("CUSTOMER_TO_ACTVITY") : null;
        // console.log("customer_to_activity_id",customer_to_activity_id)
        if (customer_to_activity_id !== null) {
            this.props.dispatch(activityActions.getDetail(customer_to_activity_id, UID));
        }
        this.props.dispatch(activityActions.getAll(UID, pagesize, pagno));

    }

    load() {

        this.setState({
            pushInactivityBuffer: true,
        })
        if (this.state.pagenumber) {
            this.setState({
                pagenumber: this.state.pagenumber + 1,
            });
            if (this.state.pagenumber != 1) {
                this.reload(this.state.pagenumber);

            }

        }
    }


    componentWillReceiveProps(nextprops) {
        var customer_id = nextprops.single_cutomer_list.items ? nextprops.single_cutomer_list.items.Content.Id : '';
        // console.log("single_cutomer_list",nextprops);
        //console.log("customer_id",customer_id);
        // console.log("close modal",localStorage.getItem('custActive'));

        if (typeof localStorage.getItem('custActive') !== "undefined") {
            //   console.log("not undefined",localStorage.getItem('custActive'));

            if (localStorage.getItem('custActive') == 'true' && localStorage.getItem('custActive') !== '' && customer_id !== '') {
                //  console.log("customer active is true");
                $('#edit-info').modal('show');

            }
        }

        if (nextprops.sendEmail && (typeof nextprops.sendEmail !== 'undefined') && nextprops.sendEmail !== '' && nextprops.sendEmail.IsSuccess == true && this.state.Emailstatus == true) {
            //   console.log("this.props.sendEmail",nextprops.sendEmail);

            this.setState({ msgPOP: 'E-mail Receipt Sent Successfully' })
            $('#PrintMessage').modal('show')
        } else if (nextprops.sendEmail && (typeof nextprops.sendEmail !== 'undefined') && nextprops.sendEmail !== '' && nextprops.sendEmail.IsSuccess == false && this.state.Emailstatus == true) {


            this.setState({ msgPOP: 'E-mail  sending failed' })
            $('#PrintMessage').modal('show')

        }

        setTimeout(function () {
            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 500);
    }

    activeClass(item, index) {
        //console.log("Item",item);

        localStorage.removeItem("CUSTOMER_TO_ACTVITY")
        this.setState({ custActive: false, msgPOP: '' })

        $(".activity-order").removeClass("customer_active");
        $(`#activity-order-${index}`).addClass("customer_active");

        var mydate = new Date(item.date);
        var month = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"][mydate.getMonth()];
        var getPdfdate = (mydate.getMonth() + 1) + '/' + mydate.getDate() + '/' + mydate.getFullYear() + ' ' + item.time;

        var gmtDateTime = moment.utc(item.date_time)
        var itemCreatedDate = gmtDateTime.local().format(config.key.DATE_FORMAT);

        this.setState({
            active: index,

            CreatedDate: itemCreatedDate,  //this.formatDate(item.date),
            getPdfdateTime: getPdfdate,
            pushInactivityBuffer: false
        })
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID = get_UDid('UDID');
        this.props.dispatch(activityActions.getDetail(item.order_id, UID));
        this.props.dispatch(checkoutActions.getOrderReceipt());

    }

    filterCustomerOrder(e) {
        let filtered = [];
        const { value } = e.target;
        this.setState({ search: value })
        const orderlist = this.props.activities && this.props.activities.activities;
        orderlist.map((item) => {
            if (item.order_status.toUpperCase().toString().indexOf(value) != -1 || item.order_status.toLowerCase().toString().indexOf(value) != -1
                || item.total.toString().indexOf(value) != -1 || item.time.toString().indexOf(value) != -1
            )

                filtered.push(item);
            this.setState({ activeFilter: true })

        })
        if (filtered.length == 0) {
            this.setState({ activeFilter: false })
        }
        this.props.dispatch(activityActions.filteredListOrder(filtered))
    }

    clearInput() {
        $("#search-orders").val(null);
        this.setState({
            search: '',
            activeFilter: false
        })

    }

    windowLocation1(type, id) {
        // console.log("windowLocation1");
        if (type == 'statuscompleted' && id) {
            let single_Order_list = this.props.single_Order_list.Content;
            setTimeout(function () {
                localStorage.setItem("getorder", JSON.stringify(single_Order_list))
                window.location = '/refund'
                //history.push('/refund');
            }, 1000)



        }

        if (type == 'statuspending' && id) {
            localStorage.removeItem("oliver_order_payments"); //remove existing payments           
            let single_Order_list = this.props.single_Order_list.Content;
            //  console.log("single_Order_list 1" , this.props.single_Order_list)
            let addcust;
            let typeOfTax = TaxSetting.typeOfTax()
            let setOrderPaymentsToLocalStorage = new Array();
            if (typeof single_Order_list.order_payments !== 'undefined') {
                single_Order_list.order_payments.map(pay => {
                    setOrderPaymentsToLocalStorage.push({
                        "Id": pay.Id,
                        "payment_type": pay.type,
                        "payment_amount": pay.amount,
                        "order_id": single_Order_list.order_id,
                    });
                })
            }

            localStorage.setItem("oliver_order_payments", JSON.stringify(setOrderPaymentsToLocalStorage))
            let ListItem = new Array();
            single_Order_list.line_items.map(item => {
               // console.log("single_Order_list 1", item, typeOfTax);
                let productData = this.state.productList.find(prdID=>prdID.WPID == item.product_id);
                // console.log("productData", productData);
                ListItem.push({
                    line_item_id: item.line_item_id,
                    quantity: item.quantity,
                    Title: item.name,
                    Price:  typeOfTax == 'incl' ? item.subtotal + item.subtotal_tax:item.subtotal - item.subtotal_tax,
                    subtotalPrice: item.subtotal,
                    subtotaltax: item.subtotal_tax,
                    totalPrice: item.total,
                    totaltax: item.total_tax,
                    product_id: (productData.Type =="variation")?productData.ParentId : item.product_id,
                    variation_id: (productData.Type =="variation") ? item.product_id:0,
                    after_discount: (item.total == item.subtotal) ? 0 : item.total,
                    discount_amount: (item.total == item.subtotal) ? 0 : item.subtotal - item.total,
                    old_price: productData.Price,
                    incl_tax: typeOfTax == 'incl' ? item.subtotal_tax : 0,
                    excl_tax: typeOfTax == 'Tax' ? item.subtotal_tax : 0,
                })
                 // console.log("single_Order_list 2" , ListItem)
            })
            if ((typeof single_Order_list.order_custom_fee !== 'undefined') && single_Order_list.order_custom_fee.length !== 0) {
                single_Order_list.order_custom_fee.map(item => {
                    ListItem.push({
                        Title: item.note,
                        Price: item.amount !== 0 ? item.amount : null,
                    })
                })
            }
            if ((typeof single_Order_list.order_payments !== 'undefined') && single_Order_list.order_payments.length == 0 && this.props.single_Order_list && this.props.single_Order_list.order_id == 0) {
                localStorage.setItem("CARD_PRODUCT_LIST", JSON.stringify(ListItem))
                localStorage.removeItem("VOID_SALE")
            } else {
                localStorage.setItem("VOID_SALE", "void_sale")
                localStorage.removeItem("CARD_PRODUCT_LIST", null)
            }

            let orderCustomerInfo = (typeof single_Order_list.orderCustomerInfo !== 'undefined') && single_Order_list.orderCustomerInfo !== null ? single_Order_list.orderCustomerInfo : null;
            // console.log('orderCustomerInfo',single_Order_list)
            // console.log('orderCustomerInfo2',ListItem)

            if (orderCustomerInfo !== null) {
                addcust = {
                    Content: {
                        AccountBalance: 0,
                        City: orderCustomerInfo.customer_city ? orderCustomerInfo.customer_city : '',
                        Email: orderCustomerInfo.customer_email ? orderCustomerInfo.customer_email : '',
                        FirstName: orderCustomerInfo.customer_first_name ? orderCustomerInfo.customer_first_name : '',
                        Id: orderCustomerInfo.customer_id ? orderCustomerInfo.customer_id : single_Order_list.customer_id,
                        LastName: orderCustomerInfo.customer_last_name ? orderCustomerInfo.customer_last_name : '',
                        Notes: orderCustomerInfo.customer_note ? orderCustomerInfo.customer_note : '',
                        Phone: orderCustomerInfo.customer_phone ? orderCustomerInfo.customer_phone : '',
                        Pin: 0,
                        Pincode: orderCustomerInfo.customer_post_code ? orderCustomerInfo.customer_post_code : '',
                        StoreCredit: orderCustomerInfo.store_credit ? orderCustomerInfo.store_credit : '',
                        StreetAddress: orderCustomerInfo.customer_address ? orderCustomerInfo.customer_address : '',
                        UID: 0,
                    }
                }
                localStorage.setItem('AdCusDetail', JSON.stringify(addcust));
            }

            var CheckoutList = {
                ListItem: ListItem,
                customerDetail: orderCustomerInfo ? addcust : null,
                totalPrice: single_Order_list.total_amount,
                discountCalculated: single_Order_list.discount,
                tax: single_Order_list.total_tax,
                subTotal: single_Order_list.total_amount,
                TaxId: this.props.taxratelist && this.props.taxratelist.TaxId,
                status: single_Order_list.order_status,
                order_id: single_Order_list && single_Order_list.order_id,
                order_date: moment(single_Order_list.OrderDateTime).format(config.key.DATETIME_FORMAT),
                showTaxStaus: typeOfTax == 'Tax'?typeOfTax:'Incl. Tax',
            }
            localStorage.setItem("CHECKLIST", JSON.stringify(CheckoutList))
            // setTimeout(function(){
            window.location = '/checkout';
            // },500)

            // history.push('/checkout');
        }


    }

    openModal(type, content) {
        const { mail_success } = this.props;
        let id = content.order_id ? content.order_id : "";
        let cust_id = content.customer_id ? content.customer_id : '';
        // let customer_mail=content.orderCustomerInfo.customer_email;
        if (type == 'print' || type == 'email' && !id) {
            this.setState({ msgPOP: 'Please Select an order first.' })
            $('#PrintMessage').modal('show');
        }
        if (type == 'email' && id) {
            if (!content.orderCustomerInfo && content.orderCustomerInfo == null) {
                this.setState({ msgPOP: 'E-mail is Not Defined ' })
                $('#PrintMessage').modal('show');
            }
            if (content.orderCustomerInfo != null) {

                var email = content.orderCustomerInfo.customer_email
                // this.props.dispatch(activityActions.Sendmail(email));
                // if(mail_success == 'sent'){
                // let decodedString = localStorage.getItem('UDID');
                // var decod=  window.atob(decodedString);
                var UID = get_UDid('UDID');
                let requestData = {
                    "OrderNo": id,
                    "EmailTo": email,
                    "Udid": UID,
                }

                this.setState({ Emailstatus: true, loading: false })
                // this.setState({ msgPOP: 'E-mail Receipt Sent Successfully' })
                // $('#PrintMessage').modal('show');
                this.props.dispatch(sendMailAction.sendMail(requestData));
                //}
            }
        }
        if (type == 'refund' && id) {
            this.setState({ msgPOP: 'Selected order has been Refunded.' })
            $('#PrintMessage').modal('show');
        }
        if (type == 'voidsale' && id) {
            this.setState({ msgPOP: 'Selected order has been Cancelled.' })
            $('#PrintMessage').modal('show');
        }

        if (type == 'print' && id) {
        }
        if (type == 'editpop') {
            localStorage.setItem('custActive', 'true')
            //console.log("editpop modal",localStorage.getItem('custActive'));
            // let decodedString = localStorage.getItem('UDID');
            // var decod=  window.atob(decodedString);
            var UID = get_UDid('UDID');
            this.props.dispatch(customerActions.getDetail(cust_id, UID));

        }
    }
    Email_Ok() {
        this.setState({ Emailstatus: false, msgPOP: '' })


    }
    activityTableFilter() {
        // Declare variables 
        var filter, table, tr, td, i, txtValue;
        filter = $("#search-orders").val();
        table = document.getElementsByClassName("CurrentActivityTable");
        // tr = table.getElementsByClassName("activity-order");
        tr = document.getElementsByClassName("activity-order");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            let status = tr[i].getElementsByTagName("td")[0]
            let email = tr[i].getElementsByTagName("td")[1];
            let name = tr[i].getElementsByTagName("td")[2];
            let phone = tr[i].getElementsByTagName("td")[3];
            if (status) {
                txtValue = status.textContent + " " + email.textContent + " " + name.textContent + " " + phone.textContent;
                if (txtValue.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    render() {
        // let new_activity_list = new array()
        const { activities, single_Order_list, filteredListOrder, mail_success } = this.props;
        const {
            active, msgPOP, activeFilter, activityBuffer, search, pushInactivityBuffer
        } = this.state;
        //var activityBuffer=[];
        var data = {
            items: activities ? activities.activities : '',
            isLoading: true
        }
        if (pushInactivityBuffer) {
            activities && activities.activities && activities.activities.map(item => {
                //   console.log("item", item)
                if (item.order_id !== null && item.order_id !== 0) {
                    var isExist = false;
                    activityBuffer && activityBuffer.map(order => {
                        if (order.order_id === item.order_id) {
                            isExist = true;
                        }
                    }
                    )
                    //  console.log("isExist", isExist)
                    if (isExist == false) {
                        activityBuffer.push(item);
                        isExist = false;
                    }
                }
            })
        }
       
        var getDistinctActivity = {};
        activityBuffer && activityBuffer.map(item => {         

            var gmtDateTime = moment.utc(item.date_time)

        
            var dateKey= gmtDateTime.local().format(config.key.DATE_FORMAT);        

            if (!getDistinctActivity.hasOwnProperty(dateKey)) {

                getDistinctActivity[dateKey] = new Array(item);
            } else {
                if (typeof getDistinctActivity[dateKey] !== 'undefined' && getDistinctActivity[dateKey].length > 0) {                  
                    getDistinctActivity[dateKey].push(item)
                }
            }
        })
     
        if (mail_success == 'sent') {
            $('#PrintMessage').modal('show');
        }
        let someData = activities && activities.activities;

        return (
            <div>
                <div className="wrapper">
                    <div className="overlay"></div>
                    <NavbarPage {...this.props} />
                    <div id="content">
                        {this.state.loading == false ? !activityBuffer || activityBuffer.length == 0 || this.state.Emailstatus == true ? <LoadingModal /> : '' : ''}
                        <CommonHeader {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <div className="col-xs-5 col-sm-3 p-0">
                                    {this.state.loading == false ? !activities.activities || activities.activities.length == 0 || this.state.Emailstatus == true ? <LoadingModal /> : '' : ''}

                                    <div className="items">
                                        <div className="panel panel-default panel-product-list p-0 bor-customer">
                                            <div className="searchDiv relDiv">
                                                {/* <input type="text" onChange={this.filterCustomerOrder} value={search} className="form-control nameSearch search_customer_input" placeholder="Search by Name, Email or Phone" id="searchUser" data-column-index="0" /> */}
                                                <input type="text" onChange={this.activityTableFilter} className="form-control nameSearch search_customer_input" placeholder="Search by Name, Email or Phone" id="search-orders" data-column-index="0" />

                                                {/* <input type="search" className="form-control nameSearch search_customer_input" placeholder="Search by Name, Email or Phone" /> */}
                                                <svg onClick={() => this.clearInput()} className="search_customer_input2" width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64" >
                                                    <g>
                                                        <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                    </g>
                                                </svg>
                                            </div>

                                            <div className="overflowscroll" id="activityLeftPanel">


                                                <ActivityFirstView orders={getDistinctActivity} loader={data} click={this.activeClass} />



                                                {/* </table> */}
                                            </div>
                                            {activities.activities && !activities.activities.length == 0 && activities.activities.length >= config.key.ACTIVITY_PAGE_SIZE ?
                                                <div className="createnewcustomer">
                                                    <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" onClick={() => this.load()}>Load More</button>
                                                </div>
                                                :
                                                null
                                                // <div className="createnewcustomer">
                                                //    <button disabled type="button" className="btn btn-block btn-primary total_checkout bg-blue">Load More</button>
                                                // </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                                <ActivitySecondView
                                    status={true}
                                    onClick1={() => { this.windowLocation1('statuscompleted', single_Order_list ? single_Order_list.Content && single_Order_list.Content.order_id : '') }}
                                    onClick2={() => { this.windowLocation1('statuspending', single_Order_list ? single_Order_list.Content && single_Order_list.Content.order_id : '') }}
                                    printPOP={() => this.openModal('print', single_Order_list ? single_Order_list.Content : '')}
                                    customerPOP={() => this.openModal('editpop', single_Order_list ? single_Order_list.Content : '')}
                                    emailPOP={() => this.openModal('email', single_Order_list ? single_Order_list.Content && single_Order_list.Content : '')}
                                    RefundPOP={() => this.openModal('refund', single_Order_list ? single_Order_list.Content && single_Order_list.Content : '')}
                                    VoidPOP={() => this.openModal('voidsale', single_Order_list ? single_Order_list.Content && single_Order_list.Content : '')}
                                    Reciept={single_Order_list ? single_Order_list.Content && single_Order_list.Content && single_Order_list.Content.order_id : ''}
                                    CustomerInfo={single_Order_list && single_Order_list.Content && single_Order_list.Content.orderCustomerInfo ? single_Order_list.Content.orderCustomerInfo.customer_name.trim() != "" ?
                                        single_Order_list.Content.orderCustomerInfo.customer_name :
                                        single_Order_list.Content.orderCustomerInfo.customer_email : ''}
                                    Orderstatus={single_Order_list ? single_Order_list.Content && single_Order_list.Content.order_status : ''}
                                    Totalamount={single_Order_list ? single_Order_list.Content && single_Order_list.Content.total_amount : 0}
                                    total_tax={single_Order_list ? single_Order_list.Content && single_Order_list.Content.total_tax : 0}
                                    refunded_amount={single_Order_list ? single_Order_list.Content && single_Order_list.Content.refunded_amount : 0}
                                    tax_refunded={single_Order_list ? single_Order_list.Content && single_Order_list.Content.tax_refunded : 0}
                                    Balance={0}
                                    Details={single_Order_list ? single_Order_list.Content : ''}
                                    CreatedDate={this.state.CreatedDate}
                                    getPdfdateTime={this.state.getPdfdateTime}
                                    UserInfo={single_Order_list ? single_Order_list.Content && single_Order_list.Content.ServedBy : ''}
                                    cash_rounding_amount={single_Order_list ? single_Order_list.Content && single_Order_list.Content.cash_rounding_amount : 0}

                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div id="PrintMessage" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="../assets/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">{msgPOP}</h3>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true" onClick={() => this.Email_Ok()}>OK</button>
                            </div>
                        </div>
                    </div>
                </div>

                <CustomerPopupModal />

            </div>


        );
    }
}

function mapStateToProps(state) {
    const { activities, single_Order_list, filteredListOrder, mail_success, taxratelist, single_cutomer_list, sendEmail } = state;
    return {
        activities,
        single_Order_list: single_Order_list.items,
        filteredListOrder: filteredListOrder.items,
        mail_success: mail_success.items,
        taxratelist: taxratelist.taxratelist,
        single_cutomer_list: single_cutomer_list,
        sendEmail: sendEmail.sendEmail

    };
}

const connectedActivity = connect(mapStateToProps)(Activity);
export { connectedActivity as Activity };

