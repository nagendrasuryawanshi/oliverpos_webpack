import React from 'react';
import { default as NumberFormat } from 'react-number-format';
import moment from 'moment';
import Config from '../../Config'

export const ActivityViewTable = (props) => {
   // console.log("props.Discount",props.Discount);
    return (
        props != null ? (
            <table className="table tableTotalCalculated">
                <tbody>
                    <tr key={props.key}>
                        <td>Sub-total</td>
                        <td className="text-right">
                        
                        <NumberFormat value={props.Subtotal != 'NaN' ? (props.Subtotal - props.cash_round) : 0}  displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                        </td>
                    </tr>
                    {/* { (props.Discount !== 0) ?
                      <tr>
                        <td>Discount</td>
                        <td className="text-right">{props.Discount}</td>
                    </tr>:
                      <tr></tr>
                    } */}
                    <tr>
                        <td>Total Tax</td>
                        <td className="text-right">{
                            (props.tax_refunded > 0) ? <div><NumberFormat value={(props.TotalTax - props.tax_refunded)}  displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> <del style={{ marginLeft: 5 }}> <NumberFormat value={props.TotalTax} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></del> </div> : <NumberFormat value={props.TotalTax} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                        }</td>
                    </tr>

                    {props.cash_round !== 0 ?
                    <tr>
                        <td> Payment (Cash Rounding)</td>
                        <td className="text-right">{
                            <NumberFormat value={props.cash_round} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                        }</td>
                    </tr>
                    :null}
                  

                    <tr className="totalAmount">
                        <td className=""><b>Total</b></td>
                        <td className="text-right"><b>{
                            (props.refunded_amount > 0) ? <div><NumberFormat value={(props.TotalAmount - props.refunded_amount)} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> <del style={{ marginLeft: 5 }}><NumberFormat value={props.TotalAmount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></del> </div> : <NumberFormat value={props.TotalAmount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                        }</b></td>
                    </tr>

                    {props.OrderPayment && props.OrderPayment.map((item, index) => {
                        var gmtDateTime = moment.utc(item.payment_date)
                        var localDate= gmtDateTime.local().format(Config.key.DATE_FORMAT);
                        return (
                            <tr key={`payment${index}`}>
                                <td>{`${item.type} (${localDate})`}</td>  
                                {/* item.payment_date */}
                                <td className="text-right"> <NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                </td>
                            </tr>
                        )
                    })}

                    <tr style={(props.refunded_amount > 0) ? { display: "" } : { display: "none" }}>
                        <td>Refunded tax</td>
                        <td className="text-right"><NumberFormat value={props.tax_refunded} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> </td>
                    </tr>
                    <tr className="totalAmount" style={(props.refunded_amount > 0) ? { display: "" } : { display: "none" }}>
                        <td className=""><b>Refunded Amount</b></td>
                        <td className="text-right"><b><NumberFormat value={props.refunded_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></b></td>
                    </tr>


                    <tr className="totalAmount" style={(props.refunded_amount > 0) ? { display: "" } : { display: "none" }}>
                        <td className=""><b>Refund Payment's</b></td>
                        <td className="text-right"><b></b></td>
                    </tr>

                    {
                        (props.refunded_amount > 0) ? (
                            (typeof props.refundPayments !== "undefined" && props.refundPayments.length > 0) ? (
                                props.refundPayments && props.refundPayments.map((item, index) => {
                                    var gmtDateTime = moment.utc(item.payment_date)
                                    var localDate= gmtDateTime.local().format(Config.key.DATE_FORMAT);
                                    return (
                                        <tr key={`refund${index}`}>
                                            <td>{`${item.type} (${localDate})`}</td>
                                            {/* item.payment_date */}
                                            <td className="text-right"><NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </td>
                                        </tr>
                                    )
                                })
                            ) :  <tr><td></td></tr>
                        ) :  <tr><td></td></tr>
                    }
                </tbody>
            </table>
        ) : null


    )

}
