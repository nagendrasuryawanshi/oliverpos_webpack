import React from 'react';
import { ActivityViewTable } from './ActivityViewTable'
import { Markup } from 'interweave';
import { default as NumberFormat } from 'react-number-format';
import { get_UDid } from '../../ALL_localstorage'
import moment from 'moment';
import Config from '../../Config'
import { TicketEventPrint } from '../../CheckoutPage/components/TicketEventPrint'
import { string } from 'prop-types';

// function PrintElem(elem,getPdfdateTime)
// {
//    console.log("getPdfdateTime",getPdfdateTime);
//     Popup(elem);
// }

function PrintElem(data, getPdfdateTime, isTotalRefund) {
    let site_name;
    let address;
    let decodedString = localStorage.getItem('sitelist');
    let decod =decodedString?window.atob(decodedString):'';
    let siteName =decod && decod !=''?JSON.parse(decod):'';
    let udid = get_UDid('UDID');
    let register_id = localStorage.getItem('register')
    let location_name = localStorage.getItem('UserLocations')?JSON.parse(localStorage.getItem('UserLocations')):'';
    let manager = JSON.parse(localStorage.getItem('user'));
    let order_reciept = localStorage.getItem('orderreciept') ? JSON.parse(localStorage.getItem('orderreciept')) : "";
    console.log("order_reciept", order_reciept);
    siteName && siteName !='' &&  siteName.map(site => {
        if (site.UDID == udid) {
            site_name = site.StatusOpt && site.StatusOpt.instance
        }
    })


    location_name && location_name.map(item => {
        if (item.Id == register_id) {
            address = item;
        }
    })
    let baseurl = order_reciept.CompanyLogo?Config.key.RECIEPT_IMAGE_DOMAIN + order_reciept.CompanyLogo:'';
    //console.log("baseurl", baseurl)
    var subtotal = parseFloat(parseFloat((data.total_amount - data.refunded_amount)) - parseFloat(data.total_tax - data.tax_refunded)).toFixed(2);
     
    var payments = ""
    data.order_payments && data.order_payments.map((item, index) => {

        payments += (payments != "" ? "," : "") + item.type;
    })
    var refundpayments = data?data.order_Refund_payments:'';
    let barcode_image= order_reciept.AddBarcode == true ? Config.key.RECIEPT_IMAGE_DOMAIN+"/Content/img/ic_barcode.svg":''

    // var refund_payments= ""   
    // var order_refund_amount= ""
    // refundpayments && refundpayments.map((item, index) => {
    //     refund_payments += (refund_payments != "" ? "," : "") + item.type;
    //     order_refund_amount += (order_refund_amount != "" ? "," : "") + item.amount;


    // })
  //  console.log("data.OrderDateTime", data.OrderDateTime, "data", data, "data.refunded_amount", data.refunded_amount)
    // var gmtDateTime = moment.utc(item.payment_date)
    // var localDate= gmtDateTime.local().format(Config.key.DATE_FORMAT);

    var gmtDateTime = order_reciept.ShowTime == true && order_reciept.ModifiedOn  ? (moment.utc(order_reciept.ModifiedOn)).local().format(Config.key.DATETIME_FORMAT)
        : (moment.utc(order_reciept.ModifiedOn)).local().format(Config.key.DATE_FORMAT)
 //   console.log("gmtDateTime", gmtDateTime)

    //console.log("data in actvity" , data)
    var mywindow = window.open('#', 'my div', 'width=600');
    mywindow.document.write(`<html><head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title  align="left"></title>
  
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <style>
  @media screen {
       @font-face {
         font-family: 'Lato';
         font-style: normal;
         font-weight: 400;
         src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
       }

 body {
         font-family: "Lato", "Lucida Grande", "Lucida Sans Unicode", Tahoma, Sans-Serif;
       }

     }
     .demo-print {
       /*background-color: #edeff4;*/
      // padding: 15px 20px;

       border-top-left-radius: 5px;
       font-size: 18px;
       line-height: 25px;
       width: 420px;
       margin: auto;
       height: 99.7%;
    
   }
   .demo-print-body {
       position: relative;
       // padding-bottom: 25px;
       background: #fff;
       border-bottom: 0px;
   }
   .demo-print-content {
       padding: 22px;
       border: 1px solid #cecece;
      
       border-top-right-radius: 4px;
       border-top-left-radius: 4px;
   }
  
   .print-logo {
     border-radius: 5px;
     border: 1px solid #cacaca;
     text-align: center; 
     height: 200px;
     overflow: hidden;
     display: flex;
     width: 100%;
     justify-content: center;
     align-items: center;
     padding: 10px 0px;
 }
 .print-logo img {
   height: 100%;
 }
 .print-logo h4 {
     color: #979797;
     font-weight: 600; 
     padding: 0px;
     margin: 0px;
 }
   .list {
       color: #979797;
       margin: 10px 0px;
   }
   .demo-hint-text {
       border-top: 1px solid #979797;
       text-align: center;
       padding: 8px 0px 0px;
       margin-top: 0px;
       margin-bottom: 5px;
       color: #979797;
   }
   .printer-text {
       color: #979797;
       font-size: 18px;
       font-weight: 300;
       text-align: center;
   }
   .printer-text .shop-url {
       font-weight: 800
   }
   .adm-text-area{
       color: #535353;
       font-size: 20px;
       font-weight: 400;
       line-height: 24px;
       border-radius: 5px;
       border: 1px solid #cacaca;
   }
   .adm-text-area::placeholder { opacity: 0.5;}
   .printer-table table {
       margin-bottom: 0px;
       width: 100%;
       border-collapse: collapse;
   }
   .printer-table table tr td {
       padding: 0px;
       color: #979797;
       border-color: #979797 !important;
       font-weight: normal;
   }
   .printer-table table tr td:first-child {
       padding-right: 10px;
   }
   .font-bold {
       font-weight: bold !important;
   }
   .printer-table table tbody tr td,
   .printer-table table tfoot tr td {
       border-top: 0px;
   }
   .printer-table table tbody tr:first-child td {
       border-top: 1px solid #979797;
   }
   .printer-table table tfoot tr:first-child td,
   .printer-table table tfoot tr:last-child td {
       border-top: 1px solid #979797;
   }
   .printer-table table tfoot tr td:first-child {
       padding-left: 20px !important;;
   }
   .printer-table table tfoot tr td:last-child,
   .printer-table table tbody tr td:last-child {
     text-align: right !important;
 }
   .print-ticket-info {
       margin-top: 10px;
       margin-bottom: 5px;
   }
   .printer-table table tfoot tr:first-child td .print-ticket-info
   {
       margin-bottom: 10px;
   }
   .print-ticket-info p {
     padding: 1px
   }
   .shop-url {
     margin-top: 0px;
     margin-bottom: 5px;
   }
   .list p {
     margin: 0px;
     margin-bottom: 3px;
 }
 .print-ticket-info p {
   margin: 0px;
  }

   
 </style></title>`);
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write(`</head><body>`);
    mywindow.document.write(
        `
        <!-- open bracket -->
          <div class="demo-print" id="page-sidebar-right">

             <div class="demo-print-body">
            
                <div class="demo-print-content">
               
                  <!-- open bracket -->
                      
                  <div class="print-logo">

                    
                  ${ baseurl ? ` <img src=${baseurl}   />`:'' }
                       
                      ${!baseurl && data.ShopName ?  `<h4>  ${ data.ShopName }</h4>`: ''}                     
                   
                    
                  
                    </div>
                 
                  <div class="list">
                
                    <p>${order_reciept.DateText?order_reciept.DateText:'DateTime'}:${gmtDateTime?gmtDateTime:''} </p>
                   
                    <p> ${order_reciept.Sale?order_reciept.Sale:'Sale'}:#${data.order_id?data.order_id:''} </p>
                    ${order_reciept.ShowServedBy == true ?
                    `<p>${order_reciept.ServedBy?order_reciept.ServedBy:'ServedBy'}: ${manager.display_name?manager.display_name:''}</p>`
                    :''}
                    <p> ${order_reciept.TaxIdText?order_reciept.TaxIdText:'TaxId'}:${ order_reciept.TaxId?order_reciept.TaxId:''}</p>
                    
                    <p>${order_reciept.PaidWith?order_reciept.PaidWith:'PaidWith'}: ${payments?payments:''}</p>
                    
                    ${data.order_status && data.order_status == 'park_sale' ||  data.order_status == "lay_away"?
                    `<p>Order Status:${data.order_status == 'park_sale'?"Park Sale" : data.order_status == "lay_away"? "Lay Away" :""}</p>`
                    :''
                }
                 </div>
                 <h4 class="demo-hint-text">
                    ${order_reciept.CustomText?order_reciept.CustomText:''}
                  </h4>
                  <div class="printer-table">
      
                    <table class="table table-border">
                    <tbody>
                    ${data && data.line_items.map((item, index) => {
            return (`<tr> 
                              <td >${
                (item.quantity_refunded < 0 || isTotalRefund == true) ? `${isTotalRefund == true ? 0 :item.quantity + item.quantity_refunded}
                                    <del style={{ marginLeft: 5 }}>${item.quantity}</del>` : item.quantity
                }</td>
                  
                              <td> <div class="print-ticket-info">
                              <p>${item.name}
                              </p></div></td>
                              <td class="font-bold">
                              ${
                (item.amount_refunded > 0 || isTotalRefund == true) ? `${parseFloat(item.total - item.amount_refunded).toFixed(2)} <del style={{ marginLeft: 5 }}>${parseFloat(item.total).toFixed(2)}</del>` : parseFloat(item.total).toFixed(2)
                }
                              </td></tr>`)
        }
        )}
                    </tbody>
      
                   <tfoot>
                   ${subtotal ?
                    `<tr>
                        <td colspan='2' align="left">
                            ${order_reciept.SubTotal?order_reciept.SubTotal:''}
                        </td>
                        <td>
                        ${subtotal}
    
                        </td>
                    </tr>`:'' }
                    ${data.discount ? 
                    `<tr>
                        <td colspan='2' align="left">
                            ${order_reciept.Discount?order_reciept.Discount:''}
 
                        </td>
                        <td>
                        ${data.discount?data.discount:''}  
                        </td>
                    </tr>`:''
                        }
                        
                    <tr>
                        <td colspan='2' align="left">
                            
                            ${order_reciept.Tax?order_reciept.Tax:''}
  
                        </td>
                        <td>
                        ${(data.tax_refunded > 0) ? `${(data.total_tax - data.tax_refunded).toFixed(2)} <del style={{ marginLeft: 5 }}>${data.total_tax.toFixed(2)}</del> ` : data.total_tax.toFixed(2)
                     }
                        </td>
                    </tr>
                   ${data.refunded_amount > 0 ?  
                    `<tr class="bt-top">
                       <td  colspan='2' align="left">
                        Refund Amount
                    </td>
                    
                     <td>
                    ${(data.refunded_amount > 0)?data.refunded_amount.toFixed(2):''}
                    </td>
                </tr>`:''}
                  
               
                    ${(data.refunded_amount > 0) ? 
                      ` <tr>
                      <td  colspan='2' align="left">
                        <b>Refund Payment's :</b>
                       </td> 
                       <td  > 
                      
                      </td> 
                       </tr>`
                       :""
                    }
                    ${(data.refunded_amount > 0) ? (
                      
                          refundpayments && refundpayments.map((item, index) => 
                          {   
                              var gmtDateTime = moment.utc(item.payment_date)
                              var localDate= gmtDateTime.local().format(Config.key.DATE_FORMAT);
                          return(
                           `<tr  key=${index}>
                              <td colspan="2">${item.type} (${localDate})</td>
                              <td >${item.amount}</td>
                           </tr>`
                           ) })
                          
                       
                      ) :  ``
                  }
                
                  <tr class="bt-top">
                  <td colspan='2' align="left">
                    <b>${order_reciept.Total?order_reciept.Total:''}</b>
                  </td>
                  <td>
                  ${(data.refunded_amount > 0) ? `${(data.total_amount - data.refunded_amount).toFixed(2)} <del style={{ marginLeft: 5 }}>${data.total_amount.toFixed(2)}</del>` : data.total_amount.toFixed(2)
             }
 
 
                  </td>
              </tr  >  
                
                      </tfoot>
      
                    </table>
                  </div>
                  <h4 class="demo-hint-text">
                    ${order_reciept.Returnpolicy?order_reciept.Returnpolicy:''}
                  </h4>
                  <div class="printer-text">
                 
                <div class="printer-barcode">
                       <img src=${barcode_image}  />

                    </div>
                    <p class="shop-url">
                    ${site_name?site_name:''}
                      <address>
                      ${address && address.Address1 ? address.Address1 : ''} ${address && address.Address2 ? address.Address2 : ''} 
                      ${address && address.Zip ? address.Zip : ''} ${address && address.City ? address.City : ''} ${address && address.CountryName ? address.CountryName : ''}
                      </address>
                    </p>
                   
                  </div>
                 
                  <!-- close bracket -->
               
                </div>
             
               </div> 
          </div>
          <!-- close bracket -->     
          `


    );
    mywindow.document.write('</body style="text-align: center;></html>');

    mywindow.print();
    mywindow.close();

    return true;

  
}
// function custmerPopup(){
//     console.log("hello customer");    
//     $('#edit-info').modal('show');

//      }

export const ActivitySecondView = (props) => {


   // console.log("note12", (props.Totalamount - props.refunded_amount).toFixed(2));
    var isTotalRefund = false;
    if((props.Totalamount - props.refunded_amount).toFixed(2) == '0.00'){
        isTotalRefund = true 
    }
    //console.log("exsistAmountIs", isTotalRefund)
    var subtotal = 0;
    if (props.Details) {

        subtotal = parseFloat(parseFloat((props.Details.total_amount - props.Details.refunded_amount)) - parseFloat(props.Details.total_tax - props.Details.tax_refunded)).toFixed(2);
    }



    function Ticket_print() {
        //let address;
        let site_name;
        let EventDetailArr = []
        var data = props.Details ? props.Details.TicketDetails : ''
        var orderList = props.Details ? props.Details.order_payments : ''
        let inovice_Id = props.Details ? props.Details.order_id : ''
        let manager =localStorage.getItem('user')?JSON.parse(localStorage.getItem('user')): '';
        let ServedBy = props.Details ? props.Details.ServedBy : ''
        let register = props.Details ? props.Details.RegisterName : ''
        let register_id = localStorage.getItem('register')
        let address = props.Details ? props.Details.LocationName : '';
        let decodedString = localStorage.getItem('sitelist') ? localStorage.getItem('sitelist') : "";
        let decod = decodedString != "" ? window.atob(decodedString) : "";
        let siteName = decod && decod != "" ? JSON.parse(decod) : "";
        let udid = get_UDid('UDID');




        //console.log("data", data);
        if (siteName && siteName != "") {
            siteName && siteName.map(site => {
                if (site.UDID == udid) {
                    site_name = site.StatusOpt && site.StatusOpt.instance
                }
            })
        }

        // location_name && location_name.map(item => {
        //     if (item.Id == register_id) {
        //         address = item;
        //         console.log("uidid" , address)

        //     }
        // })

        data && data.map(event => {

            var pp = JSON.parse(event)

            EventDetailArr.push(pp)


        })

        if (EventDetailArr) {
            // if (process.env.ENVIRONMENT == 'devtickera') {
               // console.log("EventDetailArr", EventDetailArr);
                TicketEventPrint.EventPrintElem(EventDetailArr, orderList, manager, register, address, site_name, ServedBy, inovice_Id)
           // }
        }
    }
    //console.log("activityprops",subtotal);

    // var UserDisplayName="";
    //    if(localStorage.getItem("user"))
    //    {
    //        var userinfo= JSON.parse(localStorage.getItem("user"));
    //        UserDisplayName= props.Reciept ? userinfo &&  userinfo.display_name.trim() !="" ?userinfo.display_name :userinfo.user_email:"";

    //        console.log("userinfo",userinfo);
    //        console.log("UserDisplayName",UserDisplayName);
    //    }

    //  var subtotal=props.Details.total_amount-props.Details.total_tax;
    //  var sub=subtotal.toString();
console.log("props.CreatedDate",props.CreatedDate,"props.Details", props.Details , "props.Details.OrderDateTime", props.Details.OrderDateTime);
console.log("dateCal", props.CreatedDate ? props.CreatedDate : props.Details && props.Details.OrderDateTime ?  props.Details.OrderDateTime:"")
    var gmtDateTime = moment.utc(props.CreatedDate ? props.CreatedDate : props.Details && props.Details.OrderDateTime ?  props.Details.OrderDateTime:"")
    console.log("AfterdateCal",gmtDateTime)
    var CreateDate = gmtDateTime==""?"": gmtDateTime.local().format(Config.key.DATE_FORMAT);
    console.log("CreateDate",CreateDate)

    let order_reciept=localStorage.getItem('orderreciept') ?JSON.parse(localStorage.getItem('orderreciept')):"";
    let baseurl=Config.key.RECIEPT_IMAGE_DOMAIN+order_reciept.CompanyLogo;
    let barcode_image= Config.key.RECIEPT_IMAGE_DOMAIN+"/Content/img/ic_barcode.svg"
    console.log("baseurl",props)
    return (
        <div className="col-xs-7 col-sm-9 mt-4">
        <div style={{display: 'none' }}>
                <img src={baseurl} width="50px"  /> 
                </div>
            <div  style={{display: 'none' }} >
           <img src={barcode_image} width="50px"  />

            </div>       
            <div className="panel panel-custmer">
                <div className="panel-default">
                    <div className="panel-heading text-center customer_name font18 visible2">
                        {CreateDate}
                        {/* {props.CreatedDate} */}
                    </div>
                    <div className="panel-body customer_history p-0">
                        <div className="col-sm-12">
                            <div className="row">
                                <div className="col-lg-9 col-sm-8 plr-8">
                                    <form className="customer-detail">

                                        <div className="form-group">
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Receipt Nr.:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p> {props.Reciept}</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Customer Details:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p className="text-blue pointer" onClick={props.customerPOP} > {props.CustomerInfo}</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">User:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p>{props.UserInfo}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Order Status:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p> {props.Orderstatus ? props.Orderstatus.replace(/_/g, '\u00a0') : ''}</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Total Amount:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p> {(props.Totalamount - props.refunded_amount).toFixed(2)}</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Balance:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p> {props.Balance}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-3 col-sm-4 bl-1 plr-8">
                                    <form action="#" className="w-250-px pl-3">
                                        {props.Orderstatus == 'completed' ?
                                            <div className="w-100-block button_with_checkbox pb-0 danger" onClick={props.onClick1}>
                                                <input type="radio" id="test1" name="radio-group" />
                                                <label htmlFor="test1" className="label_select_button">Refund Sale</label>
                                            </div>
                                            :
                                            props.Orderstatus == "pending"  || props.Orderstatus == "lay_away" || props.Orderstatus == "park_sale" || props.Orderstatus == "init sale" || props.Orderstatus == "processing" ?
                                                <div className="w-100-block button_with_checkbox " onClick={props.onClick2}>
                                                    <input type="radio" id="test1" name="radio-group" />
                                                    <label htmlFor="test1" className="label_select_button">Open Sale</label>
                                                </div>
                                                :
                                                props.Orderstatus == "refunded" ?
                                                    <div className="w-100-block button_with_checkbox disabled" >
                                                        <input type="radio" id="test1" name="radio-group" onClick={props.RefundPOP} />
                                                        <label htmlFor="test1" className="label_select_button" >Refunded Sale</label>
                                                    </div>
                                                    :
                                                    (props.Orderstatus == "void_sale" || props.Orderstatus == "cancelled") ?
                                                        <div className="w-100-block button_with_checkbox disabled" onClick={props.VoidPOP}>
                                                            <input type="radio" id="test1" name="radio-group" />
                                                            <label htmlFor="test1" className="label_select_button" >Cancelled</label>
                                                        </div>
                                                        : null
                                        }

                                        <div className="w-100-block text-center mt-1 mb-1 notes">
                                            <div className="line-copy"></div>
                                        </div>
                                        <div className="w-100-block button_with_checkbox" >
                                            <input type="radio" id="test2" name="radio-group" onClick={props.emailPOP} />
                                            <label htmlFor="test2" className="label_select_button">Email Receipt</label>
                                        </div>
                                        {props.Details && props.Details.TicketDetails != "" ?
                                            <div className="w-100-block button_with_checkbox">

                                                <input type="radio" id="test3" name="radio-group" onClick={props.Details != "" ? () => { PrintElem(props.Details, props.getPdfdateTime, isTotalRefund); Ticket_print() } : props.printPOP} />
                                                <label htmlFor="test3" className="label_select_button">Print Receipt</label>
                                            </div> :
                                            <div className="w-100-block button_with_checkbox">

                                                <input type="radio" id="test3" name="radio-group" onClick={props.Details != "" ? () => PrintElem(props.Details, props.getPdfdateTime, isTotalRefund) : props.printPOP} />
                                                <label htmlFor="test3" className="label_select_button">Print Receipt</label>
                                            </div>
                                        }


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer bt-0 p-0">
                        <div className="table_head">
                            <table className="table CurrentActivityTable_shortingHead">
                                <thead>
                                    <tr>
                                        <th colSpan="4">Item</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div className="overflowscroll window-header-cname_chistory">
                            <table className="table CurrentActivityTable_shortingBody">
                                <colgroup>
                                    <col width="50" />
                                    <col width="*" />
                                    <col width="120" />
                                </colgroup>
                                <tbody>
                                    {props.Details ?
                                        props.Details.line_items.map((item, index) => {
                                            //console.log("line_items",item);
                                            var varDetail = item.ProductSummery.toString();
                                          //  console.log("varDetail", varDetail)
                                            var _style = varDetail ? "baseline" : "middle";
                                            return (
                                                <tr key={index} >
                                                    <td style={{ verticalAlign: _style }}>
                                                        {
                                                            (item.quantity_refunded < 0 || isTotalRefund == true) ? <div>{isTotalRefund == true?0:item.quantity + item.quantity_refunded}
                                                                <del style={{ marginLeft: 5 }}>{item.quantity}</del></div> : item.quantity
                                                        }
                                                    </td>
                                                    <td><Markup content={item.name} />
                                                        {varDetail ?
                                                            <p className="help-block help-text">{varDetail} </p> : ""}


                                                    </td>
                                                    <td className="text-right" style={{ verticalAlign: _style }}>
                                                        {
                                                            (item.amount_refunded > 0 || isTotalRefund == true) ? <div><del style={{ marginRight: 10 }}>
                                                                <NumberFormat value={item.total} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></del><NumberFormat value={(item.total - item.amount_refunded).toFixed(2)} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> </div> : props.Details.discount != 0 ? <div><del style={{ marginRight: 10 }}>{item.subtotal}</del><NumberFormat value={item.total} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                                </div> : <NumberFormat value={item.total} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                        }
                                                        {/* <NumberFormat value={item.total} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> */}
                                                    </td>
                                                </tr>


                                            )
                                        })
                                        : <tr><td></td></tr>

                                    }
                                    {props.Details ?
                                        props.Details.order_custom_fee.map((item, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{item.note}</td>
                                                    <td />
                                                    <td className="text-right"><NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></td>
                                                </tr>
                                            )

                                        })
                                        : <tr><td></td></tr>
                                    }

                                    {props.Details ?
                                        props.Details.order_notes.map((item, index) => {
                                            //  console.log("note12",item);
                                            return (
                                                <tr key={index}>
                                                    <td>Note:</td>
                                                    <td>{item.note}</td>
                                                    <td className="text-right"> </td>
                                                </tr>


                                            )

                                        })
                                        : <tr><td></td></tr>

                                    }
                                </tbody>
                            </table>

                            <div className="col-lg-7 col-sm-6 col-xs-4 p-0">
                                {props.Details && (props.Details.discount !== 0) ?
                                    <p className="pl-2 fs16 normal-text "><i className="italicfont">Total Discount :</i> <span>{props.Details.discount}</span></p>
                                    : <p></p>
                                }
                            </div>
                            <div className="col-lg-5 col-sm-6 col-xs-8 p-0">
                                {/* <table className="table tableTotalCalculated"> */}

                                {props.Details ?
                                    <ActivityViewTable
                                        Subtotal={subtotal ? subtotal : 0}
                                        Discount={props.Details.discount}
                                        TotalTax={props.Details.total_tax}
                                        tax_refunded={props.Details.tax_refunded}
                                        TotalAmount={props.Details.total_amount}
                                        refunded_amount={props.Details.refunded_amount}
                                        OrderPayment={props.Details.order_payments}
                                        refundPayments={props.Details.order_Refund_payments}
                                        cash_round={props.cash_rounding_amount}
                                    />
                                    : ""}
                                {/* </table> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )

}
