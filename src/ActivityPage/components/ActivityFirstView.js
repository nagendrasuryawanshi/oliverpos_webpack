import React from 'react';
import moment from 'moment';
import { LoadingModal } from '../../_components/LoadingModal';
import { default as NumberFormat } from 'react-number-format';
import Config from '../../Config'


export const ActivityFirstView = (props) => {
    var current_date = moment().format(Config.key.DATE_FORMAT);
    //console.log("props", props);
    var ordersDate = new Array();
    var orders = props.orders;

    //console.log("orders",orders)
    if (typeof props.orders !== 'undefined') {
        for (const key in orders) {
            if (orders.hasOwnProperty(key)) {
                ordersDate.push(key)
            }
        }
    }
    var items=''
    if (typeof props.loader.items !== 'undefined') {
         items=props.loader.items
    } 
      
    return (
        
        <table className="table CurrentActivityTable">
            <colgroup>
                <col width="*" />
                <col width="40" />
            </colgroup>
            
            {(props.loader.isLoading && !items ) ?
                           <tbody>
                                <tr>
                                    <td>  <i className="fa fa-spinner fa-spin" >loading...</i></td>
                                </tr>
                                </tbody>
                                :
            ordersDate && ordersDate.map((getDate, index) => {
                

                var gmtDateTime = moment.utc(getDate)
                var displayLocalDate= gmtDateTime.local().format(Config.key.DATE_FORMAT);
                //console.log("OrderDate",getDate, moment.utc(getDate),displayLocalDate)
                return (
                    <tbody key={`order_date_${index}`}>
                        <tr>
                            <th colSpan="2" className="tablecaption text-center">
                                {current_date == displayLocalDate ? 'today' : displayLocalDate}
                                {/* {current_date == moment(getDate).format(Config.key.DATE_FORMAT) ? 'today' : moment(getDate).format(Config.key.DATE_FORMAT)} */}
                            </th>
                        </tr>
                        {
                            orders[getDate] && orders[getDate].map((order, index) => {
                             //   console.log("Order",order)
                                var gmtDateTime = moment.utc(order.date_time)
                               var time= gmtDateTime.local().format('LT');
                                return (
                                    <tr className={`activity-order ${typeof localStorage.getItem("CUSTOMER_TO_ACTVITY") !== 'undefined' && localStorage.getItem("CUSTOMER_TO_ACTVITY") == order.order_id? index == 0 ? 'customer_active' : '' : ''} `} key={`customer_order_${index}`} id={`activity-order-${order.order_id}`} style={{ cursor: "pointer" }} onClick={() => props.click(order, order.order_id)}>
                                        <td>
                                            {
                                                (order.refunded_amount > 0) ? <div>{<NumberFormat value={parseFloat(order.total - order.refunded_amount)} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                } <del>{<NumberFormat value={parseFloat(order.total)} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />}</del> </div> : <NumberFormat value={parseFloat(order.total)} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            }
                                            <p className="comman_subtitle">{order.order_status.replace(/_/g, '\u00a0')}</p>
                                        </td>
                                        <td style={{display : "none"}}>{order.CustEmail}</td>
                                        <td style={{display : "none"}}>{order.CustFullName}</td>
                                        <td style={{display : "none"}}>{order.CustPhone}</td>
                                        <td className="text-right">
                                        {time.replace(/ /g, '\u00a0')}
                                        {/* {order.time.replace(/ /g, '\u00a0')}moment().format('LT'); */}
                                        </td>
                                    </tr>
                                )
                            })
                        }

                    </tbody>
                )
            })
            }
        </table>


    );
}
