import { activityConstants } from '../constants/activity.constants'
import { activityService } from '../services/activity.services';



export const activityActions = {
    getAll,
    getDetail,
    filteredListOrder,
    Sendmail,
    getOne,
};

function getOne(uid, pagesize, pagenumber) {
    return dispatch => {
        dispatch(request());

        activityService.getAll(uid, pagesize, pagenumber)
            .then(
                activities => 
                   dispatch(success(activities),
                    localStorage.setItem("CUSTOMER_TO_ACTVITY" , activities && activities[0] && activities[0].order_id)
                  ),
                error => dispatch(failure(error.toString()))
            );

            function request() { return { type: activityConstants.GETONE_REQUEST } }
            function success() { return { type: activityConstants.GETONE_SUCCESS } }
            function failure(error) { return { type: activityConstants.GETONE_FAILURE, error } }
    };
}

function getAll(uid, pagesize, pagenumber) {
    return dispatch => {
        dispatch(request());

        activityService.getAll(uid, pagesize, pagenumber)
            .then(
                activities =>dispatch(success(activities)),
                error => dispatch(failure(error.toString()))
            );
    };


    function request() { return { type: activityConstants.GETALL_REQUEST } }
    function success(activities) { return { type: activityConstants.GETALL_SUCCESS, activities } }
    function failure(error) { return { type: activityConstants.GETALL_FAILURE, error } }





}
function getDetail(ordid, UID) {
     return dispatch => {
        dispatch(request());

        activityService.getDetail(ordid, UID)
            .then(
                single_Order_list => dispatch(success(single_Order_list)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: activityConstants.GET_DETAIL_REQUEST } }
    function success(single_Order_list) { return { type: activityConstants.GET_DETAIL_SUCCESS, single_Order_list } }
    function failure(error) { return { type: activityConstants.GET_DETAIL_FAILURE, error } }



}

function filteredListOrder(filteredListOrder) {
    return dispatch => {
        dispatch(success(filteredListOrder))
    };

    function success(filteredListOrder) { return { type: activityConstants.GET_FILTER_ORDER_LIST_SUCCESS, filteredListOrder } }

}

function Sendmail(mail) {
    return dispatch => {
        dispatch(request());
        activityService.mailSend(mail)
            .then(
                mail_success => dispatch(success(mail_success)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: activityConstants.SEND_EMAIL_REQUEST } }
    function success(mail_success) { return { type: activityConstants.SEND_EMAIL_SUCCESS, mail_success } }
    function failure(error) { return { type: activityConstants.SEND_EMAIL_FAILURE, error } }

}
