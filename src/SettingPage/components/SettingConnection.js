import React from 'react';
import { connect } from 'react-redux';
import { settingActions } from '../actions/setting.action';



class SettingConnection extends React.Component {

    render() {
        return (
            <div id="setting-connection" className="tab-pane fade">
            </div>
        )
    }

}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedSettingConnection = connect(mapStateToProps, settingActions)(SettingConnection);
export { connectedSettingConnection as SettingConnection };
