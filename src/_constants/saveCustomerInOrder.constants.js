export const saveCustomerInOrderConstants = { 
    SCIO_REQUEST: 'SCIO_REQUEST',
    SCIO_SUCCESS: 'SCIO_SUCCESS',
    SCIO_FAILURE: 'SCIO_FAILURE',
    SCIO_REFRESH: 'SCIO_REFRESH',   
};
