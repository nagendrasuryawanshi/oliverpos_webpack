import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class HomePage extends React.Component {
    componentDidMount() {
      //  this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
       // return (e) => this.props.dispatch(userActions.delete(id));
    }

    populateDate(){
        NewCssCal('demo1','yyyyMMdd','dropdown',true,'24',true);
    }

    render() {
        const { user, users } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
               <div>
                   <img src="assets/img/frame02-03s.gif" />
                  <div>Welcome To Oliver POS</div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };