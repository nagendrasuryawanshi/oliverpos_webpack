import Config from '../../Config'

export const registerService = {    
    getAll,  
  };

  const API_URL=Config.key.OP_API_URL

  function getAll() {    
    const requestOptions = {
      method: 'GET',           
    //  mode: 'no-cors',
    headers: {
      "access-control-allow-origin": "*",
      "access-control-allow-credentials": "true", 
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
  } 
  ,mode: 'cors'
  };

  //return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
  let decodedString = localStorage.getItem('UDID');
  var decod=  window.atob(decodedString);
  var UDID= decod;
  console.log("registerService",UDID);
  return fetch(`${API_URL}/ShopAccess/GetRegisters?Udid=`+UDID+'&LocId='+localStorage.getItem('Location'), requestOptions)
      .then(handleResponse)
      .then(registers => {
              let _registers=registers.Content;
              return _registers;
        
          
      });
  }
  
function handleResponse(response) {
  return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
          if (response.status === 401) {
              // auto logout if 401 response returned from api
              logout();
              location.reload(true);
          }

          const error = (data && data.message) || response.statusText;
          return Promise.reject(error);
      }

      return data;
  });
}