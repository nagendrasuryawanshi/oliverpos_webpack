import React from 'react';
import { connect } from 'react-redux';
import { registerActions } from '../actions/register.action';
import { history } from "../../_helpers";
import { LoadingModal } from '../../_components/LoadingModal';
import  Cookies from 'universal-cookie'
import {encode_UDid,get_UDid} from '../../ALL_localstorage'

const cookies = new Cookies();

class LoginRegisterView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {        
            check: true,
            LocationName: ''

        };
       // var {dispatch}=this.props;
         // dispatch(registerActions.getAll());
        //   $(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   

    }
    componentWillMount(){

        console.log("Location", localStorage.getItem('Location'));
        //let getudid = localStorage.getItem('UDID');
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var getudid= get_UDid('UDID');
        this.setState({LocationName:localStorage.getItem(`last_login_location_name_${getudid}`)?localStorage.getItem(`last_login_location_name_${getudid}`):''})
        
       var location= localStorage.getItem('Location');
        if(location)
        {
           // history.push('/loginpin');     
            const {dispatch}=this.props;
            dispatch(registerActions.getAll());
            var register =this.props.registers;
            console.log("this.props.registers",register);
            if(register && Array.isArray(register))
            {
                setTimeout(function () {
                $(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   
                },500)
            }         
        }
      else
      {       
        history.push('/login_location');
      }
    }
    handleSubmit(item) {      
        localStorage.setItem('register', item.Id);       
        localStorage.setItem('registerName', item.Name);      
  //  let getudid = localStorage.getItem('UDID');
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var getudid= get_UDid('UDID');
     localStorage.setItem(`last_login_register_id_${getudid}`, item.Id);
     localStorage.setItem(`last_login_register_name_${getudid}`, item.Name); 
        console.log("newregister", localStorage.getItem(`last_login_register_id_${getudid}`));
        console.log("newregisterName", localStorage.getItem(`last_login_register_name_${getudid}`));

        history.push('/loginpin');
        
    }
    windowLocation(location) {
        window.location = location
    }
     componentDidMount(){
    //      const {dispatch}=this.props;
    //    dispatch(registerActions.getAll());
        //$(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   
     }
     componentWillReceiveProps(next){
        console.log("Next",next);
        if(next.registers.registers && Array.isArray(next.registers.registers) ){
            setTimeout(function () {
            $(".chooseregisterLinks").niceScroll({ styler: "fb", cursorcolor: "#2BB6E2", cursorwidth: '3', cursorborderradius: '10px', background: '#d5d5d5', spacebarenabled: false, cursorborder: '', scrollspeed: 60, cursorfixedheight: 70 });
            },500)
        }
    }
     checkStatus(value) {

        this.setState({ check: value })
    }
    render() {
        var {registers} = this.props.registers;
        const {  check } = this.state;

       return (
        <div className="bgimg-1">
        { !registers || registers.length == 0 ? <LoadingModal/>:''}
        <div className="content_main_wapper">
        <a href="/login" className="aonboardingbackicon">
              <button className="button onboardingbackicon">&nbsp;</button>&nbsp; Go Back
          </a>
  
           <div className="aerrow pull-right arrowText aonboardingbackicon right-align_absolute">
             <a href="javascript:void(0);" >{this.state.LocationName}</a>
           </div>
              <div className="onboarding-loginBox">
              <div className="login-form">
                   <div className="onboarding-pg-heading">
                      <h1>Choose Your Register</h1>
                      <p>
                      You’ve set up the following registers online. Please choose the register to start working on.
                      </p>
                  </div>
                  <form className="onboarding-login-field" action="#">
                    <ul className="list-group chooseregisterLinks m-b-20" style={{height: 230}}>
                    {
                       
                       registers && registers.map((item, index) => {
                                    return (
                                        // <li  key={index}>{item.Name}</li>
                                        <li key={index} className="list-group-item" onClick={()=>this.handleSubmit(item)} ><a href="#">{item.Name}<img src="assets/img/green-arrow.png"/></a></li>
                                    )
                                })
                              
                                }
                       
                     </ul>
                      <div className="checkBox-custom m-b-20">
                         <div className="checkbox">
                             <label className="customcheckbox">Remember Register
                               <input type="checkbox" onClick={() => this.checkStatus(!check)} checked="checked" readOnly={true} />
                                <span className={'checkmark ' + (check == true ? ' checkUncheck' : ' ')}></span>
                             </label>
                         </div>
                     </div> 
               </form>
              </div>
          </div>
                <div className="powered-by-oliver">
                    <a href="javascript:void(0)">Powered by Oliver POS</a>
                </div>
        
      </div>
      </div>



        );
    }
}

function mapStateToProps(state) {
    const { registers } = state;
    return {
        registers
    };
}

const connectedLoginLoginRegisterView = connect(mapStateToProps)(LoginRegisterView);
export { connectedLoginLoginRegisterView as LoginRegisterView };