import { registerConstants } from "../constants/register.constants";
import { registerService } from "../services/register.service";
import { history } from '../../_helpers/history';
import {encode_UDid,get_UDid} from '../../ALL_localstorage'

export const registerActions = {
  
  getAll,
 
  };

  function getAll() {
    return dispatch => {
    //  dispatch(request());
  
      registerService
        .getAll()
        .then(
          registers => {dispatch(success(registers))
            // if only one resister is avilable the no need to select register by user---------------
            if(registers && registers.length===1)
            {
              
              localStorage.setItem('register', registers[0].Id);       
              localStorage.setItem('registerName', registers[0].Name);      
            //  let getudid = localStorage.getItem('UDID');
              // let decodedString = localStorage.getItem('UDID');
              // var decod=  window.atob(decodedString);
              var getudid= get_UDid('UDID');
              localStorage.setItem(`last_login_register_id_${getudid}`, registers[0].Id);
              localStorage.setItem(`last_login_register_name_${getudid}`, registers[0].Name); 
              console.log("newregister", localStorage.getItem(`last_login_register_id_${getudid}`));
              console.log("newregisterName", localStorage.getItem(`last_login_register_name_${getudid}`));
              history.push('/loginpin');
            }
            //---------------------------------------------------------------------------------------
        },
  
          error => dispatch(failure(error.toString()))
        );
    };
  
    function request() {
      return { type: registerConstants.GETALL_REQUEST };
    }
    function success(registers) {
      return { type: registerConstants.GETALL_SUCCESS, registers };
    }
    function failure(error) {
      return { type: registerConstants.GETALL_FAILURE, error };
    }
  }