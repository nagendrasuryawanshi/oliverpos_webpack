import { discountConstants } from '../_constants/discount.constants';
import { discountService } from '../_services/discount.service'


export const discountActions = {
    getAll,refresh   
};

function getAll() {
    return dispatch => {
        dispatch(request());
        let discountlist=[]
        dispatch(success(discountlist))
        discountService.getAll()
            .then(               
                discountlist => 
               { 
                dispatch(success(discountlist)),
                error => dispatch(failure(error.toString()))
               }
            );
    };

    function request() { return { type: discountConstants.GETALL_REQUEST } }
    function success(discountlist) { return { type: discountConstants.GETALL_SUCCESS, discountlist } }
    function failure(error) { return { type: discountConstants.GETALL_FAILURE, error } }
}

function refresh() {   
    return { type: discountConstants.GETALL_REFRESH };
}

