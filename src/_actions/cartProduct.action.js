import { cartProductConstants } from '../_constants/cartProduct.constants'
import { get_UDid } from '../ALL_localstorage';
import { getTaxCartProduct, getExclusiveTax, getInclusiveTax, getSettingCase } from '../_components';
import { inventoryService } from '../_services';
import { idbProductActions } from './idbProduct.action';
export const cartProductActions = {

    refresh,
    addtoCartProduct,
    selectedProductDis,
    singleProductDiscount,
    addInventoryQuantity,
    showSelectedProduct,
    updateVariationStock
};


function refresh() {
    return { type: cartProductConstants.PRODUCT_GETALL_REFRESH };
}

function selectedProductDis(selecteditem) {
    return dispatch => {
        dispatch(request());
        dispatch(success(selecteditem))
    };

    function request() { return { type: cartProductConstants.SELECTED_PRODUCT_DISCOUNT_REQUEST } }
    function success(selecteditem) { return { type: cartProductConstants.SELECTED_PRODUCT_DISCOUNT_SUCCESS, selecteditem } }

}

function showSelectedProduct(showSelectedProduct) {
    return dispatch => {
        dispatch(request());
        dispatch(success(showSelectedProduct))
    };

    function request() { return { type: cartProductConstants.SHOW_SELECTED_PRODUCT_REQUEST } }
    function success(showSelectedProduct) { return { type: cartProductConstants.SHOW_SELECTED_PRODUCT_SUCCESS, showSelectedProduct } }

}

function percentWiseDiscount(price, discount) {
    let discountAmount = (price * discount) / 100.00;
    return discountAmount;
}

function NumberWiseDiscount(price, discount) {
    let discountAmount = (discount * 100.00) / price;
    return discountAmount;
}

function addtoCartProduct(cartproductlist) {
    var udid = get_UDid('UDID')
    var updateProductList = null;
    //-----update quantity and price for multiple product ----------------------------------------
    var newCartList = []
    console.log("cartproductlist", cartproductlist);

    cartproductlist && cartproductlist.map((item, index) => {
        var _discount_amount = 0.00;
        if (typeof item.product_id !== 'undefined') {
            const countTypes = cartproductlist.filter(prd => prd.product_id === item.product_id && prd.variation_id == item.variation_id);
            var _price = 0.0;
            var _qty = 0;

            countTypes.map(nitem => {
                _price = _price + nitem.Price;
                _qty = _qty + nitem.quantity;
            })
            //console.log("item.cart_discount_amount",item.cart_discount_amount,"item.discount_type",item.discount_type)   
            if (item.discount_type == 'Percentage' && item.product_discount_amount !== 0) {
                //   console.log("pert")
                _discount_amount = item.product_discount_amount * _qty
            } else if (item.discount_type == 'Number' && item.product_discount_amount !== 0) {
                //     console.log("Number")

                _discount_amount = item.product_discount_amount;
            }

            // console.log(" _discount_amount", _discount_amount)

            var _newITem = {
                Price: _price,
                Title: item.Title,
                isTaxable: item.isTaxable,
                line_item_id: item.line_item_id,
                product_id: item.product_id,
                quantity: _qty,
                after_discount: item.after_discount ? item.after_discount : 0,
                discount_amount: (item.cart_discount_amount
                    !== 0 && (typeof item.cart_discount_amount !== 'undefined')) ? item.discount_amount : _discount_amount,
                variation_id: item.variation_id,
                cart_after_discount: item.cart_after_discount ? item.cart_after_discount : 0,
                cart_discount_amount: item.cart_discount_amount ? item.cart_discount_amount : 0,
                product_after_discount: item.product_after_discount ? item.product_after_discount : 0,
                product_discount_amount: item.product_discount_amount ? item.product_discount_amount : 0,
                old_price: item.old_price ? item.old_price : 0,
                incl_tax: item.incl_tax ? item.incl_tax : 0,
                excl_tax: item.excl_tax ? item.excl_tax : 0,
                ticket_status: item.ticket_status,/////ticket
                tick_event_id: item.tick_event_id,
                ticket_info: item.ticket_info ? item.ticket_info : [], /////ticket
                product_ticket: item.product_ticket ? item.product_ticket : '',
                discount_type: item.discount_type ? item.discount_type : "",
                new_product_discount_amount: item ? item.new_product_discount_amount : 0
                //   ticket_cart_display_status:item.ticket_cart_display_status?item.ticket_cart_display_status:''

            }
            _discount_amount = 0;
            var _newItemExist = newCartList.filter(prd => prd.product_id === item.product_id && prd.variation_id == item.variation_id);

            if (_newItemExist.length == 0) {
                //  console.log("update list", _newITem)
                newCartList.push(_newITem);
            } else {
                // console.log("update list1", _newITem)
                _newItemExist = _newITem
                // console.log("update list2", _newItemExist,newCartList)

                newCartList.map(newItem => {
                    if (newItem.product_id === _newITem.product_id && newItem.variation_id == _newITem.variation_id) {
                        //  newItem = _newITem
                        // newItem.discount_amount= (typeof _newITem.discount_amount !== 'undefined') && _newITem.discount_amount !== 0 ?(_discount_amount !== 0) ? _discount_amount * _qty : _newITem.discount_amount : 0; 
                        newItem.discount_amount = _newITem.discount_amount,
                        newItem.cart_after_discount = _newITem.cart_after_discount;
                        newItem.cart_discount_amount = _newITem.cart_discount_amount;
                        newItem.product_after_discount = _newITem.product_after_discount;
                        newItem.product_discount_amount = _newITem.product_discount_amount;
                        newItem.old_price = _newITem.old_price ? _newITem.old_price : 0;
                        newItem.incl_tax = _newITem.incl_tax ? _newITem.incl_tax : 0;
                        newItem.excl_tax = _newITem.excl_tax ? _newITem.excl_tax : 0;
                        newItem.discount_type = _newITem.discount_type;
                        newItem.after_discount = _newITem.after_discount
                        newItem.new_product_discount_amount = _newITem.new_product_discount_amount ? _newITem.new_product_discount_amount : 0
                    }
                })
            }
          // console.log("newCartList", newCartList)
        } else {
            newCartList = cartproductlist;
        }

    })
    // update cartproductlist by excl and incl tax
    // let selectedCartProductList = getTaxCartProduct(newCartList);
    // console.log("selectedCartProductList", selectedCartProductList)
    // // End cartproductlist by excl and incl tax
    // console.log("STEP 1", newCartList)

    cartproductlist = getTaxCartProduct(newCartList);
    //---------------------------------------------

    var totalPrice = 0;
    var customefee = 0; //should not include into discount
    cartproductlist && cartproductlist.map((item, index) => {

        if (item.product_id) {//donothing
            totalPrice += item.Price
        }
        else {
            customefee += item.Price;
        }

        if (item.product_discount_amount !== 0) {
            if (item.incl_tax !== 0) {
                let incl_tax = 0;
                if (item.discount_type == "Percentage") {
                    incl_tax = getInclusiveTax(item.after_discount * item.quantity)
                }
                else {
                    incl_tax = getInclusiveTax(item.Price - item.discount_amount)
                }
                item["incl_tax"] = incl_tax
            } else {
                let excl_tax = 0;
                if (item.discount_type == "Percentage") {
                    excl_tax = getExclusiveTax(item.after_discount * item.quantity)
                } else {
                    excl_tax = getExclusiveTax(item.Price - item.discount_amount)
                }
                item["excl_tax"] = excl_tax
            }
        }
    })

    let cart = localStorage.getItem("CART") ? JSON.parse(localStorage.getItem("CART")) : null
    // let product = localStorage.getItem("PRODUCT") ? JSON.parse(localStorage.getItem("PRODUCT")) : null
    // let single_product = localStorage.getItem("SINGLE_PRODUCT") ? JSON.parse(localStorage.getItem("SINGLE_PRODUCT")) : null
    var product_after_discount = 0;
    let incl_tax = 0;
    let excl_tax = 0;

    if (cart !== null) {
        cartproductlist && cartproductlist.map(item => {
            product_after_discount += parseFloat(item.product_discount_amount);
            var product_discount_amount = item.product_discount_amount;
            var price = item.Price;
            var discount = parseFloat(cart.discount_amount)
            var Tax = parseFloat(cart.Tax_rate);
            var discount_amount_is = 0;
            var afterDiscount = 0;
            var new_price = 0.00;
            var cart_after_discount = 0;
            var cart_discount_amount = 0;
            if (item.product_id) {
                price = parseFloat(price - parseFloat(product_discount_amount));
                if (price == 0) {
                    cart_after_discount = 0.00;
                    cart_discount_amount = 0.00;
                    new_price = 0.00;
                } else {
                    if (cart.discountType == 'Percentage') {
                        //  console.log("STEP 1")
                        if (discount > 100) {
                            localStorage.removeItem("CART")
                            $('#no_discount').modal('show')
                        } else {

                            discount_amount_is = percentWiseDiscount(price, discount)
                            afterDiscount = price - discount_amount_is;
                            cart_after_discount = afterDiscount;
                            cart_discount_amount = discount_amount_is;
                            new_price = price - parseFloat(cart_discount_amount);
                            var total_tax = afterDiscount + (afterDiscount * Tax / 100.00);
                            if (new_price == price) {
                                new_price = 0.00;
                            }
                        }
                    } else {
                        // var new_discount = parseFloat(discount * 100 / (totalPrice - product_after_discount));
                        // console.log("STEP 2")
                        // console.log("total price", price, discount)
                        var new_discount = NumberWiseDiscount(totalPrice - product_after_discount, discount);
                        // var new_discount = NumberWiseDiscount(totalPrice - product_after_discount, discount);
                        //  console.log("new_discount01", new_discount)
                        if (new_discount > 100) {
                            localStorage.removeItem("CART")
                            $('#no_discount').modal('show')
                        } else {
                            discount_amount_is = percentWiseDiscount(price, new_discount);
                            // console.log("discount_amount_is", discount_amount_is)
                            afterDiscount = price - discount_amount_is;
                            cart_after_discount = afterDiscount;
                            cart_discount_amount = discount_amount_is;
                            //total_tax = afterDiscount + (afterDiscount * Tax / 100.00);
                            new_price = price - parseFloat(cart_discount_amount);
                            if (new_price == price) {
                                new_price = 0.00;

                            }
                        }

                    }
                }
                item["cart_after_discount"] = parseFloat(cart_after_discount);
                item["cart_discount_amount"] = parseFloat(cart_discount_amount);
                item["after_discount"] = new_price
                item["discount_amount"] = parseFloat(cart_discount_amount) + parseFloat(product_discount_amount)

                if (item.discount_amount !== 0) {
                   // alert("11"+new_price)
                   //console.log("item", item)
                    if (item.incl_tax !== 0) {
                        incl_tax = getInclusiveTax(item.Price - item.discount_amount)
                        item["incl_tax"] = incl_tax
                    } else {
                        excl_tax = getExclusiveTax(item.Price - item.discount_amount);
                        item["excl_tax"] = excl_tax
                    }
                }else{
                    //alert("12"+item.Price)
                    if (item.incl_tax !== 0) {
                        incl_tax = getInclusiveTax(item.Price)
                        item["incl_tax"] = incl_tax
                    } else {
                        excl_tax = getExclusiveTax(item.Price);
                        item["excl_tax"] = excl_tax
                    }
                }
            }

        })

    }





    localStorage.setItem("CARD_PRODUCT_LIST", JSON.stringify(cartproductlist));




    //-------------------------
    return dispatch => {
        dispatch(request());
        dispatch(success(JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST"))));
    };

    function request() { return { type: cartProductConstants.PRODUCT_ADD_REQUEST } }
    function success(cartproductlist) { return { type: cartProductConstants.PRODUCT_ADD_SUCCESS, cartproductlist } }

}

function singleProductDiscount() {
    //var udid = get_UDid('UDID');
    let product = localStorage.getItem("PRODUCT") ? JSON.parse(localStorage.getItem("PRODUCT")) : null
    let product_list = localStorage.getItem("SINGLE_PRODUCT") ? JSON.parse(localStorage.getItem("SINGLE_PRODUCT")) : null
    var product_after_discount = 0;
    let single_product = null;
    //console.log("product and single_product in action page", product, product_list)
    if (product !== null && product_list !== null) {
        var product_id = 0;
        var cart_after_discount = 0;
        var cart_discount_amount = product_list.cart_discount_amount ? parseFloat(product_list.cart_discount_amount) : 0;
        var price = 0;
        var discount = parseFloat(product.discount_amount)
        var Tax = parseFloat(product.Tax_rate);
        var product_after_discount = 0;
        var product_discount_amount = 0;
        var discount_amount_is = 0;
        var afterDiscount = 0;
        var new_price = 0.00;


        if (product_list.Type !== "variable") {
            product_id = product.Id;
            single_product = product_list;
        } else {
            let new_single_product = product_list.Variations.filter(item => item.WPID === product.Id);
            single_product = new_single_product[0];
            product_id = product.Id;
        }

        localStorage.removeItem("SINGLE_PRODUCT");
        localStorage.removeItem("PRODUCT")



        // console.log("single_product->", single_product)

        // console.log("product_id", product_id)
        price = single_product.Price
        if (product_id) {
            //    console.log("cart_discount_amount " , price, cart_discount_amount, discount)
            price = price - cart_discount_amount;

            if (price == 0) {
                product_after_discount = 0;
                product_discount_amount = 0;
                new_price = 0.00;
            } else {
                if (product.discountType == 'Percentage') {
                    // console.log("STEP 1", discount)



                    if (discount > 100) {
                        localStorage.removeItem("PRODUCT")
                        localStorage.removeItem("SINGLE_PRODUCT")
                        $('#no_discount').modal('show')
                    } else {
                        discount_amount_is = percentWiseDiscount(price, discount);
                        console.log("STEP 01", discount_amount_is, price, price - discount_amount_is)
                        afterDiscount = price - discount_amount_is;

                        product_after_discount = afterDiscount;
                        product_discount_amount = discount_amount_is;
                    }

                }
                else {
                    var new_discount = NumberWiseDiscount(price, discount)
                    // console.log("STEP 2", new_discount)
                    if (new_discount > 100) {
                        localStorage.getItem("PRODUCT")
                        localStorage.removeItem("SINGLE_PRODUCT")
                        $('#no_discount').modal('show')
                    } else {
                        discount_amount_is = percentWiseDiscount(price, new_discount)
                        afterDiscount = price - discount_amount_is;
                        product_after_discount = afterDiscount;
                        product_discount_amount = discount_amount_is;
                        var total_tax = afterDiscount + (afterDiscount * Tax / 100.00);
                    }
                }
            }

            //  if (item.product_id == product_id) {
            // var new_price = price - parseFloat(product_discount_amount);
            // if (new_price == price) {
            //     new_price = 0.00;
            // }
            //localStorage.removeItem("CART")
            single_product["product_after_discount"] = parseFloat(product_after_discount)
            single_product["product_discount_amount"] = parseFloat(product_discount_amount)
            single_product["after_discount"] = afterDiscount
            single_product["discount_amount"] = parseFloat(cart_discount_amount) + parseFloat(product_discount_amount);
            single_product["discount_type"] = product.discountType ? product.discountType : 'Number'
            single_product["new_product_discount_amount"] = discount
            // if (single_product.discount_amount !== 0) {
            //     if (single_product.incl_tax !== 0) {
            //         incl_tax = getInclusiveTax(new_price)
            //         single_product["incl_tax"] = incl_tax
            //     } else {
            //         excl_tax = getExclusiveTax(new_price)
            //         single_product["excl_tax"] = excl_tax
            //     }
            // }

        }
        // console.log("single_product final" , single_product)



    }

    return dispatch => {
        dispatch(request());
        dispatch(success(single_product));
    };

    function request() { return { type: cartProductConstants.PRODUCT_SINGLE_ADD_REQUEST } }
    function success(single_product) { return { type: cartProductConstants.PRODUCT_SINGLE_ADD_SUCCESS, single_product } }


}

function addInventoryQuantity(data, inventoryData) {
    let inventory_quantitiy = null;
    return dispatch => {
        $('#txtInv').val('0')
        dispatch(request());
        //dispatch(success(data))
        if (data) {
            inventoryService.addInventoryQuantity(data)
                .then(
                    inventory_quantitiy => {
                        if (inventory_quantitiy.IsSuccess == true) {
                            dispatch(success(data));
                            dispatch(idbProductActions.updateOrderProductDB(inventoryData));
                        } else {
                            dispatch(success(data));
                        }
                        error => dispatch(failure(error.toString()))
                    }
                );
        } else {
            return inventory_quantitiy
        }
    };

    function request() { return { type: cartProductConstants.GET_SINGAL_INVENTORY_REQUEST } }
    function success(inventory_quantitiy) { return { type: cartProductConstants.GET_SINGAL_INVENTORY_SUCCESS, inventory_quantitiy } }
    function failure(error) { return { type: cartProductConstants.GET_SINGAL_INVENTORY_FAILURE, error } }
}

function updateVariationStock(product){
    if(product.Type == 'variable' && product.ManagingStock == true){
        product.Variations.map(filterStock=>{
            if(filterStock.ManagingStock === false){
                filterStock.StockQuantity =  product.StockQuantity
            }
            
        })
    }
}







