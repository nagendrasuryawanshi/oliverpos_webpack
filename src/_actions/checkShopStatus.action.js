import { checkShopStatusConstants } from '../_constants/checkShopStatus.constants';
import { checkShopStatusService } from '../_services/checkShopStatus.service'
import { alertActions } from './';
import { history } from '../_helpers';

export const checkShopSTatusAction = {
    getStatus 
};

function getStatus() {
    return dispatch => {
        // dispatch(request());
        checkShopStatusService.getStatus()
            .then(
                shopstatus => {
                    if (shopstatus && shopstatus != null) {                  
                      localStorage.setItem("shopstatus", JSON.stringify(shopstatus) )
                        dispatch(success(shopstatus)),
                            error => {
                                dispatch(failure(error.toString()))                              
                            }
                    }
                    else {
                     
                        dispatch(failure("Unable to get shop status"))
                     
                    }
                }
            );

    };

    function request() { return { type: checkShopStatusConstants.SHOPSTATUS_REQUEST } }
    function success(shopstatus) { return { type: checkShopStatusConstants.SHOPSTATUS_SUCCESS, shopstatus } }
    function failure(error) { return { type: checkShopStatusConstants.SHOPSTATUS_FAILURE, error } }
}

