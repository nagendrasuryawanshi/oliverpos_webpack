import { allProductConstants } from '../_constants/allProduct.constants'
import { idbProductService } from '../_services/idbProduct.service';
import { openDb, deleteDb } from 'idb';
import { get_UDid } from '../ALL_localstorage'

export const idbProductActions = {
  getAll,
  refresh,
  createProductDB,
  updateProductDB,
  createProductDBNew,
  filteredProduct,
  updateOrderProductDB
};

function createProductDBNew() {
  return dispatch => {
    dispatch(request());
    let productlist = [];
    let pageSize = 100;
    let pageNumber = 1;
    let totalRecord = 0
    console.log("StartCalling", pageNumber);
    //  while(totalRecord===0 || pageSize*pageNumber <totalRecord)
    //  {
    console.log("pageNumber", pageNumber);
    var ProductResponse;
    setTimeout(function () {


      ProductResponse = idbProductService.createProductDBNew(pageNumber, pageSize)
      if (ProductResponse && ProductResponse.IsSuccess === true) {

        console.log("NewArray", ProductResponse.Content.Records);
        productlist = productlist.concat(ProductResponse.Content.Records);
        totalRecord = ProductResponse.Content.TotalRecords.lenght();
        pageNumber++;
        console.log("totalRecord", totalRecord);
      }
    }, 1000)
    // }

  };

  function request() { return { type: allProductConstants.PRODUCT_GETALL_REQUEST } }
  function success(productlist) { return { type: allProductConstants.PRODUCT_GETALL_SUCCESS, productlist } }
  function failure(error) { return { type: allProductConstants.PRODUCT_GETALL_FAILURE, error } }
}

function createProductDB() {
  return dispatch => {
    dispatch(request());
    let productlist = []
    //  dispatch(success(productlist))

    idbProductService.createProductDB()

  };

  function request() { return { type: allProductConstants.PRODUCT_GETALL_REQUEST } }
  function success(productlist) { return { type: allProductConstants.PRODUCT_GETALL_SUCCESS, productlist } }
  function failure(error) { return { type: allProductConstants.PRODUCT_GETALL_FAILURE, error } }
}

function updateProductDB() {
  // alert("update");
  return dispatch => {
    dispatch(request());
    let productlist = []
    //  dispatch(success(productlist))

    //  var result= idbProductService.updateProductDB()
    //  if(result=='Success')
    //  {
    dispatch(success(idbProductService.updateProductDB()));
    //}
  };

  function request() { return { type: allProductConstants.PRODUCT_GETALL_REQUEST } }
  function success(response) { return { type: allProductConstants.PRODUCT_GETALL_SUCCESS, response } }
  function failure(error) { return { type: allProductConstants.PRODUCT_GETALL_FAILURE, error } }
}

function getAll() {
  return dispatch => {
    // idbProductService.getAll() 
    dispatch(request());
    var udid = get_UDid('UDID');
    const dbPromise = openDb('ProductDB', 1, upgradeDB => {
      upgradeDB.createObjectStore(udid);

    });
    dbPromise.then(function (db) {
      var tx = db.transaction(udid, 'readonly');
      var store = tx.objectStore(udid);
      return store.getAll();
    }).then(function (items) {
      // return dispatch => {
      console.log("ProductFound", items[0]);
      dispatch(success(items[0]))
      // };
      // return items;

    });
  };

  function request() { return { type: allProductConstants.PRODUCT_GETALL_REQUEST } }
  function success(productlist) { return { type: allProductConstants.PRODUCT_GETALL_SUCCESS, productlist } }
  function failure(error) { return { type: allProductConstants.PRODUCT_GETALL_FAILURE, error } }
}

function refresh() {
  return { type: allProductConstants.PRODUCT_GETALL_REFRESH };
}

function filteredProduct(filteredProduct = []) {
  return dispatch => {
    dispatch(success(filteredProduct))
  };

  function success(filteredProduct) { return { type: allProductConstants.FILTERED_ALL_PRODUCTS_SUCCESS, filteredProduct } }
}


function updateOrderProductDB(order) {
  return dispatch => {
    
    dispatch(request());
    dispatch(success(idbProductService.updateOrderProductDB(order)));

  };

  function request() { return { type: allProductConstants.PRODUCT_GETALL_REQUEST } }
  function success(response) { return { type: allProductConstants.PRODUCT_GETALL_SUCCESS, response } }
  function failure(error) { return { type: allProductConstants.PRODUCT_GETALL_FAILURE, error } }
}
