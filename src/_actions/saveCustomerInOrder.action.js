import { saveCustomerInOrderConstants } from '../_constants/saveCustomerInOrder.constants';
import { saveCustomerInOrderService } from '../_services/saveCustomerInOrder.service'


export const saveCustomerInOrderAction = {
    saveCustomerInOrder
};

function saveCustomerInOrder(udid, order_id, email_id) {
    console.log("udid, order_id, email_id",udid, order_id, email_id)
    return dispatch => {
       dispatch(request());
       saveCustomerInOrderService.saveCustomer(udid, order_id, email_id)
            .then(
                getSuccess => {
                     dispatch(success( getSuccess )),
                        error => dispatch(failure(error.toString()))
                }
            );
    };

    function request() { 
        return { 
            type: saveCustomerInOrderConstants.SCIO_REQUEST 
        } 
    }
    
    function success( getSuccess ) {
        return {
            type: saveCustomerInOrderConstants.SCIO_SUCCESS, getSuccess 
        } 
    }

    function failure( error ) {
        return { 
            type: saveCustomerInOrderConstants.SCIO_FAILURE, error 
        } 
    }
}

