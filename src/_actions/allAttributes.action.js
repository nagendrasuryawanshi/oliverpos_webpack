import { allProductConstants } from '../_constants/allProduct.constants';
import { allProductService } from '../_services/allProduct.service'


export const attributesActions = {
    getAll,refresh   
};

function getAll() {
    return dispatch => {
        dispatch(request());
        let attributelist=[]
        dispatch(success(attributelist))
        allProductService.getAttributes()
            .then(               
                attributelist => 
               { 
                   localStorage.setItem("attributelist",JSON.stringify(attributelist));
                dispatch(success(attributelist)),
                error => dispatch(failure(error.toString()))
               }
            );
    };

    function request() { return { type: allProductConstants.ATTRIBUTE_GETALL_REQUEST } }
    function success(attributelist) { return { type: allProductConstants.ATTRIBUTE_GETALL_SUCCESS, attributelist } }
    function failure(error) { return { type: allProductConstants.ATTRIBUTE_GETALL_FAILURE, error } }
}

function refresh() {   
    return { type: allProductConstants.ATTRIBUTE_GETALL_REFRESH };
}

