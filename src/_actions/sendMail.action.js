import { sendMailConstants } from '../_constants/sendMail.constants';
import { sendMailService } from '../_services/sendMail.service'


export const sendMailAction = {
    sendMail
};

function sendMail( data ) {
    console.log("sendmail",data);
    return dispatch => {
       dispatch(request());
       sendMailService.send( data )
            .then(
                getSuccess => {
                     dispatch(success( getSuccess )),
                        error => dispatch(failure(error.toString()))
                }
            );
    };

    function request() {
        return { 
            type: sendMailConstants.SM_REQUEST
        } 
    }
    
    function success( getSuccess ) {
        return {
            type: sendMailConstants.SM_SUCCESS, getSuccess 
        } 
    }

    function failure( error ) {
        return { 
            type: sendMailConstants.SM_FAILURE, error 
        } 
    }
}

