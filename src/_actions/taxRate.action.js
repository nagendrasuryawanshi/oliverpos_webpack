import { taxRateConstants } from '../_constants/taxRate.constants';
import { taxRateService } from '../_services/taxRate.service'
import { alertActions } from './';
import { history } from '../_helpers';

export const taxRateAction = {
    getAll,
    refresh,
    getTaxSetting
};

function getAll() {
    return dispatch => {
        // dispatch(request());
        taxRateService.getAll()
            .then(
                taxratelist => {
                    if (taxratelist && taxratelist != null) {

                        localStorage.setItem("taxratelist", { "TaxId": taxratelist.TaxId, "TaxRate": taxratelist.TaxRate })
                        dispatch(success(taxratelist)),
                            error => {
                                dispatch(failure(error.toString()))
                                dispatch(alertActions.error(error.toString()))
                            }
                    }
                    else {
                        // localStorage.removeItem("UDID");
                        localStorage.setItem("taxratelist", { "TaxId": 0, "TaxRate": 0 })
                        dispatch(failure("Unable to get tax rate"))
                        history.push('/loginpin');
                    }
                }
            );

    };

    function request() { return { type: taxRateConstants.GETALL_REQUEST } }
    function success(taxratelist) { return { type: taxRateConstants.GETALL_SUCCESS, taxratelist } }
    function failure(error) { return { type: taxRateConstants.GETALL_FAILURE, error } }
}

function refresh() {
    return { type: taxRateConstants.GETALL_REFRESH };
}


function getTaxSetting() {
    return dispatch => {
        dispatch(request());
        taxRateService.getTaxSetting()
            .then(
                get_tax_setting => {
                      if(get_tax_setting.IsSuccess== false &&  get_tax_setting.Message== "You are not authorized.")
                      {
                        history.push('/login');
                      }

                        dispatch(success(get_tax_setting))
                        //console.log("get_tax_setting" , get_tax_setting)
                        if(get_tax_setting && get_tax_setting.Content){
                            
                            localStorage.setItem("TAX_SETTING" , JSON.stringify(get_tax_setting.Content))
                        }
                        error => {
                            dispatch(failure(error.toString()))
                            dispatch(alertActions.error(error.toString()))
                        }
                }
            );
    };

    function request() { return { type: taxRateConstants.GET_TAXSETTING_REQUEST } }
    function success(get_tax_setting) { return { type: taxRateConstants.GET_TAXSETTING_SUCCESS, get_tax_setting } }
    function failure(error) { return { type: taxRateConstants.GET_TAXSETTING_FAILURE, error } }

}

