import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
// import { userActions } from '../_actions';
import { history } from "../../_helpers";

//import jQuery from 'jquery';


class RevalidatePage extends React.Component {

    log_out(){     
        localStorage.clear();
        history.push('/login');
    }
    handleSubmit(){      
        if(localStorage.getItem("userstatus") )
        {
            var userstatus=JSON.parse(localStorage.getItem("userstatus")) ;
           // console.log("link",userstatus.Content.link);
            window.location.href=userstatus.Content.link;
            //'https://testshop.creativemaple.ca/shop/';
            //https://oliverpos.com/
        }else
        {
            history.push('/login');
        }
    }
    render() {
        var errorMsg="";
        if(localStorage.getItem("userstatus") )
        {
            var userstatus=JSON.parse(localStorage.getItem("userstatus") );
           // console.log("userstatus",userstatus);
            errorMsg=userstatus.Exceptions;
           // console.log("Exceptions",errorMsg);
        }
        return (
           

            <div className="bgimg-1">
                <div className="content_main_wapper">
                    <div className="onboarding-loginBox">
                        <div className="login-form">
                            <div className="onboarding-pg-heading">
                                <h1>Subscription Message</h1>
                                <p>{errorMsg}</p>
                            </div>
                             <form name="form"  className="onboarding-login-field"  > {/* onSubmit={()=>this.handleSubmit} */}
                             
                                <button type="button" className="btn btn-login bgcolor2 mt-0" onClick={()=>this.handleSubmit()}>Revalidate</button>
                              
                            </form>
                        </div>
                       
                    </div>
                    <div className="copy_right">
                        <a onClick={()=>this.log_out()}><img src="../assets/img/logout-btn_wht.png"/></a>
                    </div>
                    
                    <div className="powered-by-oliver">
                            <a href="javascript:void(0)">Powered by Oliver POS</a>
                        </div>
                </div>
            </div>
        )
    }
}



function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedRevalidatePage = connect(mapStateToProps)(RevalidatePage);
export { connectedRevalidatePage as RevalidatePage };