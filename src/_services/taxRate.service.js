import Config from '../Config'
import { history } from '../_helpers';

import {encode_UDid,get_UDid} from '../ALL_localstorage'

export const taxRateService = {
    getAll,
    refresh,
    getTaxSetting
};

const API_URL = Config.key.OP_API_URL

function refresh() {
    // remove user from local storage to log user out

}


function getAll() {
    var UserLocation = localStorage.getItem('UserLocations') ? JSON.parse(localStorage.getItem('UserLocations')) : [];
   // let decodedString = localStorage.getItem('UDID');
   // var decod=  window.atob(decodedString);
    var UDID= get_UDid('UDID');
    //var UDID = localStorage.getItem("UDID");
    var loc_country = "";
    var loc_country_state = "";
    var loc_city = "";
    var loc_postcode = "";
    var loc_address_1 = "";
//console.log("Locations",UserLocation)
    if (UserLocation && localStorage.getItem("Location") && UserLocation.length > 0) {
        // console.log("Locations1",UserLocation)
        // console.log("Location",localStorage.getItem("Location"));
        UserLocation.map(loc=>{
            if(loc.Id==localStorage.getItem("Location"))
            {
               // console.log("loc",loc)
                 loc_country = loc.CountryName;
                loc_country_state = loc.StateName;
                loc_city = loc.City;
                loc_postcode = loc.Zip;
                loc_address_1 = loc.Address1;
            }
        })
       
    }
    else
    {
        history.push('/loginpin');
    }

    const requestOptions = {
        method: 'POST',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors',

        body: JSON.stringify({ UDID, loc_country, loc_country_state, loc_city, loc_postcode, loc_address_1 })
        //  body: "udid=4040246102&loc_country=CA&loc_country_state=NL&loc_city=Sent John's&loc_postcode=&loc_address_1=Sent John's Shop"        
    };

    return fetch(`${API_URL}/ShopSetting/GetRate`, requestOptions) //?Udid=${UDID}
        .then(handleResponse)
        .then(taxratelst => {   
            return taxratelst.Content;
        });
}

function getTaxSetting(){
    var UDID= get_UDid('UDID');
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
    return fetch(`${API_URL}/ShopSetting/GetTaxSetting?Udid=${UDID}`, requestOptions)
    .then(handleResponse)
        .then(response => {
            return response;                       
    });
      
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                // logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}




