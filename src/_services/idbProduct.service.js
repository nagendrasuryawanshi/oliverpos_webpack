import Config from '../Config'
import { openDb, deleteDb } from 'idb';
import { create } from 'domain';
import {encode_UDid,get_UDid} from '../ALL_localstorage'
import moment from 'moment';


export const idbProductService = {
    getAll,
    createProductDB,
    updateProductDB,
    createProductDBNew,
    refresh,
    updateOrderProductDB
};

const API_URL=Config.key.OP_API_URL


// var udid =localStorage.getItem("UDID");
// console.log("UserID",udid);
// const dbPromise = openDb('ProductDB-'+udid, 1, upgradeDB => {
//     upgradeDB.createObjectStore(udid);
//   });

//   const idbKeyval = {
//     async get(key) {
//       const db = await dbPromise;
//       return db.transaction(udid).objectStore(udid).get(key);
//     },
//     async set(key, val) {
//       const db = await dbPromise;
//       const tx = db.transaction(udid, 'readwrite');
//       tx.objectStore(udid).put(val, key);
//       return tx.complete;
//     },
//     async delete(key) {
//       const db = await dbPromise;
//       const tx = db.transaction(udid, 'readwrite');
//       tx.objectStore(udid).delete(key);
//       return tx.complete;
//     },
//     async clear() {
//       const db = await dbPromise;
//       const tx = db.transaction(udid, 'readwrite');
//       tx.objectStore(udid).clear();
//       return tx.complete;
//     },
//     async keys() {
//       const db = await dbPromise;
//       return db.transaction(udid).objectStore(udid).getAllKeys(key);
//     },
//   };


 function  createProductDBNew(PageNumbner,PageSize) {
  const requestOptions = {
      method: 'GET',
      headers: {
          "access-control-allow-origin": "*",
          "access-control-allow-credentials": "true", 
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
      } 
      ,mode: 'cors'

   };

  // var udid =localStorage.getItem("UDID");
   let decodedString =get_UDid('UDID');
   if(decodedString){
  // var decod=  window.atob(decodedString);
   var udid= decodedString //decod;
 //console.log("ProductUDID",udid);
  return fetch(`${API_URL}/ShopData/GetProductPageNew?udid=${udid}&pageNumber=${PageNumbner}&pageSize=${PageSize}`, requestOptions)
  .then(handleResponse)
  .then(productlst=>{
   // console.log("productlist",productlst);
       // var  productlist= productlst.Content ;       
        return productlst.Content;             
          
  });  
}
}

function refresh() {
    // remove user from local storage to log user out
  
}

function createProductDB() {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
  
     };

    // var udid =localStorage.getItem("UDID");
     let decodedString =get_UDid('UDID');
     if(decodedString){
   //  var decod=  window.atob(decodedString);
     var udid= decodedString;
  // console.log("ProductUDID",udid);
    return fetch(`${API_URL}/ShopData/GetAllProductNew?Udid=${udid}`, requestOptions)
    .then(handleResponse)
    .then(productlst=>{
          var  productlist= productlst.Content ;       
      

         // var udid =localStorage.getItem("UDID");
         // console.log("UserID",udid);
          const dbPromise = openDb('ProductDB', 1, upgradeDB => {
           // if (!upgradeDb.objectStoreNames.contains(udid, {autoIncrement: true})) {
              upgradeDB.createObjectStore(udid);
           // }
            });
          
            const idbKeyval = {
              async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
              },
              async set(key, val) {
                const db = await dbPromise;
                const tx = db.transaction(udid, 'readwrite');
                tx.objectStore(udid).put(val, key);
                return tx.complete;
              },
              
            };
          
            idbKeyval.set('ProductList',productlist);
         
              return  'Success';
          
            
    });  
  }
}


function updateProductDB() {
 // alert("updateproductDab");
  const requestOptions = {
      method: 'GET',
      headers: {
          "access-control-allow-origin": "*",
          "access-control-allow-credentials": "true", 
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
      } 
      ,mode: 'cors'

   };

   var udid =get_UDid('UDID');
   //https://app.creativemaple.ca/api/ShopData/GetUpdatedInventory?udid=4040246278&LastOrderTime=2019-04-05 10:00:00
   let UpdateDate=moment.utc(new Date()).format('YYYY-MM-DD HH:mm:ss');
   if(localStorage.getItem("PRODUCT_REFRESH_DATE"))
   {
   UpdateDate= localStorage.getItem("PRODUCT_REFRESH_DATE").toString();   
   }
   //console.log("UpdateDate" , UpdateDate)

 return fetch(`${API_URL}/shopdata/GetUpdatedInventory?Udid=${udid}&LastOrderTime=${UpdateDate}`, requestOptions).then(handleResponse)
  //.then(handleResponse)
  .then(qtyList=>{
    localStorage.setItem("PRODUCT_REFRESH_DATE" , moment.utc(new Date()).format('YYYY-MM-DD HH:mm:ss'))
      console.log("Quantity Response shop View",qtyList);
        var  quantityList= qtyList.Content ;       
    
        var productList=[];
      //  // var udid =localStorage.getItem("UDID");
      //   console.log("UserID",udid);
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
         // if (!upgradeDb.objectStoreNames.contains(udid, {autoIncrement: true})) {
            upgradeDB.createObjectStore(udid);
         // }
          });
        
          const idbKeyval = {
            async get(key) {
              const db = await dbPromise;
              return db.transaction(udid).objectStore(udid).get(key);
            },
            async set(key, val) {
              const db = await dbPromise;
              const tx = db.transaction(udid, 'readwrite');
              tx.objectStore(udid).put(val, key);
              return tx.complete;
            },
            
          };
       
          idbKeyval.get('ProductList').then(val => 
            {       
                productList=val;
              // console.log("productList localstorage" , productList)         
        
        
        //var Quant=[]
            quantityList && quantityList.map(quanList=>{ 
               // console.log("quanList",quanList);   
              
                    var filteredProduct=productList.find((value,index)=>{
                      if(value.WPID === quanList.WPID){
                       //  console.log("STEP01", true)
                        return  value.WPID===quanList.WPID
                      }else{
                       // console.log("STEP02", false)
                        localStorage.setItem('UPDATE_PRODUCT_LIST', true)
                      }
                       
                      })
                     // console.log("filteredProduct", filteredProduct)
                      if(filteredProduct !==null && filteredProduct !='undefined')
                      {
                      filteredProduct['StockQuantity'] = quanList.Quantity;
                     // filteredProduct['Price']=quanList.Price;   
                      }

            });
            //var UDID= get_UDid('UDID');
          //  localStorage.setItem("Productlist-"+UDID,JSON.stringify(productList)); 
          if(productList.length>0){
          //  console.log("productList localstorage 2 " , productList)
            idbKeyval.set('ProductList',productList);
            } 
            return  'Success';
        });
      
           
        
          
  });  

}

function getAll() {  
            //  console.log("GETAllProduct", idbKeyval.get('ProductList').then(val => console.log(val)) );  
            var udid =get_UDid('UDID');
            const dbPromise = openDb('ProductDB', 1, upgradeDB => {
                upgradeDB.createObjectStore(udid);
                
              });
              dbPromise.then(function(db) {
                var tx = db.transaction(udid, 'readonly');
                var store = tx.objectStore(udid);
                return store.getAll();
              }).then(function(items) {
                  return items;
              });
              // const idbKeyval = {
              //   async get(key) {
              //     const db = await dbPromise;
              //     return db.transaction(udid).objectStore(udid).get(key);
              //   },                
                
              // };    

              // return idbKeyval.get('ProductList').then(val => console.log(val)); 
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
               // logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}


function updateOrderProductDB(order,refundData=null){

  //console.log("order data" , order)
   
   
      const requestOptions = {
          method: 'GET',
          headers: {
              "access-control-allow-origin": "*",
              "access-control-allow-credentials": "true", 
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
          } 
          ,mode: 'cors'
    
       };
    
       var udid =get_UDid('UDID');
       var productIds="";
       order && order.map(value=>{
        var pid=value.variation_id==0?  value.product_id: value.variation_id
        productIds +=   productIds==""? pid: ',' +pid.toString() 
      })

      if(refundData !=null)
      {
        refundData && refundData.line_items.map(value=>{
          var pid=value.variation_id==0?  value.product_id: value.variation_id
          productIds +=   productIds==""? pid: ',' +pid.toString() 
        })
      }
      //console.log("productIds",productIds)
     return fetch(`${API_URL}/shopdata/GetInventory?Udid=${udid}&ids=${productIds}`, requestOptions).then(handleResponse)
      .then(qtyList=>{
        //console.log("Quantity Response",qtyList);
            var  quantityList= qtyList.Content ;       
        
             var productList=[];
             const dbPromise = openDb('ProductDB', 1, upgradeDB => {
               upgradeDB.createObjectStore(udid);
               });
            
              const idbKeyval = {
                async get(key) {
                  const db = await dbPromise;
                  return db.transaction(udid).objectStore(udid).get(key);
                },
                async set(key, val) {
                  const db = await dbPromise;
                  const tx = db.transaction(udid, 'readwrite');
                  tx.objectStore(udid).put(val, key);
                  return tx.complete;
                },
                
              };
           
              idbKeyval.get('ProductList').then(val => 
                {       
                    productList=val;
                   // console.log("productList01" , productList)
                    quantityList && quantityList.map(prodQty=>{
                      if(quantityList)
                      {
                        // var prodQty= quantityList.find(quanList=> {   
                        //  var id=  value.variation_id==0?  value.product_id: value.variation_id;                  
                        //   return(id === quanList.WPID)
                        //     })
                       // console.log("prodQty",prodQty);  
                       
                          var prodTobeUpdate= productList.find(value=>{
                               if(value.WPID === prodQty.WPID )
                              { value['StockQuantity'] = prodQty.Quantity;
                                //value['Price']=prodQty.Price;  
                                return true;                     
                              }
                            
                            })
                            //console.log("prodTobeUpdate",prodTobeUpdate);  
                            // if(prodTobeUpdate)
                            //   { prodTobeUpdate.StockQuantity = prodQty.StockQuantity;
                            //   prodTobeUpdate.Price=prodQty.Price;                           
                            // }
                        // console.log("filteredProduct02" , prodTobeUpdate)
                        //  console.log("updateproductList",productList)
                          }
                             
                    })
                    if(productList.length>0){
                      //  console.log("productList localstorage 2 " , productList)
                        idbKeyval.set('ProductList',productList);
                        }
                    return  'Success';
                })

                 
            });
          
              
            
              
     
    
    

}



