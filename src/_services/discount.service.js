import Config from '../Config'

import {encode_UDid,get_UDid} from '../ALL_localstorage'

export const discountService = {
    getAll,
    refresh
};

const API_URL = Config.key.OP_API_URL

function refresh() {
    // remove user from local storage to log user out

}
function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
    // let decodedString = localStorage.getItem('UDID');
    // var decod=  window.atob(decodedString);
    var UDID= get_UDid('UDID');
   //var UDID = localStorage.getItem("UDID")
    return fetch(`${API_URL}/ShopSetting/GetDiscount?Udid=${UDID}`, requestOptions)
        .then(handleResponse)
        .then(discountlst => {
            return discountlst.Content;
        });
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                // logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}