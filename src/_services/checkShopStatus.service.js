import Config from '../Config'
import {get_UDid} from '../ALL_localstorage'

export const checkShopStatusService = {
    getStatus,
  
};

const API_URL = Config.key.OP_API_URL

function getStatus() {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
  
    var UDID= get_UDid('UDID');
   
    return fetch(`${API_URL}/ShopAccess/GetCheckShopAlive?udid=${UDID}`, requestOptions)
        .then(handleResponse)
        .catch(error=>console.log(error))
        // .then(response => {
        //      return response;
        // });   
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
               location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}




