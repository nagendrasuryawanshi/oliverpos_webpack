import Config from '../Config'
import { history} from '../_helpers';


export const saveCustomerInOrderService = {
    saveCustomer,
 };

const API_URL = Config.key.OP_API_URL

function saveCustomer(udid, order_id, email_id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
    
    return fetch(`${API_URL}/ShopOrders/SaveCustomerInOrder?Udid=${udid}&orderId=${order_id}&CustomerEmail=${email_id}`, requestOptions)
        .then(handleResponse)
        .then(response => {
           // console.log("response", response);
            return response;
        }
    );
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}





