import Config from '../Config'
import { history} from '../_helpers';



export const userService = {
    login,
    logout,
 };

const API_URL=Config.key.OP_API_URL

function login(email, password) {
    const requestOptions = {
        method: 'POST',           
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors',
    
        body: JSON.stringify({ email, password })
    };
    return fetch(`${API_URL}/Subscription/DeviceLogin`, requestOptions)
        .then(handleResponse)
        .then(user => {
           // login successful if there's a jwt token in the response
            if (user.Content[0].UDID) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('user', JSON.stringify(user));
                const requestOptions1 = {
                    method: 'GET',    
                    headers: {
                        "access-control-allow-origin": "*",
                        "access-control-allow-credentials": "true", 
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
                    } 
                    ,mode: 'cors'
            };
                return fetch(`${API_URL}/ShopAccess/GetLocations?udid=`+user.Content[0].UDID,requestOptions1)
                .then(handleResponse)
                .then(location => {
                    setTimeout(function(){
                        localStorage.setItem('user', JSON.stringify(user));
                        localStorage.setItem('UserLocations', JSON.stringify(location && location.Content));
    
                        return user;
                    },1000)
                   
                }).catch(error=>{
                      Message:"Error- No location found"  
                })
                
            }
            
        });
}


function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('shopstatus');
     localStorage.removeItem('user');
     //localStorage.removeItem('UserLocations');
     //localStorage.removeItem('UDID');
    history.push('/login')
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}



