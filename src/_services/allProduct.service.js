import Config from '../Config'
import {encode_UDid,get_UDid} from '../ALL_localstorage'



export const allProductService = {
    getAll,
    getAttributes,
    refresh,
    ticket_FormL_ist
};

const API_URL=Config.key.OP_API_URL

function refresh() {
    // remove user from local storage to log user out
  
}
function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
  
     };

   // var UDID=localStorage.getItem("UDID")
        //   let decodedString = localStorage.getItem('UDID');
        //     var decod=  window.atob(decodedString);
            var UDID= get_UDid('UDID');
            console.log("productallaction",UDID);
    return fetch(`${API_URL}/ShopData/GetAllProducts?Udid=${UDID}`, requestOptions)
    .then(handleResponse)
    .then(productlst=>{
          var  productlist= productlst.Content        
             
         // localStorage.setItem("Productlist-"+localStorage.getItem('UDID'),JSON.stringify(productlist)); 

              //  localStorage.setItem('Productlist',JSON.stringify(productlist)) 
            //    console.log( "Productlist-",localStorage.getItem("Productlist-"+localStorage.getItem('UDID'))); 
            // console.log("productlst132",productlst.Content)
            //    console.log("productlst132",JSON.parse(localStorage.getItem(productlist)));

              return productlst.Content;
          
            
    });
}
function ticket_FormL_ist(udid,frmid){
    // alert('2')
     const requestOptions = {
         method: 'GET',
         headers: {
             "access-control-allow-origin": "*",
             "access-control-allow-credentials": "true", 
             'Accept': 'application/json',
             'Content-Type': 'application/json',
             'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
         } 
         ,mode: 'cors'
   
      };
      return fetch(`${API_URL}/ShopTickera/GetFormById?udid=${udid}&form_id=${frmid}`, requestOptions)
      .then(handleResponse)
      .then(ticket=>{
            var  ticketlist= ticket.Content  
     console.log("ticketlist233",ticketlist)            
         
          /// console.log("ticketlist12",localStorage.getItem('ticketlist')?JSON.parse(localStorage.getItem('ticket_list')):'')   

                return ticketlist;
            
              
      });


   }
function getAttributes() {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
     };

  // var UDID=localStorage.getItem("UDID")
  //  let decodedString = localStorage.getItem('UDID');
    //var decod=  window.atob(decodedString);
    var UDID= get_UDid('UDID');
    console.log("productallaction",UDID);
    return fetch(`${API_URL}/ShopSetting/GetAttributes?Udid=${UDID}`, requestOptions)
    .then(handleResponse)
    .then(attributelist=>{
        return attributelist.Content;
    });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
               // logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}



