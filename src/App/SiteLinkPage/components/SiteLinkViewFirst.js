import React from 'react';
import { connect } from 'react-redux';
import { siteActions } from '../actions/site_link.action';

const SiteLinkData = ['www.ethnohouse.ca', 'www.youyoushoes.com', 'www.bashoo.com'];
import { history } from '../../_helpers/history';
class SiteLinkViewFirst extends React.Component {
    constructor(props) {
        super(props);

    }
    componentWillMount(){
                  if ( localStorage.getItem('sitelist')==null ||  localStorage.getItem('sitelist')=='undefined') {
                    history.push('/login');
                    }
      
    }
    componentDidMount(){
        
      //  $(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   
    }
    handleSubmit(UDID) {      
         localStorage.setItem('UDID',UDID)
         history.push('/login_location');
         // alert("inside")
         // e.preventDefault();
 
         // this.setState({ submitted: true });
         // const { username, password } = this.state;
         // const { dispatch } = this.props;
         // if (username && password) {
         //     dispatch(userActions.login(username, password));
         // }
     }
    render() {
      
       let Sitelist= localStorage.getItem('sitelist')?JSON.parse(localStorage.getItem('sitelist')):this.props.sitelist?this.props.sitelist:[] ;
       //console.log("SiteLisnk first sitelst",Sitelist)
        return (
            <form className="onboarding-login-field" action="index.html">
                <ul className="list-group chooseregisterLinks" style={{height: 230}}>
                    {  Sitelist.map((link, index) => {
                        return (
                            Sitelist.Activated == "True" ?
                            <li key={index} className="list-group-item" onClick={()=>this.handleSubmit(link.UDID)}>
                              <a href="javascript:void(0)">{link.Host}
                             <img src="assets/img/green-arrow.png" /></a>
                            </li>
                            :null
                        )
                    })}

                    {/* <li className="list-group-item"><a href="#">www.youyoushoes.com<img src="assets/img/green-arrow.png" /></a></li>
                    <li className="list-group-item"><a href="#">www.bashoo.com<img src="assets/img/green-arrow.png" /></a></li> */}
                    <li className="list-group-item"></li>
                </ul>
            </form>
        )
    }

}

function mapStateToProps(state) {
    // const { sitelist } = state;

    // console.log("sitelist121",state);
    // return {
    //     sitelist
    // };
    const { authentication } = state;
    return {
        sitelist :authentication.user
    };

}

const connectedSiteLinkViewFirst = connect(mapStateToProps, siteActions)(SiteLinkViewFirst);
export { connectedSiteLinkViewFirst as SiteLinkViewFirst };