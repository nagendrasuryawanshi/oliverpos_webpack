import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute , LoadingIndexDB , PrintPage} from '../_components';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { Activity } from '../ActivityPage';
import { CustomerView } from '../CustomerPage';
import { CheckoutView } from '../CheckoutPage';
import { RefundView } from '../RefundPage';
import { CashReportView } from '../CashReportPage';
import { SettingView } from '../SettingPage';
import { SiteLinkView } from '../SiteLinkPage';
import { LoginRegisterView } from '../LoginRegisterPage';
import { PinPage } from '../LoginPin/PinPage';
import { ShopView } from '../ShopView';
import { ListView } from '../ListView';
import { LoginLocation } from '../LoginLocation';
import { CompleteRefund } from '../CheckoutPage/components/CompleteRefund';
import { RefundComplete } from '../RefundPage/components/refund_complete'; //by deepsagar
import { InternalErr } from '../Error/InternalErr'
import { RevalidatePage } from '../RevalidatePage'
import { pinLoginActions } from '../LoginPin/action/PinLogin.action';
import { ExternalLogin } from '../ExternalLogin/ExternalLogin';
import { userActions } from '../_actions/user.actions';
import { idbProductActions } from '../_actions/idbProduct.action'
import {encode_UDid,get_UDid} from '../ALL_localstorage';
import moment from 'moment';


class App extends React.Component {
    constructor(props) {
        super(props);
       
      
        const { dispatch ,alert} = this.props;
      //  console.log("console.log(window.location.protocol)",window.location.protocol)

        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    //   this.checkSubscriptionStatus();

         this.checkUserlogin()
        // this.checkBrowser()
    }

    logout(){
        this.props.dispatch(userActions.logout())
    }

    checkUserlogin(){
        var IDLE_TIMEOUT = 1800; //seconds
        var _idleSecondsCounter = 0;
        document.onclick = function () {
            _idleSecondsCounter = 0;
        };
        document.onmousemove = function () {
            _idleSecondsCounter = 0;
        };
        document.onkeypress = function () {
            _idleSecondsCounter = 0;
        };
        window.setInterval(CheckIdleTime, 1000);

        function CheckIdleTime() {
            _idleSecondsCounter++;
            var oPanel = document.getElementById("SecondsUntilExpire");
            if (oPanel)
                oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
            if (_idleSecondsCounter >= IDLE_TIMEOUT) {
                // alert("Time expired!");
               // console.log("Time expired!",this.props);
                window.location = '/loginpin'
                //document.location.href = "logout.php";
            }
        }


    }
    checkSubscriptionStatus(){
        //console.log("Get UDID in APPJS Page",localStorage.getItem('UDID'))
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID= get_UDid('UDID');
        if (UID !== null &&   typeof UID !== 'undefined') {
           let check_subscription_status_datetime = localStorage.getItem("check_subscription_status_datetime")
           if(check_subscription_status_datetime && typeof check_subscription_status_datetime !== "undefined"){              
             let currentDateTime = new Date();
             let existDateTime = new Date(check_subscription_status_datetime);        
             currentDateTime.getHours()
             var curent = currentDateTime.getDate();
             var exist = existDateTime.getDate();

             if (curent ==  exist) {         
                var timeDifference = currentDateTime.getTime() - existDateTime.getTime();
                var diff_as_date = new Date(timeDifference);
                if(diff_as_date.getHours() > 3 ) {
                    this.props.dispatch(pinLoginActions.checkSubcription())
                }
             } else {
                this.props.dispatch(pinLoginActions.checkSubcription())
             }

           }   
        }
    }

    // componentDidMount(){
    
    //     console.log("locationbar", moment().format('ss'))
    //     console.log("locationbar01",localStorage.getItem('closed'))
    //     console.log("locationbar02",Number(localStorage.getItem('closed'))+5 )
    //     if(localStorage.getItem('closed')){
    //         if(Number(localStorage.getItem('closed'))+5 < moment().format('ss')){
    //               localStorage.setItem("closed", moment().format('ss'))
    //               setTimeout(function(){
    //                    location.reload()
    //               },1000)
                 
    //         }else{
    //         localStorage.setItem("closed", moment().format('ss'))
    //         }
    //     }else{
    //         localStorage.setItem("closed", moment().format('ss'))
    //     }
         
    // }
 
    render() {
     ///  console.log("ProductLog",idbProductActions.getAll());
        const { alert } = this.props;
        //console.log("ENVIRONMENT", process.env.ENVIRONMENT, process.ENVIRONMENT);
        // console.log("Application Start")
        return (
                <Router history={history}>
                    <div>
                        <PrivateRoute exact path="/" component={LoadingIndexDB} />                            
                        <PrivateRoute path="/shopview" component={ShopView} />
                        <PrivateRoute path="/customerview" component={CustomerView} />
                        <PrivateRoute path="/checkout" component={CheckoutView} />
                        <PrivateRoute path="/setting" component={SettingView} />
                        <PrivateRoute path="/cash_report" component={CashReportView} />
                        <PrivateRoute path="/activity" component={Activity} />
                        <PrivateRoute path="/refund" component={RefundView} />                     
                        <PrivateRoute path="/choose_registration" component={LoginRegisterView} />
                        <PrivateRoute path="/CheckoutSuccessful" component={CompleteRefund} /> 
                        <PrivateRoute path="/listview" component={ListView} />                      
                        <PrivateRoute path="/refund_complete" component={RefundComplete} /> 
                        <PrivateRoute path="/printPage" component={PrintPage} />
                       

                        <Route path="/ExternalLogin" component={ExternalLogin} />
                        <Route path="/login" component={LoginPage} />
                        <Route path="/register" component={RegisterPage} />
                        <Route path="/login_location" component={LoginLocation} /> 
                        <Route path="/site_link" component={SiteLinkView} />
                        <Route exact  path="/loginpin" component={PinPage} /> 
                        <Route path='/Error' component={InternalErr} />
                        <Route path="/revalidate" component={RevalidatePage} />
                       
                    </div>
                </Router>

        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 