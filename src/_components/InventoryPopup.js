import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../_actions';
import { discountActions } from '../_actions/discount.action';
import { get_UDid } from '../ALL_localstorage'


class InventoryPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
        //const { dispatch } = this.props;
        //dispatch(discountActions.getAll());
        this.handleDiscount = this.handleDiscount.bind(this);
        this.handleClose = this.handleClose.bind(this);

    }


    calcInp(e) {

        if (e == "c") {
             if (this.state.discountAmount.toString().length ==1) {
                this.state.discountAmount = "0"}
             else {
                const txtValue = this.state.discountAmount.substring(-1, this.state.discountAmount.length - 1);
                this.state.discountAmount = txtValue == "" ? "0" : txtValue;

            }
        }
        else {
           this.state.discountAmount = $('#txtInv').val()=="0" || $('#txtInv').val()=="0.00"? e :$('#txtInv').val()+e.toString();
      
        }
            $('#txtInv').val(this.state.discountAmount);
      
      
    }

    handleDiscount() {
        const { inventoryCheck, isInventoryUpdate } = this.props;
        var inventoryAmount = $('#txtInv').val();
        console.log("inventoryAmount", inventoryAmount, inventoryCheck)
       
        var data = {
            udid:get_UDid('UDID'),
            quantity:inventoryAmount,
            wpid:(inventoryCheck.length > 0) ? inventoryCheck[0].WPID : inventoryCheck.WPID
        }  
        console.log("inventoryAmount", data)
        this.props.isInventoryUpdate(true)
        this.props.dispatch(cartProductActions.addInventoryQuantity(data,  this.props.inventoryData));
    
    }

    handleDiscountCancle(e) {
        this.setState({ 
            discountAmount: "0" ,
         })
        this.state.discountAmount = "0";
        jQuery('#txtInv').val(this.state.discountAmount);
        $('#panelCalculatorpopUp :input').removeAttr('disabled');
    }

    handleClose()
    {
        jQuery('#txtInv').val(0)
    }

    
    
    render() {
        const { selecteditem } = this.props;
      // console.log("selecteditem on inventory",selecteditem )
        return (
            <div id="InventoryPopup" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                <div className="modal-dialog modal-sm w-400" id="dialog-midle-align">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true" onClick={this.handleClose} >
                                <img src="assets/img/delete-icon.png" />
                            </button>
                            <h4 className="modal-title">Adjust Inventory</h4>
                        </div>
                        <div className="modal-body p-0">
                            <form className="clearfix">
                                <div className="p-0">
                                    <div className="panel-product-list" id="panelCalculatorpopUp">
                                        <div className="panel panelCalculator">
                                            <div className="panel-body p-0">
                                                <table className="table table-bordered shopViewPopUpCalculator">
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="2" className="text-right br-1 bl-1 bt-0">
                                                                <div className="input-group discount-input-group">
                                                                     <input type="text" id="txtInv" className="form-control text-right" placeholder="0" aria-describedby="basic-addon1" />
                                                                    <span className="input-group-addon AmoutType" id="" name="spnCalcType"></span>
                                                                </div>
                                                            </td>
                                                            <td className="text-center pointer bt-0" onClick={() => this.calcInp('c')}>

                                                                <button type="button" className="btn btn-default calculate">
                                                                    <img width="36" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zMC4xNzEsMTYuNDE2SDAuODg4QzAuMzk4LDE2LjQxNiwwLDE2LjAyLDAsMTUuNTI5YzAtMC40OSwwLjM5OC0wLjg4OCwwLjg4OC0wLjg4OGgyOS4yODMgICAgYzAuNDksMCwwLjg4OCwwLjM5OCwwLjg4OCwwLjg4OEMzMS4wNTksMTYuMDIsMzAuNjYxLDE2LjQxNiwzMC4xNzEsMTYuNDE2eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTE2LjAxNywzMS4wNTljLTAuMjIyLDAtMC40NDUtMC4wODMtMC42MTctMC4yNUwwLjI3MSwxNi4xNjZDMC4wOTgsMTUuOTk5LDAsMTUuNzcsMCwxNS41MjkgICAgYzAtMC4yNCwwLjA5OC0wLjQ3MSwwLjI3MS0wLjYzOEwxNS40LDAuMjVjMC4zNTItMC4zNDEsMC45MTQtMC4zMzIsMS4yNTUsMC4wMmMwLjM0LDAuMzUzLDAuMzMxLDAuOTE1LTAuMDIxLDEuMjU1TDIuMTYzLDE1LjUyOSAgICBsMTQuNDcxLDE0LjAwNGMwLjM1MiwwLjM0MSwwLjM2MSwwLjkwMiwwLjAyMSwxLjI1NUMxNi40OCwzMC45NjgsMTYuMjQ5LDMxLjA1OSwxNi4wMTcsMzEuMDU5eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="></img>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="td-calc-padding br-1 bl-1">
                                                                <button type="button" onClick={() => this.calcInp(1)} className="btn btn-default calculate">1</button>
                                                            </td>
                                                            <td className="td-calc-padding br-1">
                                                                <button type="button" onClick={() => this.calcInp(2)} className="btn btn-default calculate">2</button>
                                                            </td>
                                                            <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(3)} className="btn btn-default calculate">3</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="td-calc-padding br-1 bl-1">
                                                                <button type="button" onClick={() => this.calcInp(4)} className="btn btn-default calculate">4</button>
                                                            </td>
                                                            <td className="td-calc-padding br-1">
                                                                <button type="button" onClick={() => this.calcInp(5)} className="btn btn-default calculate">5</button>
                                                            </td>
                                                            <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(6)} className="btn btn-default calculate">6</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="td-calc-padding br-1 bl-1">
                                                                <button type="button" onClick={() => this.calcInp(7)} className="btn btn-default calculate">7</button>
                                                            </td>
                                                            <td className="td-calc-padding br-1">
                                                                <button type="button" onClick={() => this.calcInp(8)} className="btn btn-default calculate">8</button>
                                                            </td>
                                                            <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(9)} className="btn btn-default calculate">9</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                           <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(0)} className="btn btn-default calculate">0</button>
                                                            </td>
                                                            <td className="td-calc-padding br-1" colSpan="2">
                                                                {/* <button type="button" onClick={() => this.handleDiscountCancle('c')} className="btn btn-default calculate">clear</button> */}
                                                                <button type="button" className="btn btn-primary btn-block h70" data-dismiss="modal" onClick={() => this.handleDiscount()} >UPDATE INVENTORY</button>
                                                            </td>
                                                            {/* <div className="modal-footer p-0"> */}
                                                              
                                                           {/* </div> */}
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {/* <div className="modal-footer p-0">
                            <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" onClick={() => this.handleDiscount()} >ADD INVENTORY</button>
                        </div> */}
                    </div>
                </div>
            </div>


        );
    }
}

function mapStateToProps(state) {
    const { discountlist } = state.discountlist;
    const { selecteditem } = state.selecteditem;

    return {
        discountlist,
        selecteditem,
    };
}

const connectedInventoryPopup = connect(mapStateToProps)(InventoryPopup);
export { connectedInventoryPopup as InventoryPopup };