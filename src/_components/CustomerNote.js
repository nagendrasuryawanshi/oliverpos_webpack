import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../_actions';



class CustomerNote extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
        this.handleNote = this.handleNote.bind(this);
    }




    handleNote() {
        var txtNote = jQuery("#txtNote").val();
        if (txtNote != "") {
            const { dispatch } = this.props;

            var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [];//this.state.cartproductlist;
            cartlist = cartlist == null ? [] : cartlist;
            cartlist.push({ "Title": txtNote })
            dispatch(cartProductActions.addtoCartProduct(cartlist));
            let list = localStorage.getItem('CHECKLIST')?JSON.parse(localStorage.getItem('CHECKLIST')):null;
           // console.log("STEP WHEN ADD NOTE" , list)
            if(list!=null){
            const CheckoutList = {
                ListItem: cartlist,
                customerDetail: list.customerDetail,
                totalPrice: list.totalPrice,
                discountCalculated: list.discountCalculated,
                tax: list.tax,
                subTotal: list.subTotal,
                TaxId: list.TaxId,
                order_id : list.order_id !== 0? list.order_id : 0,
                showTaxStaus: list.showTaxStaus
            }
            localStorage.setItem('CHECKLIST', JSON.stringify(CheckoutList))
            location.reload();
           }
           
         
             // this.state.cartproductlist    
            jQuery("#txtNote").val("");
            jQuery(".close").trigger("click");

        }
    }


    render() {
        const { amount, add_title } = this.state;
        return (
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header bb-0">
                        <button type="button" className="close opacity-1 mt-1" data-dismiss="modal" aria-hidden="true" >
                            <img src="assets/img/delete-icon.png" />
                        </button>
                        <h1 className="modal-title">Add Note</h1>
                    </div>
                    <div className="modal-body p-0">
                        <textarea id="txtNote" className="form-control modal-txtArea" placeholder="Enter Note Here" ></textarea>
                    </div>
                    <div className="modal-footer p-0 b-0">
                        <button type="button" className="btn btn-primary btn-block btn-primary-cus pt-2 pb-2" onClick={this.handleNote} >SAVE & CLOSE</button>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {

    }
}

const connectedCustomerNote = connect(mapStateToProps)(CustomerNote);
export { connectedCustomerNote as CustomerNote };