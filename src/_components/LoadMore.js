import React from 'react';


export const LoadMore = (props) => {

    return (
        <tr className={props.className} onClick={props.onClick} id={props.FirstName}>
            <td>
                
               Load More
            </td>
            <td className="bg-blue">
                <a>
                    <img src="assets/img/next.png" />
                </a>
            </td>
        </tr>
    );
}
