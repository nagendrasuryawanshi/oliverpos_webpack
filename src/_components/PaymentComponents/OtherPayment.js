import React from 'react';
import { connect } from 'react-redux';


class OtherPayment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paidAmount:''
        }

    }

    hideTab(code){
        const {  closingTab , pay_amount} = this.props;
        closingTab(true);
            setTimeout(function(){
                   pay_amount(code); 
              
            },500)
        
    }

   

    render() {
        const { color, Name, code, styles } = this.props;
        
        return (
            <div style={{display:styles}} className="white-background box-flex-shadow box-flex-border mb-2 round-8 pointer d-none overflowHidden no-outline w-100 p-0 overflow-0">
                <div className="section">
                    <div className="accordion_header" data-isopen="false">
                        <div className="pointer">
                            <div style={{ borderColor: color }} id="others" value="other" name="payments-type" onClick={() => this.hideTab(code) } className="d-flex box-flex box-flex-border-left box-flex-background-others border-dynamic">
                                <div className="box-flex-text-heading">
                                    <h2>{Name}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {

    };
}

const connectedCardPayment = connect(mapStateToProps)(OtherPayment);
export { connectedCardPayment as OtherPayment };