import React from 'react';
import { connect } from 'react-redux';


class GlobalPayment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status:false
        }

    }

    
    cancel_payment() {
        location.reload()
    }
    
    hideTab(st) {
        setTimeout(function () {
            boxHeight();
       }, 50);
        if(st==true){
            this.props.activeDisplay(`${this.props.code}_true`)
        }else{
            this.props.activeDisplay(false)  
        }
        this.setState({
            status: st,
        })
    }

    render() {
        const { color, Name, code, pay_amount, msg, styles} = this.props;
        return (
            <div style={{display:styles}} className="white-background box-flex-shadow box-flex-border mb-2 round-8 d-none overflowHidden overflow-0"  >
                <input type="hidden" value={code} id="pay_amount"></input>
                <div className="section">
                    <div className="accordion_header" data-isopen="true">
                        <div className="pointer"    >
                            <div style={{ borderColor: color }} className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                <div className="box-flex-text-heading">
                                    <h2 onClick={() => {pay_amount(code); this.hideTab(!this.state.status)}}>
                                        {Name}
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="expandable_accordion">
                        <div className="border-color overflowHidden">
                            <div  style={{ backgroundColor:color }} className="box__block_caption theme-background b-0 round-top-corner" data-title="Global Payments"  >
                                <img src="../assets/img/cross_white.svg" className="accordion_close" />
                            </div>
                            <div className="overflowscroll global-body">
                                <div className="clearfix center-center d-column global_body">
                                    <div className="line-height-30 mb-3">
                                        <p className="fs16 color-4b">status</p>
                                        <p className="fs24 theme-color">{msg}</p>
                                    </div>
                                    <div className="row w-75">
                                        <div className="col-sm-4 p-0">
                                            <img onClick={() => this.cancel_payment()} src="assets/img/ic_gloabal_close.svg" width="50px" />
                                            <p><small>Cancel Payment</small></p>
                                        </div>
                                        <div className="col-sm-4 p-0">
                                            <img onClick={() => pay_amount(code)} src="assets/img/ic_gloabal_refresh.svg" width="50px" />
                                            <p><small>Resend Amount</small></p>
                                        </div>
                                        <div className="col-sm-4 p-0">
                                            <img src="assets/img/ic_gloabal_done.svg" width="50px" />
                                            <p><small>Manual Accept</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {

    };
}

const connectedCardPayment = connect(mapStateToProps)(GlobalPayment);
export { connectedCardPayment as GlobalPayment };