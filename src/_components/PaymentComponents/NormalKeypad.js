import React from 'react';
import { connect } from 'react-redux';


class NormalKeypad extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paidAmount: this.props.paidAmount,
            status: false,
            paidAmountStatus: false,
            emptyPaymentFlag: true,
            getRemainingPrice: this.getRemainingPrice(),
            temporary_vaule: ''
        }
        this.calcInp = this.calcInp.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }


    handleChange(e) {
        const re = new RegExp('^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
        if (e.target.value === '' || re.test(e.target.value)) {
            const { value } = e.target;
            this.props.normapNUM(value)
            this.setState({
                paidAmount: value,
                paidAmountStatus: true,
            })
        }

    }

    hideTab(st) {
        setTimeout(function () {
            boxHeight();
       }, 50);
        if (st == true) {
            this.props.activeDisplay(`${this.props.code}_true`)
        } else {
            this.props.activeDisplay(false)
        }
        this.setState({
            status: st,
            emptyPaymentFlag: true,
            paidAmount: parseFloat(this.state.getRemainingPrice.toFixed(2)),
            paidAmountStatus: false,
            temporary_vaule: '',
        })


    }

    getRemainingPrice() {
        if(this.props.parkOrder == 'refund'){
            return 0
        }else{
        let checkList = JSON.parse(localStorage.getItem("CHECKLIST"));
        let paid_amount = 0;

        let getPayments = (typeof JSON.parse(localStorage.getItem("oliver_order_payments")) !== "undefined") ? JSON.parse(localStorage.getItem("oliver_order_payments")) : [];

        if (getPayments !== null) {
            getPayments.forEach(paid_payments => {
                paid_amount += parseFloat(paid_payments.payment_amount);
            });
        }
        if (checkList.totalPrice >= paid_amount) {
            return (parseFloat(checkList.totalPrice) - paid_amount);

        }
      }
    }

    hideTab2(st) {
        this.setState({ status: st })
        this.props.activeDisplay(false)
    }

    SetValue(eleT, val) {
        let v1 = this.state.temporary_vaule + val;
        eleT.html(eleT.html().replace(' ', '') + val + ' ');
        this.setState({
            temporary_vaule: v1
        })

        this.props.normapNUM(v1)
    }

    calcInp(input) {

        this.setState({
            paidAmountStatus: true,
            paidAmount: ''
        })
        var elemJ = jQuery('#calc_output');
        if (this.state.emptyPaymentFlag || elemJ.val() == "0") {
            elemJ.val('');
            this.setState({
                emptyPaymentFlag: false
            });
        }
        switch (input) {
            case 1:
                this.SetValue(elemJ, '1');
                break;
            case 2:
                this.SetValue(elemJ, '2');
                break;
            case 3:
                this.SetValue(elemJ, '3');
                break;
            case 4:
                this.SetValue(elemJ, '4');
                break;
            case 5:
                this.SetValue(elemJ, '5');
                break;
            case 6:
                this.SetValue(elemJ, '6');
                break;
            case 7:
                this.SetValue(elemJ, '7');
                break;
            case 8:
                this.SetValue(elemJ, '8');
                break;
            case 9:
                this.SetValue(elemJ, '9');
                break;
            case 0:
                this.SetValue(elemJ, '0');
                break;
            case '.':
                if (elemJ.val() == "") {

                    this.SetValue(elemJ, '0.');

                } else {

                    if (elemJ.val().includes(".")) {

                    } else {

                        this.SetValue(elemJ, '.');
                    }
                }
                break;
        }
    }

    rmvInp() {
        let checkList = this.props.checkList;
        let str = $('#calc_output').val();
        str = str.slice(0, -1);
        if (str == "" || str == " ") {
            $('#calc_output').val('0');
        } else { }

        this.setState({
            temporary_vaule: str,
            paidAmountStatus: true
        })

        this.props.normapNUM(str)

    }

    componentWillReceiveProps() {
        if (this.props.closing_Tab == true) {
            this.setState({
                paidAmountStatus: false,
                paidAmount: '',
            })
            this.props.closingTab(false)
        }
    }

    parkOrder() {
        this.props.parkOrder('park_sale')
    }

    render() {
        const { parkOrder, placeholder, handleFocus, styles } = this.props;
        const { paidAmountStatus } = this.state;

        return (
            this.state.status !== true ?
                <div style={{ display: styles }} className="gray-background mb-2 round-8 d-none overflowHidden  overflow-0">
                    <div className="" data-isopen="">
                        <div className="pointer">
                            {parkOrder == 'refund' ?
                                <div className="box-flex center-center">
                                    <div className="col-sm-9" style={{ marginLeft: '16%' }}>
                                        <div className="block__box_text_price">
                                            {paidAmountStatus == true ?
                                                <input placeholder="0.00" id="my-input" value={this.state.temporary_vaule !== '' ? this.state.temporary_vaule : this.state.paidAmount} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline" autoComplete='off' />
                                                :
                                                <input placeholder="0.00" id="my-input" autoFocus="autoFocus" value={placeholder} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline" onFocus={handleFocus} />
                                            }
                                        </div>
                                    </div>
                                    <div className="col-sm-3">
                                        <div className="block__box_action_description park_avoid" >
                                            <img onClick={()=>this.hideTab(!this.state.status)} src="../assets/img/calculator_minimiser.svg" className="" />
                                        </div>
                                    </div>
                                </div>
                                :
                                <div className="box-flex center-center">
                                    <div className="col-sm-3">
                                        <div className="block__box_text_hint">
                                            <button type="button" onClick={() => this.parkOrder()} className="transparent b-0 r-0 no-outline uppercase">Park</button>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="block__box_text_price">
                                            {paidAmountStatus == true ?
                                                <input placeholder="0.00" id="my-input" autoFocus="autoFocus" value={this.state.temporary_vaule !== '' ? this.state.temporary_vaule : this.state.paidAmount} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline enter-order-amount" autoComplete='off' />
                                                :
                                                <input placeholder="0.00" id="my-input" autoFocus="autoFocus" value={placeholder} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline" onFocus={handleFocus} />
                                            }
                                        </div>
                                    </div>
                                    <div className="col-sm-3">
                                        <div className="block__box_action_description park_avoid" >
                                            <img onClick={() => this.hideTab(!this.state.status)} src="../assets/img/calculator_minimiser.svg" className="" />
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                :
                <div className="gray-background mb-2 round-8 d-none overflowHidden overflow-0">
                    <div className="section">
                        <div className="">
                            <div className="box-flex center-center park_table_row">
                                <div className="col-sm-3">
                                    <div className="block__box_text_hint">
                                        <img src="../assets/img/back_payment.svg" onClick={() => this.rmvInp()} />
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="block__box_text_price">
                                        {paidAmountStatus == true ?
                                            <input placeholder="0.00" id="calc_output" value={this.state.temporary_vaule !== '' ? this.state.temporary_vaule : this.state.paidAmount} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline enter-order-amount" autoComplete='off' />
                                            :
                                            <input placeholder="0.00" id="calc_output" value={placeholder} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline enter-order-amount" autoComplete='off' onFocus={handleFocus} />
                                        }

                                    </div>
                                </div>
                                <div className="col-sm-3" >
                                    <div className="block__box_action_description" href="#paymentReset" data-toggle="collapse" >
                                        <img onClick={() => this.hideTab(!this.state.status)} src="../assets/img/calculator_minimiser_colored.svg" className="gray-minisizer-icon" />
                                    </div>
                                </div>
                            </div>
                            <div className="panel-body p-0">
                                <div className="box__block_calculator">
                                    <table className="table table-responsive mb-0">
                                        <tbody>
                                            <tr className="park_table_row">
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline" onClick={() => this.calcInp(1)} value="1" placeholder="1" />
                                                </td>
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(2)} value="2" placeholder="2" />
                                                </td>
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(3)} value="3" placeholder="3" />
                                                </td>
                                            </tr>
                                            <tr className="park_table_row">
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(4)} value="4" placeholder="4" />
                                                </td>
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(5)} value="5" placeholder="5" />
                                                </td>
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(6)} value="6" placeholder="6" />
                                                </td>
                                            </tr>
                                            <tr className="park_table_row">
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(7)} value="7" placeholder="7" />
                                                </td>
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(8)} value="8" placeholder="8" />
                                                </td>
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(9)} value="9" placeholder="9" />
                                                </td>
                                            </tr>
                                            <tr className="park_table_row">
                                                <td>
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp('.')} value="." placeholder="," />
                                                </td>
                                                <td colSpan="2" className="border-left-0">
                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(0)} value="0" placeholder="0" />
                                                </td>
                                            </tr>
                                            <tr className="park_table_row" onClick={() => this.hideTab2(!this.state.status)}>
                                                <td colSpan="3" >
                                                    <input type="button" className="secondry-theme-background border-0 color-4b no-outline btn-enter " value="Enter" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        )
    }
}

function mapStateToProps(state) {

    return {

    };
}

const connectedCardPayment = connect(mapStateToProps)(NormalKeypad);
export { connectedCardPayment as NormalKeypad };