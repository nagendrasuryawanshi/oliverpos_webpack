import React from 'react';
import { connect } from 'react-redux';
import { ProductSubAtrribute  } from './';


class ProductAtrribute extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            getAttributes: null,
            option : null
        }

        this.handleOptionClick = this.handleOptionClick.bind(this);
    }

    handleOptionClick(option, attribute) {

        //console.log("handleOptionClick", option, attribute)
        this.state.option = option;
        this.setState({option:option})
        this.props.optionClick(option, attribute);
    }


    render() {
        const ProductAttribute = this.props;
       // console.log("selectedAttribute", this.props.selectedAttribute, this.props.showSelectedProduct)
        return (

            ProductAttribute.attribute && ProductAttribute.attribute.length > 0 ?
                (ProductAttribute.attribute.map((attribute, index) => {
                    return (
                        
                        attribute && attribute.Variation==true &&
                            <div className="variation-list"  key={index}>
                                <h4 className="variation-title">{attribute.Name}</h4>
                                <div className="varialtion-color-category">
                                    <div className="container-fluid">
                                        <div className="row">
                                            <ProductSubAtrribute showSelectedProduct={this.props.showSelectedProduct?this.props.showSelectedProduct.combination:null} key={"ProductSubAttr-" + index} options={attribute.Option} parentAttribute={attribute.Slug} click={this.handleOptionClick} filteredAttribute={attribute.Slug == this.props.selectedAttribute ? [] : this.props.filteredAttribute} productVariations={this.props.productVariations} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                          
                        
                    )
                }

                )
                ) : ""
        )
    }

}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedProductAtrribute = connect(mapStateToProps)(ProductAtrribute);
export { connectedProductAtrribute as ProductAtrribute };
//export default ProductAttribute;