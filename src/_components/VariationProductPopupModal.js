import React from 'react';
import { connect } from 'react-redux';
import { ProductAttribute } from '../ShopView/components/ProductAttribute';
import { cartProductActions } from '../_actions/cartProduct.action'
import { history } from '../_helpers';
import { Markup } from 'interweave';

class VariationProductPopupModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            variationStockQunatity:'',
            variationTitle:'',
            variationImage:'',
           // variationCombination: [],
            variationoptionArray: {},
            filteredAttribute:[],
            filterTerms:[],
            selectedAttribute:"",
            ManagingStock:null,
            old_price:0,
            incl_tax:0,
            excl_tax:0,
                      
        }
       this.clicks = [];
       this.timeout;
    
       
        this.timer = 0;
        this.delay = 300;
        this.prevent = false;

        this.incrementDefaultQuantity = this.incrementDefaultQuantity.bind(this);
        this.setDefaultQuantity = this.setDefaultQuantity.bind(this);
        this.decrementDefaultQuantity = this.decrementDefaultQuantity.bind(this);
        this.addVariationProductToCart = this.addVariationProductToCart.bind(this);

        this.optionClick = this.optionClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        
       

    }
 
    componentWillUnmount(){      
        this.setState({
                variationStockQunatity:'',
                variationTitle:'',
                variationImage:'',
               // variationCombination: [],
                variationoptionArray: {},
                 filteredAttribute:[]
                          
             });
    }

    incrementDefaultQuantity() {
        if (this.state.variationDefaultQunatity>=0) {
            //------
            var product=this.state.getVariationProductData
           
            let qty = parseInt(this.state.variationDefaultQunatity);


    if( (product.StockStatus==null || product.StockStatus== 'instock')  
            &&( product.ManagingStock==false || (product.ManagingStock==true && qty<  this.state.variationStockQunatity)) )
           { 
               qty++;            
           }
            if(qty>this.state.variationStockQunatity)
            qty=this.state.variationStockQunatity;

            this.setDefaultQuantity(qty);
        }

    }
    decrementDefaultQuantity() {
        if (this.state.variationDefaultQunatity && this.state.variationDefaultQunatity > 1) {
            let qty = parseInt(this.state.variationDefaultQunatity);
            qty--;
            this.setDefaultQuantity(qty);
        }
    }

    setDefaultQuantity(qty) {
        this.setState({
            variationDefaultQunatity: qty,
        });
    }

    addVariationProductToCart() {

        var  ticket_Data=this.state.ticket_status == true?localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'':''  
      //  console.log("ticketfieldList12222",localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''); 
        

       var tick_data= this.state.getVariationProductData?this.state.getVariationProductData.TicketInfo:''
        var data = {
            line_item_id: 0,
            quantity: this.state.variationDefaultQunatity,
            Title: this.state.variationTitle,
            Price: parseInt(this.state.variationDefaultQunatity) * parseFloat(this.state.variationPrice),
            product_id: this.state.variationParentId,
            variation_id: this.state.variationId,
            isTaxable: this.state.variationIsTaxable,
            old_price:this.state.old_price,
            incl_tax:this.state.incl_tax,
            excl_tax:this.state.excl_tax,
            ticket_status:this.state.ticket_status,
            product_ticket:this.state.ticket_status == true ?ticket_Data?ticket_Data:'':'',
            tick_event_id:this.state.ticket_status == true ?tick_data._event_name:null,
        }
     
        var qty=0;
        var product=this.state.getVariationProductData

        //check variation inStock------------------------
        console.log("this.state.variationId",this.state.variationId)
        var vatoationId= this.state.variationId;
        var isVariationInStock=false;
        if(product.Variations &&  vatoationId && vatoationId !=='undefined')
        {
            var variatonfound = product.Variations.find(function (element) {
                return element.WPID== vatoationId
            })
            console.log("variatonfound",variatonfound)
          if(variatonfound){
            if((variatonfound.StockStatus==null || variatonfound.StockStatus== 'instock')  && 
            (variatonfound.ManagingStock==false || (this.state.ManagingStock==true && qty<=this.state.variationStockQunatity )))
            {
                isVariationInStock=true;
            }
          }
        }
        //-----------------------------------------


     //   console.log("variationStockQunatity",this.state.variationStockQunatity,this.state.variationDefaultQunatity);



        console.log("product",product)
        
        let cartItemListHolder = [];
        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
        
        
        qty= qty+this.state.variationDefaultQunatity;
    
        // console.log("this.state.variationDefaultQunatity",this.state.variationDefaultQunatity)
        // console.log("condition",( (product.StockStatus==null || product.StockStatus== 'instock')  && 
        // (product.ManagingStock==false || (product.ManagingStock==true && qty<=this.state.variationStockQunatity )) ))
        // console.log("condition1", (product.StockStatus==null || product.StockStatus== 'instock') )
        // console.log("condition2", (product.ManagingStock==true && qty<=this.state.variationStockQunatity ))
        // console.log("condition3",product.StockStatus==null,product.ManagingStock==true )
        // console.log("condition4",qty<=this.state.variationStockQunatity,qty,this.state.variationStockQunatity )
        // console.log("product",product ) 
        // console.log("this.state.ManagingStock",this.state.ManagingStock)

        //console.log("condition" ,product,"this.state.variationStockQunatity",this.state.variationStockQunatity,"this.state.variationDefaultQunatity",this.state.variationDefaultQunatity)

        if(this.state.variationStockQunatity==0 || this.state.variationDefaultQunatity==0)
        {
        
            //console.log("condition 1" ,this.state.variationStockQunatity , qty)
            $('#outOfStockModal').modal('show')
            return;
        }
        else if( parseInt($("#txtPrdQuantity").val())<=0)
        {
            //console.log("condition 2" ,this.state.variationStockQunatity , qty)
           // console.log("this.state.variationStockQunatity" ,this.state.variationStockQunatity , qty)
            alert("Product quantity must be greater than zero!")
            return;
        }
        else if( isVariationInStock ==true || ((product.StockStatus==null || product.StockStatus== 'instock')  && 
                (this.state.ManagingStock==false || (this.state.ManagingStock==true && qty<=this.state.variationStockQunatity )) )) 
        {
               //   console.log("condition 3" ,this.state.variationStockQunatity , qty)
               //console.log("AddProduct",product)   
              //console.log( "text",  $("#txtInScock").val(), $("#txtInScock").val())

                    if($("#txtInScock").val()==="Unlimited" || parseInt($("#txtInScock").val())>0 || this.state.variationStockQunatity>0  || this.state.variationStockQunatity=="Unlimited"){
            cartItemList.push(data)
            this.props.dispatch(cartProductActions.addtoCartProduct(cartItemList)); // this.state.cartproductlist
                    }
                    else{
                      //  console.log("this.state.variationStockQunatity01" ,this.state.variationStockQunatity , qty)
                 
                        $('#outOfStockModal').modal('show')
                        return;
                    }

            //-----------------------------------------------------------------  
          
            this.setState({
                variationTitle: this.props.getVariationProductData.Title,
                variationId: this.props.getVariationProductData.WPID,
                variationParentId: this.props.getVariationProductData.ParentId,
                variationPrice: this.props.getVariationProductData.Price,
                variationStockQunatity: this.props.getVariationProductData ? (this.props.getVariationProductData.StockStatus==null || this.props.getVariationProductData.StockStatus== 'instock') && this.props.getVariationProductData.ManagingStock==false ?"Unlimited": this.props.getVariationProductData.StockQuantity:'0',
                variationImage:  this.props.getVariationProductData?this.props.getVariationProductData.ProductImage:null,
                variationIsTaxable: this.props.getVariationProductData?this.props.getVariationProductData.Taxable:0,
                variationDefaultQunatity:1,
                filteredAttribute:[],
                filterTerms:[],
                ManagingStock:this.props.getVariationProductData?this.props.getVariationProductData.ManagingStock:null,
                old_price:this.props.getVariationProductData?this.props.getVariationProductData.old_price:0,
                incl_tax:this.props.getVariationProductData?this.props.getVariationProductData.incl_tax:0,
                excl_tax:this.props.getVariationProductData?this.props.getVariationProductData.excl_tax:0,
                ticket_status: this.props.getVariationProductData.IsTicket,


            });

           // console.log("variationPrice1",this.state.variationPrice);
            $( ".button_with_checkbox input" ).prop( "checked", false );
           // $('.row').attr('checked', false);
            $("#add_variation_product_btn").css({ "cursor": "no-drop", "pointer-events": "none" });
          
            //--------------------------------------------------------------------
            this.forceUpdate()
           // console.log("this.state.variationStockQunatity02" ,this.state.variationStockQunatity , qty)
            $(".close").trigger("click");            
            }

            else{  
                console.log("condition 4" ,this.state.variationStockQunatity , qty)
              // console.log("this.state.variationStockQunatity03" ,this.state.variationStockQunatity , qty)            
              $('#outOfStockModal').modal('show')
            }

        this.setState({         
            variationDefaultQunatity: 1  
          
        })
        //     }
        this.forceUpdate()
       // $(".close").trigger("click");
    }

   
    componentWillMount = props => {
        this.clickTimeout = null

        this.setState({filterTerms:[]})
        console.log("componentWillMount")
      }
      componentDidMount() {
          
          setTimeout(() => {
            $( ".button_with_checkbox input" ).prop( "checked", false );
          }, 300);
     
      }
      


      optionClick(option, attribute) {
        console.log("Selected",option ,attribute)
        
            var filterTerms=this.state.filterTerms;
        var optExist=false;
           
             filterTerms && filterTerms.map(opItem=>{
                    if( opItem.attribute===attribute)
                    {        
                        opItem.attribute=attribute;
                        opItem.option=option   ;       
                        optExist=true
                    }
                })
           
            if(optExist==false)
            {
                filterTerms.push({
                    attribute:attribute,
                    option:option
                })
            }
            this.setState({filterTerms:filterTerms})
        
        
            // if (this.clickTimeout !== null) {
            //     console.log('double click executes')
            //     clearTimeout(this.clickTimeout)
            //     this.clickTimeout = null
            //     this.setState({filteredAttribute:[]});
            //   } 
            // else {
            //     console.log('single click')  
            //     this.clickTimeout = setTimeout(()=>{
            //   //  console.log('first click executes ')
            //     clearTimeout(this.clickTimeout)
            //       this.clickTimeout = null
            //     }, 300);
              
                console.log("getVariationProductData",this.props.getVariationProductData)
                console.log("getVariationProductData1",this.props.getVariationProductData.Variations)
        
               // console.log("filteredAttribute000",filteredAttribute)
                //console.log("optionClick",option,attribute)
                this.setState({
                    filteredAttribute:[],
                    selectedAttribute:attribute
                });
                if(this.props.getVariationProductData.ProductAttributes && this.props.getVariationProductData.ProductAttributes.length>1)
                {
                var filteredAttribute= this.props.getVariationProductData.Variations.filter(item=>{ 
                    //includes(option.toLowerCase())
                
                //console.log("item",item);
                var optionRes = option.replace(/\s/g,'-').toLowerCase();
                optionRes=optionRes.replace(/\//g, "-").toLowerCase();
                // console.log("option1",optionRes);
                var isExist=false;
                item.combination.split("~").map(combination=>{
                   // console.log("combination", combination.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase());
                   // console.log("optionRes", optionRes);
                    if(  combination.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase()===optionRes)
                    isExist=true;
                })
                   // console.log("IsCombinationExist",isExist);
                return isExist;
                    
                })  
                this.setState({filteredAttribute:filteredAttribute})
         //       $("#add_variation_product_btn").css({ "cursor": "pointer", "pointer-events": "auto" });
                console.log("filteredAttribute123",filteredAttribute);
        
            }
        
        
        
        
              // console.log("clicked",option, attribute);
                let attributeLenght = this.getAttributeLenght();
              //  this.state.variationoptionArray[attribute] = option;
               // let variationOptionclick = (Object.keys(this.state.variationoptionArray)).length;
               // if (attributeLenght == variationOptionclick) {
                   // this.searchvariationProduct(this.state.variationoptionArray);
                   this.searchvariationProduct(option);
                //}
            //}
        }
  
    doClickAction() {
      console.log(' click');
    }
    
    
     combo(c) {
        var r = [];
        var len = c.length;
        var tmp = [];
        function nodup() {
          var got = {};
          for (var l = 0; l < tmp.length; l++) {
            if (got[tmp[l]]) return false;
            got[tmp[l]] = true;
          }
          return true;
        }
        function iter(col,done) {    
          var l, rr;
          if (col === len) {      
            if (nodup()) {
              rr = [];
              for (l = 0; l < tmp.length; l++) 
                rr.push(c[tmp[l]]);        
              r.push(rr.join('~'));
            }
          } else {
            for (l = 0; l < len; l ++) {            
              tmp[col] = l;
              iter(col +1);
            }
          }
        }
        iter(0);
        return r;
      }

    searchvariationProduct(options) {
        var filteredArr=[]
        console.log("this.state.filterTerms",this.state.filterTerms)
        this.state.filterTerms.map(itm=>{


            let attribute_list = localStorage.getItem("attributelist")?  JSON.parse(localStorage.getItem("attributelist")) :null; 
           // console.log("Selected",options ,attribute)
            console.log("attribute_list", attribute_list)
            var sub_attribute;
           // var option;
            var found = attribute_list.find(function (element) {
                return element.Code.toLowerCase() == itm.attribute.toLowerCase()
            })
            console.log("found" , found)
            if(found){
                let SubAttributes =  found.SubAttributes;
                if(SubAttributes){
                  sub_attribute = SubAttributes.find(function(element){
                     // console.log("element.", (element.Value).toLowerCase(), options)
                      return  (element.Value).toLowerCase() == itm.option.toLowerCase();
                  }) 
                }

              console.log("sub_attribute.Code", sub_attribute);
              }
              filteredArr.push(sub_attribute?sub_attribute.Code: itm.option);
           // filteredArr.push(itm.option);
        })
           
        var cominationArr=this.combo(filteredArr);
       console.log("AllCombination",cominationArr)
        
        let variations = this.state.getVariationProductData.Variations;
        // let filterItem1= filteredArr.join("~");
        // var filterItem2 = filteredArr.reverse().join("~"); 




        var found = variations.find(function (element) {
            //var filteredArr=[];
            var checkExist=false;
          
             
            cominationArr && cominationArr.map(comb=>{

          console.log("comb",comb)
                if(element.combination.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase()===comb.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase()
                   // || element.combination.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase()===filterItem2.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase()
                    )
                    {                   
                        checkExist=true; 
                        console.log("matched",element.combination,element)
                        return element;
                    }
                })                  
               // })
            //    if( checkExist==true)
            //          {   return element}
               
            //     if(oItem.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase()===options.replace(/\s/g,'-').replace(/\//g, "-").toLowerCase())
            //    { 
             
            //        checkExist=true;
            //        return element;
            //    }
           // })
          
            if(checkExist==true)
            {
                return element;
            }
         
        });

  console.log("Found",found);

        if (typeof found !== 'undefined') {

            let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
            var qty=0;
            cartItemList.map(item=>{
                console.log("cartItemList",item);

                if(found.WPID==item.variation_id){

                    qty=  item.quantity;
                    console.log("cartItemListqty",qty);


                }
            })
         
            this.setState({
                variationTitle: found.Title,
                variationId: found.WPID,
                variationParentId: found.ParentId,
                variationPrice: found.Price,
                variationStockQunatity: (found.StockStatus==null || found.StockStatus== 'instock') && found.ManagingStock==false ?"Unlimited": found.StockQuantity -qty,
                variationImage: (found.ProductImage == null) ? this.state.variationImage : found.ProductImage,
                variationIsTaxable: found.Taxable,
                variationDefaultQunatity:1,
                ManagingStock:found.ManagingStock,
                old_price:found.old_price,
                incl_tax:this.state.incl_tax,
                excl_tax:this.state.excl_tax

            });
            console.log("variationPrice2",this.state.variationPrice);
            $("#add_variation_product_btn").css({ "cursor": "pointer", "pointer-events": "auto" });
        }
        else{
            this.setState({
              
                variationParentId: 0,
                variationPrice: 0,
                variationStockQunatity: 0,
                variationImage:"",
                ManagingStock:null
            });
        }

    }

    getAttributeLenght() {
        return this.props.getVariationProductData.ProductAttributes.length;
    }

    componentWillReceiveProps(nextPros){
    // console.log("componentWillReceiveProps",nextPros);
        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []  
        var qty=0;    
        if( cartItemList && cartItemList.length>0)  {
        cartItemList.map(item=>{
            if(nextPros.getVariationProductData && nextPros.getVariationProductData.WPID==item.variation_id){
                qty=  item.quantity;
            }
        })
    }

    //var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''  
    // console.log("",ticket_Data);

        if(nextPros.getVariationProductData){          
                  this.setState({
                    getVariationProductData: nextPros.getVariationProductData,
                    hasVariationProductData: true,
                    loadProductAttributeComponent: true,
                    variationOptionclick: 0,
                    variationTitle: nextPros.getVariationProductData ? nextPros.getVariationProductData.Title  : '',
                    variationId: 0,
                    variationPrice: nextPros.getVariationProductData ? nextPros.getVariationProductData.Price :  0,
                    variationStockQunatity: 
                    (nextPros.getVariationProductData.StockStatus==null || nextPros.getVariationProductData.StockStatus== 'instock') && nextPros.getVariationProductData.ManagingStock==false ? "Unlimited": (typeof nextPros.getVariationProductData.StockQuantity!= 'undefined') && nextPros.getVariationProductData.StockQuantity!= '' ? nextPros.getVariationProductData.StockQuantity-qty: '0',
                    variationImage: nextPros.getVariationProductData?nextPros.getVariationProductData.ProductImage ? nextPros.getVariationProductData.ProductImage  : '':'',
                    variationDefaultQunatity: 1 ? 1 : nextPros.getVariationProductData ? nextPros.getVariationProductData.DefaultQunatity : '',
                    ManagingStock:nextPros.getVariationProductData.ManagingStock,
                    old_price:nextPros.getVariationProductData?nextPros.getVariationProductData.old_price:0,
                    incl_tax:nextPros.getVariationProductData?nextPros.getVariationProductData.incl_tax:0,
                    excl_tax:nextPros.getVariationProductData?nextPros.getVariationProductData.excl_tax:0,
                    ticket_status:nextPros.getVariationProductData?nextPros.getVariationProductData.IsTicket:'',
                    //product_ticket:nextPros.getVariationProductData.IsTicket==true ? ticket_Data:''
               });
  

             
         }
   // else if(nextPros.getVariationProductData!=null && nextPros.getVariationProductData.IsTicket == false  ){
    //       //  console.log("ticket nai hain")
    //       this.setState({
    //         getVariationProductData: nextPros.getVariationProductData,
    //         hasVariationProductData: true,
    //         loadProductAttributeComponent: true,
    //         variationOptionclick: 0,
    //         variationTitle: nextPros.getVariationProductData.Title ? nextPros.getVariationProductData.Title  : '',
    //         variationId: 0,
    //         variationPrice: nextPros.getVariationProductData.Price ? nextPros.getVariationProductData.Price :  0,
    //         variationStockQunatity: 
    //         (nextPros.getVariationProductData.StockStatus==null || nextPros.getVariationProductData.StockStatus== 'instock') && nextPros.getVariationProductData.ManagingStock==false ? "Unlimited": (typeof nextPros.getVariationProductData.StockQuantity!= 'undefined') && nextPros.getVariationProductData.StockQuantity!= '' ? nextPros.getVariationProductData.StockQuantity-qty: '0',
    //         variationImage: nextPros.getVariationProductData.ProductImage ? nextPros.getVariationProductData.ProductImage  : '',
    //         variationDefaultQunatity: 1 ? 1 : nextPros.getVariationProductData ? nextPros.getVariationProductData.DefaultQunatity : '',
    //         ManagingStock:nextPros.getVariationProductData.ManagingStock,
    //         ticket_status:nextPros.getVariationProductData?nextPros.getVariationProductData.IsTicket:''

    //    });

          




    //       }
        //console.log("variationPrice",this.state.variationPrice);
      
    }
  
    handleChange(e) {
        if(e.target.value ==="")
        {
            this.setState({variationDefaultQunatity:0}); 
        }
        else if( e.target.value  && ! isNaN(e.target.value) && !e.target.value.includes(".")) 
        {
            if(this.state.variationStockQunatity=="Unlimited" || parseInt(this.state.variationStockQunatity)>= parseInt(e.target.value))
            {
                this.setState({variationDefaultQunatity:parseInt(e.target.value)});          
            }
        }
    }


    handleNoChange(e) {}
    handleClose()
    {
        $( ".button_with_checkbox input" ).prop( "checked", false );
       
    //    console.log("getVariationProductData",this.props.getVariationProductData)
  if(this.props.getVariationProductData){
        this.setState({
           
            hasVariationProductData: true,
            loadProductAttributeComponent: true,
            variationOptionclick: 0,
            variationTitle: this.props.getVariationProductData ? this.props.getVariationProductData.Title  : '',
            variationId: 0,
            variationPrice: this.props.getVariationProductData ? this.props.getVariationProductData.Price :  0,
            variationStockQunatity: this.props.getVariationProductData?
            (this.props.getVariationProductData.StockStatus==null || this.props.getVariationProductData.StockStatus== 'instock') && this.props.getVariationProductData.ManagingStock==false ? "Unlimited": (typeof this.props.getVariationProductData.StockQuantity!= 'undefined') && this.props.getVariationProductData.StockQuantity!= '' ? this.props.getVariationProductData.StockQuantity: '0':'0',
            variationImage: this.props.getVariationProductData ? this.props.getVariationProductData.ProductImage ? this.props.getVariationProductData.ProductImage  : '':'',
            variationDefaultQunatity: 1 ? 1 : this.props.getVariationProductData ? this.props.getVariationProductData.DefaultQunatity : '',
            ManagingStock: this.props.getVariationProductData ? this.props.getVariationProductData.ManagingStock : null,
           filteredAttribute:[],
           filterTerms:[],
           old_price:this.props.getVariationProductData ? this.props.getVariationProductData.old_price:0,
           incl_tax:this.props.getVariationProductData?this.props.getVariationProductData.incl_tax:0,
           excl_tax:this.props.getVariationProductData?this.props.getVariationProductData.excl_tax:0,
    //    });

    //         ManagingStock:this.props.getVariationProductData.ManagingStock,
       
       
            filteredAttribute:[],
            filterTerms:[]
         });
      }

    }
   
    render() {
      const { getVariationProductData,hasVariationProductData } = this.props
     // console.log("variationProductPopup",getVariationProductData,hasVariationProductData);

      var img=this.state.variationImage?this.state.variationImage.split('/'):'';
    //   console.log("rr12",img)
    //   console.log("rr13",img[8])
       //console.log("this.state.variationPrice ",this.state.variationPrice )

    var title= this.state.variationTitle && this.state.variationTitle.length>34?this.state.variationTitle.substring(0,31)+"..." : this.state.variationTitle
    console.log("this.state.variationTitle",title)
        return (
            <div id="variationProductPopup" tabIndex="-1" className="modal modal-wide fade full_height_modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal"  onClick={this.handleClose}>
                                <img src="assets/img/delete-icon.png"  />
                            </button>
                            {/* <h4 className="modal-title">{hasVariationProductData ? getVariationProductData.Title : ''}</h4> */}
                            <h4 className="modal-title" title={this.state.variationTitle}>{hasVariationProductData ? <Markup content={title}></Markup>  : ''}</h4>
                        
                        </div>

                        <div className="modal-body overflowscroll ButtonRadius" id="scroll_mdl_body">
                            <div className="row">
                                <div className="col-md-4 col-xs-4 text-center">
                                    <img src={hasVariationProductData ? this.state.variationImage ? img == 'placeholder.png'  ?'':this.state.variationImage : '':''} onError={(e)=>{e.target.onerror = null; e.target.src="assets/img/placeholder.png"}}  id="prdImg" style={{ width: '100%', height: 200, borderRadius: 8, marginLeft: 20 }} />
                                </div>
                                <div className="col-md-8 col-xs-8">
                                    <div className="col-md-2">
                                        <div className="modal_wide_field">
                                            <input type="text" id="txtPrdQuantity" value={hasVariationProductData ?  this.state.variationStockQunatity==0?0:this.state.variationDefaultQunatity: ''} className="form-control h_eight70  p-0" onChange={this.handleChange.bind(this)} />
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-xs-4">
                                        <div className="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div className="btn-group" role="group">
                                                <a onClick={this.incrementDefaultQuantity} className="btn btn-default bd-r h_eight70 btn-plus">
                                                    <button className="icon icon-plus button"></button>
                                                </a>
                                            </div>
                                            <div className="btn-group" role="group">
                                                <a onClick={this.decrementDefaultQuantity} className="btn btn-default h_eight70 btn-plus bl-0">
                                                    <button className="icon icon-minus button"></button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-xs-6">
                                        <div className="input-group">
                                            <div className="input-group-btn w-50 btn-left">
                                                <button type="button" className="btn btn-default br-0 h_eight70">In Stock</button>
                                            </div>
                                            <div className="modal_wide_field">
                                                <input type="text" id="txtInScock" className="form-control h_eight70 borderRightBottomRadius" value={hasVariationProductData ? this.state.variationStockQunatity!='Unlimited'? this.state.variationStockQunatity:this.state.variationStockQunatity : 1} onChange={this.handleNoChange.bind(this)} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-xs-12 mt-4">
                                        <div className="input-group">
                                            <div className="input-group-btn w-50 btn-left">
                                                <button type="button" className="btn btn-default bd-r h_eight70">Price</button>
                                            </div>
                                            <div className="modal_wide_field">
                                                <input type="text" className="form-control bl-0 h_eight70 borderRightBottomRadius" value={parseFloat(hasVariationProductData ? this.state.variationPrice > 0 ?this.state.variationPrice :0 : 0).toFixed(2)} onChange={this.handleNoChange.bind(this)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ProductAttribute attribute={hasVariationProductData ? getVariationProductData.ProductAttributes : null} optionClick={this.optionClick} filteredAttribute={this.state.filteredAttribute}  selectedAttribute={this.state.selectedAttribute} productVariations={this.props.getVariationProductData? this.props.getVariationProductData.Variations:[]}/>


                            </div>
                        </div>

                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-block btn-primary checkout-items buttonProductVariation" id="add_variation_product_btn" style={{ cursor: "no-drop", pointerEvents: "none" }} data-variation-id={hasVariationProductData ? this.state.variationId : 0} onClick={this.addVariationProductToCart.bind(this)}>Add Product</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



function mapStateToProps(state) {
    const { categorylist, productlist, attributelist } = state;

    return {
        categorylist: categorylist,
        productlist: productlist,
        attributelist: attributelist
    };
}

const connectedVariationProductPopupModal = connect(mapStateToProps)(VariationProductPopupModal);
export { connectedVariationProductPopupModal as VariationProductPopupModal };