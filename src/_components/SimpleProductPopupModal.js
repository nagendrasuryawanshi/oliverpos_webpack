import React from 'react';
import { connect } from 'react-redux';
import { ProductAttribute } from '../ShopView/components/ProductAttribute';
import { cartProductActions } from '../_actions/cartProduct.action';
import { Markup } from 'interweave';


class SimpleProductPopupModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            variationStockQunatity:'',
            simpleTitle:'',
            variationImage:'',
            variationCombination: [],
            variationoptionArray: {},
        }
        this.incrementDefaultQuantity = this.incrementDefaultQuantity.bind(this);
        this.setDefaultQuantity = this.setDefaultQuantity.bind(this);
        this.decrementDefaultQuantity = this.decrementDefaultQuantity.bind(this);
        this.addSimpleProducttoCart = this.addSimpleProducttoCart.bind(this);
    }

    incrementDefaultQuantity() {
      //  alert("aa")
   
        const {dispatch,  cartproductlist } = this.props;
        var cartlist = cartproductlist ? cartproductlist : [];
        var  product=this.state.getSimpleProductData;
        var cartProqty=0
        if (this.state.simpleDefaultQunatity>=0) {            
            let qty = parseInt(this.state.simpleDefaultQunatity);
            var totalQty=this.state.simpleStockQunatity || this.state.simpleStockQunatity == 0 ?this.state.simpleStockQunatity:product.StockQuantity;
          
            if(this.state.simpleStockQunatity =='Unlimited' ||  qty<totalQty)
            qty++;
            if(qty>totalQty)
              qty=totalQty;

            cartlist  && cartlist.map(item=>{
                if(product.WPID==item.product_id){
                      cartProqty =  item.quantity;
                 }              
            })
             
            if( (  (product.StockStatus==null || product.StockStatus== 'instock') && product.ManagingStock==false)
                     || ( ( product.StockStatus==null || product.StockStatus== 'instock') && product.ManagingStock==true &&  cartProqty<= product.StockQuantity))
                     {
                    if(qty>0)  
                   this.setDefaultQuantity(qty);             
             
           }
       }

    }
    decrementDefaultQuantity() {
        var  product=this.state.getSimpleProductData;
        if (this.state.simpleDefaultQunatity && this.state.simpleDefaultQunatity > 1) {
            let qty = parseInt(this.state.simpleDefaultQunatity);
            qty--;
            this.setDefaultQuantity(qty);
        }
    }

    setDefaultQuantity(qty) {
       // console.log("qty",qty)
        this.setState({
            simpleDefaultQunatity: qty,
        });
    }

    addSimpleProducttoCart() {
        console.log("submit data");
        const {dispatch,  cartproductlist } = this.props;
        var cartlist = cartproductlist ? cartproductlist : [];

        console.log("cartproductlist inside" , cartproductlist);
        var tick_data= this.state.getSimpleProductData?this.state.getSimpleProductData.TicketInfo:''
        var data = {
            line_item_id: 0,
            cart_after_discount:cartlist.cart_after_discount?cartlist.cart_after_discount:0,
            cart_discount_amount:cartlist.cart_discount_amount?cartlist.cart_discount_amount:0,
            after_discount:cartlist.after_discount?cartlist.after_discount:0,
            discount_amount:cartlist.discount_amount?cartlist.discount_amount:0,
            product_after_discount:cartlist.product_after_discount?cartlist.product_after_discount:0,
            product_discount_amount:cartlist.product_discount_amount?cartlist.product_discount_amount:0,
            quantity: this.state.simpleDefaultQunatity,
            Title: this.state.simpleTitle,
            Price: parseInt(this.state.simpleDefaultQunatity) * parseFloat(this.state.simplePrice),
            product_id: this.state.simpleproductid,
            variation_id:0,
            isTaxable: this.state.simpleisTaxable,
            old_price:this.state.old_price,
            incl_tax:this.state.incl_tax,
            excl_tax:this.state.excl_tax,
            ticket_status:this.state.ticket_status,
            product_ticket:this.state.product_ticket?this.state.product_ticket:[],
            tick_event_id:this.state.ticket_status == true ?tick_data._event_name:null,
        }
       var  product=this.state.getSimpleProductData
        var qty=0;
        cartlist.map(item=>{
            if(product.WPID==item.product_id){
                  qty =  item.quantity;
            }          
            
        })
        qty=qty+ this.state.simpleDefaultQunatity;
        console.log("qty",qty);

         var txtPrdQuantity=document.getElementById("myText").value;

        if( parseInt(txtPrdQuantity) <= 0 )
        {
            alert("Product quantity must be greater than zero!")
            return;
        }

            if(((product.StockStatus==null || product.StockStatus== 'instock') && product.ManagingStock==false) || ( ( product.StockStatus==null || product.StockStatus== 'instock') && 
            product.ManagingStock==true && qty <= product.StockQuantity)) {
                console.log("product1",product);

                    //if($("#txtInScock").val()==="Unlimited" || parseInt($("#txtInScock").val())>0){
                        cartlist.push(data);
                        dispatch(cartProductActions.addtoCartProduct(cartlist));  // this.state.cartproductlist
                        $(".close").trigger("click");
                    // }else
                    // {
                    //     console.log("product123",product);
                    //     $('#outOfStockModal').modal('show')
                    // }
                    
                }else{
                //  alert("stock quantity exceed");
                
                   $('#outOfStockModal').modal('show')

                    // $(".close").trigger("click");
        
                }
         
    }

   

    getAttributeLenght() {
        return this.props.getVariationProductData.ProductAttributes.length;
    }

    componentWillReceiveProps(nextPros){
      //  console.log("SIMPLE",nextPros);

        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []  
        var qty=0;    
        if( cartItemList && cartItemList.length>0)  {
        cartItemList.map(item=>{
            if(nextPros.getSimpleProductData && nextPros.getSimpleProductData.WPID==item.product_id){
                qty=  item.quantity;
            }
        })
    }
    var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''  

           
        if(nextPros.getSimpleProductData!=null){
                  this.setState({
                    getSimpleProductData: nextPros.getSimpleProductData,
                    hasSimpleProductData: true,
                    loadProductAttributeComponent: true,
                    variationOptionclick: 0,
                    simpleTitle: nextPros.getSimpleProductData.Title ? nextPros.getSimpleProductData.Title  : '',
                    simplePrice: nextPros.getSimpleProductData.Price ? nextPros.getSimpleProductData.Price :  '',
                    simpleStockQunatity:
                     (nextPros.getSimpleProductData.StockStatus==null || nextPros.getSimpleProductData.StockStatus== 'instock')  && nextPros.getSimpleProductData.ManagingStock==false
                    ? "Unlimited": (typeof nextPros.getSimpleProductData.StockQuantity!='undefined') && nextPros.getSimpleProductData.StockQuantity!='' ?nextPros.getSimpleProductData.StockQuantity > 0 ? nextPros.getSimpleProductData.StockQuantity-qty:nextPros.getSimpleProductData.StockQuantity:'0',
                    simpleImage:nextPros.getSimpleProductData.ProductImage ? nextPros.getSimpleProductData.ProductImage  : '',
                    simpleDefaultQunatity: 1 ? 1 : nextPros.getSimpleProductData ? nextPros.getSimpleProductData.DefaultQunatity : '',
                    simpleproductid:nextPros.getSimpleProductData?nextPros.getSimpleProductData.WPID:'',
                    simpleisTaxable:nextPros.getSimpleProductData?nextPros.getSimpleProductData.Taxable:'',
                    old_price:nextPros.getSimpleProductData?nextPros.getSimpleProductData.old_price:0,
                    incl_tax:nextPros.getSimpleProductData?nextPros.getSimpleProductData.incl_tax:0,
                    excl_tax:nextPros.getSimpleProductData?nextPros.getSimpleProductData.excl_tax:0,
                    ticket_status:nextPros.getSimpleProductData?nextPros.getSimpleProductData.IsTicket:'',
                    product_ticket: nextPros.getSimpleProductData.IsTicket == true ?ticket_Data:''

        
               });
  
        }
        // else if(nextPros.getSimpleProductData!=null && nextPros.getSimpleProductData.IsTicket == false ){

        //     this.setState({
        //         getSimpleProductData: nextPros.getSimpleProductData,
        //         hasSimpleProductData: true,
        //         loadProductAttributeComponent: true,
        //         variationOptionclick: 0,
        //         simpleTitle: nextPros.getSimpleProductData.Title ? nextPros.getSimpleProductData.Title  : '',
        //         simplePrice: nextPros.getSimpleProductData.Price ? nextPros.getSimpleProductData.Price :  '',
        //         simpleStockQunatity:
        //          (nextPros.getSimpleProductData.StockStatus==null || nextPros.getSimpleProductData.StockStatus== 'instock')  && nextPros.getSimpleProductData.ManagingStock==false
        //         ? "Unlimited": (typeof nextPros.getSimpleProductData.StockQuantity!='undefined') && nextPros.getSimpleProductData.StockQuantity!='' ?nextPros.getSimpleProductData.StockQuantity > 0 ? nextPros.getSimpleProductData.StockQuantity-qty:nextPros.getSimpleProductData.StockQuantity:'0',
        //         simpleImage:nextPros.getSimpleProductData.ProductImage ? nextPros.getSimpleProductData.ProductImage  : '',
        //         simpleDefaultQunatity: 1 ? 1 : nextPros.getSimpleProductData ? nextPros.getSimpleProductData.DefaultQunatity : '',
        //         simpleproductid:nextPros.getSimpleProductData?nextPros.getSimpleProductData.WPID:'',
        //         simpleisTaxable:nextPros.getSimpleProductData?nextPros.getSimpleProductData.Taxable:'',
        //         ticket_status:nextPros.getSimpleProductData?nextPros.getSimpleProductData.IsTicket:''


    
        //    });
      
        //  }
        
    }
  
    handleChange(e) {
        if(e.target.value ==="")
        {
            this.setState({simpleDefaultQunatity:0}); 
        }
        else if( e.target.value  && ! isNaN(e.target.value) && !e.target.value.includes(".")) 
        {
            if(this.state.simpleStockQunatity=="Unlimited" || parseInt(this.state.simpleStockQunatity)>= parseInt(e.target.value))
            {
                this.setState({simpleDefaultQunatity:parseInt(e.target.value)});          
            }
        }
    }
    handleNoChange(e) {
    }

    render() {
      const { getSimpleProductData, hasSimpleProductData } = this.props
    //   var img=this.state.simpleImage.split('/');
    //   console.log("rr12",img)
     var img=this.state.simpleImage ?this.state.simpleImage.split('/'):'';
    // console.log("rr12",hasSimpleProductData ? this.state.simpleDefaultQunatity : '')

    var title= this.state.simpleTitle && this.state.simpleTitle.length>34?this.state.simpleTitle.substring(0,31)+"..." : this.state.simpleTitle
    return (

            <div id="simpleProductPopup" tabIndex="-1" className="modal modal-wide fade full_height_modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                <img src="assets/img/delete-icon.png" />
                            </button>
                            {/* <h4 className="modal-title">{hasVariationProductData ? getVariationProductData.Title : ''}</h4> */}

                            <h4 className="modal-title" title={this.state.simpleTitle}>{hasSimpleProductData ? <Markup content={title}></Markup> : ''}</h4>
                        </div>
                        <div className="modal-body overflowscroll ButtonRadius" id="scroll_mdl_body">
                            <div className="row">
                                <div className="col-md-4 col-xs-4 text-center">
                              
                                    <img   src={hasSimpleProductData ? this.state.simpleImage ? img[8] =='placeholder.png' ?'' :this.state.simpleImage:'': ''} onError={(e)=>{e.target.onerror = null; e.target.src="assets/img/placeholder.png"}}  id="prdImg" style={{ width: '100%', height: 200, borderRadius: 8, marginLeft: 20 }} />
                                </div>
                                <div className="col-md-8 col-xs-8">
                                    <div className="col-md-2 col-xs-2">
                                        <div className="modal_wide_field">
                                            <input type="text" id="myText" value={hasSimpleProductData ? this.state.simpleDefaultQunatity : ''} className="form-control h_eight70 p-0" onChange={this.handleChange.bind(this)} />
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-xs-4">
                                        <div className="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div className="btn-group" role="group">
                                                <a onClick={this.incrementDefaultQuantity} className="btn btn-default bd-r h_eight70 btn-plus">
                                                    <button className="icon icon-plus button"></button>
                                                </a>
                                            </div>
                                            <div className="btn-group" role="group">
                                                <a onClick={this.decrementDefaultQuantity} className="btn btn-default h_eight70 btn-plus bl-0">
                                                    <button className="icon icon-minus button"></button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-xs-6">
                                        <div className="input-group">
                                            <div className="input-group-btn w-50 btn-left">
                                                <button type="button" className="btn btn-default br-0 h_eight70">In Stock</button>
                                            </div>
                                            <div className="modal_wide_field">
                                                <input type="text" id="txtInScock" className="form-control h_eight70 borderRightBottomRadius" value={hasSimpleProductData ? this.state.simpleStockQunatity : 0} onChange={this.handleNoChange.bind(this)} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-xs-12 mt-4">
                                        <div className="input-group">
                                            <div className="input-group-btn w-50 btn-left">
                                                <button type="button" className="btn btn-default bd-r h_eight70">Price</button>
                                            </div>
                                            <div className="modal_wide_field">
                                                <input type="text" className="form-control bl-0 h_eight70 borderRightBottomRadius" value={parseFloat(hasSimpleProductData ? this.state.simplePrice : 0).toFixed(2) } onChange={this.handleNoChange.bind(this)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>

                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-block btn-primary checkout-items buttonProductVariation"    onClick={this.addSimpleProducttoCart.bind(this)}>Add Product</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



function mapStateToProps(state) {
    const { categorylist, productlist,cartproductlist, attributelist } = state;

    return {
        categorylist: categorylist,
        productlist: productlist,
        attributelist: attributelist,
        cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],
    };
}

const connectedSimpleProductPopupModal = connect(mapStateToProps)(SimpleProductPopupModal);
export { connectedSimpleProductPopupModal as SimpleProductPopupModal };