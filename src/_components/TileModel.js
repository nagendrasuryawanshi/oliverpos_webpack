import React from 'react';
import { connect } from 'react-redux';
import { categoriesActions } from '../_actions/categories.action'
import { attributesActions } from '../_actions/allAttributes.action'
import { favouriteListActions } from '../ShopView/action/favouritelist.actions'
import { default as NumberFormat } from 'react-number-format'
import { Markup } from 'interweave';
import { Alreadyexist } from '../_components/Alreadyexist'
import { openDb, deleteDb } from 'idb';
import Config from '../Config'
import { encode_UDid, get_UDid } from '../ALL_localstorage'
import { LoadingModal } from './'
class TileModel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTile: true,
            active: false,
            subattributelist: [],
            subcategorylist: [],
            item: 0,
            filterActive: false,
            parentCategorys: [],
            parentAttributes: [],
            productList: [],
            AllProductList: [],

            chunk_size: Config.key.PPRODUCT_PAGE_SIZE,
            pageNumber: 0,
            totalRecords: 0
        }

        this.props.dispatch(attributesActions.getAll());
        // this.filterProduct = this.filterProduct.bind(this)
        this.getProduct();



    }

    addToFavourite() {
        // $("#setFavoriteBtn").click(function(){ 
        var type = $('input[name=setFavorite]:checked').data('type');
        var id = $('input[name=setFavorite]:checked').data('id');
        var slug = $('input[name=setFavorite]:checked').data('slug');
        //  console.log("type", type, "id", id, "slug", slug);
        //  console.log("favourites",this.props.favourites);  
        //  var favourites=this.props.favourites.items?this.props.favourites.items.split('Fav'):'';
        var isExist = false;
        if (type == "product") {
            this.props.favourites.items.FavProduct.map(prod => {
                if (prod.Product_Id == id) {
                    isExist = true;
                }
               // console.log("favourites12", prod);
            })
        }
        else if (type == "attribute") {
            this.props.favourites.items.FavAttribute.map(attr => {
                if (attr.attribute_id == id) {
                    isExist = true;
                }
                //console.log("favourites12", attr);
            })
        }
        else if (type == "category") {
            this.props.favourites.items.FavCategory.map(cat => {
                if (cat.category_id == id) {
                    isExist = true;
                }
               // console.log("favourites12", cat);
            })
        }
        else if (type == "SubAttribute") {
            this.props.favourites.items.FavSubAttribute.map(suatt => {
                if (suatt.attribute_id == id) {
                    isExist = true;
                }
               // console.log("favourites12", suatt);
            })
        }
        else if (type == "SubCategory") {
            this.props.favourites.items && this.props.favourites.items.FavSubCategory.map(subcat => {
                if (subcat.category_id == id) {

                    isExist = true;
                }
               // console.log("favourites12", subcat);
            })
        }


        if (id && type && isExist == false) {
            this.props.dispatch(favouriteListActions.addToFavourite(type, id, slug));

        }
        else {
            this.setState({
                activeTile: true,
                active: false,
                subcategorylist: [],
                item: 0,
                filterActive: false,
                parentCategorys: [],
                parentAttributes: [],

            })
            $('#alreadyexist').modal('show');

            //   alert("Already Exist");

        }

    }

    handleAddFilterProduct(type, id, slug) {
        console.log("filtype",type,"id",id,"slug",slug,);
        if (id && type) {
            this.props.dispatch(favouriteListActions.addToFavourite(type, id, slug));

        }

    }

    filterProductByTile() {
        let filter = $("#search_field").val();

        var td_index = 0;
        //  console.log("filter",filter);
        var input, table, tr, td, i, txtValue;
        table = document.getElementById("list");
        // console.log("table",table);

        tr = table.getElementsByTagName("tr");
        //     console.log("tr",tr);

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[td_index];
            if (td) {
                if (td_index == 0) {
                    let ftrNmae = tr[i].getElementsByTagName("td")[0];
                    // let ftrSku  = tr[i].getElementsByTagName("td")[6];
                    txtValue = ftrNmae.innerText;
                } else {
                    txtValue = td.textContent || td.innerText;
                }

                if (txtValue.toUpperCase().toString().indexOf(filter.toUpperCase().toString()) > -1 || txtValue.toLowerCase().toString().indexOf(filter.toLowerCase().toString()) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    clearInput() {
        $("#search_field").val(null)
        this.setState({ filterActive: false })

    }

    componentWillMount() {
        this.props.dispatch(categoriesActions.getAll());
    }

    handleIsVariationProduct(type, product) {
        if (type == "simple") {
            this.addSimpleProducttoCart(product);
        } else {
            this.addvariableProducttoCart(product);
        }
    }

    addSimpleProducttoCart(product) {
        const { dispatch, checkout_list, cartproductlist } = this.props;

        var cartlist = cartproductlist ? cartproductlist : [];//this.state.cartproductlist;
        cartlist.push(product)

        dispatch(cartProductActions.addtoCartProduct(cartlist));  // this.state.cartproductlist


    }

    ActiveList(item) {
        this.setState({
            activeTile: false,
            item: item,
            active: false,
        })

    }

    loadSubAttribute(subattributelist, parentAttributes) {
        this.state.activeTile = false;
        this.state.active = true;
        this.state.subattributelist = subattributelist;
        this.state.parentAttributes = parentAttributes;
        this.setState({
            activeTile: false,
            active: true,
            subattributelist: subattributelist,
            parentAttributes: parentAttributes
        })
    }

    loadSubCategory(subcategorylist, parentCategorys) {
        this.state.activeTile = false;
        this.state.active = true;
        this.state.subcategorylist = subcategorylist;
        this.state.parentCategorys = parentCategorys;
        this.setState({
            activeTile: false,
            active: true,
            subcategorylist: subcategorylist,
            parentCategorys: parentCategorys
        })
    }

    loadParentCategory() {

        if (!this.state.parentCategorys || this.state.parentCategorys == null || this.state.parentCategorys.length == 0) {
            this.closeTile();
        }
        else {
            this.loadSubCategory(this.state.parentCategorys);
        }
    }

    loadParentAttribute() {
        if (!this.state.parentAttributes || this.state.parentAttributes == null || this.state.parentAttributes.length == 0) {
            this.closeTile();
        }
        else {
            this.loadSubAttribute(this.state.parentAttributes);
        }
    }

    closeTile() {
        $("#tileback").val(null)

        this.state.activeTile = true,
            this.state.active = false,
            this.state.subattributelist = [],
            this.state.item = 0,
            this.state.filterActive = false,
            this.state.parentAttributes = [],
            this.state.parentCategorys = []

        this.setState({
            activeTile: true,
            active: false,
            subcategorylist: [],
            item: 0,
            filterActive: false,
            parentCategorys: [],
            parentAttributes: [],

        })
    }

    closeModel() {
        $("#tileback").val(null)

        this.state.activeTile = true,
            this.state.active = false,
            this.state.subattributelist = [],
            this.state.item = 0,
            this.state.filterActive = false
        this.setState({
            activeTile: true,
            active: false,
            subcategorylist: [],
            item: 0,
            filterActive: false
        })

        $('.close').trigger('click')
    }

    prodTilefilter(item) {
        this.setState({ filterActive: true })
    }

    getProduct() {
        // var udid= localStorage.getItem('UDID');
        //    let decodedString = localStorage.getItem('UDID');
        //    var decod=  window.atob(decodedString);
        var udid = get_UDid('UDID');
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },
            async set(key, val) {
                const db = await dbPromise;
                const tx = db.transaction(udid, 'readwrite');
                tx.objectStore(udid).put(val, key);
                return tx.complete;
            },
            async delete(key) {
                const db = await dbPromise;
                const tx = db.transaction(udid, 'readwrite');
                tx.objectStore(udid).delete(key);
                return tx.complete;
            },
            async clear() {
                const db = await dbPromise;
                const tx = db.transaction(udid, 'readwrite');
                tx.objectStore(udid).clear();
                return tx.complete;
            },
            async keys() {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).getAllKeys(key);
            },
        };

        var _ProductList = [];
        idbKeyval.get('ProductList').then(val => {
          //  console.log("all product data of fav" , val)
          if( !val || val.length==0 || val==null || val=="") { //do nothing
            }else{
                val.map(item=>{
                    if(item.ParentId == 0){
                        _ProductList.push(item);
                    }
                }) 
            //  console.log("_ProductList" , _ProductList)
                this.setState({ AllProductList: _ProductList });
                this.state.totalRecords = _ProductList.length;
                // this.state.product_Array=val;
                //console.log("logValue",val,this.state.product_Array)
                this.loadingData()
            }
        }
        );
    }

    loadingData() {

        var prdList = [];
        var tilDisplayIndex = this.state.pageNumber * this.state.chunk_size + this.state.chunk_size;
        let AllProductList = this.state.AllProductList.sort(function(a, b) {
            var nameA = a.Title.toUpperCase(); // ignore upper and lowercase
            var nameB = b.Title.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
            return -1;
            }
            if (nameA > nameB) {
            return 1;
            }
        
            // names must be equal
            return 0;
        });

        AllProductList.map((prod, index) => {
            if (index >= 0 && index < tilDisplayIndex)
            {
                var isExpired=false;
                if( prod.IsTicket && prod.IsTicket==true ){
                    var ticketInfo= JSON.parse(prod.TicketInfo);
                   // ticketInfo._ticket_checkin_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)
                    if(ticketInfo._ticket_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)){
                     
                        var dt = new Date(ticketInfo._ticket_availability_to_date);  
                        var expirationDate=  dt.setDate( dt.getDate() +1 );  

                        var currentDate= new Date();// moment.utc(new Date)
                        // console.log("currentDate",currentDate)
                        // console.log("expirationDate", new Date(expirationDate))
                        // console.log("TicketDate",expirationDate,ticketInfo._ticket_availability_to_date)

                        if( currentDate> expirationDate)
                        {
                            isExpired=true;
                        }
                    }
                }
                //------------------------------------------------------------
              if(isExpired==false)
                prdList.push(prod)

             }
               // prdList.push(prod)
        });

        this.state.productList = prdList;
        this.setState({ productList: prdList })

        this.state.pageNumber = this.state.pageNumber + 1;


    }


    render() {
        const { active, activeTile, item, productList } = this.state;
        //const { productlist } = this.props.productlist;
        ///  var prdlst=   localStorage.getItem("Productlist-"+localStorage.getItem('UDID'));


        const productlist = productList;// prdlst ? JSON.parse(prdlst) : [];
        const { attributelist } = this.props.attributelist;
        const { subattributelist } = this.state.subattributelist;
        const { categorylist } = this.props.categorylist;
       // console.log("categorylist",categorylist)
        if (this.props.filteredProduct && this.props.filteredProduct.length) {
            productlist = this.props.filteredProduct;
        }
        // console.log("Add to Tile",this.state)
        return (
            <div id="tallModal" tabIndex="-1" className="modal modal-wide fade full_height_one disabled_popup_close" >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" aria-hidden="true" data-dismiss="modal" onClick={() => this.closeModel()} >
                                <img src="assets/img/delete-icon.png" />
                            </button>
                            <h4 className="modal-title">Add To Tile</h4>
                        </div>
                        <div className="modal-body pl-0 pr-0 pt-0" id="scroll_mdl_body">
                            <div className="all_product">
                                <div className="overflowscroll" id="scroll_mdl_body">
                                    {/* <List /> */}
                                    {this.state.filterActive == false ?
                                        activeTile == true ?
                                            <div id="viewTile">
                                                <div className="clearfix">
                                                    <div className="searchDiv relDiv">
                                                        <input className="form-control nameSearch search_customer_input backbutton bb-0 noshadow pl-5" placeholder="Search Product" type="search" onKeyUp={() => this.prodTilefilter('true')} />
                                                        <svg className="search_customer_input2" style={{ right: 24 }} width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64">
                                                            <g>
                                                                <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <table className="table ShopProductTable">
                                                    <colgroup>
                                                        <col style={{ width: '*' }} />
                                                        <col style={{ width: 40 }} />
                                                    </colgroup>
                                                    <tbody>
                                                        <tr className="pointer" key='1' onClick={() => this.ActiveList(1)}>
                                                            <td>All Items</td>
                                                            <td><a><img src="assets/img/next.png" /></a></td>
                                                        </tr>
                                                        <tr className="pointer" key='2' onClick={() => this.ActiveList(2)}>
                                                            <td>Attributes</td>
                                                            <td><a><img src="assets/img/next.png" /></a></td>
                                                        </tr>
                                                        <tr className="pointer" key='3' onClick={() => this.ActiveList(3)}>
                                                            <td>Categories</td>
                                                            <td><a><img src="assets/img/next.png" /></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            :
                                            (
                                                item == 1 ?  //Product

                                                    <div>
                                                        <div className="clearfix">
                                                            <div className="searchDiv relDiv">
                                                                <input className="form-control nameSearch bb-0 backbutton" id="tileback" value="Back" readOnly={true} data-toggle="modal" onClick={() => this.closeTile()} />  {/* href="#viewTile" */}
                                                            </div>
                                                        </div>
                                                        <table className="table ShopProductTable">
                                                            {/* <colgroup>
                                                                <col style={{ width: '*' }} />
                                                                <col style={{ width: 40 }} />
                                                            </colgroup> */}

                                                            <tbody>
                                                                {(this.state.isLoading && !productlist) ?
                                                                    <tr>
                                                                        <td>  <i className="fa fa-spinner fa-spin">loading...</i></td>
                                                                    </tr>
                                                                    :

                                                                    productlist && productlist.map((item, index) => {
                                                                        //  console.log("produclist2");
                                                                        let isSimpleProduct = (item.Type != "simple") ? true : false;
                                                                        return (
                                                                            <tr key={index} >
                                                                                <td style={divStyle} className="pointer">
                                                                                    <div className="custom_radio custom_radio_set">
                                                                                        <input type="radio" name="setFavorite" id={item.WPID} value={item.WPID} data-type="product" data-slug={item.Type} data-id={item.WPID} />
                                                                                        <label htmlFor={item.WPID} className="pl-3">&nbsp;</label>
                                                                                    </div>
                                                                                </td>
                                                                                <td align="left" className="pl-2">
                                                                                    {item.Title ? <Markup content={item.Title}></Markup> : 'N/A'
                                                                                    }       </td>

                                                                                {/* <td style={divStyle2} > </td>
                                                                                                     */}
                                                                                <td className="text-right" ></td>
                                                                            </tr>

                                                                        )
                                                                    })
                                                                }
                                                            </tbody>
                                                        </table>
                                                        {((!this.state.search) && this.state.totalRecords > this.state.chunk_size * this.state.pageNumber) ?
                                                            <div className="createnewcustomer">
                                                                <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" id='hideButton' onClick={() => this.loadingData()}>Load More</button>
                                                            </div>
                                                            :
                                                            <div></div>
                                                        }
                                                    </div>
                                                    : item == 2 ?  //Attribute
                                                        <div>
                                                            <div className="clearfix">
                                                                <div className="searchDiv relDiv">
                                                                    <input className="form-control nameSearch bb-0 backbutton" id="tileback" value="Back" readOnly={true} data-toggle="modal" onClick={() => this.loadParentAttribute()} />  {/* href="#viewTile" */}
                                                                </div>
                                                            </div>
                                                            {
                                                                this.state.active != true ?
                                                                    <div >
                                                                        {!attributelist || attributelist.length == 0 ? <LoadingModal /> : ''}

                                                                        <div>
                                                                            <table className="table ShopProductTable">

                                                                                <tbody>
                                                                                    {(this.state.isLoading && !attributelist) ?
                                                                                        <tr  >
                                                                                            <td>  <i className="fa fa-spinner fa-spin">loading...</i></td>
                                                                                        </tr>
                                                                                        :
                                                                                        (attributelist.map((item, index) => {
                                                                                            // console.log("attributelist",item);
                                                                                            return (
                                                                                                <tr key={index} className="pointer">
                                                                                                    <td style={divStyle}>
                                                                                                        <div className="custom_radio custom_radio_set">
                                                                                                            <input type="radio" name="setFavorite" id={item.Id} value={item.Id} data-slug={item.Value} data-type="attribute" data-id={item.Id} />
                                                                                                            <label htmlFor={item.Id} className="pl-3">&nbsp;</label>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                    <td align="left" className="pl-2" onClick={() => this.loadSubAttribute(item.SubAttributes, attributelist)}>
                                                                                                        <Markup content={item.Value}></Markup>    </td>

                                                                                                    <td style={divStyle2} > </td>
                                                                                                    <td className="text-right" onClick={() => this.loadSubAttribute(item.SubAttributes, attributelist)}><a> <img src="assets/img/next.png" /></a></td>

                                                                                                </tr>

                                                                                            )
                                                                                        })
                                                                                        )
                                                                                    }
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    : //<SubAttributes subattributelist={subattributelist}/>
                                                                    <div>
                                                                        <table className="table ShopProductTable">
                                                                            <tbody>
                                                                                {
                                                                                    (this.state.subattributelist && this.state.subattributelist.length > 0 ? this.state.subattributelist.map((item, index) => {
                                                                                        return (
                                                                                            <tr key={index} className="pointer">

                                                                                                <td style={divStyle}>

                                                                                                    <div className="custom_radio custom_radio_set">
                                                                                                        <input type="radio" name="setFavorite" id={item.Id} value={item.Id} data-slug={item.Value} data-type="SubAttribute" data-id={item.Id} />
                                                                                                        <label htmlFor={item.Id} className="pl-3">&nbsp;</label>
                                                                                                    </div>
                                                                                                </td>

                                                                                                {item.SubAttributes ?
                                                                                                    <td align="left" className="pl-2" onClick={() => this.loadSubAttribute(item.SubAttributes, this.state.subattributelist)}> <Markup content={item.Value}></Markup>     </td> :
                                                                                                    <td align="left" className="pl-2"> <Markup content={item.Value}></Markup> </td>
                                                                                                }

                                                                                                {item.SubAttributes ?
                                                                                                    <td style={divStyle2} onClick={() => this.loadSubAttribute(item.SubAttributes, this.state.subattributelist)} ><a> <img src="assets/img/next.png" /></a> </td>
                                                                                                    : <td className="text-right">
                                                                                                        <div className="custom_radio">
                                                                                                            <input type="radio" name="setFavorite" id={item.Id} value={item.Id} data-slug={item.Value} data-type="SubAttribute" data-id={item.Id} />
                                                                                                            <label htmlFor={item.Id} className="pl-3">&nbsp;</label>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                }

                                                                                            </tr>
                                                                                        )
                                                                                    })
                                                                                        :
                                                                                        <tr data-toggle="modal"><td>No Data Found</td></tr>)

                                                                                }
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                            }
                                                        </div>
                                                        : item == 3 ?
                                                            // <Categories />
                                                            <div>
                                                                <div className="clearfix">
                                                                    <div className="searchDiv relDiv">
                                                                        <input className="form-control nameSearch bb-0 backbutton" id="tileback" value="Back" type="search" readOnly={true} data-toggle="modal" onClick={() => this.loadParentCategory()} />  {/* href="#viewTile" */}
                                                                    </div>
                                                                </div>
                                                                {active != true ?
                                                                    <table className="table ShopProductTable ">
                                                                        <tbody>
                                                                            {categorylist && categorylist.map((item, index) => {
                                                                                return (

                                                                                    <tr key={index} >
                                                                                        <td align="left" className="p-0" style={divStyle}>
                                                                                            <div className="custom_radio custom_radio_set">
                                                                                                <input type="radio" name="setFavorite" id={item.id} value={item.id} data-slug={item.Value ? item.Value : 'N/A'} data-type="category" data-id={item.id} />
                                                                                                <label htmlFor={item.id} className="pl-3">&nbsp;</label>
                                                                                            </div>
                                                                                        </td>

                                                                                        {item.Subcategories && item.Subcategories.length > 0 ?
                                                                                            <td align="left" className="pl-2 pointer" onClick={() => this.loadSubCategory(item.Subcategories, categorylist)}> <Markup content={item.Value}></Markup>   </td>
                                                                                            :
                                                                                            <td align="left" className="pl-2"> <Markup content={item.Value}></Markup></td>
                                                                                        }

                                                                                        {item.Subcategories && item.Subcategories.length > 0 ?
                                                                                            <td className="p-0 pointer" style={divStyle2} onClick={() => this.loadSubCategory(item.Subcategories, categorylist)} ><a> <img src="assets/img/next.png" /></a> </td>
                                                                                            : <td className="text-right"></td>
                                                                                        }

                                                                                    </tr>

                                                                                )
                                                                            })

                                                                            }
                                                                        </tbody>
                                                                    </table>
                                                                    :
                                                                    <div>
                                                                        <table className="table ShopProductTable">

                                                                            <tbody>
                                                                                {
                                                                                    (this.state.subcategorylist && this.state.subcategorylist.length > 0 ? this.state.subcategorylist.map((item, index) => {
                                                                                       // console.log("item Subcategories", item);
                                                                                        return (
                                                                                            <tr key={index} >
                                                                                                <td align="left" className="p-0" style={divStyle}>
                                                                                                    <div className="custom_radio custom_radio_set">
                                                                                                        <input type="radio" name="setFavorite" id={item.id} value={item.id} data-slug={item.Value} data-type="SubCategory" data-id={item.id} />
                                                                                                        <label htmlFor={item.id} className="pl-3">&nbsp;</label>
                                                                                                    </div>

                                                                                                </td>

                                                                                                {item.Subcategories && item.Subcategories.length > 0 ?
                                                                                                    <td align="left" className="pl-2 pointer" onClick={() => this.loadSubCategory(item.Subcategories, this.state.subcategorylist)}>  {item.Value}      </td> :
                                                                                                    <td align="left" className="pl-2"> {item.Value} </td>
                                                                                                }

                                                                                                {item.Subcategories && item.Subcategories.length > 0 ?
                                                                                                    <td className="p-0 pointer" style={divStyle2} onClick={() => this.loadSubCategory(item.Subcategories, this.state.subcategorylist)} ><a> <img src="assets/img/next.png" /></a> </td>
                                                                                                    : <td className="text-right">
                                                                                                        <div className="custom_radio">
                                                                                                            <input type="radio" name="setFavorite" id={item.id} value={item.id} data-slug={item.Value} data-type="SubCategory" data-id={item.id} />
                                                                                                            <label htmlFor={item.id} className="pl-3">&nbsp;</label>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                }
                                                                                            </tr>
                                                                                        )
                                                                                    })
                                                                                        :
                                                                                        <tr data-toggle="modal"><td>No Data Found</td></tr>)
                                                                                }
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                }
                                                            </div>
                                                            : null
                                            )
                                        : <div id="viewTile">
                                            <div className="clearfix" >
                                                <div className="searchDiv relDiv" >
                                                    <input type="text" id="search_field" name="search" className="form-control nameSearch search_customer_input backbutton bb-0 noshadow pl-5" onChange={() => this.filterProductByTile()} placeholder="Search Product" />
                                                    <svg onClick={() => this.clearInput()} className="search_customer_input2" style={{ right: 24 }} width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64">
                                                        <g>
                                                            <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                        </g>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div>

                                                <table className="table ShopProductTable" id="list">
                                                    <colgroup>
                                                        <col style={{ width: '*' }} />
                                                        <col style={{ width: 40 }} />
                                                    </colgroup>

                                                    <tbody>
                                                        {(this.state.isLoading && !productlist) ?
                                                            <tr>
                                                                <td>  <i className="fa fa-spinner fa-spin">loading...</i></td>
                                                            </tr>
                                                            :
                                                            productlist && productlist.map((item, index) => {
                                                                //   console.log("productlist", item);
                                                                let isSimpleProduct = (item.Type == "variable") ? true : false;
                                                                return (
                                                                    <tr key={index} >
                                                                        <td align="left" className="pl-3" >{item.Title ? <Markup content={item.Title}></Markup> : 'N/A'}
                                                                            {/* <label htmlFor={item.WPID} className="pl-3">{item.Title ? item.Title : 'N/A'}</label> */}
                                                                        </td>
                                                                        <td className="text-right"><NumberFormat value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                                        </td>

                                                                        <td className="text-right pointer" style={{ padding: 20 }} onClick={() => this.handleAddFilterProduct('product', item.WPID, item.Type)} >
                                                                            <a>
                                                                                <img src="assets/img/add_29.png" />
                                                                            </a>
                                                                        </td>
                                                                        {/* onClick={this.handleAddFilterProduct.bind(item.WPID, item.Type, 'product')} */}

                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    }
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-primary btn-block h66" onClick={() => this.addToFavourite()}>SAVE & UPDATE</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const divStyle = {
    width: "56px"
};

const divStyle2 = {
    width: "14px"
};

function mapStateToProps(state) {
    const { categorylist, productlist, attributelist, favourites } = state;

    return {
        categorylist: categorylist,
        productlist: productlist,
        attributelist: attributelist,
        favourites: favourites
    };
}

const connectedList = connect(mapStateToProps)(TileModel);
export { connectedList as TileModel };