import React from 'react';
import { connect } from 'react-redux';
import { attributesActions } from '../_actions/allAttributes.action'
import { history } from '../_helpers';
import { LoadingModal } from '.';
import { SubCategories, SubAttributes } from './SubAttributes'

class Attributes extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(attributesActions.refresh());
        this.state = {
            active: false,
            isLoading: true,
            subattributelist: [],
            //attributelist:[],
            isLoading: true,
            loading:false
        }

        const { dispatch } = this.props;
        dispatch(attributesActions.getAll());
    }


    loadSubAttribute(item) {
        this.setState({
            active: true,
            subattributelist: item
        })


    }

    componentWillMount() {
        // setTimeout(() => {
            this.setState({
                loading: true
          })
          
        // }, 500);
     
        $(".overflowscroll ").mCustomScrollbar("update");
    }
    componentDidMount(){
        setTimeout(function () {
            setHeightDesktop();
        }, 500);
    }
    Back() {
        history.go();
    }
  

    render() {
        const { active, subattributelist } = this.state;

        return (
            <div>

                {active != true ?
                    <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                        { this.state.loading == false ? !this.props.attributelist || this.props.attributelist.length == 0 ? <LoadingModal /> : '':''}
                        <div className="items pt-3">
                            <div className="item-heading text-center">Library</div>
                            <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight" style={{ height: 300 }} >
                                <div className="searchDiv" style={{ display: 'none' }}>
                                    <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan" />
                                </div>

                                <div className="pl-1 pr-4 previews_setting  pointer">
                                    <a onClick={() => this.Back()} className="back-button " id="mainBack" >
                                        <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                        <span>Back</span>
                                    </a>
                                </div>

                                <table className="table ShopProductTable">
                                    <colgroup>
                                        <col style={{ width: '*' }} />
                                        <col style={{ width: 40 }} />
                                    </colgroup>

                                    <tbody>
                                        {(this.state.isLoading && !this.props.attributelist) ?
                                            <tr  >
                                                <td>  <i className="fa fa-spinner fa-spin">loading...</i></td>
                                            </tr>
                                            :

                                            (this.props.attributelist.map((item, index) => {
                                                console.log("attributelist",item);
                                                return (
                                                    item.SubAttributes && item.SubAttributes.length > 0 ?
                                                        <tr  className="pointer" key={index} onClick={() => this.loadSubAttribute(item)}>
                                                            <td>{item.Value}</td>
                                                            <td ><a><img src="assets/img/next.png" className="pointer" /> </a></td>
                                                        </tr>
                                                        :
                                                        <tr key={index} >
                                                            <td>{item.Value}</td>
                                                            <td ></td>
                                                        </tr>
                                                )
                                            })
                                            )


                                        }

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    : <SubAttributes subattributelist={subattributelist} productData={this.props.productData} />
                }
            </div>

        )
    }

}


function mapStateToProps(state) {
    const { attributelist } = state.attributelist;

    return {
        attributelist
    };
}

const connectedList = connect(mapStateToProps)(Attributes);
export { connectedList as Attributes };

