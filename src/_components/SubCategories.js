import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../_actions/cartProduct.action'
import { Categories } from './Categories';
import { Markup } from 'interweave';
import { default as NumberFormat } from 'react-number-format'
import { openDb, deleteDb } from 'idb';
import {encode_UDid,get_UDid} from '../ALL_localstorage'
import { getTaxAllProduct } from './'
import { allProductActions } from '../_actions/allProduct.action'
class SubCategories extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            subcategorylist: this.props.subcategorylist,
            productlist:[],// JSON.parse(  localStorage.getItem("Productlist-"+localStorage.getItem('UDID') )), //JSON.parse(localStorage.getItem('Productlist')),
            Product_category:[],
            active:false,
            parentCategories:[],
            backToCategories:false,
            ticket_Product_status:false
        }
      ///  console.log("localstorage",this.state.subcategorylist);
        

        var prdList = [];
        this.state.subcategorylist && this.state.subcategorylist.map((prod, index) => {
           // check tickera product is expired?--------------------------
           var isExpired=false;
           if( prod.IsTicket && prod.IsTicket==true ){
               var ticketInfo= JSON.parse(prod.TicketInfo);
              // ticketInfo._ticket_checkin_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)
               if(ticketInfo._ticket_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)){
                
                   var dt = new Date(ticketInfo._ticket_availability_to_date);  
                   var expirationDate=  dt.setDate( dt.getDate() +1 );  

                   var currentDate= new Date();// moment.utc(new Date)
                   // console.log("currentDate",currentDate)
                   // console.log("expirationDate", new Date(expirationDate))
                   // console.log("TicketDate",expirationDate,ticketInfo._ticket_availability_to_date)

                   if( currentDate> expirationDate)
                   {
                       isExpired=true;
                   }
               }
           }


              if(isExpired==false){
                prdList.push(prod)
            }
        })
        if (prdList.length > 0) {
            this.state.subcategorylist = prdList;
            this.setState({ subcategorylist: prdList })
        }
       //--------------------------------------------------------------
    //    let decodedString = localStorage.getItem('UDID');
    //    var decod=  window.atob(decodedString);
       var udid= get_UDid('UDID');
       const dbPromise = openDb('ProductDB', 1, upgradeDB => {
           upgradeDB.createObjectStore(udid);
         });

         const idbKeyval = {
           async get(key) {
             const db = await dbPromise;
             return db.transaction(udid).objectStore(udid).get(key);
           },
           
         };
       
         idbKeyval.get('ProductList').then(val => 
           {
            this.setState({ productlist: getTaxAllProduct(val) });             
           }
           ); 
         //--------------------------------------------------------------
    }
    loadSubCategory(subcatlist,parentCategories) {
       
        this.setState({
            active: true,
            parentCategories:parentCategories            
        })
        this.state.Product_category.push(subcatlist)

    }
        loadProdCategory(item,parentCategories){
          
            this.setState({
                active: false,
                parentCategories:parentCategories
            })

         var  Code=item.Code
      
          var  catg_Code = Code?Code.toString().replace(/-/g, " "):Code;
                    this.setState({
                        active: true                     
                    })
                    this.state.productlist && this.state.productlist.map(prod => {
                prod.Categories.split(',').map(categ =>{
                  var  prod_Code =categ? categ.toString().replace(/-/g, " ") :categ; 

                if(prod_Code.toUpperCase() == catg_Code.toUpperCase() || prod_Code.toLowerCase() == catg_Code.toLowerCase()  ){

                    var variationProdect=  this.state.productlist.filter(item=>{ 
                        return  (item.ParentId===prod.WPID && ( item.ManagingStock ==false ||( item.ManagingStock ==true && item.StockQuantity>-1)))    
                      })        
                    
                    prod['Variations'] = variationProdect

                    this.state.Product_category.push(prod)
                 
                   }   

                })
          
            })

            
        }

        getTicketFields(product,tick_type=null) {
           // console.log("getTicketFields",product);
            var tick_data= JSON.parse(product.TicketInfo)
            //var  udid=4040246357
            //console.log("first condition1",tick_data);

            var   form_id=tick_data._owner_form_template
          //  console.log("first condition",form_id);
    
           this.props.dispatch(allProductActions.ticketFormList(form_id));
            this.state.ticket_Product_status=true
           this.state.tick_type=tick_type
           this.state.ticket_Product=product
           this.setState({
               ticket_Product:product,
               ticket_Product_status:true,
    
              // tick_type:'simpleadd'
             })
    
           // }
        }     
    ActiveList(item,parentCategories,ticketFields=null){
      
        this.setState({
            parentCategories:parentCategories
        })
       // console.log("ActiveList",item);
        var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
    
        //  var cartlist=cartproductlist?cartproductlist:[];//this.state.cartproductlist;    
        if (item.Type !== "variable") {
            //for inclusive and axclusive tax------------------
            var arr=[]
            
            arr.push(item);
            arr= getTaxAllProduct(arr) ;              
            item=arr[0];
            //ticketFields==null && 
            if ( ticketFields==null &&  item  && item.IsTicket == true ) {
              //  console.log("first condition");
                  var tick_typ='simpleadd' 
                 
                this.getTicketFields(item,tick_typ);

             }

            //-------------------------------------------

            if (item.InStock == true &&  item.IsTicket == false ) {
                var data = {
                    Price: parseFloat(item.Price),
                    Title: item.Title,
                    line_item_id: 0,
                    quantity:1,                   
                    product_id: item.WPID,
                    variation_id: 0,
                    isTaxable: true,

                    after_discount: item.after_discount,
                    cart_after_discount: item.cart_after_discount,
                    cart_discount_amount: item.cart_discount_amount,
                    discount_amount: item.discount_amount,
                    excl_tax: item.excl_tax,
                    incl_tax: item.incl_tax,  
                    old_price: item.old_price,
                    product_after_discount: item.product_after_discount,
                    product_discount_amount: item.product_discount_amount,
                    ticket_status: item.ticket_status,
                   
                }
    //  console.log("ProductData",data)

                localStorage.setItem('Favproduct', null)             
                var  product=item
                 var qty=0;
                 cartlist.map(item=>{
                     if(product.WPID==item.product_id){
                         qty=  item.quantity;
                      }                   
                     
                 })


              if( (product.StockStatus==null || product.StockStatus== 'instock') &&  (product.ManagingStock==false || (product.ManagingStock==true && qty<product.StockQuantity)) ){
                cartlist.push(data)
                this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));
         
                 }else{
                       //alert("stock quantity exceed");
                       $('#outOfStockModal').modal('show')

              }
          } else if(item.InStock == true &&  item.IsTicket == true && ticketFields!=null){
            var tick_data=item && item.TicketInfo != ''?JSON.parse(item.TicketInfo):'';
               //  console.log("true condition")
            var data = {
                Price: parseFloat(item.Price),
                Title: item.Title,
                line_item_id: 0,
                quantity:1,                   
                product_id: item.WPID,
                variation_id: 0,
                isTaxable: true,

                after_discount: item.after_discount,
                cart_after_discount: item.cart_after_discount,
                cart_discount_amount: item.cart_discount_amount,
                discount_amount: item.discount_amount,
                excl_tax: item.excl_tax,
                incl_tax: item.incl_tax,  
                old_price: item.old_price,
                product_after_discount: item.product_after_discount,
                product_discount_amount: item.product_discount_amount,
                ticket_status: item.IsTicket,
                tick_event_id:tick_data._event_name,
                product_ticket: ticketFields,
               
            }
         //console.log("ProductData",data,"product",product)

            localStorage.setItem('Favproduct', null)             
            var  product=item
             var qty=0;
             cartlist.map(item=>{
                 if(product.WPID==item.product_id){
                     qty=  item.quantity;
                  }                   
                 
             })
             //console.log("ProductData11",product)


           if( (product.StockStatus==null || product.StockStatus== 'instock') &&  (product.ManagingStock==false || (product.ManagingStock==true && qty<product.StockQuantity)) ){
          //   console.log("ProductData12",data)

            cartlist.push(data)
            this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));

             }else{

                   //alert("stock quantity exceed");
                   $('#outOfStockModal').modal('show')

             }
          } else if(item.InStock == false) {
           // console.log("else part1")

                $('#outOfStockModal').modal('show'); 
               
            }
        }
       
          if(item.Type == "variable"){

            if( item  && item.IsTicket == true ) {
                console.log("first condition1");         
                this.getTicketFields(item);
            }
            if(item.InStock == true) {
               var product= item
                this.props.productData(product);
                $('#VariationPopUp').modal('show');
    
             } else {
    
              $('#outOfStockModal').modal('show'); 
               
              }
          }
        
    
    }
    Back() {
       
        this.setState({Product_category:[]});
        if(! this.state.parentCategories || this.state.parentCategories==null ||this.state.parentCategories.length==0)     
        {    
            this.setState({backToCategories:true})        ;
            //history.go();          
        }
        else{
            this.setState({active:false,  parentCategories:null});
        
            this.state.Product_category.push(this.state.parentCategories)
        }            
      
    }
    componentDidMount(){
        setTimeout(function () {
            setHeightDesktop();
        }, 500);
    }
    componentWillReceiveProps(props) {
  
        console.log("propsa",props);
        //  console.log("ticketfieldListfavourite",localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'',"this.state.ticket_Product_status",this.state.ticket_Product_status,"props464664644646",props);
     
         var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''
         var tick_data=this.state.ticket_Product_status == true? JSON.parse(this.state.ticket_Product.TicketInfo):''
         var   form_id=tick_data._owner_form_template
       //  console.log("call simple1" ,form_id)
     
     if(localStorage.getItem('ticket_list') && localStorage.getItem('ticket_list') !=='null' && localStorage.getItem('ticket_list') !== '' && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd' || form_id == -1 || form_id == '' && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd'   ){  
        console.log("call simple")
        this.setState({ticket_Product_status:false})
     
      //  var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''
          // console.log("this.state.ticket_Product_status",this.state.ticket_Product_status,ticket_Data)
          var parentCategories=null;
        this.ActiveList(this.state.ticket_Product,parentCategories,localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'')
        }
   
         
          
     }  
    render() {
        const { subcategorylist } = this.state;
        console.log("subcategorylist",subcategorylist)
        return (
            <div>
                { this.state.backToCategories==false?
                   
            <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                <div className="items pt-3">
                    <div className="item-heading text-center">Library</div>
                    <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                        <div className="searchDiv" style={{ display: 'none' }}>
                            <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan" />
                        </div>

                        {/* <div className="pl-4 pr-4 previews_setting">
                            <a href="/listview" className="back-button " id="mainBack">
                                <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                <span>Back</span>
                            </a>
                        </div> */}
                        <div className="pl-1 pr-4 previews_setting  pointer">
                            <a onClick={() => this.Back()} className="back-button " id="mainBack">
                                <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                <span>Back</span>
                            </a>
                        </div>
                        <table className="table ShopProductTable  table-striped table-hover table-borderless paddignI mb-0 font">
                            {/* <colgroup>
                                             <col style={{width:'*'}}/>
                                                 <col style={{width:40}}/>
                                         </colgroup> */}

                            <tbody>

                                { this.state.active == false ? (

                                    (subcategorylist && subcategorylist.length > 0 ? subcategorylist.map((item, index) => {
                                        console.log("category",item)
                                    return (
                                        item.Subcategories && item.Subcategories.length > 0 ?
                                            <tr  className="pointer" key={index} data-toggle="modal" onClick={() => this.loadSubCategory(item.Subcategories,subcategorylist)}>
                                                <td>{item.Value ? item.Value : 'N/A'}</td>
                                                <td className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                            </tr>
                                            :
                                            <tr key={index} data-toggle="modal" className="pointer" onClick={() => (item.Price || item.Price==0)?"": this.loadProdCategory(item,subcategorylist)}>
                                                <td>{item.Value ? <Markup content={item.Value} /> : item.Title?<Markup content={item.Title} />:'N/A'}</td>
                                                <td className="text-right">
                                              
                                                <NumberFormat value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                </td>
                                                { item.Price || item.Price==0? 
                                                    <td className="text-right" data-toggle="" href="javascript:void(0)"><a><img src="assets/img/add_29.png" className="pointer"
                                                    onClick={() => this.ActiveList(item)} /></a></td>  
                                                    :  <td className="text-right"><a> <img src="assets/img/next.png"  /></a></td>
                                                   }                                              
                                              
                                            </tr>
                                    )
                                })
                                    : <tr data-toggle="modal"><td style={{textAlign:'center'}}>No Data Found</td></tr>)
                                 ): 
                                 this.state.Product_category && this.state.Product_category.length > 0 ? this.state.Product_category.map((item, index) => {
                                    return (

                                        <tr key={index} data-toggle="modal">

                                            <td>{item.Value ?<Markup content={item.Value} /> : item.Title?<Markup content={item.Title} />:'N/A'}</td>
                                            <td className="text-right">
                                          
                                            <NumberFormat value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </td>
                                            {/* {item.Price ? */}
                                                    <td className="text-right" data-toggle="" href="javascript:void(0)"><a><img src="assets/img/add_29.png" className="pointer"
                                                    onClick={() => this.ActiveList(item)} /></a></td>  
                                                    {/* :  <td className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                   } */}
                                        </tr>
                                    )

                                })
                                    :
                                    <tr data-toggle="modal"><td style={{textAlign:'center'}}>No Data Found</td></tr>}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            :  <Categories productData={this.props.productData} onRef={this.props.onRef}/>
            }
            </div>
        )
    }

}

// export  default SubCategories;

function mapStateToProps(state) {
    const { categorylist,ticketfield } = state;

    
console.log("categorylist",categorylist,"ticketfield123",ticketfield);
    return {
        categorylist:categorylist,
        ticketfield:ticketfield
    };
}

const connectedList = connect(mapStateToProps)(SubCategories);
export { connectedList as SubCategories };