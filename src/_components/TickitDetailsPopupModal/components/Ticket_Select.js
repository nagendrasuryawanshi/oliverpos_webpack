import React from 'react';
import { connect } from 'react-redux'


class Ticket_Select extends  React.Component {
    constructor(props) {
        super(props);
        this.state = name
        this.state ={
        Tic_Select:[],
         }
        
        this.handleChange = this.handleChange.bind(this);
     //   this.onChange = this.onChange.bind(this);
     this.state.name=this.props.displayvalue
     this.setState({[name]:this.props.displayvalue})
    }


    handleChange(e){
        const { value , name } = e.target;
        console.log("selectedValue" , value , this.props.indexing)
        this.setState({name:value})
        if(this.props.indexing){
            var index = this.props.indexing - 1
            this.props.TickSelect(name,value,index)
      
        
        }
  }

    componentDidMount() {
        var select =this.props.tick_content.split(',');

       // console.log("Checkbox",select);

        this.setState({Tic_Select:select 
        })

    }


    render() {
   //     console.log("this.props.tick_content",this.props.tick_content);
        const { name,required } = this.props;
        console.log(" name,required", name,required ,this.state.name)
        return (
            <div className={'form-group' + ( (!this.state.name || this.state.name ==""  ) && (required && required==true) ? ' has-error' : '')}>    
               <div className="input-group">
                                 <div className="input-group-addon">
                                      {this.props.tick_title}
                                    </div>
                                     <select className="form-control"  name={name} onChange={this.handleChange}
                                       // placeholder="Select"
                                       id={this.props.id}
                                        value={this.state.name?this.state.name :this.props.displayvalue}
                                      >        
                                        <option value=''>select</option>  
                                        {this.state.Tic_Select.map((item,index) =>
                                         {
                                            // console.log("item",item.name);
                                            return (
                                            <option key={index} value={item}>
                                                {item}
                                             </option>
                                         )})}
                                         {/* <option value="Stading" >Stading</option>
                                         <option value="Shitting">Shitting</option>
                                        <option value="1 Year or 6 Month below">1 Year or 6 Month below</option> */}
                                    </select>
                                
                {/* <label>{ ( (!this.state.name || this.state.name =="") && (required && required==true))? "field required":""}</label> */}
                                 </div>
                                 {  ( (!this.state.name || this.state.name =="" ) && (required && required==true)) &&
                                    <div className="help-block">required</div>
                                    } 
                              </div>   
        )
}
}
function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedTicket_Select = connect(mapStateToProps)(Ticket_Select);
export { connectedTicket_Select as Ticket_Select };