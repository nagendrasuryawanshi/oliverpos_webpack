import React from 'react';
import { connect } from 'react-redux';
import { Ticket_Checkbox } from '../../TickitDetailsPopupModal/components/Ticket_Checkbox'
import { Ticket_Select } from '../../TickitDetailsPopupModal/components/Ticket_Select'
import { Ticket_Textarea } from '../../TickitDetailsPopupModal/components/Ticket_Textarea'
import { Ticket_radio } from '../../TickitDetailsPopupModal/components/Ticket_radio'
import { Ticket_Input } from '../../TickitDetailsPopupModal/components/Ticket_Input';
import { Ticket_Firstname } from '../../TickitDetailsPopupModal/components/Ticket_Firstname'
class Tickit_Create extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: [],
            
            
        }

        this.handleChange = this.handleChange.bind(this);

    }
   

    InputClick(name, inp_text, index) {
      //   console.log("tickitCreate","index", index, "name", name,"inp_text",inp_text);
        var rename = name.split('?')[0]
        var str = rename.replace(/-|\s/g, '_')
        //console.log("str",str);
        this.props.InputClick(index, str, inp_text)
    }


    addTickettoCart() {

        this.props.status = false


    }
    handleChange(e) {
        const { value, name } = e.target;
        //    console.log("full_name", value, name)

        this.setState({ [name]: value })
        if (this.props.indexing) {
            var index = this.props.indexing - 1
            var rename = name.split('?')[0]

            this.props.InputClick(index, rename, value)
        }
    }
 getfieldValue(fieldindex,name)

 {
    
var  staic_name
    //  console.log("fieldindex",name,fieldindex)
    if(this.props.ticket_View){
        this.props.ticket_View &&  this.props.ticket_View.map((itmview , index)=>{  
            if(fieldindex==index)
            staic_name=itmview[name]
           // console.log("display_name",staic_name);

                return staic_name
        }) 
    }else{
        return "";
    }
  
 }
    render() {
        const { Item } = this.props;
        // console.log("item123456", Item,this.props);
         console.log(" this.props.ticket_View" ,  this.props.ticket_View);
        var FistName='';
        var LastName='';
        var OwnerEmail='';
        if(this.props.ticket_View){         

           var ticketownerInfo= this.props.ticket_View[this.props.indexing -1] ;
            if(ticketownerInfo)
            {
                console.log("ticketownerInfo.first_name",ticketownerInfo.first_name,"ticketownerInfo.last_name",ticketownerInfo.last_name)
                 FistName=ticketownerInfo.first_name;
                 LastName=ticketownerInfo.last_name;
                 OwnerEmail=ticketownerInfo.owner_email;


            }
       
         }

        return (
                        <div className="panel">
                            <div className="panel-heading" role="tab" id="headingOne">
                                <h4 className="panel-title ticket-number after-border-remove" data-toggle="collapse" data-parent="#accordion" href={"#tickit-" + this.props.indexing} aria-expanded="true" aria-controls={"#tickit-" + this.props.indexing}>
                                    <a role="button">
                                        <i className="tickira-less glyphicon glyphicon-minus"></i>
                                        Ticket{this.props.indexing}
                                            </a>
                                </h4>
                            </div>
                            <div id={"tickit-" + this.props.indexing} className={this.props.indexing == 1?"panel-collapse collapse in":"panel-collapse collapse"} role="tabpanel" aria-labelledby="headingOne">
                                <div className="panel-body p-0">
                                    <div className="tickt-list clearfix">

                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <Ticket_Firstname   indexing={this.props.indexing} name={"first_name?" + this.props.indexing} tick_title={"First Name"} TickInput={(name, text,index) => this.InputClick(name, text,index)} placeholder={"First Name"} 
                                         EditValue ={FistName?FistName:''}
                                        />
                                         
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <Ticket_Input   indexing={this.props.indexing} name={"last_name?" + this.props.indexing} tick_title={"Last Name"} TickInput={(name, text,index) => this.InputClick(name, text,index)}
                                       displayvalue={LastName?LastName:''}
                                      placeholder={"Last Name"}/>
                                          
                                        </div>
                                        {/* displayvalue ={OwnerEmail} */}
                                        <div className="col-sm-12">
                                        <Ticket_Input   indexing={this.props.indexing} name={"owner_email?" + this.props.indexing} tick_title={"Email"} TickInput={(name, text,index) => this.InputClick(name, text,index)} displayvalue ={OwnerEmail?OwnerEmail:'' } placeholder={"Email"} />
                                         
                                        </div>
                                        {Item && Item.map((item, index) => {
                                              //   console.log("before map loop",parseInt(this.props.indexing -1));

                            var field_info= JSON.parse(item.field_info,"this.props.indexing",this.props.indexing)
                           var display_name=''
                         //  console.log("before if loop",parseInt(this.props.indexing -1));
                                      
                            if(this.props.ticket_View){
                               // console.log("display_name",this.props.indexing -1,field_info);

                                this.props.ticket_View &&  this.props.ticket_View.map((itmview , index)=>{  
                                    //console.log("itmview",itmview,field_info.title);
                                  //  console.log("after if loop", "this.props.indexing:", parseInt(this.props.indexing -1),"index",index);
                                  
                                    if(parseInt(this.props.indexing -1) == index){
                                        //  console.log("enter new index", "this.props.indexing:",parseInt(this.props.indexing -1),"index:",index);
                                    var rename = field_info.title?field_info.title.split('?')[0]:''
                                  //  console.log("rename",rename);
                                    var str = rename?rename.replace(/-|\s/g, '_'):''
                                    display_name=itmview[str]
                                //    console.log("display_name1",display_name);

                                    }
                                   
                                }) 
 
                            }
                          // console.log("display_name",display_name);
 
                            return (
                                <div className="col-sm-12" key={index}>
                                    {field_info.field_type == "input" ?
                                        <Ticket_Input   id={this.props.id} required={field_info.is_required?field_info.is_required:''} indexing={this.props.indexing} name={field_info.title?field_info.title+"?" + this.props.indexing:field_info.field_type+"?" + this.props.indexing} tick_content={field_info.content ? field_info.content : ''} tick_title={field_info.title ? field_info.title : ''} TickInput={(name, text,index) => this.InputClick(name, text,index)} placeholder={field_info.placeholder ? field_info.placeholder : ''}  displayvalue ={display_name?display_name:''}
                                        /> :
                                         field_info.field_type == "checkbox"  && field_info.content !='' && field_info.content !=null ? 
                                             <Ticket_Checkbox  id={this.props.id}  required={field_info.is_required?field_info.is_required:''} indexing={this.props.indexing} name={field_info.title?field_info.title+"?"+ this.props.indexing:field_info.field_type+"?" + this.props.indexing} tick_content={field_info.content  ? field_info.content  : ''} tick_title={field_info.title ? field_info.title :''} 
                                             validateArry={this.props.required_field}
                                              TickCheck={(name, text,index) => this.InputClick(name, text,index)} 
                                             displayvalue ={display_name} 
                                             /> :
                                             field_info.field_type == "select"  && field_info.content !='' && field_info.content !=null ?
                                                 <Ticket_Select  required={field_info.is_required?field_info.is_required:''} id={this.props.id} indexing={this.props.indexing} name={field_info.title?field_info.title+"?" + this.props.indexing:field_info.field_type+"?" + this.props.indexing} tick_content={field_info.content ? field_info.content : ''} tick_title={field_info.title ? field_info.title : ''} TickSelect={(name, text,index) => this.InputClick(name, text,index)} displayvalue ={display_name}
                                                  /> :
                                                 field_info.field_type == "textarea" ?
                                                    <Ticket_Textarea   id={this.props.id} required={field_info.is_required?field_info.is_required:''} indexing={this.props.indexing} name={field_info.title?field_info.title+"?" + this.props.indexing:field_info.field_type+"?" + this.props.indexing} tick_content={field_info.content ?field_info.content : ''} tick_title={field_info.title ? field_info.title : ''} TickTextarea={(name, text,index) => this.InputClick(name, text,index)} placeholder={field_info.placeholder ? field_info.placeholder : ''} 
                                                    displayvalue ={display_name}
                                                      /> :
                                                    field_info.field_type == "radio" && field_info.content !=''  && field_info.content !=null?
                                                         <Ticket_radio  required={field_info.is_required?field_info.is_required:''} id={this.props.id} indexing={this.props.indexing} name={field_info.title?field_info.title+"?" + this.props.indexing:field_info.field_type+"?" + this.props.indexing} tick_content={field_info.content ? field_info.content : ''} tick_title={field_info.title ? field_info.title : ''} TickRadio={(name, text,index) => this.InputClick(name, text,index)} displayvalue ={display_name} 
                                                         /> :
                                                        ''
                                    }          
                                 </div>
                               
                            )
                        })
                        } 
                                    </div>
                                </div>
                            </div>
                        </div>

                       
             

        )

    }

}


function mapStateToProps(state) {
    const { cartproductlist } = state;

    return {
        // categorylist: categorylist,
        // productlist: productlist,
        // attributelist: attributelist
        cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],

    };
}

const connectedTickit_Create = connect(mapStateToProps)(Tickit_Create);
export { connectedTickit_Create as Tickit_Create };     