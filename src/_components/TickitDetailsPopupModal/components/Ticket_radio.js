import React from 'react';
import { connect } from 'react-redux'


class Ticket_radio extends  React.Component {
  constructor(props) {
    super(props);
    this.state = name
      this.state={
        status:false
      }
        
    // this.handleChange = this.handleChange.bind(this);
 //   this.onChange = this.onChange.bind(this);

}


paytype(name , value){
   
   // console.log("ticket_name" , value , name)
    this.setState({name:value,status:true})
    if(this.props.indexing){
      var index = this.props.indexing - 1
      this.props.TickRadio(name,value,index)

  
  }
     
 }
 componentDidMount() {
    var radio =this.props.tick_content.split(',');

    this.setState({Tick_radio:radio 
    })

}
    render() {
        const { name } = this.props;
    
      return(
        <div className="form-group">
        <div className="input-group">
           <div className="input-group-addon">
              {this.props.tick_title?this.props.tick_title:''}
           </div>
           <div className="form-control" id="user_notes" name="user_notes" type="text" style={{height:'auto'}}>
            {this.state.Tick_radio && this.state.Tick_radio.map((check , index)=>{
               return(     
               this.state.status == true ?      
                <div key={index} className="col-sm-6 button_with_checkbox mt-1" onClick={()=>this.paytype(name , check)}>
                   <input type="radio" id={this.props.id} id={"Ticket" + this.props.indexing + "_" + check} name={name}  value={check}
                    //  checked={check == this.props.displayvalue ?true:false}  
                         />
                   <label htmlFor={"Ticket" + this.props.indexing + "_" + check} className="label_select_button">{check}</label>
                   </div>

                   :
                    <div key={index} className="col-sm-6 button_with_checkbox mt-1" onClick={()=>this.paytype(name , check)}>
                   <input type="radio" id={this.props.id} id={"Ticket" + this.props.indexing + "_" + check} name={name}  value={check}
                      checked={check == this.props.displayvalue ?true:false}  
                         />
                   <label htmlFor={"Ticket" + this.props.indexing + "_" + check} className="label_select_button">{check}</label>
                   </div>

                ) })}
           </div>
       </div>
      </div>
        
      )

    }

}


function mapStateToProps(state) {
  const { registering } = state.registration;
  return {
      registering
  };
}

const connectedTicket_radio = connect(mapStateToProps)(Ticket_radio);
export { connectedTicket_radio as Ticket_radio };