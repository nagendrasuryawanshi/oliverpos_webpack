import React from 'react';
import { connect } from 'react-redux';
import { authentication } from '../../_reducers/authentication.reducer';
import { cartProductActions } from '../../_actions/cartProduct.action';
import { Tickit_Create } from '../../_components/TickitDetailsPopupModal/components/Tickit_Create'
import { array } from 'prop-types';


class TickitDetailsPopupModal extends React.Component {
    

    constructor(props) {
        super(props);
        
        this.state = {
            id:'',
            productid:'',          
            formData:[],
            Input_field:[],
            ticket_View:'',
            validation_array:[],
           // status:false
           

        }
        this.addTickettoCart = this.addTickettoCart.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onChange = this.onChange.bind(this);
        this.InputClick = this.InputClick.bind(this);
      
    }

    handleChange(e) {

        var name = e.target.value
        this.setState({ selectedValue: name })
    }
    onChange(e) {
       // console.log("select", e.target.value);

        this.setState({ full_name: e.target.value })

    }
    paytype(item) {

      //  console.log("paytype", item);
        this.setState({ SelectedAddon: item })

    }
    componentWillReceiveProps(nextprops) {

        let ticket_form = new Array();
       //console.log("nextprops.Ticket_Detail",nextprops)
       if(nextprops.Ticket_Detail && nextprops.Ticket_Detail != ''){
     //   console.log("nextprops.Ticket_Detail",nextprops.Ticket_Detail.quantity);
        this.setState({productid:nextprops.Ticket_Detail.product_id,
           ticket_View: nextprops.Ticket_Detail.ticket_info?nextprops.Ticket_Detail.ticket_info:'',
           variation_id:nextprops.Ticket_Detail.variation_id

        })

        for (let ticket_num = 0; ticket_num < nextprops.Ticket_Detail.quantity; ticket_num++) {
             //  console.log("nextprops.Ticket_Detail",nextprops.Ticket_Detail.product_ticket.fields);
               var tickfield=nextprops.Ticket_Detail.product_ticket.fields               
               ticket_form.push(nextprops.Ticket_Detail.product_ticket.fields)
               //ticket_form.push(product_ticket);
                this.setState({Input_field:ticket_form,
                    form_title:nextprops.Ticket_Detail.product_ticket.title
                })     

        }
      }
        localStorage.setItem('selectProduct',JSON.stringify(ticket_form))
    }

    checKValidation(){
     var chechked;
     var fil1;
     //var filter=this.state.formData;
     var filter=this.state.formData

   var  armArr=[];
  //  console.log("filter",filter)
     var validation_array=[];

//  //--check static fields------------------ 
      var staticField=["first_name","last_name","owner_email"];
      // if(field_info.is_required ==  true){
      //   var title= field_info.title.replace(" ","_");    
     // console.log("CheckingStart");   
      
    
       staticField && staticField.map(staticItem=>{    
       // console.log("staticItem",this.state.ticket_View);
       var found=false;
        // if(filter && filter.length>0)
        // { 
     
            var isStaticFieldEmpty=false; 
          //  console.log("this.state.ticket_View.length ",this.state.ticket_View.length,this.state.Input_field.length);

   if(this.state.ticket_View =='' || this.state.ticket_View !=='' && this.state.Input_field.length > 0 && this.state.ticket_View.length !== this.state.Input_field.length){
     //  console.log("ticket is null or not null");
            filter && filter.map((field_value,field_index)=>{  
                var field_data=field_value.newArr?field_value.newArr :field_value;

           //   console.log("field_value1111",field_data);
              field_data.map(val=>{             
               
                 //   console.log("valArr2323",val);
                    for (name in val) {  
                       //    console.log("res12343",name);  
                        if (val.hasOwnProperty(name)) {
                                  
                      //  console.log("CheckingValues2323",name + " = " + val[name]);           
                          if(staticItem===name   ){
                         //     console.log("Checkingname2323",staticItem,name)
                            found==true;
                            if( val[name]!== ""){
                               isStaticFieldEmpty=true;                   
                            }
                          }                       
                          
                          }  
                        }  
                   
                })
            })   
        }else if(this.state.ticket_View !=='' && this.state.Input_field.length > 0 && this.state.ticket_View.length == this.state.Input_field.length){
           //    console.log("ticket is not null");
           
            var check_validation;

            filter &&   filter.map(edit_field=>{
                    var field_data=edit_field.newArr?edit_field.newArr :edit_field;

                     this.state.ticket_View.map((item,itms_index)=>{
                        for(var name  in item){ 

                          field_data.map((val,editIndex)=>{   
                             for(var key in val){
                        
                             if(name == key){
                              console.log("key123",key);
                         //   check_validation=staticItem == key && val[key]== ''?false:true
                                if(staticItem == key){
                                    check_validation=false 

                                    if(val[key]!==''){
                                        check_validation=true 
                                    }

                                }
                          //  console.log("check_validation",check_validation);

                              
                                 }
                            

                           }
                        }) 

                       }
                    })
                 })
                isStaticFieldEmpty= this.state.ticket_View && filter.length == 0?true:check_validation
  
              }    

            if(isStaticFieldEmpty==false)
            {

              var  CurrForm={key:0,title:staticItem}
              armArr.push(CurrForm );
            }
      
      })   
      
      // console.log("isStaticFieldEmpty0",isStaticFieldEmpty);
      // if( !filter || filter.length==0)
      // {
      //   isStaticFieldEmpty=true; 
      // }
      // console.log("isStaticFieldEmpty",isStaticFieldEmpty);
   // }
//  //----------------------------------------

 //console.log("armArr",armArr)

        // console.log("Input_field",this.state.Input_field);
        this.state.Input_field && this.state.Input_field.map((item,inputIndex)=>{
         // console.log("inputIndex",item);
              

            item && item.map(checkfield=>{
                var field_info= JSON.parse(checkfield.field_info)
               // console.log("checKValidation",field_info)
                if(field_info.is_required ==  true){
                    var remove_special_charac=field_info.title.split('?')[0]
                   var title=remove_special_charac.replace(/ /g,"_");   
                   console.log("title",title);
                   var isExist=false;

                 
             if(this.state.ticket_View =='' || this.state.ticket_View !=='' && this.state.Input_field.length > 0 && this.state.ticket_View.length !== this.state.Input_field.length){      
                console.log("stay here111 ")
   
                    filter && filter.map((field_value,field_index)=>{
                      //  console.log("field_index",field_index);
                      var field_data=field_value.newArr?field_value.newArr :field_value;
                      console.log("field_index",field_data);
                      field_data.map(val=>{
                           
                            //console.log("valArr",val);

                            for (name in val) {
                                  
                             
                               
                             //   console.log("res123",fil1);
                                if (val.hasOwnProperty(name)) {
                                  //  console.log("res12",fil1);            
                                console.log(name + " = " + val[name]);
                         
                                if(title===name &&  (val!== "") && field_index == inputIndex ){
                                    isExist=true;
                                 
                                }                       
                                   
                                   }  
                                }  
            
                        })
            
                    })   
                }else if(this.state.ticket_View !=='' && this.state.Input_field.length > 0 && this.state.ticket_View.length == this.state.Input_field.length){
                        console.log("stay here ")
                        var check_validation;

                        filter &&   filter.map(edit_field=>{
                            var field_data=edit_field.newArr?edit_field.newArr :edit_field;
        
                             this.state.ticket_View.map((item,itms_index)=>{
                                for(var name  in item){ 
        
                                  field_data.map((val,editIndex)=>{   
                                     for(var key in val){
                                  
                                     if(name == key){
                                      
                                        if(title == key){
                                            check_validation=false
                                          if(val[key]!== ''){

                                            check_validation=true
                                             }

                                          }
                                    //check_validation=title == key && val[key]== ''?false:true                         
        
                                      //  return ss;
                                         }
                                    //  }
        
                                   }
                                }) 
        
                               }
                            })
                         })
                         isExist= this.state.ticket_View && filter.length == 0?true:check_validation
      
                 
                    }
                       
                  
                  if(isExist===false){
                    console.log("CurrForm",CurrForm,"inputIndex",inputIndex,"title",title);
                   validation_array.push(CurrForm)
    
                   // newArr.push({[name]:value})           
                   var  CurrForm={key:inputIndex,title:title}
                     armArr.push(CurrForm );
    
                   
                  } 
                }
                     
            })
           
       
        })
            this.state.validation_array=armArr      
            this.setState({validation_array:armArr})  
       // console.log("chechked",rr);     
      // console.log("validation_array12",this.state.validation_array);

    }
    addTickettoCart(e) {
        const { dispatch, cartproductlist } = this.props;
        var cartlist = cartproductlist ? cartproductlist : [];
       // var ticket_info = []
     //  this.setState({status:false})
   var validation_check=this.checKValidation()
   //console.log("validation_check",cartlist);

        let  new_array=[]

        cartlist.map((Crdprod, index) =>{
                if(this.state.variation_id==0 && this.state.productid == Crdprod.product_id || this.state.variation_id !=0 && this.state.variation_id == Crdprod.variation_id ){
                
                   // console.log("pervious_array1" ,this.state.formData)
             
                    var ticketArr= this.state.ticket_View;
                  //  ticketArr && ticketArr.map((tick_item,ticketIndex)=>{
                        var res="";

                     //   console.log("data",tick_item,ticketIndex,"tick_item[ticketIndex]",tick_item[ticketIndex]);    
                           //   console.log("this.state.formData", this.state.formData)
                              var isExist=false

                            this.state.formData && this.state.formData.map((item,updatedTicketIndex)=>{
                              //  this.state.ticket_View && this.state.ticket_View.filter()
                                    // console.log("checked",checked)
                                var  tick_item= null
                      //          console.log("data1",item.key,this.state.ticket_View)
                                var isExist=false

                                if(this.state.ticket_View && this.state.ticket_View.length > 0 && this.state.ticket_View !=null){
                               //     console.log("data",item.key)
                                    tick_item= this.state.ticket_View.find(function(element,index) {   
                                     //   console.log("element.index",element.index, index,item.key);
                                    return index===item.key;
                                  });
                                  isExist=!tick_item?true: tick_item == 'undefined'?false:''
                                }
                                // res=item[index]=itm[key]
                              
                              //  console.log("c",tick_item,isExist);

                                //if(ticketIndex==item.key)
                                if(tick_item  ) 
                                { 

                                  //  console.log("tick_item",tick_item);

                                   // console.log("tick_item",tick_item)
                                      for (var key  in tick_item) {
                                  // alert(key)
                                       // console.log("key");  
                                    var  name_key=key;
                                   // console.log("key_name" + ': ' + name_key);
                                     var res="";

                                    item && item.newArr.map((itm,index)=>{
                                        var name=""
                                        
                                       for (name in itm) {

                                         if (itm.hasOwnProperty(name)) {

                                            //  res += res!==""? (',"'+ name + '":"' + itm[name])+'"' :'"'+ name + '":"' + itm[name] +'"';
                                            //  console.log("res",res);
                                          
                                            if(name_key == name )
                                               {
                                            //console.log("key barabar hain esme",ticketIndex);
                                                tick_item[name]=itm[name]
                                            //    console.log("ticketArr",ticketArr);
                                               }
                                               else{
                                                // var frmdataCollection=JSON.parse(res);
                                                  // console.log("ticketArr12",ticketIndex,name,itm[name]);
                                                   tick_item[name]=itm[name]

                                                //   console.log("ticketArr",ticketArr);            
                                                     }
                                                                                              
                                               }  
                                            }   
                                             
                                         })

                                      }
                                      new_array.push(tick_item)
                                  } else { 
                                  //  console.log("key is matched1",item.key, "tick_item" ,item);

                                  var newData=item.newArr?item.newArr :item;
                              
                                    //console.log("isExist i s false",newData)

                                    var res="";
                                    newData && newData.map((itm,index)=>{
                                        var name=""
                                       for (name in itm) {
                                           // console.log("matched1",item.key);
                                         if (itm.hasOwnProperty(name)) {

                                           // console.log(name + " = " + itm[name]);
                                            res += res!==""? (',"'+ name + '":"' + itm[name])+'"' :'"'+ name + '":"' + itm[name] +'"';
                                    
                                           
                                               // console.log("res12",res);
                                                                                              
                                               }  
                                            }  
                                         })
                                     //    console.log("res123",'{' +res+ '}')
                                         var frmdataCollection=JSON.parse('{' +res+ '}');
                                       //  console.log("formCollection",JSON.stringify(frmdataCollection))
                                         new_array.push(frmdataCollection)
                                       
                                    }
                                  
                                
                                })
                               // console.log("new_array",new_array,"ticketArr",ticketArr);
                             if( ticketArr && new_array){
                                ticketArr.map((item,index)=>{                                      
                                 var tick_item= new_array.find(function(element,newIndex) {   
                                        return item.first_name=== element.first_name && item.last_name=== element.last_name && item.owner_email=== element.owner_email;
                                })  
                                if(!tick_item){
                                    new_array.push(item)
                                } 

                                })
                             }
                         

     
                        
                   
                    /// this.setState({ticket_View:new_array});
                    // new_array= ticketArr;
                    // console.log("ticketArr",new_array);
                     Crdprod.ticket_info=new_array;
            

               }
                 //console.log("data1",cartlist);


                //  this.setState({formData:''})
                //  this.setState({Input_field:''})

     })
   //  console.log("cartlist",cartlist);
    
     //window.location='/shopview'

     
    console.log("this.state.validation_array",this.state.validation_array,"this.state.ticket_View",this.state.ticket_View   );

     if( this.state.validation_array.length == 0 || this.state.validation_array == 'undefined'){ $('#user_fname').val(null);
     $('#user_lname').val(null);
        dispatch(cartProductActions.addtoCartProduct(cartlist));
          window.location='/shopview'
        $(".close").trigger("click");
     }
    // $(".close").trigger("click");
    }

    InputClick(index,name, value) {
   
     //   console.log("value and name", index,name, value ,)
  
    //    console.log("foemData112",this.state.formData);
    
      if(this.state.ticket_View == ''){    
            //   console.log("this.state.ticket_View  is null")
         var armArr=this.state.formData;

       if(this.state.formData.length>0)
       {
      //  console.log("step1")
          var  CurrForm=armArr.find(function(element,key) {
            return key===index;
          });
          if(CurrForm){
            var frmcollection =CurrForm;
            //console.log("frmcollection[name]",frmcollection[name])
            // console.log("frmcollection",frmcollection);

             var  existFieldArr=frmcollection.find(function(element) {
                //  console.log("element",element);
                var data = Object.keys(element)
                //  console.log("data",data[0],name);
                //  console.log("existFieldArr" , data[0]===name)
                return data[0]===name;
              });
            if(existFieldArr)
            existFieldArr[name]=value;
            else
            {
                frmcollection.push({[name]:value})
            }
        // console.log("frmcollection",frmcollection)
           }
          else{
            var newArr=[];
            newArr.push({[name]:value})
            var  CurrForm=newArr
          // var  CurrForm={key:index,newArr }
          
            armArr.push(CurrForm );
         }
       }
       else
       {
      //  console.log("step2")
        var newArr=[];
        newArr.push({[name]:value})
       //var  CurrForm={key:index,newArr }
       var  CurrForm=newArr
        armArr.push(CurrForm );

        
       }
       this.setState( { formData:armArr  });
     
       }else if(this.state.ticket_View !=''){
          // console.log("this.state.ticket_View  is  not null ",this.state.ticket_View)
        var armArr=this.state.formData;
        if(this.state.formData.length>0){

            var  CurrForm=armArr.find(function(element,key) {
                return element.key===index;
              });
   
               if(CurrForm){
                 var frmcollection =CurrForm;
            //     //console.log("frmcollection[name]",frmcollection[name])
            //     // console.log("frmcollection",frmcollection);
    
                 var  existFieldArr=frmcollection.newArr.find(function(element) {
            //          console.log("element",element);
                    var data = Object.keys(element)
                    //   console.log("data",data[0],name);
                    //  console.log("existFieldArr" , data[0]===name)
                    return data[0]===name;
                 });
                //  console.log("existFieldArr",existFieldArr);    
                if(!existFieldArr)                 
               {
                // console.log("existFieldArr is false");    
            //    console.log("existFieldArr is ", index,name, value );    

                    frmcollection.newArr.push({[name]:value})
                    // console.log("frmcollection false condtion1",frmcollection)

                 }else{ existFieldArr[name]=value};
                 
            // console.log("frmcollection false condtion",frmcollection)
               }
           else{
            // console.log("CurrForm comndition is false",CurrForm);

                var newArr=[];
                newArr.push({[name]:value})
               // var  CurrForm=newArr
               var  CurrForm={key:index,newArr }
              
                armArr.push(CurrForm );
             }       


         } else {
           // console.log("step2",index)
            var newArr=[];
            newArr.push({[name]:value})           
             var  CurrForm={key:index,newArr }

             // var  CurrForm=newArr
            //   console.log("this.state.CurrForm" ,CurrForm)
            armArr.push(CurrForm );
    
            
           }


           this.setState( { formData:armArr  });
    



       }
      // formdataArray.push(this.state.formData)

      //  console.log("foemData123",this.state.formData);
      // 
     ///  console.log("this.state.pervious_array" , this.state.pervious_array)
    ///   this.state.pervious_array.push(data)
     
   
    }
    close_Model(){
    //    window.location='/'


    }

 
    componentDidMount() {
       
    }   
    render() {
       //  console.log("this.state.InputClick_field",this.state.validation_array)
       var Prosuct_ticket=localStorage.getItem('selectProduct')?JSON.parse(localStorage.getItem('selectProduct')):''
        return (
         
            <div id="tickitDetails" className="modal modal-wide fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true"  >
                                <img src="assets/img/delete-icon.png" />
                            </button>
                            {/* <h4 className="modal-title">{hasVariationProductData ? getVariationProductData.Title : ''}</h4> */}
                            <h4 className="modal-title">{this.state.form_title?this.state.form_title:''}</h4>
                        </div>
                          <div className="modal-body" id="fullHeightPopup">

                           <form className="clearfix form_editinfo form_tickira">
                <div className="tickira-panel">
                    <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                        {   this.state.Input_field &&   this.state.Input_field.map((field_Name, index) => {
                            var Tic_index = index + 1
                           
                            return (

                                <Tickit_Create
                                    key={index}
                                    Item={field_Name}
                                    indexing={Tic_index}
                                    real_index={index}
                                    InputClick={this.InputClick}
                                    id="input_blank"
                                    id1="input_blank1" 
                                    ticket_View={this.props.Ticket_Detail.ticket_info?this.props.Ticket_Detail.ticket_info:''}
                                    required_field={this.state.validation_array}
                                    type={this.state.ticket_View == ''?'create':'edit'}
                                />

                            )
                        })}
                               </div>
                </div>
            </form>
          
                        </div>
                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-primary btn-block h66" onClick={this.addTickettoCart.bind(this)}>SAVE & UPDATE</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}





function mapStateToProps(state) {
    const { cartproductlist } = state;

    return {
        // categorylist: categorylist,
        // productlist: productlist,
        // attributelist: attributelist
        cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],

    };
}

const connectedTickitDetailsPopupModal = connect(mapStateToProps)(TickitDetailsPopupModal);
export { connectedTickitDetailsPopupModal as TickitDetailsPopupModal };    