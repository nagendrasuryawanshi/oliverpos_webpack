import React from 'react';
import { connect } from 'react-redux';


class ProductSubAtrribute extends React.Component {

    constructor(props) {
        super(props);
    }

removeSpecialChar(item){
  return  item.replace(/\s/g,'-').replace(/\//g, "-").replace("'","").replace(".","").replace("-","").toLowerCase();
}
   

    render() {
        const ProductSubAttribute = this.props;
        // console.log("this.props.showSelectedProduct", this.props.showSelectedProduct);
        // console.log("parentAttribute",ProductSubAttribute);
        // console.log("this.props.filteredAttribute",this.props.filteredAttribute);
        return (
            ProductSubAttribute.options ? (
                ProductSubAttribute.options.split(',').map((option, index) => {    
              
                    var isExist=false;  //(!this.props.filteredAttribute) || this.props.filteredAttribute.length==0?true:false;
                    var newOption=option;
                    // //---------Check Product Vareations Exists----------------------------------------------------------------------------
                    // var isVariationExist= false;
                    // this.props.productVariations && this.props.productVariations.filter(item=>{                      
                    //    item.combination.split("~").map(combination=>{   
                    //   //   console.log("match",  combination.replace(/\s/g,'-').replace(/\//g, "-").replace("'","").replace(".","").replace("-","")  .toLowerCase(),    option.replace(/\s/g,'-').replace(/\//g, "-").replace("'","").replace(".","").replace("-","").toLowerCase());                   
                    //    //    if(  (combination.replace(/\s/g,'-').replace(/\//g, "-").replace("'","").replace(".","").replace("-","").toLowerCase()).includes(option.replace(/\s/g,'-').replace(/\//g, "-").replace("'","").replace(".","").replace("-","").toLowerCase()))
                    //    if( this.removeSpecialChar(combination).includes(this.removeSpecialChar(option))  )
                    //        isVariationExist=true;
                    //    })
                     
                    //    })  
                    //  //  console.log("isVAriationsExists",option,newOption,isVariationExist)
                      
                    //    //---------------------------------------------------------------------------------------------------

//Find Attribute Code----------------------------------------------
        let attribute_list = localStorage.getItem("attributelist")?  JSON.parse(localStorage.getItem("attributelist")) :null; 
      
            var sub_attribute;
           //var option;
            var found = attribute_list.find(function (element) {
                return element.Code.toLowerCase()==ProductSubAttribute.parentAttribute.toLowerCase()        

            })
       // console.log("SubAttrFound",found  ,"option",option);
            if(found)
            {
                sub_attribute=  found.SubAttributes.find(function (element) {
                    return element.Value.toLowerCase()==option.toLowerCase()         
    
                })
            }

            newOption= sub_attribute ? sub_attribute.Value:newOption;
           // console.log("newOption",newOption  ,"option",option);
//------------------------------------------------------------------

 //---------Check Product Vareations Exists----------------------------------------------------------------------------
 var isVariationExist= false;
 this.props.productVariations && this.props.productVariations.filter(item=>{                      
    item.combination.split("~").map(combination=>{   
   
       var atrrtibute=null;
        var codeOfCombination=""
       
        if(found)
        {
            atrrtibute=  found.SubAttributes.find(function (element) {
                return element.Value.toLowerCase()==combination.toLowerCase()         

            })

            if(atrrtibute){
                codeOfCombination=atrrtibute.Value;
            }

        }
        if(codeOfCombination && codeOfCombination !="" || combination=="**" )
        if( this.removeSpecialChar(codeOfCombination).includes(this.removeSpecialChar(option))  || combination=="**"  )
            isVariationExist=true;               
         
    })
            
  
    })  

   
    //---------------------------------------------------------------------------------------------------
               
                   newOption= this.removeSpecialChar(newOption);                
                    this.props.filteredAttribute && this.props.filteredAttribute.map((item,idx)=>{
                        item.combination.split("~").map(fItem=>{
                         //   console.log("fItem",fItem)                         
                            var fItemRes =this.removeSpecialChar(fItem);   
                                if( fItemRes===newOption || fItem=="**")
                                { 
                                    isExist=true;  
                                 
                                }
                        })
  
                    })

                    
                if( this.props.filteredAttribute &&  this.props.filteredAttribute.length==0)
                isExist=true;
               
                    var isEnabled= isVariationExist===true && isExist === true ?true:false;
                   var checked=this.props.showSelectedProduct  && this.props.showSelectedProduct.toLowerCase().includes(option.toLowerCase()) ?'checked' :null
              
                    return (     
                            <div className="col-sm-4" key={"subattr-" + index}>
                                <div className="button_with_checkbox p-0">
                                    <input  disabled={!isEnabled}  type="radio" checked={checked}  id={`variation-size-${option}`} name={`variation-option-${ProductSubAttribute.parentAttribute}`} value={option} onClick={this.props.click.bind(this, option, ProductSubAttribute.parentAttribute)} />
                                    <label htmlFor={`variation-size-${option}`} className="label_select_button" title={option}>{option.length > 13 ? option.substring(0, 10) + "..." : option}</label>
                                </div>
                            </div>
                        //     : 
                            
                        
                    )
                })
            ) : ""
        )
    }

}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedProductSubAtrribute = connect(mapStateToProps)(ProductSubAtrribute);
export { connectedProductSubAtrribute as ProductSubAtrribute };
