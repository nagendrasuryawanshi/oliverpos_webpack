export const getInclusiveTax = (price) => {
    let TaxRate = localStorage.getItem('TAXT_RATE_LIST') ? JSON.parse(localStorage.getItem("TAXT_RATE_LIST")) : null;
    let tax_rate = TaxRate ? parseFloat(TaxRate.TaxRate) : 0;
    // console.log("getInclusiveTax" , price)
    let actual_price = 0;
    let inclusiveTax = 0;
    actual_price = parseFloat(price) / ((tax_rate / 100) + 1);
    inclusiveTax = (price - actual_price)
    return inclusiveTax
}

export const getExclusiveTax = (price) => {
    let TaxRate = localStorage.getItem('TAXT_RATE_LIST') ? JSON.parse(localStorage.getItem("TAXT_RATE_LIST")) : null;
    let tax_rate = TaxRate ? parseFloat(TaxRate.TaxRate) : 0;
    // console.log("getExclusiveTax" , price)
    let exclusiveTax = 0;
    exclusiveTax = parseFloat((parseFloat(price) * tax_rate) / 100);
    return exclusiveTax
}

export const getCheckoutList = (products) => {
    let TAX_CASE = getSettingCase()
    var all_products = [];
    let price = 0;
    let tax = 0;
    switch (TAX_CASE) {
        //  case 1  
        case 1:
            products.map(item => {

                price = (item.discount_amount !== 0) ? item.after_discount - getInclusiveTax(item.after_discount) : (item.old_price - getInclusiveTax(item.old_price)) * item.quantity;

                tax = (item.discount_amount !== 0) ? getInclusiveTax(item.after_discount) : getInclusiveTax(item.old_price) * item.quantity

                item['subtotal'] = (item.old_price - getInclusiveTax(item.old_price)) * item.quantity
                item['total'] = price
                item['total_tax'] = tax
                item['subtotal_tax'] = getInclusiveTax(item.old_price) * item.quantity
            })
            all_products = products;
            break;
        // case 2
        case 2:
            products.map(item => {

                price = (item.discount_amount !== 0) ? item.after_discount : (item.old_price - getInclusiveTax(item.old_price)) * item.quantity;

                tax = (item.discount_amount !== 0) ? item.excl_tax : getInclusiveTax(item.old_price) * item.quantity

                item['subtotal'] = (item.old_price - getInclusiveTax(item.old_price)) * item.quantity
                item['total'] = price
                item['total_tax'] = tax
                item['subtotal_tax'] = getInclusiveTax(item.old_price) * item.quantity

            })
            all_products = products;
            break;
        // case 3
        case 3:
            products.map(item => {
                price = (item.discount_amount !== 0) ? item.after_discount - getInclusiveTax(item.after_discount) : (item.old_price - getInclusiveTax(item.old_price)) * quantity;

                tax = (item.discount_amount !== 0) ? item.incl_tax : getInclusiveTax(item.old_price) * item.quantity

                item['subtotal'] = (item.old_price - getInclusiveTax(item.old_price)) * item.quantity
                item['total'] = price
                item['total_tax'] = tax
                item['subtotal_tax'] = getInclusiveTax(item.old_price) * item.quantity
            })
            all_products = products;
            break;
        // case 4
        case 4:
            products.map(item => {
                price = (item.discount_amount !== 0) ? item.after_discount : (item.old_price - getInclusiveTax(item.old_price)) * item.quantity;

                tax = (item.discount_amount !== 0) ? item.excl_tax : getInclusiveTax(item.old_price) * item.quantity

                item['subtotal'] = (item.old_price - getInclusiveTax(item.old_price)) * item.quantity
                item['total'] = price
                item['total_tax'] = tax
                item['subtotal_tax'] = getInclusiveTax(item.old_price) * item.quantity
            })
            break;
        case 6: case 8:
            products.map(item => {
                // console.log("item.old_price",item)
                price = (item.discount_amount !== 0) ? (item.old_price * item.quantity) - item.discount_amount : item.old_price * item.quantity;
                // console.log("item.old_price", item.old_price,  parseFloat(item.discount_amount), item.old_price - parseFloat(item.discount_amount))
                tax = (item.discount_amount !== 0) ? (item.incl_tax !== 0) ? item.incl_tax : item.excl_tax : getExclusiveTax(item.old_price) * item.quantity

                item['subtotal'] = item.old_price * item.quantity
                item['total'] = price
                item['total_tax'] = tax
                item['subtotal_tax'] = getExclusiveTax(item.old_price) * item.quantity
            })
            break;
        default:

            products.map(item => {
                // console.log("item.old_price",item)
                price = (item.discount_amount !== 0) ? (item.old_price * item.quantity) - exclusiveDiscount(item.after_discount, item.discount_amount, (item.old_price * item.quantity)) : item.old_price * item.quantity;
                //console.log("item.old_price", item.old_price,  parseFloat(item.discount_amount), item.old_price - parseFloat(item.discount_amount))
                tax = (item.discount_amount !== 0) ? (item.incl_tax !== 0) ? item.incl_tax : item.excl_tax : getExclusiveTax(item.old_price) * item.quantity

                item['subtotal'] = item.old_price * item.quantity
                item['total'] = price
                item['total_tax'] = tax
                item['subtotal_tax'] = getExclusiveTax(item.old_price) * item.quantity
            })
            all_products = products;
            break;
    }

    return all_products

}

export const productPriceWithTax = (price, condition) => {
    let TaxRate = localStorage.getItem('TAXT_RATE_LIST') ? JSON.parse(localStorage.getItem("TAXT_RATE_LIST")) : null;
    let tax_rate = TaxRate ? parseFloat(TaxRate.TaxRate) : 0;
    let actual_price = 0;
    if (condition == '3' || condition == '4')
        return actual_price = parseFloat(price) / ((tax_rate / 100) + 1);
    else
        return actual_price = parseFloat((parseFloat(price) * tax_rate) / 100) + parseFloat(price);
}

export const typeOfTax = () => {
    let TAX_CASE = getSettingCase();
    var TAX_IS = "Tax";
    switch (TAX_CASE) {
        case 1: case 3: case 5: case 7:
            TAX_IS = "incl";

            break;
        default:
            TAX_IS = "Tax";

            break;
    }
    return TAX_IS

}


// Apply tax when show product list on shop 
export const getTaxAllProduct = (products) => {
    var all_products = [];
    let price = 0;
    let TAX_CASE = getSettingCase()
    //console.log("TAX_CASE", TAX_CASE)

    switch (TAX_CASE) {
        //  case 1  
        case 1:
            products.map(item => {
                item['old_price'] = item.Price
            })
            all_products = products;
            break;
        // case 2
        case 2:
            products.map(item => {
                item['old_price'] = item.Price
            })
            all_products = products;
            break;
        // case 3
        case 3:
            products.map(item => {
                item['old_price'] = item.Price
                price = productPriceWithTax(item.Price, 3)
                item['Price'] = parseFloat(price)
            })
            all_products = products;
            break;
        // case 4
        case 4:
            products.map(item => {
                item['old_price'] = item.Price
                price = productPriceWithTax(item.Price, 4)
                item['Price'] = parseFloat(price)
            })
            all_products = products;
            break;
        // case 5
        case 5:
            products.map(item => {
                item['old_price'] = item.Price
                price = productPriceWithTax(item.Price, 5)
                item['Price'] = parseFloat(price)
            })
            all_products = products;
            break;
        // case 6
        case 6:
            products.map(item => {
                item['old_price'] = item.Price
                price = productPriceWithTax(item.Price, 6)
                item['Price'] = price
            })
            all_products = products;
            break;
        // case 7
        case 7:
            products.map(item => {
                item['old_price'] = item.Price
            })
            all_products = products;
            break;
        // case 8
        default:
            products.map(item => {
                item['old_price'] = item.Price
            })
            all_products = products;
            break;
    }

    return all_products
}

export const cartPriceWithTax = (price, condition) => {
    let TaxRate = localStorage.getItem('TAXT_RATE_LIST') ? JSON.parse(localStorage.getItem("TAXT_RATE_LIST")) : null;
    let tax_rate = TaxRate ? parseFloat(TaxRate.TaxRate) : 0;
    //console.log("TaxRate", tax_rate, price)
    let actual_price = 0;
    if (condition == '7')
        return actual_price = parseFloat((parseFloat(price) * tax_rate) / 100) + parseFloat(price);
    else if (condition == '2' || condition == '4')
        return actual_price = parseFloat(price) / ((tax_rate / 100) + 1);
    else
        return actual_price = parseFloat(price) - parseFloat((parseFloat(price) * tax_rate) / 100);
}

// Apply tax when product add on cart
export const getTaxCartProduct = (products) => {
    let TAX_CASE = getSettingCase()
    var all_products = [];
    let price = 0;
    let incl_tax = 0;
    let excl_tax = 0;


    switch (TAX_CASE) {
        // case 1
        case 1:
            products.map(item => {
                if (item.product_id) {
                    incl_tax = getInclusiveTax(item.old_price)
                    item['incl_tax'] = incl_tax * item.quantity
                } else {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
        // case 2
        case 2:
            products.map(item => {
                if (item.product_id) {
                    excl_tax = getInclusiveTax(item.old_price)
                    price = cartPriceWithTax(item.old_price, 2)
                    item['Price'] = price * item.quantity
                    item['excl_tax'] = excl_tax * item.quantity
                } else {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
        // case 3
        case 3:
            products && products.map(item => {
                if (item.product_id) {
                    incl_tax = getInclusiveTax(item.old_price)
                    item['Price'] = item.old_price * item.quantity
                    item['incl_tax'] = incl_tax * item.quantity
                } else {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
        // case 4
        case 4:
            products && products.map(item => {
                if (item.product_id) {
                    excl_tax = getInclusiveTax(item.old_price)
                    price = cartPriceWithTax(item.old_price, 4);
                    item['Price'] = price * item.quantity
                    item['excl_tax'] = excl_tax * item.quantity
                } else {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
        // case 5
        case 5:
            products.map(item => {
                if (item.product_id) {
                    incl_tax = getExclusiveTax(item.old_price)
                    item['Price'] = item.Price
                    item['incl_tax'] = incl_tax * item.quantity
                } else {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
        // case 6
        case 6:
            products && products.map(item => {
                if (item.product_id) {
                    excl_tax = getExclusiveTax(item.old_price)
                    item['Price'] = item.old_price * item.quantity
                    item['excl_tax'] = excl_tax * item.quantity
                } else {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
        // case 7
        case 7:
            products && products.map(item => {
                if (item.product_id) {
                    price = cartPriceWithTax(item.old_price, 7)
                    incl_tax = getExclusiveTax(item.old_price)
                    item['Price'] = price * item.quantity
                    item['incl_tax'] = incl_tax * item.quantity
                } else if (item.Price) {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
        // case 8
        default:
            products && products.map(item => {
                if (item.product_id) {
                    excl_tax = getExclusiveTax(item.old_price)
                    item['excl_tax'] = excl_tax * item.quantity;
                } else {
                    item['Price'] = item.Price;
                    item['old_price'] = item.Price
                }
            })
            all_products = products;
            break;
    }

    return all_products
}

export const inclusiveDiscount = (after_discount, discount_amount, old_price) => {
    //console.log("inclusiveDiscount", after_discount, discount_amount, old_price)
    let subtotal = old_price - getInclusiveTax(old_price);
    let price = after_discount + discount_amount;
    let discount = (discount_amount * 100.00) / price;
    let get_discount = (subtotal * discount) / 100.00;

    return get_discount;

}

export const exclusiveDiscount = (after_discount, discount_amount, old_price) => {
    let subtotal = old_price
    let price = after_discount + discount_amount;
    let discount = (discount_amount * 100.00) / price;
    //console.log("exclusiveDiscount", discount)
    let get_discount = (subtotal * discount) / 100.00;
    return get_discount;
}

// apply discount when finaly payment save
export const getDiscountAmount = (products) => {
    let TAX_CASE = getSettingCase()
    var discountIs = 0;
    let after_discount = 0;
    let discount_amount = 0;
    let old_price = 0;

    switch (TAX_CASE) {
        //  case 1  
        case 1: case 2: case 3: case 4:
            products.map(item => {
                if (item.discount_amount !== 0)
                    after_discount += item.after_discount;
                else {
                    after_discount += item.Price;
                }
                discount_amount += item.discount_amount;
                old_price += item.old_price * item.quantity;
            })
            discountIs = inclusiveDiscount(after_discount, discount_amount, old_price);
            break;
        // case 5
        case 6: case 8:
            products.map(item => {
                discountIs += item.discount_amount;
            })
            break;

        default:
            products.map(item => {
                if (item.discount_amount !== 0)
                    after_discount += item.after_discount;
                else {
                    after_discount += item.Price;
                }
                discount_amount += item.discount_amount;
                old_price += item.old_price * item.quantity;
            })
            discountIs = exclusiveDiscount(after_discount, discount_amount, old_price)
            break;
    }

    return discountIs
}

// case for all type of shop tax setting
export const getSettingCase = () => {
    let TaxSetting = localStorage.getItem("TAX_SETTING") ? JSON.parse(localStorage.getItem("TAX_SETTING")) : null;
    var all_products = [];
    let prices_include_tax = TaxSetting ? TaxSetting.pos_prices_include_tax : 'no';
    let tax_display_shop = TaxSetting ? TaxSetting.pos_tax_display_shop : 'excl';
    let tax_display_cart = TaxSetting ? TaxSetting.pos_tax_display_cart : 'excl';
    let tax_round_at_subtotal = TaxSetting ? TaxSetting.pos_tax_round_at_subtotal : 'no';
    let tax_case = 0;

    switch (true) {
        //  case 1  
        case (prices_include_tax == "yes" && tax_display_shop == "incl" && tax_display_cart == "incl"):
            tax_case = 1;
            break;
        // case 2
        case (prices_include_tax == "yes" && tax_display_shop == "incl" && tax_display_cart == "excl"):
            tax_case = 2;
            break;
        // case 3
        case (prices_include_tax == "yes" && tax_display_shop == "excl" && tax_display_cart == "incl"):
            tax_case = 3;
            break;
        // case 4
        case (prices_include_tax == "yes" && tax_display_shop == "excl" && tax_display_cart == "excl"):
            tax_case = 4;
            break;
        // case 5
        case (prices_include_tax == "no" && tax_display_shop == "incl" && tax_display_cart == "incl"):
            tax_case = 5;
            break;
        // case 6
        case (prices_include_tax == "no" && tax_display_shop == "incl" && tax_display_cart == "excl"):
            tax_case = 6;
            break;
        // case 7
        case (prices_include_tax == "no" && tax_display_shop == "excl" && tax_display_cart == "incl"):
            tax_case = 7;
            break;
        // case 8
        default:
            tax_case = 8;
            break;
    }

    return tax_case
}

export const TaxSetting = {
    getTaxAllProduct, productPriceWithTax, getTaxCartProduct, cartPriceWithTax, getInclusiveTax, getExclusiveTax, getCheckoutList, getDiscountAmount, getSettingCase, typeOfTax
}

export default TaxSetting;