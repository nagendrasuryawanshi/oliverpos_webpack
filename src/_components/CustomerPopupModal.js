import React from 'react';
import { connect } from 'react-redux';
// import { ProductAttribute } from '../ShopView/components/ProductAttribute';
//import { cartProductActions } from '../_actions/cartProduct.action';
import { customerActions } from '../CustomerPage/actions/customer.action';
import {encode_UDid,get_UDid} from '../ALL_localstorage'


class CustomerPopupModal extends React.Component {
    constructor(props) {
        super(props);
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID= get_UDid('UDID');
        this.state = {
            user_fname: '',
            user_lname: '',
            user_address: '',
            user_city: '',
            user_contact: '',
            user_notes: '',
            user_email: '',
            user_pcode: '',
            user_id: '',
            UDID:UID,
            add_note: null,
            paid_amount: null,
            store_credit: 0,
            custActive:''
            // order_status : "pending",
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
       
    }   
componentWillReceiveProps(nextprops){

// console.log("single_cutomer_list23",nextprops);
 var customerdata=nextprops.single_cutomer_list.items?nextprops.single_cutomer_list.items.Content:'';

 this.setState({
    //singleItem:item,
    user_address: customerdata.StreetAddress ? customerdata.StreetAddress : '',
    user_city: customerdata.City ? customerdata.City : '',
    user_contact: customerdata.Phone ? customerdata.Phone : '',
    user_email: customerdata.Email ? customerdata.Email : '',
    user_fname: customerdata.FirstName ? customerdata.FirstName : '',
    user_lname: customerdata.LastName ? customerdata.LastName : '',
    user_pcode: customerdata.Pincode ? customerdata.Pincode : '',
    user_note: customerdata.Notes ? customerdata.Notes : '',
    user_id: customerdata.Id ? customerdata.Id : '',
    user_store_credit: customerdata.StoreCredit ? customerdata.StoreCredit : ''
})
    
}
handleSubmit(e) {

    e.preventDefault();
    const { UDID, user_id, user_address, user_city, user_contact, user_email, user_fname, user_lname, user_note, user_pcode } = this.state;
    //console.log("this.state",this.state);
    const { dispatch } = this.props;
    if (UDID && user_id && user_address && user_city && user_contact && user_email && user_fname && user_lname ) {
        //dispatch(userActions.login(username, password));
        const update = {
            Id: user_id,
            FirstName: user_fname,
            LastName: user_lname,
            Contact: user_contact,
            startAmount: 0,
            Email: user_email,
            udid: UDID,
            notes: user_note,
            StreetAddress: user_address,
            Pincode: user_pcode,
            City: user_city
        }

         dispatch(customerActions.save(update));
      //   localStorage.setItem("customerId",null)

        $(".close").click();
        localStorage.setItem('custActive','false')
    }
}

handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
}
closePopup(){
    localStorage.setItem('custActive','false')
    $("#edit-info").modal("hide")
     // console.log("custId",localStorage.getItem('custActive'));
    //  console.log("closepop")

}
 render() {
     return (
<div id="edit-info" className="modal modal-wide fade full_height_one">
<div className="modal-dialog">
    <div className="modal-content">
        <div className="modal-header">
            <button type="button" className="close" >
                <img src="assets/img/delete-icon.png"  onClick={()=>this.closePopup()} />
            </button>
            <h4 className="modal-title">Edit Info</h4>
        </div>
         <div className="modal-body overflowscroll">
            <form className="clearfix form_editinfo">
                <div className="col-sm-6">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                First Name
                             </div>
                            <input className="form-control" value={this.state.user_fname ? this.state.user_fname : ''} id="user_fname" name="user_fname" type="text" onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                Last Name
                              </div>
                            <input className="form-control" value={this.state.user_lname ? this.state.user_lname : ''} id="user_lname" name="user_lname" type="text" onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                Email
                              </div>
                            <input className="form-control"  value={this.state.user_email ? this.state.user_email : ''} id="user_email" name="user_email" type="text" onChange={this.handleChange}  disabled/>
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                Phone Number
                             </div>
                            <input className="form-control" value={this.state.user_contact ? this.state.user_contact : ''} id="user_contact" name="user_contact" type="text" onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
                <div className="col-sm-12">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                Street Address&nbsp;
                             </div>
                            <input className="form-control" value={this.state.user_address ? this.state.user_address : ''} id="user_address" name="user_address" type="text" onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                City&nbsp;&nbsp;
                             </div>
                            <input className="form-control" id="user_city" value={this.state.user_city ? this.state.user_city : ''} name="user_city" type="text" onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                Post Code
                            </div>
                            <input className="form-control" id="user_pcode" name="user_pcode" value={this.state.user_pcode ? this.state.user_pcode : ''} type="text" onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
                <div className="col-sm-12">
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon">
                                Notes
                            </div>
                            <textarea className="form-control" id="user_note" value={this.state.user_note ? this.state.user_note : ''} name="user_note" type="text" onChange={this.handleChange}></textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <div className="modal-footer p-0">
            <button type="button" onClick={this.handleSubmit} className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
        </div>
    </div>
</div>
</div>
     )
 }

}



function mapStateToProps(state) {
    const { favourites, favouritesChildCategoryList, favouritesSubAttributeList, favProductDelete,single_cutomer_list } = state;
    return {
        favourites,
        favouritesChildCategoryList,
        favouritesSubAttributeList,
        favProductDelete,
        single_cutomer_list
    };
}

const connectedCustomerPopupModal = connect(mapStateToProps)(CustomerPopupModal);
export { connectedCustomerPopupModal as CustomerPopupModal };