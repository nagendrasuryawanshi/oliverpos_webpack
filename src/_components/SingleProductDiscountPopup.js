import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../_actions';
import { discountActions } from '../_actions/discount.action'
import { Markup } from 'interweave';

class SingleProductDiscountPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            discountAmount: "",
            discountType: "",
            clearDiscount:false
        }
        this.props.onDiscountAmountChange(0, "Number");
        const { dispatch } = this.props;
        dispatch(discountActions.getAll());
        this.handleDiscount = this.handleDiscount.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }


    calcInp(e) {

        if (e == "c") {
            // console.log("e",e);
            // console.log("this.state.discountAmount",this.state.discountAmount.toString().length);
            if (this.state.discountAmount.toString().length == 1) {
                this.state.discountAmount = "0"
            }
            else {
                const txtValue = this.state.discountAmount.substring(-1, this.state.discountAmount.length - 1);
                this.state.discountAmount = txtValue == "" ? "0" : txtValue;

            }
        }
        else {
            if (e == "." && this.state.discountAmount.toString().indexOf(".") >= 0) {
                //do nothing
            } else {
                this.state.discountAmount = $('#textDis').val() == "0" || $('#textDis').val() == "0.00" ? e : $('#textDis').val() + e.toString();
            }

        }

        $('#textDis').val(this.state.discountAmount);


    }

    handleClose() {
        jQuery('#CalcType').text("$");
        jQuery('#lbPercent').text("%");
        localStorage.removeItem("PRODUCT")
        localStorage.removeItem("SINGLE_PRODUCT")
        this.props.dispatch(cartProductActions.singleProductDiscount()); 
    }


    handleDiscount() {
      /// alert("helllo1")
        let discount_amount = this.state.discountAmount ? this.state.discountAmount : 0;
        var product = this.props.selecteditem.product;
        let clearDis = false;
        var discount_type =  jQuery('#CalcType').text();
        if(discount_type == "%") {
            this.state.discountType = 'Percentage'
        }else{
            this.state.discountType = 'Number'
        }

        console.log("discount_type", discount_type , this.state.discountType)
       
        console.log("this.props.selecteditem", this.props.selecteditem);
        if(this.state.clearDiscount == true && discount_amount == 0){
            clearDis = true
        }
        console.log("clearDis", clearDis, discount_amount);
        if (product == "product") {
            var type = 'product'
            var product = {
                type: 'product',
                discountType: this.state.discountType,
                discount_amount,
                Tax_rate: this.props.taxratelist.TaxRate,
                Id: this.props.selecteditem.id,
                clearDiscount : false
            }
            jQuery('#CalcType').text("$");
            jQuery('#lbPercent').text("%");
            localStorage.setItem("PRODUCT", JSON.stringify(product))
            localStorage.setItem("SINGLE_PRODUCT", JSON.stringify(this.props.selecteditem.item))
            this.props.dispatch(cartProductActions.singleProductDiscount());
        }
    }

    handleDiscountCancle(e) {
        //alert("helllo2")

        // localStorage.removeItem("PRODUCT")
        // localStorage.removeItem("SINGLE_PRODUCT")
        // this.setState({ discountAmount: "0", discountType: "Number" })
        this.setState({
            discountAmount: "0",
            discountType: 'Number',
            clearDiscount:true
        })
        this.state.discountAmount = "0";
        this.state.clearDiscount = true
        // this.state.discountType = "Number";
        jQuery('#textDis').val(this.state.discountAmount);
        this.props.onDiscountAmountChange("0", "Number");
        $('#panelCalculatorpopUp :input').removeAttr('disabled');
    }

    applyfixDiscount(item) {
        //alert("helllo3")

        // console.log("applyfixDiscount",item);
        $('#panelCalculatorpopUp :input').attr('disabled', true);
        jQuery('#panelCalculatorpopUp').val("0");
        jQuery('#textDis').val("0");
        if (item) {
            this.setState({
                discountType: item.Type,
                discountAmount: item.Amount
            })

            this.props.onDiscountAmountChange(item.Amount, item.Type);    //Percentage / Number   
        }
    }

    minplus() {
        if (jQuery('#CalcType').text() == "%") {
            jQuery('#CalcType').text("$");
            jQuery('#lbPercent').text("%");
            // this.setState({
            //     discountAmount: "0",
            //     discountType: "Number"
            // })
            this.state.discountType = "Number";
        } else {
            jQuery('#lbPercent').text("$");
            jQuery('#CalcType').text("%");
            // this.setState({
            //     discountAmount: "0",
            //     discountType: "Percentage"
            // })
            this.state.discountType = "Percentage";
            this.props.onDiscountAmountChange(this.state.discountAmount, "Percentage");
        }

    }

    render() {
        const { selecteditem } = this.props;
        // var title = selecteditem ? selecteditem.item.Title.length > 34 ? selecteditem.item.Title.substring(0, 31) + "..." : selecteditem.item.Title:"";

        // console.log("title",title)
        return (
            <div id="single_popup_discount"  className="modal modal-wide modal-wide1 fade">
                <div className="modal-dialog" id="dialog-midle-align">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true" onClick={this.handleClose} >
                                <img src="assets/img/delete-icon.png" />
                            </button>
                            <h5 className="modal-title" style={{fontSize:"30px"}}>{selecteditem ? selecteditem.card ? "" : selecteditem.item.Title ?    <Markup content={ "Add Discount (" +selecteditem.item.Title+ ")" }  />   : "Add Discount" : "Add Discount"}</h5>
                        </div>
                        <div className="modal-body p-0">
                            <form className="clearfix">
                                <div className="col-sm-5">
                                    <div className="fixedinCalHeight overflowscroll">
                                        <div className="pt-3">
                                            {

                                                this.props.discountlist ?

                                                    this.props.discountlist.map((item, index) => {
                                                        return (
                                                            <div key={index} className="button_with_checkbox">
                                                                <a type="button" onClick={() => this.applyfixDiscount(item)} id="oliver_discount" name="radio-group">
                                                                    <input type="radio" id={"oliver_discount" + index} name="radio-group" />
                                                                    <label htmlFor={"oliver_discount" + index} className="label_select_button">{(item.Name.length > 15 ? item.Name.substring(0, 15) + '...' : item.Name) + (item.Type == "Percentage" ? item.Amount + "%" : item.Type == "Number" ? item.Amount : "")}</label>
                                                                </a>
                                                            </div>
                                                        )
                                                    })
                                                    : <div></div>

                                            }
                                            <div className="button_with_checkbox">
                                                <a type="button" onClick={() => this.handleDiscountCancle('c')} id="oliver_discount" name="radio-group">
                                                    <input type="radio" id="oliver_discount_clear" name="radio-group" />
                                                    <label htmlFor="oliver_discount__clear" className="label_select_button">Clear Discount</label>
                                                </a>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                                <div className="col-sm-7 p-0">
                                    <div className="panel-product-list" id="panelCalculatorpopUp">
                                        <div className="panel panelCalculator">
                                            <div className="panel-body p-0">
                                                <table className="table table-bordered shopViewPopUpCalculator">
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="2" className="text-right br-1 bl-1 bt-0">
                                                                <div className="input-group discount-input-group">
                                                                    <input type="text" id="textDis" className="form-control text-right" placeholder="0.00" aria-describedby="basic-addon1" />
                                                                    <span className="input-group-addon AmoutType" id="CalcType" name="CalcType">$</span>
                                                                </div>
                                                            </td>
                                                            <td className="text-center pointer bt-0" onClick={() => this.calcInp('c')}>

                                                                <button type="button" className="btn btn-default calculate">
                                                                    <img width="36" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zMC4xNzEsMTYuNDE2SDAuODg4QzAuMzk4LDE2LjQxNiwwLDE2LjAyLDAsMTUuNTI5YzAtMC40OSwwLjM5OC0wLjg4OCwwLjg4OC0wLjg4OGgyOS4yODMgICAgYzAuNDksMCwwLjg4OCwwLjM5OCwwLjg4OCwwLjg4OEMzMS4wNTksMTYuMDIsMzAuNjYxLDE2LjQxNiwzMC4xNzEsMTYuNDE2eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTE2LjAxNywzMS4wNTljLTAuMjIyLDAtMC40NDUtMC4wODMtMC42MTctMC4yNUwwLjI3MSwxNi4xNjZDMC4wOTgsMTUuOTk5LDAsMTUuNzcsMCwxNS41MjkgICAgYzAtMC4yNCwwLjA5OC0wLjQ3MSwwLjI3MS0wLjYzOEwxNS40LDAuMjVjMC4zNTItMC4zNDEsMC45MTQtMC4zMzIsMS4yNTUsMC4wMmMwLjM0LDAuMzUzLDAuMzMxLDAuOTE1LTAuMDIxLDEuMjU1TDIuMTYzLDE1LjUyOSAgICBsMTQuNDcxLDE0LjAwNGMwLjM1MiwwLjM0MSwwLjM2MSwwLjkwMiwwLjAyMSwxLjI1NUMxNi40OCwzMC45NjgsMTYuMjQ5LDMxLjA1OSwxNi4wMTcsMzEuMDU5eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="></img>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="td-calc-padding br-1 bl-1">
                                                                <button type="button" onClick={() => this.calcInp(1)} className="btn btn-default calculate">1</button>
                                                            </td>
                                                            <td className="td-calc-padding br-1">
                                                                <button type="button" onClick={() => this.calcInp(2)} className="btn btn-default calculate">2</button>
                                                            </td>
                                                            <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(3)} className="btn btn-default calculate">3</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="td-calc-padding br-1 bl-1">
                                                                <button type="button" onClick={() => this.calcInp(4)} className="btn btn-default calculate">4</button>
                                                            </td>
                                                            <td className="td-calc-padding br-1">
                                                                <button type="button" onClick={() => this.calcInp(5)} className="btn btn-default calculate">5</button>
                                                            </td>
                                                            <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(6)} className="btn btn-default calculate">6</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="td-calc-padding br-1 bl-1">
                                                                <button type="button" onClick={() => this.calcInp(7)} className="btn btn-default calculate">7</button>
                                                            </td>
                                                            <td className="td-calc-padding br-1">
                                                                <button type="button" onClick={() => this.calcInp(8)} className="btn btn-default calculate">8</button>
                                                            </td>
                                                            <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(9)} className="btn btn-default calculate">9</button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="td-calc-padding br-1 bl-1" >
                                                                <button type="button" onClick={() => this.minplus()} className="btn btn-default calculate"> <label id="lbPercent">%</label></button>

                                                            </td>
                                                            <td className="td-calc-padding br-1">
                                                                <button type="button" onClick={() => this.calcInp('.')} className="btn btn-default calculate">.</button>
                                                            </td>
                                                            <td className="td-calc-padding">
                                                                <button type="button" onClick={() => this.calcInp(0)} className="btn btn-default calculate">0</button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" onClick={() => this.handleDiscount()} >ADD DISCOUNT</button>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}

function mapStateToProps(state) {
    const { discountlist } = state.discountlist;
    const { selecteditem } = state.selecteditem;

    return {
        discountlist,
        selecteditem,
    };
}

const connectedSingleProductDiscountPopup = connect(mapStateToProps)(SingleProductDiscountPopup);
export { connectedSingleProductDiscountPopup as SingleProductDiscountPopup };