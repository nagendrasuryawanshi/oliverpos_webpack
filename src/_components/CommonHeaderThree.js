import React from 'react';
import { connect } from 'react-redux';


class CommonHeaderThree extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            backUrl: history.state.state ? history.state.state : null
        }


    }
    componentDidMount() {
        // const page  = this.props;


    }
    render() {
        const { match } = this.props;
        return (
            <nav className="navbar navbar-default" id="colorFullHeader">
                <div className="col-lg-3 col-sm-4 col-xs-4 pl-0 cart_header_overlap cart_left">
                    {/* Check Out */}
                    {this.state.backUrl && this.state.backUrl != null ?
                        <div className="cart_header">
                            <a href={this.state.backUrl} className="ch_icon">
                                <button className="button icon icon-backarrow-head"></button>
                            </a>
                            <div className="ch_heading">
                                &nbsp;
                            Go Back
                        </div>
                        </div>
                        : null
                    }

                </div>


            </nav>
        )
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedCommonHeaderThree = connect(mapStateToProps)(CommonHeaderThree);
export { connectedCommonHeaderThree as CommonHeaderThree };