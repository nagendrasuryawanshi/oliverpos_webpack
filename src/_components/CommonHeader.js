import React from 'react';
import { connect } from 'react-redux';


class CommonHeader extends React.Component {

    componentDidMount(){
       // const page  = this.props;
        
    }
    action(){
       //window.location = '/activity' ;
    }
    render() {

       const { match }  =  this.props;
        return (
            <nav className="navbar navbar-default" id="colorFullHeader">
                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">  
                      
                        {/* Activity View */}  
                    {match.path == '/activity'?
                        <div className="container-fluid p-0">
                        <div className="navbar-header">
                            <button onClick={()=>this.action()} type="button" id="sidebarCollapse" className="navbar-btn active">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        <div className="mobile_menu clearfix">
                            <ul>
                                <li  className="active">
                                    <a href="/activity">Activity View</a>
                                </li>
                            </ul>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-left">
                                <li className="active"><a href="/activity">Activiy View</a></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                            </ul> 
                        </div>
                        </div>
                     :null}

                      {/* Cash Report */}
                     {match.path == '/cash_report'?
                        <div className="container-fluid p-0">
                        <div className="navbar-header">
                            <button type="button" id="sidebarCollapse" className="navbar-btn active">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        <div className="mobile_menu clearfix">
                            <ul>
                                <li  className="active">
                                    <a href="/cash_report">Cash Report</a>
                                </li>
                            </ul>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-left">
                                <li className="active"><a href="/cash_report">Cash Report</a></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                            </ul> 
                        </div>
                        </div>
                     :null}

                     {/* General */}

                     {match.path == '/setting'?
                        <div className="container-fluid p-0">
                        <div className="navbar-header">
                            <button type="button" id="sidebarCollapse" className="navbar-btn active">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        <div className="mobile_menu clearfix">
                            <ul>
                                <li  className="active">
                                    <a href="/setting">General</a>
                                </li>
                            </ul>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-left">
                                <li className="active"><a href="/setting">General</a></li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                            </ul> 
                        </div>
                        </div>
                     :null}

                  
                </div>    
            </nav>
        )
    }
}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedCommonHeader = connect(mapStateToProps)(CommonHeader);
export { connectedCommonHeader as CommonHeader };