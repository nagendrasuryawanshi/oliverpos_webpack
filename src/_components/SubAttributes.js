import React from 'react';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { cartProductActions } from '../_actions/cartProduct.action'
import { Attributes } from './Attributes';
import { Markup } from 'interweave';
import { default as NumberFormat } from 'react-number-format'
import { openDb, deleteDb } from 'idb';
import {encode_UDid,get_UDid} from '../ALL_localstorage'
import { getTaxAllProduct } from './'
import { allProductActions } from '../_actions/allProduct.action'
class SubAttributes extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            subattributelist: this.props.subattributelist,
            productlist:[],// JSON.parse(  localStorage.getItem("Productlist-"+localStorage.getItem['UDID']) ),// JSON.parse(localStorage.getItem('Productlist')),
            code: this.props.subattributelist.Code,
            subProduct: [],
            sub_active: false,
            parentAttributes:[],
            backToAttribute:false,
            ticket_Product_status:false
        }
       // console.log("localstorage", JSON.parse(  localStorage.getItem("Productlist-"+localStorage.getItem['UDID']) ));// JSON.parse(localStorage.getItem('Productlist')));
            console.log("code123", this.state.subattributelist);

        // -----------------------------------------------------
       // var udid= localStorage.getItem('UDID');
    //    let decodedString = localStorage.getItem('UDID');
    //    var decod=  window.atob(decodedString);
       var udid= get_UDid('UDID');
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
          }); 
          const idbKeyval = {
            async get(key) {
              const db = await dbPromise;
              return db.transaction(udid).objectStore(udid).get(key);
            },            
          };        
          idbKeyval.get('ProductList').then(val => 
            {                
               this.setState({ productlist: getTaxAllProduct(val) });             
            }
            ); 
        //  -----------------------------------------------------
    }

    loadSubAttr(subAttrlist,parentAttributes) {
        console.log("loadAttributes",this.state.parentAttributes)
        this.setState({
            subattributelist: subAttrlist,
            parentAttributes:parentAttributes
        })

    }

    Back() {
      //  console.log("parentAttributes",this.state.parentAttributes)

        this.setState({subProduct:[]});
        if(! this.state.parentAttributes || this.state.parentAttributes==null ||this.state.parentAttributes.length==0)     
        {    
            this.setState({backToAttribute:true})        ;
            //history.go();
           // this.props.hendleBack()
        }
        else{
            this.setState({sub_active:false});
            this.loadSubAttr(this.state.parentAttributes);
        }            
      
    }

    componentDidMount() {
        setTimeout(function () {
            setHeightDesktop();
        }, 500);
    }
    loadSubAttribute(item, code,parentAttributes) {  
        console.log("this.function");   
       this.setState({     
            parentAttributes:parentAttributes
        })
        var code = code
      
        var subattricode = item.Code
        var prdList = [];

        this.state.productlist && this.state.productlist.map(prod => {
            //prod.has_attribute.map(att => {
                var isExpired=false;

            prod.ProductAttributes.map(att => {
                
                this.setState({ sub_active: true })

                //if (att == code) {
                if (att.Name.toUpperCase() === code.toUpperCase() || att.Slug.toUpperCase() === code.toUpperCase()) {
                    att.Option.split(',').map(hasub => {              
                        if (hasub.toUpperCase() === subattricode.toUpperCase() ) {


                            var variationProdect=  this.state.productlist.filter(item=>{ 
                                return  (item.ParentId===prod.WPID && ( item.ManagingStock ==false ||( item.ManagingStock ==true && item.StockQuantity>-1)))    
                              })
                
                            // console.log("FilterVariation",variationProdect,product);
                            prod['Variations'] = variationProdect

                                this.state.subProduct.push(prod)
                             //   console.log("sab attribute match ho rahe11111",prod );
                               /// console.log("sab attribute match",prod.IsTicket );

                             if( prod.IsTicket && prod.IsTicket==true){
                                console.log("sab attribute match ho rahe",prod );
                                var ticketInfo= JSON.parse(prod.TicketInfo);
                                // ticketInfo._ticket_checkin_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)
                                 if(ticketInfo._ticket_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)){
                                  
                                     var dt = new Date(ticketInfo._ticket_availability_to_date);  
                                     var expirationDate=  dt.setDate( dt.getDate() +1 );  
                  
                                     var currentDate= new Date();// moment.utc(new Date)
                                     // console.log("currentDate",currentDate)
                                     // console.log("expirationDate", new Date(expirationDate))
                                     // console.log("TicketDate",expirationDate,ticketInfo._ticket_availability_to_date)
                  
                                     if( currentDate> expirationDate)
                                     {
                                         isExpired=true;
                                     }
                                 }
                            }
                          //  console.log("sab attribute match ho rahe", this.state.subProduct);

                        }

                    })

                }
            })
          console.log("isExpired", isExpired);

            if(isExpired==false){
                prdList.push(prod)
            }
        })

        if (prdList.length > 0) {
            console.log("prdList",prdList)
            this.state.productlist = prdList;
            this.setState({ productlist: prdList })
        }
        setTimeout(function () {
            setHeightDesktop();
        }, 500);
    }
    getTicketFields(product,tick_type=null) {
        console.log("getTicketFields",product);
        var tick_data= JSON.parse(product.TicketInfo)
        //var  udid=4040246357
        var   form_id=tick_data._owner_form_template
        console.log("ticketfield1",form_id);

       this.props.dispatch(allProductActions.ticketFormList(form_id));
        this.state.ticket_Product_status=true
       this.state.tick_type=tick_type
       this.state.ticket_Product=product
       this.setState({
           ticket_Product:product,
           ticket_Product_status:true,

          // tick_type:'simpleadd'
         })

       // }
    }

  ActiveList(item,ticketFields=null){
     /// console.ticketFields=nulllog("ActiveList",item);
    var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []

    //  var cartlist=cartproductlist?cartproductlist:[];//this.state.cartproductlist;    
    if (item.Type !== "variable") {
            //for inclusive and axclusive tax------------------
            var arr=[]
            arr.push(item);
            arr= getTaxAllProduct(arr) ;              
            item=arr[0];
            //-------------------------------------------
          //  ticketFields==null &&
            if ( ticketFields==null &&  item  && item.IsTicket == true ) {
                console.log("first condition");
                  var tick_typ='simpleadd' 
                 
                this.getTicketFields(item,tick_typ);

            }

        if (item.InStock == true &&  item.IsTicket == false) {
            console.log("second condition");
            
            var data = {
                line_item_id: 0,
                quantity: 1,
                Title: item.Title,
                Price: parseFloat(item.Price),
                product_id: item.WPID,
                variation_id: 0,
                isTaxable: true,

                after_discount: item.after_discount,
                    cart_after_discount: item.cart_after_discount,
                    cart_discount_amount: item.cart_discount_amount,
                    discount_amount: item.discount_amount,
                    excl_tax: item.excl_tax,
                    incl_tax: item.incl_tax,  
                    old_price: item.old_price,
                    product_after_discount: item.product_after_discount,
                    product_discount_amount: item.product_discount_amount,
                    ticket_status:item.IsTicket,

            }
console.log("Data",data);

            localStorage.setItem('Favproduct', null)
            var  product=item
            //console.log("product",product);         
             var qty=0;
             cartlist.map(item=>{
               //  console.log("qty",item);       
                 if(product.WPID==item.product_id){
                     qty=  item.quantity;
                  //   console.log("qty",qty);         
                  }                   
                 
             })
             // console.log("qty1",qty);                       
          if( (product.StockStatus==null || product.StockStatus== 'instock') && ( product.ManagingStock==false || (product.ManagingStock==true && qty<product.StockQuantity)) ){
           // console.log("stock exist");
            cartlist.push(data)
            this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));
     
             }else{
                //   alert("stcock Quantity exceed");
                $('#outOfStockModal').modal('show')

          }

       
        }
        else if(item.InStock == true &&  item.IsTicket == true && ticketFields!=null){
                 console.log("third condition")
            this.setState({ticket_Product_status:false})
             var tick_data=item && item.TicketInfo != ''?JSON.parse(item.TicketInfo):'';
                var data = {
                    line_item_id: 0,
                    quantity: 1,
                    Title: item.Title,
                    Price: parseFloat(item.Price),
                    product_id: item.WPID,
                    variation_id: 0,
                    isTaxable: true,
    
                    after_discount: item.after_discount,
                        cart_after_discount: item.cart_after_discount,
                        cart_discount_amount: item.cart_discount_amount,
                        discount_amount: item.discount_amount,
                        excl_tax: item.excl_tax,
                        incl_tax: item.incl_tax,  
                        old_price: item.old_price,
                        product_after_discount: item.product_after_discount,
                        product_discount_amount: item.product_discount_amount,
                        ticket_status:item.IsTicket,
                        tick_event_id:tick_data._event_name,
                        product_ticket: ticketFields,
                           
                }
                localStorage.setItem('Favproduct', null)
                var  product=item
                //console.log("product",product);         
                 var qty=0;
                 cartlist.map(item=>{
                   //  console.log("qty",item);       
                     if(product.WPID==item.product_id){
                         qty=  item.quantity;
                      //   console.log("qty",qty);         
                      }                   
                     
                 })
                 // console.log("qty1",qty);                       
              if( (product.StockStatus==null || product.StockStatus== 'instock') && ( product.ManagingStock==false || (product.ManagingStock==true && qty<product.StockQuantity)) ){
               // console.log("stock exist");
                cartlist.push(data)
                this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));
         
                 }else{
                    //   alert("stcock Quantity exceed");
                    $('#outOfStockModal').modal('show')
    
              }
    

          }else {

            $('#outOfStockModal').modal('show'); 
           
        }
    }
      if(item.Type == "variable"){
           console.log("variable condition1")
           if ( item  && item.IsTicket == true ) {
            console.log("first condition1");
             
            this.getTicketFields(item);

        }
        if(item.InStock == true) 
        {
            
           var product= item
            this.props.productData(product);
            $('#VariationPopUp').modal('show');


         } else {

          $('#outOfStockModal').modal('show'); 
           
          }
      }

}

componentWillReceiveProps(props) {
  
   // console.log("propsa",this.props.ticketfield,"his.state.ticket_Product",this.state.ticket_Product);
    console.log("ticketfieldListfavourite",localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'',"this.state.ticket_Product_status",this.state.ticket_Product_status);

    var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''
    var tick_data=this.state.ticket_Product_status == true? JSON.parse(this.state.ticket_Product.TicketInfo):''
    var   form_id=tick_data._owner_form_template
  //  console.log("call simple1" ,form_id)

if(localStorage.getItem('ticket_list') && localStorage.getItem('ticket_list') !=='null' && localStorage.getItem('ticket_list') !== '' && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd' || form_id == -1 || form_id == '' && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd'   ){  
   console.log("call simple")
   this.setState({ticket_Product_status:false})

 //  var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''
     // console.log("this.state.ticket_Product_status",this.state.ticket_Product_status,ticket_Data)
     var index=null;
     var type=null;
   this.ActiveList(this.state.ticket_Product,localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'')
   }
} 
    render() {
       
        const { subattributelist } = this.state;
        console.log("RenderSubAttributes",subattributelist,this.state.sub_active);
        return (
            <div>
            {this.state.backToAttribute==false? 
            <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                <div className="items pt-3">
                    <div className="item-heading text-center">Library</div>
                    <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                        <div className="searchDiv" style={{ display: 'none' }}>
                            <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan" />
                        </div>

                        <div className="pl-1 pr-4 previews_setting  pointer">
                            <a onClick={() => this.Back()} className="back-button " id="mainBack">
                                <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                <span>Back</span>
                            </a>
                        </div>

                        <table className="table ShopProductTable  table-striped table-hover table-borderless paddignI mb-0 font">
                            <colgroup>
                                <col style={{ width: '*' }} />
                                <col style={{ width: 40 }} />
                            </colgroup>


                            <tbody>

                                {this.state.sub_active == false ? (
                                    (subattributelist.SubAttributes && subattributelist.SubAttributes.length > 0 ? subattributelist.SubAttributes.map((item, index) => {
                                         ///  console.log("subattributelist",item);
                                        return (
                                            item.SubAttributes && item.SubAttributes.length > 0 ?
                                                <tr  className="pointer"  key={index} data-toggle="modal" onClick={() => this.loadSubAttr(item.SubAttributes,subattributelist)}>

                                                    <td>{item.Value ? item.Value : 'N/A'}</td>
                                                    <td className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                </tr>
                                                :
                                                <tr key={index} data-toggle="modal" onClick={() => this.loadSubAttribute(item, this.state.code,subattributelist)}>
                                                    <td>{item.Value ? item.Value : 'N/A'}</td>
                                                    <td className="text-right"></td>
                                                    <td className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                </tr>
                                        )
                                    })
                                        :
                                        <tr data-toggle="modal"><td>No Data Found</td></tr>)
                                ) :

                                    this.state.subProduct && this.state.subProduct.length > 0 ? this.state.subProduct.map((item, index) => {
                                        console.log("hello tilemodal",item);
                                        return (

                                            <tr key={index} data-toggle="modal">

                                                <td>{item.Title ?<Markup content={item.Title} />: 'N/A'}</td>
                                                <td className="text-right"><NumberFormat value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></td>
                                                <td className="text-right" data-toggle="" href="javascript:void(0)"><a><img src="assets/img/add_29.png" className="pointer"
                                                 onClick={() => this.ActiveList(item)} /></a></td>
                                            </tr>
                                        )

                                    })
                                        :
                                        <tr data-toggle="modal"><td style={{textAlign:'center'}}>No Data Found</td></tr>
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
             :  <Attributes   productData={this.props.productData} onRef={this.props.onRef}  loadAttributes={this.ActiveList}/>
           }
            </div>
        )
    }

}

// export  default SubAttributes;

function mapStateToProps(state) {
    const { attributelist } = state.attributelist;
    const { ticketfield } = state.ticketfield;

    return {
        attributelist,
        ticketfield
    };
}

const connectedList = connect(mapStateToProps)(SubAttributes);
export { connectedList as SubAttributes };