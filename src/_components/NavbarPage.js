import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions/user.actions';
import { activityActions } from '../ActivityPage/actions/activity.action';
import { customerActions } from '../CustomerPage/actions/customer.action';
import {encode_UDid,get_UDid} from '../ALL_localstorage'


class NavbarPage extends React.Component {
    constructor(props){
        super(props);
       
        this.state={
            manager_name:'',
            managerData : localStorage.getItem('user'),
            shop_name:'',
            opemIfram:''
        }
     //   let decodedString = localStorage.getItem('UDID');
    ///    var decod=  window.atob(decodedString);
        var udid= get_UDid('UDID');
        this.props.dispatch(activityActions.getOne(udid, 1));
      //  this.props.dispatch(customerActions.getAll(localStorage.getItem('UDID')));
    }

    componentDidMount(){
       // const page  = this.props;
       const { managerData } = this.state;
       if(!managerData)
        {
            this.setState({ manager_name:"",
            shop_name:"",
        })
        }
      else if( managerData && managerData.display_name !== " " && managerData.display_name !== 'undefined'){
           this.setState({
               manager_name:managerData.display_name,
               shop_name:managerData.shop_name,
            })
        }else{
            this.setState({
                manager_name:managerData.user_email,
                shop_name:managerData.shop_name,
            })
        }
       
    }

    logout(){
        this.props.dispatch(userActions.logout())
    }
    supportPopup(url) {
        // alert("hello");
       var newwindow=window.open(url,'name',"resizable=yes,top=180,left=400,width=800,height=400");
        if (window.focus) {newwindow.focus()}
        return false;
    //  var iframe = $("#forPostyouradd");
    //  console.log("iframe",iframe);
    //  iframe.attr("src", iframe.dataset("src")); 
    // this.setState({opemIfram:true})
    }
    

    render() {
       const { match , user }  =  this.props;
       const { manager_name ,shop_name  }  =  this.state;
           /* Younique Shoes    */
       //console.log("user",user)
      console.log("this.state.opemIfram",this.state.opemIfram)
      var registerName= localStorage.getItem('registerName');
      var locattionName=localStorage.getItem("LocationName");

          /* Younique Shoes    */
             /* <p>'/'Register 1 St. John´s/Register 1</p> */
        return (
            <nav id="sidebar" className="sidebarChanges active cm-menus">
                <div className="cm-switcher clearfix">

                <div className="cm-user">
                    <h4>
                        {this.state.shop_name?this.state.shop_name:"Oliver POS"}
                    </h4>
                    <p>{registerName}/{user && user.display_name!="" && user.display_name!=" "? user.display_name:  user ?user.user_email:""} </p>
                </div>
                  {/* <div className="cm-user">
                        {user.display_name}
                        <h4>{user.user_email}</h4>
                            <p>{user.shop_name}</p>
                        </div> */}
                      {/* <div className="cm-user-switcher">
                        <div className="flat-toggle cm-flat-toggle">
                            <span data-open="Open" data-close="Close"></span>
                            <input value="0" type="hidden" />
                        </div>
                    </div> */}
                </div>
                <div className="cm-body">
                    <div className="cm-body-init-scroll overflowscroll">
                        <ul className="list-unstyled ulsidebar mb-0 pl-0">
                            <li className={'pt-2 pb-2' + (match.path=='/register'||match.path=='/shopview'?' active ': null)} >
                                <a href="/shopview">
                                    <span className="strip_icon register"></span>
                                    <span className="txt_menu animated fadeInUp">Register</span>
                                </a>
                            </li>
                            <li className={'pt-2 pb-2' + (match.path=='/activity'?' active ': null )}>
                                <a href="/activity">
                                    <span className="strip_icon activity"></span>
                                    <span className="txt_menu animated fadeInUp">Activity View</span>
                                </a>
                            </li>
                            {/* <li className={'pt-2 pb-2' + (match.path=='/cash_report'?' active ': null )}>
                                <a href="/cash_report">
                                    <span className="strip_icon cashdrable"></span>
                                    <span className="txt_menu animated fadeInUp">Cash Drawer</span>
                                </a>
                            </li> */}
                            {/* <li className={'pt-2 pb-2' + (match.path=='/setting'?' active ': null )}>
                                <a href="/setting">
                                    <span className="strip_icon setting"></span>
                                    <span className="txt_menu animated fadeInUp">Settings</span>
                                </a>
                            </li> */}
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="cm-sec-acc clearfix">
                    <div className="cm-sec-accTop">
                        <ul className="list-unstyled ulsidebar mb-0 pl-0">
                            {/* <li className="">
                                <a href="/login">
                                    <span className="strip_icon wp_panel"></span>
                                    <span className="txt_menu animated fadeInUp">WP Panel</span>
                                </a>
                            </li> */}
                            <li className='w-50'  onClick={()=> this.supportPopup('http://help.oliverpos.com/')}>
                                <a href="javascript:void(0)">
                                    <span className="strip_icon support"></span>
                                    <span className="txt_menu animated fadeInUp" >
                                    Support</span>
                                </a>
                            </li>
                            <li className='w-50' onClick={()=>this.logout()}>
                                <a href="javascript:void(0)">
                                    <span className="strip_icon logout"></span>
                                    <span className="txt_menu animated fadeInUp">Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                  {/*   <div className="cm-sec-accBottom clearfix">
                        <div className="secmk secmka">
                            <button className="swipclock">
                                MK
                            </button>
                            <div className="simply-countdown-one"></div>
                        </div> 
                    </div>*/}
                </div>
                {/* {this.state.opemIfram == true? */}
                 {/* <div>
                <iframe id="forPostyouradd" data-src="http://www.w3schools.com" src="about:blank"  style={{backgroundColor:'#ffffff',width:'500', height:'500'}} allowFullScreen></iframe>    
            </div>  */}
                {/* :""} */}
            </nav>

        )
    }
}


function mapStateToProps(state) {
    const { authentication , activities} = state;
    return {
        user:authentication.user,
        activities:activities.activities
    };
}

const connectedNavbarPage = connect(mapStateToProps)(NavbarPage);
export { connectedNavbarPage as NavbarPage };