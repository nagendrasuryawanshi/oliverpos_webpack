import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../_actions/cartProduct.action';
import BarcodeReader from 'react-barcode-reader'
import { openDb } from 'idb';
import {  get_UDid } from '../ALL_localstorage'

class CommonHeaderTwo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ItemCount: 0,
            input: '',
            activeFilter: true,
            result: 'No result',
            updateProductIs:false
        }
        this.filterProduct = this.filterProduct.bind(this)
        this.handleScan = this.handleScan.bind(this)

    }

    filterProduct() {
        let input = $("#product_search_field").val();
        //console.log("input",input);
        //console.log("input length", input.length)
        if (input.length > 2 || input.length == 0) {
            this.props.searchProductFilter(input, "product-search");
        }
    }

    removeCheckOutList() {
        localStorage.removeItem('CHECKLIST');
        localStorage.removeItem('oliver_order_payments');
        localStorage.removeItem('AdCusDetail')
        //localStorage.setItem('AdCusDetail', null)
        const { dispatch } = this.props;
        localStorage.removeItem("CART")
        localStorage.removeItem('CARD_PRODUCT_LIST');
        localStorage.removeItem("PRODUCT")
        localStorage.removeItem("SINGLE_PRODUCT")
        dispatch(cartProductActions.addtoCartProduct(null));
        setTimeout(function () {
            location.reload()
        }, 500)

    }

    componentDidUpdate() {
    }

    handleItemCount() {
        this.setState({ discountAmount: amt, discountType: type });
        let CartItem = JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST"));
        if (CartItem) {
            this.setState({ ItemCount: CartItem.length });
        }
    }

    clearInput() {


        this.filterProduct();



    }

    handleScan(data) {
        //alert()
        console.log("barcoededata", data);
        this.setState({
            result: data,
        })
        var scanBarcode = data
        var productlist = []; // JSON.parse(  localStorage.getItem("Productlist-"+localStorage.getItem('UDID')) ); //  JSON.parse(localStorage.getItem('Productlist'))
        //--------------------------------------------------------------
        // var udid = localStorage.getItem('UDID');
        // let decodedString = localStorage.getItem('UDID');
        //var decod=  window.atob(decodedString);
        var udid = get_UDid('UDID');
        console.log("productall", udid);
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });
        console.log("dbPromise", dbPromise);

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },

        };
        console.log("idbKeyval", idbKeyval);
        idbKeyval.get('ProductList').then(val => {
            this.setState({ productlist: val });
            console.log("")
        }
        );


        ///Search in index DB ============
        const request = window.indexedDB.open('ProductDB', 1);
        request.onsuccess = () => {
            const db = request.result;
            const transaction = db.transaction(udid, 'readwrite');
            const invStore = transaction.objectStore(udid);
            const cursorRequest = invStore.openCursor();
            var isItemexist = false;
            cursorRequest.onsuccess = e => {
                const cursor = e.target.result;
                console.log("cursor", cursor)
                if (cursor) {
                    console.log("cursor.value", cursor.value)

                    cursor.value.map(item => {


                        if (scanBarcode && item.Barcode && (scanBarcode === item.Barcode || scanBarcode === item.Sku)) {
                            console.log("barcode match", scanBarcode, item);

                            isItemexist = true;
                            if (item.Type !== "variable" && item.Type !== "variation") {
                                console.log("its a simple product");
                                var data = {
                                    line_item_id: 0,
                                    quantity: 1,
                                    Title: item.Title,
                                    Price: parseFloat(item.Price),
                                    product_id: item.WPID,
                                    variation_id: 0,
                                    isTaxable: true,
                                }
                                var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
                                var qty = 0;
                                cartlist.map(items => {
                                    console.log("cart me data hain");
                                    if (item.WPID == items.product_id) {
                                        qty = items.quantity;
                                    }
                                })
                                if ((item.StockStatus == null || item.StockStatus == 'instock') &&
                                    (item.ManagingStock == false || (item.ManagingStock == true && qty < item.StockQuantity))) {
                                    console.log("ManagingStockt", item.ManagingStock);
                                    cartlist.push(data);
                                    this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));   // this.state.cartproductlist
                                } else {

                                    $('#outOfStockModal').modal('show');

                                    //   alert("stcock Quantity exceed");                        
                                }

                            } else if (item.Type == "variable") {
                                console.log("its a variable product");
                                if (item.ProductAttributes !== '' && item.ProductAttributes.length > 0) {
                                    console.log("its  not null attribute");

                                    var variationProdect = cursor.value.filter(filterItem => {
                                        return (filterItem.ParentId === item.WPID)
                                    })

                                    // console.log("FilterVariation",variationProdect,product);
                                    item['Variations'] = variationProdect
                                    this.props.productData(item);

                                    //this.handleProductData(item);
                                    $('#variationProductPopup').modal('show');

                                    console.log("its a not null attribute", this.state.getVariationProductData, "hasVariationProductData", this.state.hasVariationProductData);

                                } else {

                                    console.log("its null attribute");
                                    var data = {
                                        line_item_id: 0,
                                        quantity: 1,
                                        Title: item.Title,
                                        Price: parseInt(item.Price),
                                        product_id: item.ParentId,
                                        variation_id: item.WPID,
                                        isTaxable: item.Taxable,
                                    }
                                    var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
                                    var qty = 0;
                                    cartlist.map(items => {
                                        console.log("cart me data hain");
                                        if (item.WPID == items.variation_id) {
                                            qty = items.quantity;
                                        }
                                    })
                                    if ((item.StockStatus == null || item.StockStatus == 'instock') &&
                                        (item.ManagingStock == false || (item.ManagingStock == true && qty < item.StockQuantity))) {
                                        console.log("ManagingStockt", item.ManagingStock);
                                        cartlist.push(data);
                                        this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));   // this.state.cartproductlist
                                    } else {
                                        $('#outOfStockModal').modal('show');

                                        // alert("stcock Quantity exceed");

                                    }

                                }
                                //  block of code to be executed if the condition1 is false and condition2 is true
                            }
                        }

                        if (isItemexist == true) {
                            console.log("match barcode product variation", item);
                            // this.props.productlist.productlist && this.props.productlist.productlist.map(item =>{ 
                            if (item.Type == "variation") {
                                console.log("type variation", item);
                                let VariableProduct = []
                                VariableProduct.push(item)
                                // item && item.map(variab => {
                                console.log("scanBarcode123", VariableProduct);
                                console.log("scanBarcode12", scanBarcode == item.Barcode, scanBarcode, item.Barcode);

                                if (item.Barcode && scanBarcode && scanBarcode == item.Barcode || scanBarcode == item.Sku) {
                                    console.log("match variation", scanBarcode, item.Barcode);
                                    let setQunatity = 1;
                                    isItemexist = true;
                                    var data = {
                                        line_item_id: 0,
                                        quantity: 1,
                                        Title: item.Title,
                                        Price: parseInt(item.Price),
                                        product_id: item.ParentId,
                                        variation_id: item.WPID,
                                        isTaxable: item.Taxable,
                                    }


                                    var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
                                    console.log("cartlistBarcode function", cartlist);
                                    var qty = 0;
                                    cartlist.map(cartitem => {
                                        if (item.WPID == cartitem.variation_id) {
                                            qty = cartitem.quantity;
                                            console.log("cart me data hain", cartitem, qty);

                                        }
                                    })
                                    if ((item.StockStatus == null || item.StockStatus == 'instock') &&
                                        (item.ManagingStock == false || (item.ManagingStock == true && qty < item.StockQuantity))) {
                                        console.log("ManagingStockt", item.ManagingStock);
                                        cartlist.push(data);
                                        this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));   // this.state.cartproductlist
                                    } else {

                                        $('#outOfStockModal').modal('show');

                                        //  alert("stcock Quantity exceed");                        
                                    }
                                }

                                //  })
                            }
                            // })

                        }

                    })
                    cursor.continue();
                }
                if (!isItemexist) {
                    $('#productNotfound').modal('show');
                }
            }


        };

    }

    handleError(err) {
        console.error(err)
    }

    componentWillReceiveProps(nextProps) {
       // console.log("UPDATE_PRODUCT_LIST", localStorage.getItem('UPDATE_PRODUCT_LIST'))
        if (localStorage.getItem('UPDATE_PRODUCT_LIST') && localStorage.getItem('UPDATE_PRODUCT_LIST') == 'true') {
            this.state.updateProductIs = true;
        } else {
            this.state.updateProductIs = false
        }
    }

    updateProducts(){
        window.location = '/loginpin'
    }

    updateCancelProducts(){
        this.setState({updateProductIs:false})
        this.state.updateProductIs = false
        localStorage.removeItem('UPDATE_PRODUCT_LIST')
    }

    render() {
        const { match, productlist, attributelist, categorylist, cartproductlist } = this.props;
       // console.log("this.state.updateProductIs", this.state.updateProductIs)
        return (

            <nav className="navbar navbar-default" id="colorFullHeader">
                {/* <ProductNotFound/>  */}
                <BarcodeReader
                    onError={this.handleError}
                    onScan={this.handleScan}
                />
                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">
                    <div className="container-fluid p-0">
                        <div className="navbar-header">
                            <button type="button" id="sidebarCollapse" className="navbar-btn active">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>

                        <div className="mobile_menu clearfix">
                            <ul>
                                <li className={match.path == '/shopview' ? "active" : null}>
                                    <a href="/shopview">tile View</a>
                                </li>
                                <li className={match.path == '/listview' ? "active" : null}>
                                    <a href="/listview">list View</a>
                                </li>
                                <li className={match.path == '/customerview' ? "active" : null}>
                                    <a href="/customerview">customer View</a>
                                </li>
                            </ul>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-left">
                                <li className={match.path == '/shopview' ? "active" : null}><a href="/shopview">Tile View</a></li>
                                <li className={match.path == '/listview' ? "active" : null}><a href="/listview">List View</a></li>
                                <li className={match.path == '/customerview' ? "active" : null}><a href="/customerview">Customer View</a></li>
                            </ul>
                            {match.path != '/customerview' ?
                                <ul className="nav navbar-nav navbar-right">
                                    <li className="nav-notify p-0">
                                        <div className="form-search-nav_block" >
                                            <input type="text" id="product_search_field" name="search" className="expand_search" onChange={() => this.filterProduct()} placeholder="Search" />
                                            <button type="button" className="expand_magnify_search magnify-white"></button>
                                            <button type="reset" className="expand_search_close" onClick={() => this.clearInput()}></button>
                                        </div>
                                    </li>
                                    {/* style={{display:this.state.updateProductIs==true?'':'none'}} */}
                                    <li className="nav-notify" style={{display:this.state.updateProductIs==true?'':'none'}}>
                                        <div className="dropdown">
                                            <a className="dropdown-toggle no-backgrond" type="button" id="menu1" data-toggle="dropdown">
                                                <div className="divParent">
                                                  <img width="29" src="assets/img/wht-notification.png"/> 
                                                <div className="divChild notification-counter text-center"></div>
                                             </div>
                                            </a>
                                            <ul className="dropdown-menu custom-drpbox dropdown-menu-right dropbox" role="menu" aria-labelledby="menu1">

                                                <li rol="presentation">
                                                    <a href="javascript:void(0)">Do you want to update your product</a>
                                                </li>
                                                <li rol="presentation" className="text-center pt-2 pb-2">
                                                    <button type="button" onClick={()=>this.updateProducts()} className="btn btn-primary">
                                                        Yes
                                                </button>
                                                    <button type="button" onClick={()=>this.updateCancelProducts()} className="btn btn-danger ml-2">
                                                        No
                                                </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    {/* <div className="search_block">
                                            <div className="sample one">
                                                <input type="text" id="product_search_field"   className="search_field cmn_search_field" name="search"   onChange={() => this.filterProduct()} placeholder="search" />
                                               <button className="close-icon" type="reset" onClick={() => this.clearInput()}></button>  
                                            </div>
                                        </div>  */}
                                    {/* </li> */}
                                </ul>
                                : null}
                        </div>
                    </div>
                </div>
                {match.path != '/customerview' ?
                    // <CartItemCount  itemcounthandler={this.handleItemCount()}/>
                    <div className="col-lg-3 col-sm-4 col-xs-4 pr-0 pl-heading cart_header_overlap">
                        <div className="cart_header">
                            <a href="javascript:void(0);" className="ch_icon">
                                <button className="button icon icon-delete" onClick={() => this.removeCheckOutList()}></button>
                            </a>
                            <div className="ch_heading">
                                Cart({
                                    cartproductlist ? cartproductlist.length : 0})
                        </div>
                            <div className="dropdown ch_icon">
                                <a className="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                    <button className="button icon icon-plus-white"></button>
                                </a>
                                <ul className="dropdown-menu custom-drpbox dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                    <li role="presentation"><a role="menuitem" tabIndex="-1" data-toggle="modal" data-target="#addnotehere">Add Notes</a></li>
                                    <li role="presentation"><a role="menuitem" tabIndex="-1" data-toggle="modal" data-target='#popup_oliver_add_fee'>Add Fee</a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    : null}


            </nav>
        )
    }
}



function mapStateToProps(state) {

    const { productlist, attributelist, categorylist, cartproductlist } = state;
    return {
        productlist: productlist.productlist,
        attributelist: attributelist.attributelist,
        categorylist: categorylist.categorylist,
        cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],//cartproductlist.cartproductlist
    };
}

const connectedCommonHeaderTwo = connect(mapStateToProps)(CommonHeaderTwo);
export { connectedCommonHeaderTwo as CommonHeaderTwo };