import React from 'react';
import { connect } from 'react-redux';
import { idbProductActions , taxRateAction,checkShopSTatusAction } from '../_actions';
import { get_UDid } from '../ALL_localstorage'
import { openDb, deleteDb } from 'idb';
import Config from '../Config';
import { history } from '../_helpers';

class LoadingIndexDB extends React.Component {

    constructor(props) {
        super(props);
       // this.props.dispatch(idbProductActions.createProductDB());
       if(!localStorage.getItem("shopstatus"))
       {
            this.props.dispatch(checkShopSTatusAction.getStatus());   
       }   
       this.props.dispatch(taxRateAction.getTaxSetting());            
    }

     componentWillMount() {
          window.indexedDB.deleteDatabase('ProductDB');    
          
          if (!localStorage.getItem('UDID')) {
            history.push('/login');
        }

          if (!localStorage.getItem('user') ||   ! sessionStorage.getItem("issuccess")) {
            history.push('/loginpin');
        }        
     
      }


    async  componentDidMount(){
       
        if (!localStorage.getItem('user') ||   ! sessionStorage.getItem("issuccess")) {
            history.push('/loginpin');
        }
        var udid = get_UDid(localStorage.getItem("UDID"));
     
        var pageNumber = 1;
        var PageSize = Config.key.FETCH_PRODUCTS_PAGESIZE;
        var ProductArray = [];
        var TotaltotalRecord = 0;
        const requestOptions = {
            method: 'GET',
            headers: {
                "access-control-allow-origin": "*",
                "access-control-allow-credentials": "true",
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
            }
            , mode: 'cors'

        };
        // call firstTime------------------
        fetch(`${Config.key.OP_API_URL}/ShopData/GetProductPageUpdatedWithCount?udid=${udid}&pageNumber=${pageNumber}&pageSize=${PageSize}`, requestOptions)
            .then(response => {
                //  console.log("pageNumber", pageNumber, response);
                if (response.ok) { return response.json(); }
                throw new Error(response.statusText)  // throw an error if there's something wrong with the response

            })
            .then(function handleData(data) {
                var reloadCount=localStorage.getItem("ReloadCount");
              
                    ProductArray = [...new Set([...ProductArray, ...data.Content.Records])];
                    TotaltotalRecord = data.Content.TotalRecords;
               
                // console.log("ProductArray 1" ,TotaltotalRecord , ProductArray.length)
                //this.SetIndexDB(udid,ProductArray);
                const dbPromise = openDb('ProductDB', 1, upgradeDB => {
                    upgradeDB.createObjectStore(udid);

                });
                const idbKeyval = {
                    async get(key) {
                        const db = await dbPromise;
                        return db.transaction(udid).objectStore(udid).get(key);
                    },
                    async set(key, val) {
                        const db = await dbPromise;
                        const tx = db.transaction(udid, 'readwrite');
                        tx.objectStore(udid).put(val, key);
                        return tx.complete;
                    },

                };
                idbKeyval.set('ProductList', ProductArray);


                //check dataExist into indexdb-------------------------
                var isExist=false;
               
                //--------------------------------------------------------

                
                if (TotaltotalRecord == ProductArray.length) {

                    idbKeyval.get('ProductList').then(val => {     
                      
                        if( ProductArray.length==0 || !val || val.length==0 || val==null || val==""){                          
                            console.error('Console.save: No data')
                            var reloadCount=localStorage.getItem("ReloadCount");
                            //if(reloadCount<3){
                                localStorage.setItem("ReloadCount",(parseInt(reloadCount)+1) );
                                setTimeout(function(){
                                    window.location = '/' //Reload to get product
                               // window.location = '/shopview'
                                } , 1000)
                        }else if (val.length>0){
                        
                            setTimeout(function(){                          
                             window.location = '/shopview'
                             } , 500)
                        }            
                    });
                  
                }
               
                   
               
                for (pageNumber = 1; pageNumber <= (TotaltotalRecord / PageSize * 1.0); pageNumber++) {
                    (function (x) {

                        fetch(`${Config.key.OP_API_URL}/ShopData/GetProductPageUpdatedWithCount?udid=${udid}&pageNumber=${pageNumber + 1}&pageSize=${PageSize}`, requestOptions)
                            .then(response => {
                                // console.log("pageNumber", pageNumber, response);
                                if (response.ok) { return response.json(); }
                                throw new Error(response.statusText)  // throw an error if there's something wrong with the response

                            })
                            .then(function handleData(data) {

                                ProductArray = [...new Set([...ProductArray, ...data.Content.Records])];
                                //this.SetIndexDB(udid,ProductArray);
                                const dbPromise = openDb('ProductDB', 1, upgradeDB => {
                                    upgradeDB.createObjectStore(udid);

                                });

                                idbKeyval.set('ProductList', ProductArray);
                                console.log("ProductArray" , data.Content.TotalRecords , ProductArray.length)
                                if (data.Content.TotalRecords == ProductArray.length) {
                                    if(ProductArray.length==0){
                                        console.error('Console.save: No data')
                                    }else{
                                    setTimeout(function(){
                                         window.location = '/shopview'
                                         //history.push('/shopview')
                                    } , 500)
                                }
                                    /// history.push("/shopview");
                                    // this.setState({
                                    //     totalProduct: data.Content.TotalRecords
                                    // })

                                }
                            })
                            .catch(function handleError(error) {
                                // handle errors here
                                console.Error('Console.save: No data')
                                console.log("%c ERROR IN PRODUCT FETCHING  ", "color:red", error)
        
                               // console.log("Error".error);

                            })

                    })(pageNumber);
                }

            })
        .catch(function handleError(error) {
            console.error('Console.save: No data')
            // handle errors here
             if(reloadCount<3){
                localStorage.setItem("ReloadCount",(parseInt(reloadCount)+1) );
                setTimeout(function(){
                    window.location = '/' //Reload to get product
               // window.location = '/shopview'
                } , 1000)
         
           //   history.push('/')
            }
           
        }) 
 

    }




    render() {        
        return (
            <div className="bgimg-1">
                <div className="content_main_wapper">
                    <div className="onboarding-loginBox">
                        <img src="assets/img/frame02-03s.gif" style={{ height: '40%', width: '40%' }} />
                        <div className="onboarding-pg-heading" ><h1>Please wait...</h1><h2>Do not close this window while we cache your shop</h2></div>
                    </div>
                    <div className="powered-by-oliver">
                        <a href="javascript:void(0)">Powered by Oliver POS</a>
                    </div>
                </div>               
            </div>           
        )
    }
}

function mapStateToProps(state) {
    const { productlist } = state;
    return {
        productlist: productlist
    };
}

const connectedList = connect(mapStateToProps)(LoadingIndexDB);
export { connectedList as LoadingIndexDB };