import React from 'react';
import { connect } from 'react-redux';
import { checkoutActions } from '../CheckoutPage/actions/checkout.action'


class UpdateProductInventoryModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    closePopup() {
        this.props.dispatch(checkoutActions.checkItemList())
    }


    render() {
        const { checkout_list } = this.props;
        let cartProductList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : null;
        let blank_quntity = []
        var new_data;
        //console.log("checkoutList", cartProductList, checkout_list);
        if (cartProductList && checkout_list) {
            checkout_list.map(itemC => {

                new_data = cartProductList.find(function (element) {
                    return (element.variation_id == 0 ? element.product_id == itemC.ProductId : element.variation_id == itemC.ProductId && itemC.success == false)
                })
                //new_data = cartProductList.map(prdId=> prdId.variation_id == 0 ? prdId.product_id : prdId.variation_id == itemC.ProductId && itemC.success == true)
                //console.log("new_data", new_data) 
                if (new_data) {
                    blank_quntity.push(new_data.Title)
                }
                // console.log("blank_quntity", blank_quntity)
            })
            //console.log("new_data", new_data) 

        }
        return (
            <div id="checkout1" className="modal modal-wide modal-wide1 fade">
                <div className="modal-dialog" id="dialog-midle-align">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button onClick={() => this.closePopup()} type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                <img src="../assets/img/delete-icon.png" />
                            </button>
                            <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                        </div>
                        <div className="modal-body p-0">
                            {blank_quntity.length > 0 ?
                                <div>
                                    <h3 id="epos_error_model_message" className="popup_payment_error_msg">Please remove below this product !</h3>
                                    {blank_quntity.map((name, index) => {
                                        return (
                                            <h5 key={index} id="epos_error_model_message" style={{ padding: 3 }} className="popup_payment_error_msg">{name}</h5>
                                        )
                                    })}

                                </div>
                                :
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">Please add at least one product in cart !</h3>
                            }
                        </div>
                        <div className="modal-footer p-0">
                            <button onClick={() => this.closePopup()} type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



function mapStateToProps(state) {
    const { checkout_list } = state;
    return {
        checkout_list: checkout_list.items,
    };
}

const connectedUpdateProductInventoryModal = connect(mapStateToProps)(UpdateProductInventoryModal);
export { connectedUpdateProductInventoryModal as UpdateProductInventoryModal };