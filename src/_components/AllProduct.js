import React from 'react';
import { connect } from 'react-redux';
import { allProductActions } from '../_actions/allProduct.action'
import { cartProductActions } from '../_actions/cartProduct.action'
import { default as NumberFormat } from 'react-number-format'
import { history } from '../_helpers';
import { LoadingModal } from './'
import { Markup } from 'interweave';
import { idbProductActions } from '../_actions/idbProduct.action'
import { openDb, deleteDb } from 'idb';
import Config from '../Config'
import { getTaxAllProduct } from './'
import moment from 'moment';

// import { constants } from 'http2';
import { encode_UDid, get_UDid } from '../ALL_localstorage'

import { idbProductService } from '../_services/idbProduct.service';

class AllProduct extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(allProductActions.refresh());
        this.state = {
            active: false,
            isLoading: true,
            showVariationPopup: false,
            variationPopupQuantity: 1,
            cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],
            product_List: [],
            ParentProductList: [],
            AllProduct: [],
            chunk_size: Config.key.PPRODUCT_PAGE_SIZE,
            pageNumber: 0,
            product_Array: [],
            productDisplayStatus: true,
            hideButton: false,
            search: '',
            totalRecords: 0,
            filteredProuctList: [],
            ticket_Product_status: false
        }

        const { dispatch } = this.props;

        // This binding is necessary to make `this` work in the callback 
        this.handleIsVariationProduct = this.handleIsVariationProduct.bind(this);
        this.addSimpleProducttoCart = this.addSimpleProducttoCart.bind(this);
        this.addvariableProducttoCart = this.addvariableProducttoCart.bind(this);
        this.productOutOfStock = this.productOutOfStock.bind(this);

        //   var _productList= idbProductActions.getAll();//localStorage.getItem("Productlist-"+localStorage.getItem('UDID'));
        //     this.state.product_Array = JSON.parse(_productList) ? JSON.parse(_productList) : []
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var udid = get_UDid('UDID');
        //console.log("productall",udid);
        //  var udid= localStorage.getItem('UDID');

        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },
        };
        let allData = []
        idbKeyval.get('ProductList').then(val => {
            //console.log("val123456",val);
            if( !val || val.length==0 || val==null || val==""){
                // console.error('Console.save: No data')
                 this.setState({ AllProductList:[] });                
            }
            else{
            
                        this.state.AllProduct = getTaxAllProduct(val);
                        val.map(item => {
                            if (item.ParentId == 0 && (item.ManagingStock == false || (item.ManagingStock == true && item.StockQuantity > -1))) {
                                allData.push(item);
                            }
                        })

                        if(allData.length==0){
                        console.error('Console.save: No data')
                        }

                        this.setState({ ParentProductList: allData, totalRecords: allData ? allData.length : 0 });
                        this.state.totalRecords = this.state.ParentProductList ? this.state.ParentProductList.length : 0;
                        this.loadingData();
            }
        }
        );
        ////
        // console.log("serviceCalled");
        if (this.state.search) {
            this.loadingFilterData()
        }
        //setTimeout(function () {
        // this.props.dispatch( idbProductActions.getAll());
        //  console.log("serviceEnd",prod);
        // }, 1000);



    }


    // handleBarcode(e) {
    //     if (wordTyped == '12345') { alert('Woohoo!'); }
    // }
    componentDidMount() {
        setTimeout(function () {
            setHeightDesktop();
        }, 1000);
        this.props.onRef(this)
        setTimeout(function () {
            setHeightDesktop();
        }, 1000);

        // this.props.dispatch(allProductActions.getTickeraReceipt());

    }

    componentDidUpdate(prop, nextState) {
        //console.log("nextState" , nextState)
        if (nextState.totalRecords < nextState.pageNumber) {
            if (nextState.search) {
                this.loadingFilterData()
            }
        }
    }

    loadingData() {
        var prdList = [];
        var tilDisplayIndex = this.state.pageNumber * this.state.chunk_size + this.state.chunk_size;
        // console.log("this.state.ParentProductList",this.state.ParentProductList);
        let ParentProductList = this.state.ParentProductList.sort(function (a, b) {
            var nameA = a.Title.toUpperCase(); // ignore upper and lowercase
            var nameB = b.Title.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }

            // names must be equal
            return 0;
        });

        ParentProductList && ParentProductList.map((prod, index) => {

            //console.log("prod",prod);
            if (index >= 0 && index < tilDisplayIndex) {
                // check tickera product is expired?--------------------------
                var isExpired = false;
                if (prod.IsTicket && prod.IsTicket == true) {
                    var ticketInfo = JSON.parse(prod.TicketInfo);
                    // ticketInfo._ticket_checkin_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)
                    if (ticketInfo._ticket_availability.toLowerCase() == 'range' && (ticketInfo._ticket_availability_to_date)) {

                        var dt = new Date(ticketInfo._ticket_availability_to_date);
                        var expirationDate = dt.setDate(dt.getDate() + 1);

                        var currentDate = new Date();// moment.utc(new Date)
                        // console.log("currentDate",currentDate)
                        // console.log("expirationDate", new Date(expirationDate))
                        // console.log("TicketDate",expirationDate,ticketInfo._ticket_availability_to_date)

                        if (currentDate > expirationDate) {
                            isExpired = true;
                        }
                    }
                }
                //------------------------------------------------------------
                if (isExpired == false)
                    prdList.push(prod)
            }
        });
        if (prdList.length > 0) {
            this.state.product_List = prdList;
            this.setState({ product_List: prdList })
        }
        this.state.pageNumber = this.state.pageNumber + 1;

        if (this.state.ParentProductList.length == 0) {
            console.log("%c PRODUCT NOT AVAILABLE", "color:red")
            // console.log("ParentProductList",this.state.ParentProductList);
        }
    }


    loadingFilterData() {
        //console.log("loadmore");
        // if(this.state.filteredProuctList.length>0){
        var prdList = [];
        var tilDisplayIndex = this.state.pageNumber * this.state.chunk_size + this.state.chunk_size;
        // console.log("this.state.filteredProuctList",  this.state.filteredProuctList );

        this.state.filteredProuctList && this.state.filteredProuctList.map((prod, index) => {
            if (index >= 0 && index < tilDisplayIndex)
                prdList.push(prod)
        });
        //console.log("prdList",prdList)
        if (prdList.length > 0) {

            this.state.product_List = prdList;
            this.setState({ product_List: prdList })

        }
        this.state.pageNumber = this.state.pageNumber + 1;
        //}
        // else
        //     {
        //         this.setState({product_List :this.state.ParentProductList});
        //     }

    }

    componentWillUnmount() {
        this.props.onRef(null)
    }//

    filterProductByTile(type, item, parent) {
        //  console.log("filterProductByTile", type ,item,"parent",parent);
        this.setState({ pageNumber: 0 })
        switch (type) {
            case "attribute":
                //has_attribute
                this.filterProductForAttribute(item);

                break;
            case "sub-attribute":
                this.filterProductForSubAttribute(item);
                break;
            case "category":
                this.filterProductForCateGory(item);
                break;
            case "sub-category":
                this.filterProductForSubCateGory(item);
                break;
            case "inner-sub-attribute":
                //has_subattribute
                // this.productTableFilter(item, 4);
                this.productDataSearch(item, 4, parent)

                break;
            case "inner-sub-category":
                //this.productTableFilter(item, 5);
                this.productDataSearch(item, 2)

                break;
            case "product-search":
                //this.productTableFilter(item, 0);
                this.productDataSearch(item, 0)
                break;
        }
    }

    filterProductForAttribute(item) {
        //this.productTableFilter(item.attribute_code, 3);
        this.productDataSearch(item.attribute_slug, 1)
    }

    filterProductForSubAttribute(item) {
        // this.productTableFilter(item.attribute_slug, 4);
        this.productDataSearch(item.attribute_slug, 3, item.parent_attribute.replace("pa_", ""));

    }

    filterProductForCateGory(item) {
        //    this.productTableFilter(item.category_slug, 5);
        //console.log("filterProductForCateGory item",item)
        this.productDataSearch(item.category_slug, 2)

    }

    filterProductForSubCateGory(item) {
        //this.productTableFilter(item.category_slug, 5);
        this.productDataSearch(item.category_slug, 2)

    }

    productDataSearch(item1, index, parent) {
     //console.log( "checksubAttribute",  item1, index, parent )
        var filtered = []
        let value = item1;
        let parentAttribute = parent;
        this.state.product_List = []
        this.setState({
            search: value,
            pageNumber: 0
        })


        if (value == '' || value == null) {
            // console.log("value",value);

        }

        if (index == 0) {
            // console.log(0,this.state.AllProduct)

            var serchFromAll = this.state.AllProduct.filter((item) => (item.Title.toLowerCase().includes(value.toLowerCase()) || item.Barcode.toString().toLowerCase().includes(value.toLowerCase()) || item.Sku.toString().toLowerCase().includes(value.toLowerCase())))

            //-------Filter child and parent-------------
            if (!serchFromAll || serchFromAll.length > 0) {

                var parentProduct = serchFromAll.filter(item => {
                    return (item.ParentId == 0)
                })
                filtered = [...new Set([...filtered, ...parentProduct])];

                // Searching parent product of child child product------------------
                var childProduct = serchFromAll.filter(item => {
                    return (item.ParentId !== 0)
                })

                childProduct && childProduct.map(chieldItem => {
                    var parentOfChild = this.state.AllProduct.filter((item) => (item.WPID === chieldItem.ParentId))
                    filtered = [...new Set([...filtered, ...parentOfChild])];
                })

            }

            //----------------------------------------------------------------

            this.setState({
                filteredProuctList: filtered,
                totalRecords: filtered.length,
                product_List: filtered,

            })
            this.state.filteredProuctList = filtered;
            this.state.totalRecords = filtered.length;

            this.loadingFilterData()

        }

        if (index == 1) {
            // console.log(1)
            //    console.log("search", value, index);
            this.state.ParentProductList && this.state.ParentProductList.map((item) => {
                item.ProductAttributes && item.ProductAttributes.map(attri => {
                    // console.log("attri", attri);
                    if (String(attri.Slug).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1 ||
                        String(attri.Name).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1) {
                        filtered.push(item)


                    }

                })
            })
            this.setState({
                filteredProuctList: filtered,
                totalRecords: filtered.length
            })

            this.state.filteredProuctList = filtered;
            this.state.totalRecords = filtered.length;

            this.state.product_List = filtered;
            this.loadingFilterData()
        }
        else if (index == 2) {
            //console.log(2)
            // console.log("filter",value);

            ///------Get Subcategory Code------------------------------------------------ 
            var filterCategoryCode = []
            filterCategoryCode.push(value);

            var categoryList = [];
            if (localStorage.getItem("categorieslist"))
                categoryList = JSON.parse(localStorage.getItem("categorieslist"))
            //console.log("categoryList",categoryList)               
            if (categoryList) {
                var found = categoryList.find(function (element) {
                    return (element.Code.replace(/-/g, "").toLowerCase() == value.replace(/-/g, "").toLowerCase())
                });
                if (found) {
                    found.Subcategories.map(item => {
                        filterCategoryCode.push(item.Code);
                    })
                }
            }
            // console.log("filterCategoryCode",filterCategoryCode);

            //--------------------------------------------------------------

            this.state.ParentProductList && this.state.ParentProductList.map((item) => {

                item.Categories && item.Categories.split(",").map(category => {
                    filterCategoryCode.map(filterCode => {


                        var prod_Code = filterCode.replace(/-/g, ""); //  value.replace(/-/g, ""); 
                        category = category.replace(/-/g, "");
                        //console.log("category",category);
                        if (category.toUpperCase().toString() === prod_Code.toUpperCase()) {
                            if (filtered.indexOf(item) === -1) {
                                filtered.push(item)
                                //console.log(this.items);
                            }
                        }
                    })
                })


            })

            this.setState({
                filteredProuctList: filtered,
                totalRecords: filtered.length
            })
            this.state.filteredProuctList = filtered;
            this.state.totalRecords = filtered.length;
            this.state.product_List = filtered
            this.loadingFilterData()

        } else if (index == 3) {


                ///------Get attribute Code------------------------------------------------ 
                var filterAttribyteCode = []
                filterAttribyteCode.push(value);

                var attributelist = [];
                if (localStorage.getItem("attributelist"))
                    attributelist = JSON.parse(localStorage.getItem("attributelist"))
                // console.log("attributelist",attributelist)               
                if (attributelist) {
                    var found = attributelist.find(function (element) {
                        return (element.Code.replace(/-/g, "").toLowerCase() == value.replace(/-/g, "").toLowerCase())
                    });
                    if (found) {
                        found.SubAttributes.map(item => {
                            filterAttribyteCode.push(item.Code);
                        })
                    }
                }
                // console.log("filterAttribyteCode",filterAttribyteCode);

                //--------------------------------------------------------------

                this.state.ParentProductList && this.state.ParentProductList.map((item) => {
                    item.ProductAttributes && item.ProductAttributes.map(proAtt => {

                        var dataSplitArycomma = proAtt.Option.split(',');
                        dataSplitArycomma && dataSplitArycomma.map(opt => {
                            filterAttribyteCode.map(filterAttribute => {

                                opt = opt.replace(/-/g, "");
                                value = filterAttribute.replace(/-/g, ""); // value.replace(/-/g, ""); 
                                if (opt.toString().toUpperCase() === String(value).toUpperCase() && String(proAtt.Slug).toUpperCase() === String(parentAttribute).toUpperCase()) {

                                    if (filtered.indexOf(item) === -1) {
                                        filtered.push(item)
                                        //console.log(this.items);
                                    }


                                }
                            })
                        })
                    })
                })
                this.setState({
                    filteredProuctList: filtered,
                    totalRecords: filtered.length
                })

                this.state.product_List = filtered

                this.state.filteredProuctList = filtered;
                this.state.totalRecords = filtered.length;
                this.loadingFilterData()
        } else if (index == 4) {

               // console.log("this.state.ParentProductList",this.state.ParentProductList)
            this.state.ParentProductList && this.state.ParentProductList.map((item) => {
               // console.log("item",item)
                let dataSplitAry = item.ProductAttributes.map(Opt => {
                  //  console.log("Opt",Opt)
                    var dataSplitArycomma = Opt.Option.split(',');
                  
                    dataSplitArycomma.map(optValve => {
                           var itemCode= this.getAttributeCode(optValve,parent);
                       // if (optValve.toString().toUpperCase() === String(value).toUpperCase() && String(Opt.Slug).toUpperCase() === String(parentAttribute).toUpperCase()) {                          
                        if (itemCode.toString().toUpperCase() === String(value).toUpperCase() ) {
                            filtered.push(item)
                        }

                    })
                })
            })
            this.setState({
                filteredProuctList: filtered,
                totalRecords: filtered.length
            })
            this.state.product_List = filtered
            this.state.filteredProuctList = filtered;
            this.state.totalRecords = filtered.length;

            this.loadingFilterData()
        }

    }
    getAttributeCode(optionvalue,parent){
        ///------Get attribute Code------------------------------------------------   
            var itemCode = null;
            var found=null;
            var attributelist=[];
            if (localStorage.getItem("attributelist"))
                attributelist = JSON.parse(localStorage.getItem("attributelist"))
           
            if (attributelist) {
                var found = attributelist.find(function (element) {
                    return (element.Code.replace(/-/g, "").toLowerCase() == parent.replace(/-/g, "").toLowerCase())
                });
                if (found) {
                  var  subAttributes= found.SubAttributes.find(function (element) {
                        return (element.Value.replace(/-/g, "").toLowerCase() == optionvalue.replace(/-/g, "").toLowerCase())
                    });

                    itemCode=subAttributes?subAttributes.Code:"";
                }
            }
            //console.log("itemCode",itemCode);
            return itemCode;
            
            // console.log("filterAttribyteCode",filterAttribyteCode);
        //--------------------------------------------------------------
    }
 
    handleIsVariationProduct(type, product) {
        // console.log("handleIsVariationProduct",type,product)
        localStorage.removeItem("PRODUCT")
        localStorage.removeItem("SINGLE_PRODUCT")
        const { dispatch } = this.props;
        dispatch(cartProductActions.singleProductDiscount())
        if (type !== "variable") {
            this.addSimpleProducttoCart(product);
            //this.addSimpleProducttoCart(this.availableCartDataInProduct(product));
        } else {
            //  localStorage.setItem('variationProduct',JSON.stringify(product))
            this.addvariableProducttoCart(product);
        }
    }

    productOutOfStock(title) {
        $('#outOfStockModal').modal('show');

    }

    getTicketFields(product, tick_type = null) {
        // console.log("tick_type",tick_type);
        var tick_data = JSON.parse(product.TicketInfo)
        //var  udid=4040246357
        var form_id = tick_data._owner_form_template
        //  console.log("ticketfield1",form_id);

        this.props.dispatch(allProductActions.ticketFormList(form_id));
        this.state.ticket_Product_status = true
        this.state.tick_type = tick_type
        this.state.ticket_Product = product
        this.setState({
            ticket_Product: product,
            ticket_Product_status: true,

            // tick_type:'simpleadd'
        })

        // }

    }


    addSimpleProducttoCart(product, ticketFields = null) {
        const { dispatch, checkout_list, cartproductlist } = this.props;

        var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [];
       // console.log("product121363",cartlist, product);
        if(cartlist.length > 0){
           // alert()
            cartlist.map(findId=>{
                if(findId.product_id === product.WPID){
                   // console.log("123456",findId);
                    product['after_discount'] = findId ? findId.after_discount : 0,
                    product['discount_amount'] = findId ? findId.discount_amount : 0,
                    product['product_after_discount'] = findId ? findId.product_after_discount : 0,
                    product['product_discount_amount'] = findId ? findId.product_discount_amount : 0,
                    product['discount_type'] = findId ? findId.discount_type : "",
                    product['new_product_discount_amount'] = findId ? findId.new_product_discount_amount : 0
                }
            });
          
        }
        let setQunatity = 1;
       //   console.log("product121363",product);
        //if(process.env.ENVIRONMENT =='devtickera'){

        var tick_data = product && product.IsTicket == true &&  product.TicketInfo != '' ? JSON.parse(product.TicketInfo) : '';
      //   console.log("TicketInfo",tick_data);
        var availability_to_date = tick_data && tick_data !=='null'? moment(tick_data._ticket_availability_to_date).format('YYYY-MM-DD'):''
        var today_date = moment().format('YYYY-MM-DD')

        if (product && product.IsTicket == true && ticketFields == null) {
            var tick_type = 'simpleadd'
            // if(tick_data._ticket_availability == "open_ended"   ){
            this.getTicketFields(product, tick_type)

            // }else if(tick_data._ticket_availability == "range"){              

            //   console.log("availability_to_date",availability_to_date,"today_date",today_date);
            // //   if(availability_to_date > today_date){
            //     ///  alert('availabity hain')
            //       this.getTicketFields(product,tick_type)

            // //   }
            // //   else if(availability_to_date == today_date || availability_to_date < today_date){
            // //       console.log("availability_to_date",availability_to_date,"today_date",today_date)
            // //          alert('Ticket date is expired');


            // //   }

            // }

        }
        // }   
        if (product && product.IsTicket == false) {

            var data = {
                line_item_id: 0,
                cart_after_discount: product.cart_after_discount ? product.cart_after_discount : 0,
                cart_discount_amount: product.cart_discount_amount ? product.cart_discount_amount : 0,
                after_discount: product.after_discount ? product.after_discount : 0,
                discount_amount: product.discount_amount ? product.discount_amount : 0,
                product_after_discount: product.product_after_discount ? product.product_after_discount : 0,
                product_discount_amount: product.product_discount_amount ? product.product_discount_amount : 0,
                quantity: setQunatity,
                Title: product.Title,
                Price: setQunatity * parseFloat(product.Price),
                product_id: product.WPID,
                variation_id: 0,
                isTaxable: product.Taxable,
                old_price: product.old_price,
                incl_tax: product.incl_tax,
                excl_tax: product.excl_tax,
                ticket_status: product.IsTicket,
                discount_type: product.discount_type ? product.discount_type : "",
                new_product_discount_amount: product ? product.new_product_discount_amount : 0

            }
            var qty = 0;
            cartproductlist.map(item => {
                if (product.WPID == item.product_id) {
                    qty = item.quantity;
                }
            })
            if (((product.StockStatus == null || product.StockStatus == 'instock') && product.ManagingStock == false) || ((product.StockStatus == null || product.StockStatus == 'instock') &&
                product.ManagingStock == true && qty < product.StockQuantity)) {
                cartlist.push(data);
                this.stockUpdateQuantity(cartlist,data, product)

                dispatch(cartProductActions.addtoCartProduct(cartlist));  // this.state.cartproductlist
            } else {
                //alert("stcock Quantity exceed");
                $('#outOfStockModal').modal('show')

            }
        }
        else if (product && product.IsTicket == true && ticketFields != null) {
            //    console.log("ticketFields",ticketFields)
            this.setState({ ticket_Product_status: false })
            var data = {
                line_item_id: 0,
                cart_after_discount: product.cart_after_discount ? product.cart_after_discount : 0,
                cart_discount_amount: product.cart_discount_amount ? product.cart_discount_amount : 0,
                after_discount: product.after_discount ? product.after_discount : 0,
                discount_amount: product.discount_amount ? product.discount_amount : 0,
                product_after_discount: product.product_after_discount ? product.product_after_discount : 0,
                product_discount_amount: product.product_discount_amount ? product.product_discount_amount : 0,
                quantity: setQunatity,
                Title: product.Title,
                Price: setQunatity * parseFloat(product.Price),
                product_id: product.WPID,
                variation_id: 0,
                isTaxable: product.Taxable,
                tick_event_id: tick_data._event_name,
                ticket_status: product.IsTicket,
                product_ticket: ticketFields,
                old_price: product.old_price,
                incl_tax: product.incl_tax,
                excl_tax: product.excl_tax,
                discount_type: product.discount_type ? product.discount_type : "",
                new_product_discount_amount: product ? product.new_product_discount_amount : 0
                // ticket_cart_display_status: availability_to_date > today_date?'expired':''
                //   ticket_cart_display_status:tick_data._ticket_availability == "range" &&  availability_to_date == today_date ||  availability_to_date < today_date?'expired':''

            }

            var qty = 0;
            cartproductlist.map(item => {
                if (product.WPID == item.product_id) {
                    qty = item.quantity;
                }
            })
            if (((product.StockStatus == null || product.StockStatus == 'instock') && product.ManagingStock == false) || ((product.StockStatus == null || product.StockStatus == 'instock') &&
                product.ManagingStock == true && qty < product.StockQuantity)) {
                cartlist.push(data);
                this.stockUpdateQuantity(cartlist,data,product)

                dispatch(cartProductActions.addtoCartProduct(cartlist));  // this.state.cartproductlist
            } else {
                //alert("stcock Quantity exceed");
                $('#outOfStockModal').modal('show')

            }
        }
    }

    stockUpdateQuantity(cardData, data,product) {
        var qty = 0
        cardData.map(item => {
            if (data.product_id === item.product_id) {
                qty += item.quantity;

            }

        })
        //  this.setState({variationStockQunatity:this.state.variationStockQunatity == 'Unlimited'?this.state.variationStockQunatity:this.props.getVariationProductData.StockQuantity - qty })
       if(product){
          let quantity =  (product.StockStatus == null || product.StockStatus == 'instock') && product.ManagingStock == false ? "Unlimited" : (typeof product.StockQuantity != 'undefined') && product.StockQuantity != '' ? product.StockQuantity - qty : '0' ;
          localStorage.setItem("CART_QTY", quantity);
        }
   // console.log("getQuantity",localStorage.getItem('CART_QTY'))
    }

    componentWillReceiveProps(nextProps) {
        // console.log("ticketfieldList1111111",localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'',"this.state.tick_type",this.state.tick_type , typeof  this.state.tick_type);
        var ticket_Data = localStorage.getItem('ticket_list') ? JSON.parse(localStorage.getItem('ticket_list')) : ''
        var tick_data = this.state.ticket_Product_status == true ? JSON.parse(this.state.ticket_Product.TicketInfo) : ''
        var form_id = tick_data._owner_form_template
        // console.log("call simple1" ,form_id)

        if ((localStorage.getItem('ticket_list') && localStorage.getItem('ticket_list') !== 'null' && localStorage.getItem('ticket_list') !== '' && (typeof this.state.tick_type !== 'object') && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd' && this.state.tick_type !== 'null') || (form_id == -1 || form_id == '' && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd' && this.state.tick_type !== 'null' && (typeof this.state.tick_type !== 'object'))) {
            //   console.log("call simple")
            this.setState({ ticket_Product_status: false })
            //  var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''
            //  console.log("this.state.ticket_Product_status",this.state.ticket_Product_status,ticket_Data)
            if (typeof this.state.tick_type !== 'object') {
                this.addSimpleProducttoCart(this.state.ticket_Product, localStorage.getItem('ticket_list') ? JSON.parse(localStorage.getItem('ticket_list')) : '')
            }
        }
    }

    addvariableProducttoCart(product) {
        //console.log(" this.state.ParentProductList", this.state.AllProduct)

        var variationProdect = this.state.AllProduct.filter(item => {
            return (item.ParentId === product.WPID && (item.ManagingStock == false || (item.ManagingStock == true && item.StockQuantity > -1)))
        })
        product['Variations'] = variationProdect

        if (product.IsTicket == true) {
            var tick_data = JSON.parse(product.TicketInfo)
            if (tick_data._ticket_availability == "open_ended") {

                this.getTicketFields(product);
                // var variationProdect=  this.state.AllProduct.filter(item=>{ 
                //     return  (item.ParentId===product.WPID && ( item.ManagingStock ==false ||( item.ManagingStock ==true && item.StockQuantity>-1)))    
                //   })

                // // console.log("FilterVariation",variationProdect,product);
                // product['Variations'] = variationProdect
                //console.log("add variation" , product,variationProdect)
                //  this.props.productData(product);  
            } else if (tick_data._ticket_availability == "range") {
                var availability_to_date = moment(tick_data._ticket_availability_to_date).format('YYYY-MM-DD')
                var today_date = moment().format('YYYY-MM-DD')
                // console.log("availability_to_date",availability_to_date,"today_date",today_date);
                if (availability_to_date > today_date) {
                    this.getTicketFields(product);
                    // var variationProdect=  this.state.AllProduct.filter(item=>{ 
                    //     return  (item.ParentId===product.WPID && ( item.ManagingStock ==false ||( item.ManagingStock ==true && item.StockQuantity>-1)))    
                    //   })

                    // // console.log("FilterVariation",variationProdect,product);
                    // product['Variations'] = variationProdect
                    //console.log("add variation" , product,variationProdect)
                    //     this.props.productData(product);  
                }
                // else if(availability_to_date == today_date || availability_to_date < today_date){
                //      alert('Ticket date is expired')


                //    }
            }
        }
        //else{
        // console.log("else part me jayega")
        //   var variationProdect=  this.state.AllProduct.filter(item=>{ 
        //     return  (item.ParentId===product.WPID && ( item.ManagingStock ==false ||( item.ManagingStock ==true && item.StockQuantity>-1)))    
        //   })

        //   //console.log("FilterVariation",variationProdect,product);
        //     product['Variations'] = variationProdect
        //console.log("add variation" , product,variationProdect)

        // this.props.productData(product);

        // }
        //   localStorage.removeItem("PRODUCT")
        //   localStorage.removeItem("SINGLE_PRODUCT")
        //   this.props.dispatch(cartProductActions.singleProductDiscount());
        this.props.productData(product);
        //this.props.productData(this.availableCartDataInProduct(product));
    }

    goBack() {
        history.push('/listview');
    }

    handleSimplePop(type, product) {

        if (type !== "variable") {
            if (product.IsTicket == true) {
                // console.log("type",type,"product_Data",product);
                var tick_data = JSON.parse(product.TicketInfo)
                this.getTicketFields(product)
                $('#VariationPopUp').modal('show');
                console.log("handleSimplePop",product)
                this.props.simpleProductData(product);
                // if(tick_data._ticket_availability == "open_ended"){
                //     this.getTicketFields(product)
                //     $('#VariationPopUp').modal('show');
                //     this.props.simpleProductData(product);
                // }else if(tick_data._ticket_availability == "range"){              
                //   var availability_to_date= moment( tick_data. _ticket_availability_to_date).format('YYYY-MM-DD')
                //   var today_date= moment().format('YYYY-MM-DD')
                //   console.log("availability_to_date",availability_to_date,"today_date",today_date);
                //   if(availability_to_date > today_date){
                //       this.getTicketFields(product)
                //       $('#VariationPopUp').modal('show');
                //       this.props.simpleProductData(product);
                //   }
                // //   else if(availability_to_date == today_date || availability_to_date < today_date){
                // //     alert('Ticket date is expired')


                // //   }

                // }



            } else {
           ///  alert("test");
                // console.log("product",product);
                $('#VariationPopUp').modal('show');
                this.props.simpleProductData(product);
                // this.props.simpleProductData(this.availableCartDataInProduct(product));
            }
        }
    }


    render() {
        const { active, product_List } = this.state;
        const { cartproductlist, productlist } = this.props;
        //  console.log("productlist",this.state.product_List,"this.state.isLoading",this.state.isLoading);

        const pStyle = {
            width: '75%'
        };
        const tdNotFound = {
            textAlign: "unset"
        };
        const pStylenotFound = {
            textAlign: 'center'
        };
        return (

            <div className="items pt-3">

                {/* {  this.state.isSearchStart===true ? <LoadingModal /> : ''}  */}
                {/* || this.props.productlist.length */}

                <div className="item-heading text-center">PRODUCT LIST</div>
                <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                    <div className="searchDiv" style={{ display: 'none' }}>
                        <input type="search" className="form-control nameSearch" id="product_search_field" placeholder="Search Product / Scan" />
                    </div>

                    {this.props.parantPage && this.props.parantPage == 'list' ? <div className="pl-1 pr-4 previews_setting">
                        <a href="/listview" className="back-button " id="mainBack" onClick={() => this.goBack()}>
                            <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                            <span>Back</span>
                        </a>
                    </div> : ""}

                    <table className="table ShopProductTable" id="all-product-list">
                        <colgroup>
                            <col style={{ width: '*' }} />
                            {/* <col style={{ width: 20 }} /> */}
                        </colgroup>


                        <tbody>

                            {(!this.state.product_List || this.state.product_List == '') ?
                                <tr>
                                    <td style={tdNotFound}><p style={pStylenotFound}>No product found</p></td>
                                    {/* <td >  <i className="fa fa-spinner fa-spin col-centered" >No Product Found</i></td> */}
                                </tr>
                                :

                                this.state.product_List && this.state.product_List.map((item, index) => {
                                    //console.log("item123",item);
                                    // if(item.ParentId == 0){
                                    var display_expireTicketTime;
                                    let isVariableProduct = (item.Type == "variable") ? true : false;
                                    if (item.IsTicket && item.IsTicket == true) {
                                        var ticketInfo = JSON.parse(item.TicketInfo);
                                        // ticketInfo._ticket_checkin_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)
                                        if (ticketInfo._ticket_availability.toLowerCase() == 'range' && (ticketInfo._ticket_availability_to_date)) {

                                            var dt = new Date(ticketInfo._ticket_availability_to_date);
                                            display_expireTicketTime = moment(dt).format('LT');
                                            //console.log("expirationDate1123", moment(dt).format('LT'))

                                        }
                                    }
                                    return (
                                        <tr className="pointer" key={index}
                                            data-toggle={isVariableProduct ? "modal" : ""} href={isVariableProduct ? "#VariationPopUp" : "javascript:void(0)"}
                                            onClick={isVariableProduct == true ? this.handleIsVariationProduct.bind(this, item.Type, item) : null} >

                                            <td className="pointer" onClick={this.handleSimplePop.bind(this, item.Type, item)}>{item.Title ? <Markup content={item.Title} /> : item.Sku? item.Sku:'N/A'}
                                                {display_expireTicketTime && display_expireTicketTime != '' &&
                                                    <label className='text-danger heading'>Valid till {display_expireTicketTime} </label>
                                                }
                                            </td>

                                            <td className="text-right pointer" onClick={this.handleSimplePop.bind(this, item.Type, item)}><NumberFormat value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </td>
                                            {
                                                (item.InStock) ? (
                                                    <td className="text-right pointer" onClick={this.handleIsVariationProduct.bind(this, item.Type, item)} style={{ padding: 20 }}>
                                                        <a>
                                                            <img src="assets/img/add_29.png" />
                                                        </a>
                                                    </td>
                                                ) : ((item.Type !== 'variable')?
                                                        <td className="text-right pointer" onClick={() => this.productOutOfStock(item.Title)} style={{ padding: 20 }}>
                                                            <a href="#outOfStockModal">
                                                                <img src="assets/img/add_29.png" />
                                                            </a>
                                                        </td>
                                                        :
                                                        <td className="text-right pointer" style={{ padding: 20 }}>
                                                            <a href="#outOfStockModal">
                                                                <img src="assets/img/add_29.png" />
                                                            </a>
                                                        </td>
                                                    )
                                            }
                                            {/* <td style={{ display: "none" }}>{item.has_attribute.toString().replace(",", " ")}</td>
                                            <td style={{ display: "none" }}>{item.has_subattribute.toString().replace(",", " ")}</td>
                                            <td style={{ display: "none" }}>{item.Categories.toString().replace(",", " ")}</td>
                                            <td style={{ display: "none" }}>{item.Sku ? item.Sku : ''}</td>
                                            <td style={{ display: "none" }}>{item.Sku ? item.Barcode : ''}</td> */}

                                        </tr>
                                    )
                                    // }
                                })
                            }


                        </tbody>
                    </table>

                    {
                        // this.state.productDisplayStatus == true?
                        ((!this.state.search) && this.state.totalRecords > this.state.chunk_size * this.state.pageNumber && this.state.totalRecords > this.state.chunk_size) ?
                            <div className="createnewcustomer">
                                <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" id='hideButton' onClick={() => this.loadingData()}>Load More</button>
                            </div>
                            :
                            (this.state.search && this.state.totalRecords > this.state.chunk_size * this.state.pageNumber && this.state.totalRecords > this.state.chunk_size) ?
                                <div className="createnewcustomer">
                                    <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" id='hideButton' onClick={() => this.loadingFilterData()}>Load More</button>
                                </div>
                                :
                                <div />
                        //  <div className="createnewcustomer">
                        //     <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" onClick={() => this.loadingData()} disabled> Load More</button>
                        // </div>



                    }

                </div>

            </div>

        )
    }

}


function mapStateToProps(state) {
    const { productlist, cartproductlist, filteredProduct, checkout_list } = state;
    return {
        productlist: productlist.productlist,
        cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],//cartproductlist.cartproductlist,
        filteredProduct: filteredProduct.items,
        checkout_list: checkout_list.items
    };
}

const connectedList = connect(mapStateToProps)(AllProduct);
export { connectedList as AllProduct };