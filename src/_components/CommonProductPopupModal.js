import React from 'react';
import { connect } from 'react-redux';
import { ProductAtrribute } from './';
import { cartProductActions } from '../_actions/cartProduct.action'
import { Markup } from 'interweave';
import { default as NumberFormat } from 'react-number-format';
import { LoadingModal } from './'

class CommonProductPopupModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            variationStockQunatity: '',
            variationTitle: '',
            variationImage: '',
            variationoptionArray: {},
            filteredAttribute: [],
            filterTerms: [],
            selectedAttribute: "",
            ManagingStock: null,
            old_price: 0,
            incl_tax: 0,
            excl_tax: 0,
            variationStyles: { cursor: "no-drop", pointerEvents: "none" },
            variationfound: null,
            showSelectStatus: false,
            showQantity: false,
            after_discount: 0,
            discount_type: '',
            new_product_discount_amount: 0,
            product_after_discount: 0,
            product_discount_amount: 0

        }
        this.clicks = [];
        this.timeout;
        this.timer = 0;
        this.delay = 300;
        this.prevent = false;
        this.incrementDefaultQuantity = this.incrementDefaultQuantity.bind(this);
        this.setDefaultQuantity = this.setDefaultQuantity.bind(this);
        this.decrementDefaultQuantity = this.decrementDefaultQuantity.bind(this);
        this.addVariationProductToCart = this.addVariationProductToCart.bind(this);
        this.addSimpleProducttoCart = this.addSimpleProducttoCart.bind(this);
        this.optionClick = this.optionClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
 

    componentWillUnmount() {
        this.setState({
            variationStockQunatity: '',
            variationTitle: '',
            variationImage: '',
            variationoptionArray: {},
            filteredAttribute: []

        });
    }

    incrementDefaultQuantity() {
        const { showSelectedProduct } = this.props;
        const { showSelectStatus } = this.state;
        console.log("setDefaultQuantity:" , showSelectedProduct, showSelectStatus)
        if(showSelectedProduct){
            console.log("setDefaultQuantity:" , this.state.variationfound, this.props.getVariationProductData)
            let qty = parseInt(this.state.variationDefaultQunatity);
            if(this.state.variationfound?showSelectedProduct.WPID == this.state.variationfound.WPID : showSelectedProduct.WPID == this.props.getVariationProductData.WPID){
                var maxQty = parseFloat(this.state.variationStockQunatity) + parseFloat(showSelectedProduct.quantity);
               // console.log("STEP 01", maxQty, qty)

            if (maxQty == 'Unlimited' || qty < maxQty) {
                qty++;
            }
             this.setDefaultQuantity(qty);
            }else{
               var maxQty = $("#txtInScock").text();
              // console.log("STEP 03", maxQty, qty)

            if (maxQty == 'Unlimited' || qty < maxQty) {
                qty++;
            }
            if (qty > this.state.variationStockQunatity)
                qty = this.state.variationStockQunatity;
            this.setDefaultQuantity(qty);
            }
            
           
            
        }else{
            var maxQty = $("#txtInScock").text();
           // console.log("STEP 02", maxQty)
        
        if (maxQty == 'Unlimited' || this.state.variationDefaultQunatity >= 0) {
            var product = this.state.getVariationProductData
            let qty = parseInt(this.state.variationDefaultQunatity);
            // if ((product.StockStatus == null || product.StockStatus == 'instock')
            //     && (product.ManagingStock == false || (product.ManagingStock == true && qty < this.state.variationStockQunatity))) {
            //     qty++;
            // }
            if (maxQty == 'Unlimited' || qty < maxQty) {
                qty++;
            }
            if (qty > this.state.variationStockQunatity)
                qty = this.state.variationStockQunatity;
            this.setDefaultQuantity(qty);
        }
      }

    }

    decrementDefaultQuantity() {
        if (this.state.variationDefaultQunatity && this.state.variationDefaultQunatity > 1) {
            let qty = parseInt(this.state.variationDefaultQunatity);
            qty--;
            this.setDefaultQuantity(qty);
        }
    }

    setDefaultQuantity(qty) {
        this.setState({
            variationDefaultQunatity: qty,
        });
    }

    addVariationProductToCart() {
        let showSelectedProduct = this.props.showSelectedProduct;
        let cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
        var ticket_Data = this.state.ticket_status == true ? localStorage.getItem('ticket_list') ? JSON.parse(localStorage.getItem('ticket_list')) : '' : ''
        var tick_data = this.state.getVariationProductData ? JSON.parse(this.state.getVariationProductData.TicketInfo) : '';
        let price = parseFloat(this.state.variationPrice);
        var data = null;
        const { single_product } = this.props;
        // console.log("enter in variation", data, this.state.variationfound, this.state.variationParentId, this.state.variationDefaultQunatity)
        if (cartlist.length > 0 && !single_product) {
            //  console.log("STEP 01")
            cartlist.map(prdId => {
                if (prdId.product_id === this.state.variationfound.ParentId && prdId.variation_id === this.state.variationfound.WPID) {
                    this.state.variationfound['after_discount'] = prdId.after_discount;
                    this.state.variationfound['product_discount_amount'] = prdId.product_discount_amount;
                    this.state.variationfound['product_after_discount'] = prdId.product_after_discount;
                    this.state.variationfound['new_product_discount_amount'] = prdId.new_product_discount_amount;
                    this.state.variationfound['discount_amount'] = prdId.discount_amount;
                    this.state.variationfound['discount_type'] = prdId.discount_type;
                    this.state.variationfound['cart_after_discount'] = prdId.cart_after_discount;
                    this.state.variationfound['cart_discount_amount'] = prdId.cart_discount_amount;
                }
            })
        }
        data = {
            line_item_id: this.state.variationfound.line_item_id?this.state.variationfound.line_item_id:0,
            quantity: this.state.variationDefaultQunatity,
            Title: this.state.variationfound.Title && this.state.variationfound.Title !="" ?this.state.variationfound.Title :this.state.variationfound.Sku,
            Price: this.state.variationfound.Price ? parseInt(this.state.variationDefaultQunatity) * this.state.variationfound.Price : parseInt(this.state.variationDefaultQunatity) * price,
            product_id: this.state.variationfound.ParentId,
            variation_id: this.state.variationfound.WPID,
            isTaxable: this.state.variationIsTaxable,
            old_price: this.state.variationfound.old_price ? this.state.variationfound.old_price : this.state.old_price,
            incl_tax: this.state.variationfound.incl_tax ? this.state.variationfound.incl_tax : this.state.incl_tax,
            excl_tax: this.state.variationfound.excl_tax ? this.state.variationfound.excl_tax : this.state.excl_tax,
            ticket_status: this.state.ticket_status,
            product_ticket: this.state.ticket_status == true ? ticket_Data ? ticket_Data : '' : '',
            tick_event_id: this.state.ticket_status == true ? tick_data._event_name : null,
            cart_after_discount: this.state.variationfound.cart_after_discount ? this.state.variationfound.cart_after_discount : 0,
            cart_discount_amount: this.state.variationfound.cart_discount_amount ? this.state.variationfound.cart_discount_amount : 0,
            after_discount: this.state.variationfound.after_discount ? this.state.variationfound.after_discount : 0,
            discount_amount: this.state.variationfound.discount_amount ? this.state.variationfound.discount_amount : 0,
            product_after_discount: this.state.variationfound.product_after_discount ? this.state.variationfound.product_after_discount : 0,
            product_discount_amount: this.state.variationfound.product_discount_amount ? this.state.variationfound.product_discount_amount : 0,
            discount_type: this.state.variationfound.discount_type ? this.state.variationfound.discount_type : "",
            new_product_discount_amount: this.state.variationfound.new_product_discount_amount ? this.state.variationfound.new_product_discount_amount : 0
        }
         console.log("data of variation", data)
        var qty = 0;
        var product = this.state.getVariationProductData
        var variationfound = this.state.variationfound;
        let variationQantity = (variationfound.StockStatus == "outofstock") ? "outofstock" :
            (variationfound.StockStatus == null || variationfound.StockStatus == 'instock') && variationfound.ManagingStock == false ? "Unlimited" : (typeof variationfound.StockQuantity != 'undefined') && variationfound.StockQuantity != '' ? variationfound.StockQuantity - qty : '0'

        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
        qty = qty + this.state.variationDefaultQunatity;
        var txtPrdQuantity = document.getElementById("qualityUpdater").value;

        if (parseInt(txtPrdQuantity) <= 0) {
            alert("Product quantity must be greater than zero!")
            return;
        } else if (variationQantity == 'Unlimited' || qty <= this.state.variationStockQunatity) {
            //checking if variation reacord editing and  new variation added then remove first one from cart----------------------
            //---Remove item already exit into cart at the edit time------------------------
            console.log("type of", typeof showSelectedProduct, showSelectedProduct)
            if (cartlist.length > 0) {

                var isItemFoundToUpdate = false;
                cartItemList.map((item, index) => {
                    if (typeof showSelectedProduct !== 'undefined' && showSelectedProduct !== null) {
                        var _index = -1;
                        if (showSelectedProduct['selectedIndex'] >= 0) { _index = parseInt(showSelectedProduct.selectedIndex) }


                        if (_index > -1 && showSelectedProduct.selectedIndex == index) {
                            isItemFoundToUpdate = true;
                            cartItemList[index] = data
                        }
                    }
                })
                // console.log("isItemFoundToUpdate", isItemFoundToUpdate)

                if (isItemFoundToUpdate == false) {

                    cartItemList.push(data);
                }
            }
            else {

                cartItemList.push(data);
            }

            //--------------------------------------------------------------------------------------------

            localStorage.removeItem("PRODUCT");
            localStorage.removeItem("SINGLE_PRODUCT")
            this.props.dispatch(cartProductActions.addtoCartProduct(cartItemList));
            this.props.dispatch(cartProductActions.showSelectedProduct(null));
            this.props.dispatch(cartProductActions.singleProductDiscount());
            this.state.showSelectStatus = false;
            this.state.variationfound = null;
            //-----------------------------------------------------------------  

            this.setState({
                variationTitle: this.props.getVariationProductData.Title && this.props.getVariationProductData.Title !="" ?this.props.getVariationProductData.Title :this.props.getVariationProductData.Sku,
                variationId: this.props.getVariationProductData.WPID,
                variationParentId: this.props.getVariationProductData.ParentId,
                variationPrice: this.props.getVariationProductData.Price,
                variationStockQunatity: this.props.getVariationProductData ? (this.props.getVariationProductData.StockStatus == null || this.props.getVariationProductData.StockStatus == 'instock') && this.props.getVariationProductData.ManagingStock == false ? "Unlimited" : this.props.getVariationProductData.StockQuantity : '0',
                variationImage: this.props.getVariationProductData ? this.props.getVariationProductData.ProductImage : null,
                variationIsTaxable: this.props.getVariationProductData ? this.props.getVariationProductData.Taxable : 0,
                variationDefaultQunatity: 1,
                filteredAttribute: [],
                filterTerms: [],
                ManagingStock: this.props.getVariationProductData ? this.props.getVariationProductData.ManagingStock : null,
                old_price: this.props.getVariationProductData ? this.props.getVariationProductData.old_price : 0,
                incl_tax: this.props.getVariationProductData ? this.props.getVariationProductData.incl_tax : 0,
                excl_tax: this.props.getVariationProductData ? this.props.getVariationProductData.excl_tax : 0,
                ticket_status: this.props.getVariationProductData.IsTicket,
                variationfound:null

            });

            $(".button_with_checkbox input").prop("checked", false);
            this.state.variationStyles = { cursor: "no-drop", pointerEvents: "none" }
            $("#add_variation_product_btn").css({ "cursor": "no-drop", "pointer-events": "none" });

            //--------------------------------------------------------------------
         //   this.forceUpdate()
            // $(".close").trigger("click");
            $('#VariationPopUp').modal('hide')
        }

        else {
            // alert("this")
            $('#VariationPopUp').modal('hide')
            $('#outOfStockModal').modal('show')
        }

        this.setState({
            variationDefaultQunatity: 1

        })
        //     }
      //  this.forceUpdate()
        //$(".close").trigger("click");
        $('#VariationPopUp').modal('hide')
    }

    addSimpleProducttoCart() {
        const { dispatch, single_product, showSelectedProduct } = this.props;
        //var cartlist = cartproductlist ? cartproductlist : [];
        var ticket_Data = this.state.ticket_status == true ? localStorage.getItem('ticket_list') ? JSON.parse(localStorage.getItem('ticket_list')) : '' : ''
        let cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
        var tick_data = this.state.getVariationProductData ? JSON.parse(this.state.getVariationProductData.TicketInfo) : '';
        var data = null;
       // console.log("enter in simple ", single_product, this.state.getVariationProductData, cartlist)
        var SingleProduct = null;
        if (single_product) {
            if (single_product.WPID == this.state.getVariationProductData.WPID) {
                SingleProduct = single_product
            } else {
                SingleProduct = this.state.getVariationProductData
            }
        } else {
            if (cartlist.length > 0) {
                cartlist.map(prdId => {
                    if (prdId.product_id === this.state.getVariationProductData.WPID) {
                        SingleProduct = this.state.getVariationProductData
                        SingleProduct['after_discount'] = prdId.after_discount;
                        SingleProduct['product_discount_amount'] = prdId.product_discount_amount;
                        SingleProduct['product_after_discount'] = prdId.product_after_discount;
                        SingleProduct['new_product_discount_amount'] = prdId.new_product_discount_amount;
                        SingleProduct['discount_amount'] = prdId.discount_amount;
                        SingleProduct['discount_type'] = prdId.discount_type;
                        SingleProduct['cart_after_discount'] = prdId.cart_after_discount;
                        SingleProduct['cart_discount_amount'] = prdId.cart_discount_amount;
                        SingleProduct['line_item_id'] = prdId.line_item_id;
                    }
                })
            }
        }
        data = {
            line_item_id: SingleProduct ? SingleProduct.line_item_id :0,
            cart_after_discount: SingleProduct ? SingleProduct.cart_after_discount : 0,
            cart_discount_amount: SingleProduct ? SingleProduct.cart_discount_amount : 0,
            after_discount: SingleProduct ? SingleProduct.after_discount : 0,
            discount_amount: SingleProduct ? SingleProduct.discount_amount : 0,
            product_after_discount: SingleProduct ? SingleProduct.product_after_discount : 0,
            product_discount_amount: SingleProduct ? SingleProduct.product_discount_amount : 0,
            quantity: this.state.variationDefaultQunatity,
            Title: this.state.variationTitle,
            Price: parseInt(this.state.variationDefaultQunatity) * parseFloat(this.state.variationPrice),
            product_id: this.state.getVariationProductData.WPID ? this.state.getVariationProductData.WPID : this.state.getVariationProductData.product_id,
            variation_id: 0,
            isTaxable: this.state.variationIsTaxable,
            old_price: this.state.old_price,
            incl_tax: this.state.incl_tax,
            excl_tax: this.state.excl_tax,
            ticket_status: this.state.ticket_status,
            product_ticket: this.state.ticket_status == true ? ticket_Data ? ticket_Data : '' : '',
            tick_event_id: this.state.ticket_status == true ? tick_data._event_name : null,
            discount_type: SingleProduct ? SingleProduct.discount_type : "",
            new_product_discount_amount: SingleProduct ? SingleProduct.new_product_discount_amount : 0
            //stock_quantity: this.state.variationStockQunatity != 'Unlimited' ? this.state.variationStockQunatity : this.state.variationStockQunatity
        }
        // console.log("data of simple", data)
        var product = this.state.getVariationProductData
        var qty = 0;
        cartlist.map(item => {
            if (product.WPID === item.product_id) {
                qty = item.quantity;
            }

        })

        var txtPrdQuantity = document.getElementById("qualityUpdater").value;
        if (parseInt(txtPrdQuantity) <= 0) {
            alert("Product quantity must be greater than zero!")
            return;
        }

        // if (((product.StockStatus == null || product.StockStatus == 'instock') && product.ManagingStock == false) || ((product.StockStatus == null || product.StockStatus == 'instock') &&
        //     product.ManagingStock == true && qty <= product.StockQuantity)) {
        //    console.log("qty",qty,"this.state.variationStockQunatity",this.state.variationStockQunatity)
        if (this.state.variationStockQunatity == 'Unlimited' || qty <= this.state.variationStockQunatity || qty <= product.StockQuantity) {

            if (showSelectedProduct && cartlist.length > 0) {
                var isItemFoundToUpdate = false;
                cartlist.map((item, index) => {
                    if (typeof showSelectedProduct !== 'undefined' && showSelectedProduct !== null) {
                        var _index = -1;
                        if (showSelectedProduct['selectedIndex'] >= 0) { _index = parseInt(showSelectedProduct.selectedIndex) }
                        if (_index > -1 && showSelectedProduct.selectedIndex == index) {
                            isItemFoundToUpdate = true;
                            cartlist[index] = data
                        }
                    }
                })
                if (isItemFoundToUpdate == false) {

                    cartlist.push(data);
                }
            } else {
                cartlist.push(data);
            }
            this.setState({
                showSelectStatus: false,
                variationfound: null
            })

            this.stockUpdateQuantity(cartlist, data);
            localStorage.removeItem("PRODUCT");
            localStorage.removeItem("SINGLE_PRODUCT")
            dispatch(cartProductActions.addtoCartProduct(cartlist)); // this.state.cartproductlist
            dispatch(cartProductActions.showSelectedProduct(null));
            dispatch(cartProductActions.singleProductDiscount());
            this.state.showSelectStatus = false;
            $('#VariationPopUp').modal('hide')
            //this.setState({variationStockQunatity:this.state.variationStockQunatity-qty});
        } else {
            $('#VariationPopUp').modal('hide')
            $('#outOfStockModal').modal('show')

        }
    }

    stockUpdateQuantity(cardData, data) {
        var qty = 0
        cardData.map(item => {
            if (data.product_id === item.product_id) {
                qty += item.quantity;

            }

        })
        //  this.setState({variationStockQunatity:this.state.variationStockQunatity == 'Unlimited'?this.state.variationStockQunatity:this.props.getVariationProductData.StockQuantity - qty })
        this.setState({
            variationStockQunatity: this.props.getVariationProductData ?
                (this.props.getVariationProductData.StockStatus == null || this.props.getVariationProductData.StockStatus == 'instock') && this.props.getVariationProductData.ManagingStock == false ? "Unlimited" : (typeof this.props.getVariationProductData.StockQuantity != 'undefined') && this.props.getVariationProductData.StockQuantity != '' ? this.props.getVariationProductData.StockQuantity - qty : '0' : '0',
                variationDefaultQunatity:1
            })
    }

    componentWillMount = props => {
        this.clickTimeout = null

        this.setState({ filterTerms: [] })
    }

    componentDidMount() {

        setTimeout(() => {
            $(".button_with_checkbox input").prop("checked", false);
        }, 300);

    }

    optionClick(option, attribute) {
        var filterTerms = this.state.filterTerms;
        var optExist = false;

        filterTerms && filterTerms.map(opItem => {
            if (opItem.attribute === attribute) {
                opItem.attribute = attribute;
                opItem.option = option;
                optExist = true
            }
        })

        if (optExist == false) {
            filterTerms.push({
                attribute: attribute,
                option: option
            })
            // this.state.filterTerms=filterTerms
            // this.setState({ filterTerms: filterTerms })  
        }

        this.setState({ filterTerms: filterTerms })


        if (this.clickTimeout !== null) {
            clearTimeout(this.clickTimeout)
            this.clickTimeout = null
            this.setState({ filteredAttribute: [] });
        }
        else {
            this.clickTimeout = setTimeout(() => {
                clearTimeout(this.clickTimeout)
                this.clickTimeout = null
            }, 300);
            this.setState({
                filteredAttribute: [],
                selectedAttribute: attribute
            });
            if (this.props.getVariationProductData.ProductAttributes && this.props.getVariationProductData.ProductAttributes.length > 1) {
                var filteredAttribute = this.props.getVariationProductData.Variations.filter(item => {
                    var optionRes = option.replace(/\s/g, '-').toLowerCase();
                    optionRes = optionRes.replace(/\//g, "-").toLowerCase();
                    var isExist = false;
                    item.combination.split("~").map(combination => {
                        if (combination.replace(/\s/g, '-').replace(/\//g, "-").toLowerCase() === optionRes || combination=="**")
                            isExist = true;
                    })
                    return isExist;

                })
                this.setState({ filteredAttribute: filteredAttribute })

            }

            let attributeLenght = this.getAttributeLenght();
            this.searchvariationProduct(option);

        }
    }

    doClickAction() {

    }

    combo(c) {
        var r = [];
        var len = c.length;
        var tmp = [];
        function nodup() {
            var got = {};
            for (var l = 0; l < tmp.length; l++) {
                if (got[tmp[l]]) return false;
                got[tmp[l]] = true;
            }
            return true;
        }
        function iter(col, done) {
            var l, rr;
            if (col === len) {
                if (nodup()) {
                    rr = [];
                    for (l = 0; l < tmp.length; l++)
                        rr.push(c[tmp[l]]);
                    r.push(rr.join('~'));
                }
            } else {
                for (l = 0; l < len; l++) {
                    tmp[col] = l;
                    iter(col + 1);
                }
            }
        }
        iter(0);
        return r;
    }

    searchvariationProduct(options) {
        var filteredArr = []
        this.state.showQantity = false
        this.state.filterTerms.map(itm => {

console.log("itm",itm)
            let attribute_list = localStorage.getItem("attributelist") ? JSON.parse(localStorage.getItem("attributelist")) : null;
            var sub_attribute;
            var found = attribute_list.find(function (element) {
                return element.Code.toLowerCase() == itm.attribute.toLowerCase()
            })
            if (found) {
                let SubAttributes = found.SubAttributes;
                if (SubAttributes) {
                    sub_attribute = SubAttributes.find(function (element) {
                        return (element.Value).toLowerCase() == itm.option.toLowerCase();
                    })
                }


            }
            filteredArr.push(sub_attribute ? sub_attribute.Code : itm.option);

        })

        var cominationArr = this.combo(filteredArr);
        let variations = this.state.getVariationProductData && this.state.getVariationProductData.Variations;
        // let variations = this.props.getVariationProductData && this.props.getVariationProductData.Variations;
        var found = variations.find(function (element) {
            var checkExist = false;
          
            cominationArr && cominationArr.map(comb => {
                console.log("element.combination",element.combination,comb)
                if ( // element.combination.includes("**") ||
                    element.combination.replace(/\s/g, '-').replace(/\//g, "-").toLowerCase() === comb.replace(/\s/g, '-').replace(/\//g, "-").toLowerCase()
                ) {
                    checkExist = true;
                    return element;
                }
            })

            ///------check 'Any One' option --------------------------------
            if(element.combination.includes("**") )
            {
                var  elementComb= element.combination.substring(0, element.combination.indexOf("**")-1);
                console.log("elementComb",elementComb);
                cominationArr && cominationArr.map(comb => {
                    console.log("element.combination",element.combination,comb)
                    if ( comb.replace(/\s/g, '-').replace(/\//g, "-").toLowerCase().includes(elementComb)  ) {
                        checkExist = true;
                        return element;
                    }
                })
            }
            //--------------------------------------------------------------
            if (checkExist == true) {
                return element;
            }

        });

        console.log("Found", found);
        if (this.props.single_product) {
            if (found.WPID !== this.props.single_product.WPID) {
                localStorage.removeItem("PRODUCT");
                localStorage.removeItem("SINGLE_PRODUCT")
                this.props.dispatch(cartProductActions.singleProductDiscount());
            }
        }

        this.setState({ showSelectStatus: false })

        if (typeof found !== 'undefined') {

            let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
            var qty = 0;
            cartItemList.map(item => {
                if (found.WPID == item.variation_id) {

                    qty = item.quantity;

                }
            })
            this.state.variationfound = found;

            this.setState({
                variationTitle: found.Title && found.Title !=""?found.Title:found.Sku,
                variationId: found.WPID,
                variationParentId: found.ParentId,
                variationPrice: found.Price,                
                variationStockQunatity: (found.ManagingStock == false && found.StockStatus == "outofstock") ? "outofstock" : (found.StockStatus == null || found.StockStatus == 'instock') && found.ManagingStock == false ? "Unlimited" : found.StockQuantity - qty,
                variationImage: (found.ProductImage == null) ? this.state.variationImage : found.ProductImage,
                variationIsTaxable: found.Taxable,
                variationDefaultQunatity: 1,
                ManagingStock: found.ManagingStock,
                old_price: found.old_price,
                incl_tax: this.state.incl_tax,
                excl_tax: this.state.excl_tax,
                variationfound: found
                // variationProduct:

            });
            this.state.variationStyles = { cursor: "pointer", pointerEvents: "auto" }
            $("#add_variation_product_btn").css({ "cursor": "pointer", "pointer-events": "auto" });
        }
        else {
            this.setState({

                variationParentId: 0,
                variationPrice: 0,
                variationStockQunatity: 0,
                variationImage: "",
                ManagingStock: null,

            });
        }

    }

    getAttributeLenght() {
        return this.props.getVariationProductData.ProductAttributes.length;
    }

    componentWillReceiveProps(nextPros) {
        localStorage.removeItem("CART_QTY")
        console.log("%c RECEIVE PRODUCT DATA", "color:green", nextPros)
        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [];
       //  console.log("cartItemList",cartItemList)
        var qty = 0;
        if (cartItemList && cartItemList.length > 0) {
            cartItemList.map(item => {
                if (nextPros.getVariationProductData && nextPros.getVariationProductData.Type == "variable") {
                    if (nextPros.getVariationProductData && nextPros.getVariationProductData.WPID == item.product_id) {

                        qty = item.quantity;
                     
                    }
                } else {
                    if (nextPros.getVariationProductData && nextPros.getVariationProductData.WPID == item.product_id) {
//alert('hhh')
                     //   console.log("hhhhh")
                        qty = item.quantity;
                        this.setState({variationStockQunatity:nextPros.getVariationProductData ?
                            (nextPros.getVariationProductData.StockStatus == null || nextPros.getVariationProductData.StockStatus == 'instock') && nextPros.getVariationProductData.ManagingStock == false ? "Unlimited" 
                            : (typeof nextPros.getVariationProductData.StockQuantity != 'undefined') && nextPros.getVariationProductData.StockQuantity != '' ? nextPros.getVariationProductData.StockQuantity - qty: '0' : '0'})     
                    }
                }
            })

        }
        //  for current show name of product 

        if (nextPros.showSelectedProduct) {
            this.setState({
                showSelectStatus: true,
                variationStyles: { cursor: "pointer", pointerEvents: "auto" }


            })

            this.state.showSelectStatus = true;
            if (nextPros.getVariationProductData && nextPros.getVariationProductData.Type == "variable") {
                this.state.variationfound = nextPros.showSelectedProduct;
                qty = nextPros.showSelectedProduct.quantity;
                this.setState({ variationfound: nextPros.showSelectedProduct })
            } else {

            }
            if (nextPros.get_single_inventory_quantity) {
                if (nextPros.getVariationProductData && nextPros.getVariationProductData.Type == "variable") {
                    if (nextPros.showSelectedProduct.WPID !== nextPros.get_single_inventory_quantity.wpid) {
                        this.setState({ showSelectStatus: false })
                    }else{
                        nextPros.showSelectedProduct['StockQuantity'] = nextPros.get_single_inventory_quantity.quantity;
                    }
                }
            }
            if (nextPros.single_product) {
                this.setState({ showSelectStatus: false })
            }

        }


        if (nextPros.single_product && nextPros.getVariationProductData) {
            if (nextPros.getVariationProductData.Type == "simple") {
                if (nextPros.getVariationProductData.WPID !== nextPros.single_product.WPID) {
                    //  alert("when simple case not match id")
                    localStorage.removeItem("PRODUCT");
                    localStorage.removeItem("SINGLE_PRODUCT")
                    this.props.dispatch(cartProductActions.singleProductDiscount());
                } else {
                    //  alert("when simple case match id")
                }

            } else {
                //  alert("when varition case")
                this.state.variationfound = nextPros.single_product;
                this.setState({ variationfound: nextPros.single_product })
            }
        }
        
        if (nextPros.get_single_inventory_quantity) {
            if (nextPros.getVariationProductData.Type == "variable") {
                nextPros.getVariationProductData.Variations.map(updateItem => {
                    if (updateItem.WPID == nextPros.get_single_inventory_quantity.wpid){
                        updateItem['StockQuantity'] = nextPros.get_single_inventory_quantity.quantity
                        this.state.showQantity = true
                    }else{
                        this.state.showQantity = false  
                    }
                })
                if(this.state.showQantity == true){
                let FindItems = nextPros.getVariationProductData.Variations.find(item => item.WPID === nextPros.get_single_inventory_quantity.wpid)
                this.state.variationTitle = FindItems && FindItems.Title ? FindItems.Title  &&  FindItems.Title!="":  FindItems.Sku?FindItems.Sku:"";
                this.state.variationImage = FindItems && FindItems.ProductImage ? FindItems.ProductImage : '';
                this.state.variationPrice = FindItems && FindItems.Price ? FindItems.Price : '';
                this.state.old_price = FindItems && FindItems.old_price ? FindItems.old_price : '';
                this.state.ManagingStock = FindItems && FindItems.ManagingStock ? FindItems.ManagingStock : '';
                this.state.variationfound = FindItems ? FindItems : '';
                this.setState({
                    variationTitle: FindItems && FindItems.Title &&  FindItems.Title!=""? FindItems.Title : FindItems.Sku?FindItems.Sku:'',
                    variationImage: FindItems && FindItems.ProductImage ? FindItems.ProductImage : '',
                    variationPrice: FindItems && FindItems.Price ? FindItems.Price : '',
                    old_price: FindItems && FindItems.old_price ? FindItems.old_price : '',
                    ManagingStock: FindItems && FindItems.ManagingStock ? FindItems.ManagingStock : '',
                    variationfound: FindItems ? FindItems : '',
                    //showQantity: true
                })
            }
            } else {
                if (nextPros.getVariationProductData.WPID == nextPros.get_single_inventory_quantity.wpid){
                    nextPros.getVariationProductData['StockQuantity'] = nextPros.get_single_inventory_quantity.quantity
                    this.state.showQantity = true
                    this.setState({ showQantity: true })
                }else{
                    this.state.showQantity = false
                    this.setState({ showQantity: false })
                }
            }
        }

        if (nextPros.getVariationProductData) {
         // console.log("ckeck Prop",  nextPros.getVariationProductData.Title,nextPros.getVariationProductData.Sku)
            this.setState({
                getVariationProductData: nextPros.getVariationProductData,
                hasVariationProductData: true,
                loadProductAttributeComponent: true,
                variationOptionclick: 0,
                variationTitle: this.state.showQantity == true ? this.state.variationTitle : nextPros.getVariationProductData ? nextPros.getVariationProductData.Title && nextPros.getVariationProductData.Title!=""?nextPros.getVariationProductData.Title  :nextPros.getVariationProductData.Sku:  '',
                variationId: 0,
                variationPrice: this.state.showQantity == true ? this.state.variationPrice : nextPros.getVariationProductData ? nextPros.getVariationProductData.Price : 0,
                variationStockQunatity: this.state.showQantity == true ? nextPros.get_single_inventory_quantity && nextPros.get_single_inventory_quantity.quantity - qty :
                    (nextPros.getVariationProductData.ManagingStock == false  && nextPros.getVariationProductData.StockStatus == "outofstock") ? "outofstock" :
                        (nextPros.getVariationProductData.StockStatus == null || nextPros.getVariationProductData.StockStatus == 'instock') && nextPros.getVariationProductData.ManagingStock == false ? "Unlimited" : (typeof nextPros.getVariationProductData.StockQuantity != 'undefined') && nextPros.getVariationProductData.StockQuantity != '' ? nextPros.getVariationProductData.StockQuantity - qty : '0',

                variationImage: this.state.showQantity == true ? this.state.variationImage : nextPros.getVariationProductData ? nextPros.getVariationProductData.ProductImage ? nextPros.getVariationProductData.ProductImage : '' : '',
                variationDefaultQunatity: nextPros.showSelectedProduct && qty > 0 ? qty : 1, //nextPros.getVariationProductData && nextPros.getVariationProductData.DefaultQunatity!=="" ? nextPros.getVariationProductData.DefaultQunatity : '1',                
                ManagingStock: this.state.showQantity == true ? this.state.ManagingStock : nextPros.getVariationProductData.ManagingStock,
                old_price: this.state.showQantity == true ? this.state.old_price : nextPros.getVariationProductData ? nextPros.getVariationProductData.old_price : 0,
                incl_tax: nextPros.getVariationProductData ? nextPros.getVariationProductData.incl_tax : 0,
                excl_tax: nextPros.getVariationProductData ? nextPros.getVariationProductData.excl_tax : 0,
                ticket_status: nextPros.getVariationProductData ? nextPros.getVariationProductData.IsTicket : '',
                after_discount: nextPros.after_discount ? nextPros.after_discount : 0

                //product_ticket:nextPros.getVariationProductData.IsTicket==true ? ticket_Data:''
            });
            //   }
            if (nextPros.showSelectedProduct || nextPros.single_product) {
                var prd = nextPros.showSelectedProduct ? nextPros.showSelectedProduct : nextPros.single_product;
                console.log("when open cartlist popup", prd)
                
                this.state.variationDefaultQunatity = prd.quantity;
                console.log("when open cartlist popup", this.state.variationDefaultQunatity)
                this.setState({
                    variationStockQunatity: (prd.StockStatus == "outofstock") ? "outofstock" :
                        (prd.StockStatus == null || prd.StockStatus == 'instock') && prd.ManagingStock == false ? "Unlimited" : (typeof prd.StockQuantity != 'undefined') && prd.StockQuantity != '' ? prd.StockQuantity - qty : '0',
                        variationDefaultQunatity: prd.quantity   
               
                    })
              

            }
            // nextPros.showSelectedProduct
            // variationStockQunatity
        }

    }

    handleChange(e) {
        if (e.target.value === "") {
            this.setState({ variationDefaultQunatity: 0 });
        }
        else if (e.target.value && !isNaN(e.target.value) && !e.target.value.includes(".")) {
            if (this.state.variationStockQunatity == "Unlimited" || parseInt(this.state.variationStockQunatity) >= parseInt(e.target.value)) {
                this.setState({ variationDefaultQunatity: parseInt(e.target.value) });
            }
        }
    }

    handleNoChange(e) { }

    handleClose() {
        $(".button_with_checkbox input").prop("checked", false);


        if (this.props.getVariationProductData) {
            this.setState({
                showSelectStatus: false,
                hasVariationProductData: true,
                loadProductAttributeComponent: true,
                variationOptionclick: 0,
                variationTitle: this.props.getVariationProductData ? this.props.getVariationProductData.Title : '',
                variationId: 0,
                variationPrice: this.props.getVariationProductData ? this.props.getVariationProductData.Price : 0,
                //variationStockQunatity: this.props.getVariationProductData ?
                // (this.props.getVariationProductData.StockStatus == null || this.props.getVariationProductData.StockStatus == 'instock') && this.props.getVariationProductData.ManagingStock == false ? "Unlimited" : (typeof this.props.getVariationProductData.StockQuantity != 'undefined') && this.props.getVariationProductData.StockQuantity != '' ? this.props.getVariationProductData.StockQuantity : '0' : '0',
                variationImage: this.props.getVariationProductData ? this.props.getVariationProductData.ProductImage ? this.props.getVariationProductData.ProductImage : '' : '',
                variationDefaultQunatity: 1 ? 1 : this.props.getVariationProductData ? this.props.getVariationProductData.DefaultQunatity : '',
                ManagingStock: this.props.getVariationProductData ? this.props.getVariationProductData.ManagingStock : null,
                filteredAttribute: [],
                filterTerms: [],
                old_price: this.props.getVariationProductData ? this.props.getVariationProductData.old_price : 0,
                incl_tax: this.props.getVariationProductData ? this.props.getVariationProductData.incl_tax : 0,
                excl_tax: this.props.getVariationProductData ? this.props.getVariationProductData.excl_tax : 0,
                showQantity: false,
                variationfound: null,
                showSelectStatus: false
                
            });
        }

        localStorage.removeItem("PRODUCT");
        localStorage.removeItem("SINGLE_PRODUCT")
        this.props.dispatch(cartProductActions.singleProductDiscount());
        this.props.dispatch(cartProductActions.showSelectedProduct(null));
    }
    // Apply discount for selected product
    discountModal(item) {
        jQuery('#textDis').val(0)
        localStorage.removeItem("PRODUCT")
        localStorage.removeItem("SINGLE_PRODUCT")
        let VarSingleData = null;

        if (item.Type == "variable") {
            if (this.state.variationfound) {
                VarSingleData = item.Variations.filter(items => items.WPID == this.state.variationfound.WPID);
            }
        }
        //console.log("VarSingleData", VarSingleData)

        if (item.Type == "variable") {
            $('#single_popup_discount').modal('show')
        } else {
            $('#single_popup_discount').modal('show')
        }
        var data = {
            product: 'product',
            item: VarSingleData && VarSingleData.length > 0 ? VarSingleData[0] : item,
            id: VarSingleData && VarSingleData.length > 0 ? VarSingleData[0].WPID : item.WPID ? item.WPID : item.product_id
        }
        this.props.dispatch(cartProductActions.selectedProductDis(data))
    }
    //  Update inventory for selected product
    inventoryUpdate(item) {
        let VarSingleData = null;
        if (item.Type == "variable") {
            if (this.state.variationfound) {
                VarSingleData = this.state.variationfound
            } else if (this.state.variationfound) {
                VarSingleData = item.Variations.filter(items => items.WPID == this.state.variationfound.WPID);
            }
        } else {
            VarSingleData = item
        }
        this.props.inventoryData(VarSingleData);
        $('#InventoryPopup').modal('show')
    }


    render() {
        const { getVariationProductData, hasVariationProductData, single_product, get_single_inventory_quantity, showSelectedProduct, isInventoryUpdate } = this.props;
        const { variationfound, showSelectStatus } = this.state;
        console.log("%c SHOW SELECTED PRODUCT", "color : pink", showSelectStatus, showSelectedProduct)
        var img = this.state.variationImage ? this.state.variationImage.split('/') : '';
        var title = this.state.variationTitle && this.state.variationTitle.length > 34 ? this.state.variationTitle.substring(0, 31) + "..." : this.state.variationTitle;
        console.log("title",title)
        // console.log("%c COMBINE POPUP", "color:greenyellow", getVariationProductData, hasVariationProductData, single_product)
        var variation_single_data = single_product;
        let SelectedTitle = (showSelectStatus == true && showSelectedProduct) ? showSelectedProduct.Title && showSelectedProduct.Title !="" ?showSelectedProduct.Title:showSelectedProduct.Sku : this.state.variationTitle;
        const outofstock = {
            fontWeight: 'bold',
            color: 'red',
            textAlign: 'center'
        };
    //    console.log("%c SHOW QRY", 'color:dark pink',  this.props.getQuantity)
    console.log("Show Title", variation_single_data, SelectedTitle)
        return (
            <div className="modal fade modal-fullscreen" id="VariationPopUp" role="dialog">
                {isInventoryUpdate == true ? <LoadingModal /> : ""}
                <div className="modal-dialog modal-xl">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="modalLargeLabel" title={variation_single_data ? variation_single_data.Title : SelectedTitle}>{hasVariationProductData ? <Markup content={variation_single_data ? variation_single_data.Title?variation_single_data.Title:variation_single_data.Sku : SelectedTitle }></Markup> : ''}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.handleClose}>
                                <span aria-hidden="true">
                                    <img src="assets/img/ic_circle_delete.svg" />
                                </span>
                            </button>
                        </div>
                        <div className="modal-body p-0">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-sm-7 col-lg-7 p-0">
                                        <div className="variation-wrapper">
                                            {/* simlple start */}
                                            {getVariationProductData ?
                                                getVariationProductData.Type !== 'variable' ?
                                                    <p className="text-alert-record">
                                                        No Variations Availble
                                                </p>
                                                    :
                                                    <ProductAtrribute showSelectedProduct={showSelectStatus == true ? showSelectedProduct : ''} attribute={hasVariationProductData ? getVariationProductData.ProductAttributes : null} optionClick={this.optionClick} filteredAttribute={this.state.filteredAttribute} selectedAttribute={this.state.selectedAttribute} productVariations={this.props.getVariationProductData ? this.props.getVariationProductData.Variations : []} />
                                                : null}
                                        </div>
                                    </div>
                                    <div className="col-sm-5 col-lg-5 p-0">
                                        <div className="variation-product-details">
                                            <div className="variation-product-image">
                                                {/* <!-- <img src="assets/img/product-variation-image.png" /> --> */}
                                                <img src={hasVariationProductData ? this.state.variationImage ? img == 'placeholder.png' ? '' : this.state.variationImage : '' : ''} onError={(e) => { e.target.onerror = null; e.target.src = "assets/img/placeholder.png" }} id="prdImg" />
                                            </div>

                                            <div className="variation-product-description p-3">
                                                <div className="gray-background mb-2 round-8 d-none overflowHidden">
                                                    <div className="">
                                                        <div className="box-flex center-center">
                                                            <div className="col-sm-6 text-center">
                                                                <p className="text-center">Price</p>


                                                                {variation_single_data == null ?
                                                                    // simple price start 
                                                                    variationfound && variationfound.discount_amount ?
                                                                        <div className="block__box_text_price">
                                                                            <p className="no-variant-product-price">{variationfound.after_discount.toFixed(2)} </p>
                                                                            <p className="org-price">Original Price  {variationfound.Price}</p>

                                                                        </div>
                                                                        :
                                                                        (getVariationProductData && (typeof getVariationProductData.discount_amount !== 'undefined') && getVariationProductData.discount_amount !== 0) ?
                                                                            <div className="block__box_text_price">
                                                                                <p className="no-variant-product-price">{getVariationProductData && parseFloat(getVariationProductData.after_discount).toFixed(2)} </p>
                                                                                <p className="org-price">Original Price  {getVariationProductData.Price}</p>

                                                                            </div>
                                                                            :
                                                                            (showSelectStatus == true && showSelectedProduct) ? showSelectedProduct.discount_amount !== 0 ?
                                                                                <div className="block__box_text_price">
                                                                                    <p className="no-variant-product-price">{showSelectedProduct.after_discount.toFixed(2)}</p>
                                                                                    <p className="org-price">Original Price  {showSelectedProduct.old_price}</p>
                                                                                </div>
                                                                                :
                                                                                <div className="block__box_text_price">
                                                                                    <p>{showSelectedProduct.old_price}</p>
                                                                                </div>

                                                                                :
                                                                                <div className="block__box_text_price">
                                                                                    <p>{parseFloat(hasVariationProductData ? this.state.variationPrice > 0 ? this.state.variationPrice : 0 : 0).toFixed(2)}</p>
                                                                                </div>

                                                                    // simple price end */
                                                                    :
                                                                    variation_single_data !== null && variation_single_data.discount_amount == 0 ?
                                                                        <div className="block__box_text_price">
                                                                            <p>{parseFloat(hasVariationProductData ? this.state.variationPrice > 0 ? this.state.variationPrice : 0 : 0).toFixed(2)}</p>
                                                                        </div>
                                                                        :
                                                                        // apply discount start 
                                                                        <div className="block__box_text_price">
                                                                            <p className="no-variant-product-price">{variation_single_data.after_discount.toFixed(2)} </p>
                                                                            <p className="org-price">Original Price  {variation_single_data.Price}</p>

                                                                        </div>
                                                                    //  apply discount end 
                                                                }

                                                            </div>


                                                            {this.state.variationStockQunatity == 'outofstock' ? <div className="col-sm-6 text-center"><p className="text-center ">Inventory</p><p style={outofstock}> Out of stock </p> </div>
                                                                :
                                                                <div className="col-sm-6 text-center">
                                                                    <p className="text-center ">Inventory</p>
                                                                    {(showSelectStatus == true && showSelectedProduct) ?
                                                                        <div className="block__box_text_price" id="txtInScock">{(showSelectedProduct.StockStatus == null || showSelectedProduct.StockStatus == 'instock') && showSelectedProduct.ManagingStock == false ? "Unlimited" : showSelectedProduct.StockQuantity - showSelectedProduct.quantity}</div>
                                                                        :
                                                                        // <div className="block__box_text_price" id="txtInScock">{get_single_inventory_quantity && ((get_single_inventory_quantity.wpid == this.state.variationId) || variationfound && (get_single_inventory_quantity.wpid == variationfound.WPID) || (get_single_inventory_quantity.wpid == getVariationProductData.WPID)) ? get_single_inventory_quantity.quantity : variation_single_data ? variation_single_data ? (variation_single_data.StockStatus == null || variation_single_data.StockStatus == 'instock') && variation_single_data.ManagingStock == false ? "Unlimited" : variation_single_data.StockQuantity : '0' : hasVariationProductData ? this.state.variationStockQunatity != 'Unlimited' ? this.state.variationStockQunatity : this.state.variationStockQunatity : 1}</div>
                                                                        <div className="block__box_text_price" id="txtInScock">{this.props.getQuantity?this.props.getQuantity:variation_single_data ? (variation_single_data.StockStatus == null || variation_single_data.StockStatus == 'instock') && variation_single_data.ManagingStock == false ? "Unlimited" : hasVariationProductData ? this.state.variationStockQunatity != 'Unlimited' ? this.state.variationStockQunatity : this.state.variationStockQunatity : 1 : hasVariationProductData ? this.state.variationStockQunatity != 'Unlimited' ? this.state.variationStockQunatity : this.state.variationStockQunatity : 1}</div>
                                                                    }
                                                                </div>

                                                            }

                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="white-background box-flex-shadow box-flex-border mb-2 round-8 d-none overflowHidden">
                                                    <div className="section" onClick={() => this.discountModal(getVariationProductData ? getVariationProductData : null)}>
                                                        <div className="">
                                                            <div className="pointer">
                                                                <div className="d-flex box-flex box-flex-small">
                                                                    <div className="box-flex-text-heading">
                                                                        <h2>Add Discount</h2>
                                                                        {!variation_single_data ?
                                                                            variationfound && variationfound.discount_amount ?
                                                                                <p className="org-discount"><NumberFormat value={variationfound.product_discount_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                                                    {variationfound.discount_type == "Number" ? "" : "%"} Discount  added</p>
                                                                                :
                                                                                (getVariationProductData && (typeof getVariationProductData.discount_amount !== 'undefined') && getVariationProductData.discount_amount !== 0) ? <p className="org-discount">
                                                                                    <NumberFormat value={getVariationProductData.new_product_discount_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />{getVariationProductData.discount_type == "Number" ? "$" : "%"} Discount  added</p>
                                                                                    :
                                                                                    (showSelectStatus == true && showSelectedProduct && showSelectedProduct.discount_amount !== 0) ?
                                                                                        <p> <NumberFormat value={showSelectedProduct.new_product_discount_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />{showSelectedProduct.discount_type == "Number" ? "$" : "%"} Discount  added</p>
                                                                                        :
                                                                                        ""
                                                                            : variation_single_data !== null && variation_single_data.discount_amount == 0 ?
                                                                                ""
                                                                                :
                                                                                <p className="org-discount">
                                                                                    <NumberFormat value={variation_single_data.new_product_discount_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />{variation_single_data.discount_type == "Number" ? "$" : "%"} Discount  added</p>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {localStorage.getItem('user') ? (JSON.parse(localStorage.getItem('user')).CanManageInventory == false) || ((this.state.variationStockQunatity && (this.state.variationStockQunatity == 'Unlimited' || this.state.variationStockQunatity == 'outofstock')) || (showSelectedProduct && showSelectStatus == true && showSelectedProduct.ManagingStock == false)) ?
                                                    <button className="white-background box-flex-shadow box-flex-border mb-2 round-8 d-none overflowHidden w-100 no-outline white-background-disabled" disabled>
                                                        <div className="d-flex box-flex box-flex-small transparent">
                                                            <div className="box-flex-text-heading transparent">
                                                                <h2>Adjust Inventory</h2>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    :
                                                    <button className="white-background box-flex-shadow box-flex-border mb-2 round-8 d-none overflowHidden w-100 no-outline white-background-disabled" onClick={() => this.inventoryUpdate(getVariationProductData ? getVariationProductData : null)}>
                                                        <div className="d-flex box-flex box-flex-small transparent">
                                                            <div className="box-flex-text-heading transparent">
                                                                <h2>Adjust Inventory</h2>
                                                            </div>
                                                        </div>
                                                    </button>


                                                    : null}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer p-0">
                            <div className="w-100 variation-footer">
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-sm-7 p-0">
                                            <div className="variation_quality">
                                                <div className="input-group bootstrap-touchspin">
                                                    <span className="input-group-btn input-group-prepend bootstrap-touchspin-injected">
                                                        <button onClick={this.decrementDefaultQuantity} className="btn btn-primary bootstrap-touchspin-down" type="button">
                                                            <img className="verticle_align-middle" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzNCIgaGVpZ2h0PSIzIiB2aWV3Qm94PSIwIDAgMzQgMyI+PGc+PGc+PHBhdGggZmlsbD0iIzRiNGI0YiIgZD0iTTE3LjQwMyAxLjA3M0gxLjQyOGEuNDI4LjQyOCAwIDAgMCAwIC44NTRoMzEuMDk3YS40MjguNDI4IDAgMCAwIDAtLjg1NHoiLz48cGF0aCBmaWxsPSJub25lIiBzdHJva2U9IiM0YjRiNGIiIHN0cm9rZS1taXRlcmxpbWl0PSI1MCIgc3Ryb2tlLXdpZHRoPSIyIiBkPSJNMTcuNDAzIDEuMDczdjBoLS44NTV2MEgxLjQyOGEuNDI4LjQyOCAwIDAgMCAwIC44NTRoMTUuMTJ2MGguODU1djBoMTUuMTIyYS40MjguNDI4IDAgMCAwIDAtLjg1NHoiLz48L2c+PC9nPjwvc3ZnPg==" />
                                                        </button>
                                                    </span>
                                                    <span className="input-group-addon input-group-prepend bootstrap-touchspin-prefix input-group-prepend bootstrap-touchspin-injected">
                                                        <span className="input-group-text">Qty.</span>
                                                    </span>
                                                    <input id="qualityUpdater" type="text" className="form-control no-outline" name="qualityUpdater" value={hasVariationProductData ? this.state.variationStockQunatity == 0 ?  (showSelectStatus == true && showSelectedProduct) ? this.state.variationDefaultQunatity : 0 : this.state.variationDefaultQunatity : ''} onChange={this.handleChange.bind(this)} />
                                                    <span className="input-group-btn input-group-append bootstrap-touchspin-injected">
                                                        <button onClick={this.incrementDefaultQuantity} className="btn btn-primary bootstrap-touchspin-up" type="button">
                                                            <img className="verticle_align-middle" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzNCIgaGVpZ2h0PSIzNCIgdmlld0JveD0iMCAwIDM0IDM0Ij48Zz48Zz48cGF0aCBmaWxsPSIjNGI0YjRiIiBkPSJNMTcuNDAzIDE2LjU0OFYxLjQyOGEuNDI4LjQyOCAwIDAgMC0uODU1IDB2MTUuMTJIMS40MjhhLjQyOC40MjggMCAwIDAgMCAuODU1aDE1LjEydjE1LjEyMmEuNDI4LjQyOCAwIDAgMCAuODU1IDBWMTcuNDAzaDE1LjEyMmEuNDI4LjQyOCAwIDAgMCAwLS44NTV6Ii8+PHBhdGggZmlsbD0ibm9uZSIgc3Ryb2tlPSIjNGI0YjRiIiBzdHJva2UtbWl0ZXJsaW1pdD0iNTAiIHN0cm9rZS13aWR0aD0iMiIgZD0iTTE3LjQwMyAxNi41NDh2MC0xNS4xMmEuNDI4LjQyOCAwIDAgMC0uODU1IDB2MTUuMTJIMS40MjhhLjQyOC40MjggMCAwIDAgMCAuODU1aDE1LjEydjE1LjEyMmEuNDI4LjQyOCAwIDAgMCAuODU1IDBWMTcuNDAzdjBoMTUuMTIyYS40MjguNDI4IDAgMCAwIDAtLjg1NXoiLz48L2c+PC9nPjwvc3ZnPg==" /></button>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="col-sm-5 p-0">

                                            <button
                                                style={getVariationProductData ?
                                                    getVariationProductData.Type !== 'variable' ? { cursor: "pointer", pointerEvents: "auto" } : this.state.variationStyles : null}
                                                onClick={getVariationProductData ?
                                                    getVariationProductData.Type !== 'variable' ? this.addSimpleProducttoCart.bind(this) : this.addVariationProductToCart.bind(this) : null} className="w-100 box-flex-shadow box-flex-border d-none overflowHidden bg-blue">
                                                <div className="pointer">
                                                    <div className="d-flex  box-flex box-flex-small">
                                                        <div className="box-flex-text-heading">
                                                            <h2 className="text-white"> {this.props.showSelectedProduct ? 'Update Cart' : 'Add To Cart'}</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    const { categorylist, productlist, attributelist, single_product, get_single_inventory_quantity, showSelectedProduct } = state;

    return {
        categorylist: categorylist,
        productlist: productlist,
        attributelist: attributelist,
        single_product: single_product.items,
        get_single_inventory_quantity: get_single_inventory_quantity.items,
        showSelectedProduct: showSelectedProduct.items
    };
}

const connectedCommonProductPopupModal = connect(mapStateToProps)(CommonProductPopupModal);
export { connectedCommonProductPopupModal as CommonProductPopupModal };
