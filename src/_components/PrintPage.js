import Config from '../Config';
import moment from 'moment';

export const PrintPage = {
  PrintElem
};


 function PrintElem(data){
  console.log("data", data)
  var mywindow = window.open('#', 'my div', 'height=400,width=600');
  mywindow.document.write(`<html><head><meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard - Oliver</title>
    
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
          <style>
     @media screen {
          @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
          }

    body {
            font-family: "Lato", "Lucida Grande", "Lucida Sans Unicode", Tahoma, Sans-Serif;
          }
        }
   
    </style></title>`);
  /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
  mywindow.document.write(`</head><body>`);
  mywindow.document.write(
    ` 
       <div class="printer80" style="width: 100%; display: inline-block; color: #000 ; line-height: normal;">
           <div class="panel panel-default" style="box-shadow: 0 1px 1px rgba(0,0,0,.05); border-radius: 4px;border: 1px solid transparent;background-color: #fff;margin-bottom: 16px;border-color: #ddd;padding: 0px;">
             <div class="panel-heading" style="color: #333;border-color: #ddd;padding: 5px;border-bottom: 1px solid transparent;border-top-left-radius: 3px;
                      border-top-right-radius: 3px;">
               <!-- <p style="margin: 0px; display: inline-block; text-align: center; margin: auto; line-height: 58px;">
                 <img src="img/oliver.png" style="width: 70px; float: left; ">
                 <strong><i></i></strong>
               </p> -->
                <!--  <div style="display: block; text-align: center;">*****************</div> -->
                 <address 
                   style="margin: 0px;padding: 2px 5px 0px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center; color: #000;">
                     <!-- <a href="#" style="color: #000; margin-bottom: 2px; line-height: normal;">wptest.creativemaple.com</a><br> -->
                     ${data.display_name}
                          <p>${data.shop_name}<p/>
                     <p>${data.address}</p>
                 </address>
                 <div class="clearfix"></div>
            </div>
            <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden; color: #000;">---------------------------------------------------</div>
             <div class="panel-body">
                 <div style="display:inline-block;width: 100%;">
                   <div style="padding-left: 10px;">
                     <h2 style="text-align: left; margin-top: 10px; margin-bottom: 0px;font-size: 17px;margin-bottom: 0px;  ">Receipt / Tax Invoice </h2>
                      <div style="display: inline-block; float: left; text-align: left;">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px; width:100%">
                            Invoice: #${data.inovice_Id ? data.inovice_Id : data.list.order_id}
                         <br>
                       </p>
                     </div>
                     <div  style="display: inline-block; float: right; text-align: left; width:100%">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px; width:100%"> Time: ${data.list.order_date ? data.list.order_date : moment().format(Config.key.DATETIME_FORMAT)} </p>
                     </div>
                     <div style="display: inline-block; float: left; text-align: left;">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px width:100%;">
                         Register: ${data.register}
                       </p>
                     </div>
                  </div>
                 </div> 
                 <div style="padding:15px;">  
                 <table class="table table-printer-price" style="width: 100%; padding-top: 5px; padding-bottom: 5px;border-collapse: separate;
                   border-spacing: 0px; margin-top: 10px;">
                    <tbody>
                    ${data.list && data.list.ListItem.map((item, index) =>
      `  <tr key=${index}>
                             <td style=" text-align: left; border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.qunatity ? item.qunatity : index + 1}</td>
                             <td style="text-align: left;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.Title}</td>
                             <td style="text-align: right;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.discount_amount == 0 ? item.Price.toFixed(2) : (item.Price - item.discount_amount).toFixed(2)}</td>
                          </tr>`
    )}
                    </tbody> 
               </table>
               <div class="p-0">
                   <div class="widget" style="background-color: #fff;padding: 2px;border-radius: 0px;margin: 5px 0px;padding: 0px 10px; ">
                    <div class="summary-block" style="">
                    <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                        <div class="summary-head" style="display: inline-block;text-align: left;"> 
                        <h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px;">Subtotal</h5>
                        </div>
                        <div class="summary-price" style="display: inline-block; float: right; ">
                            <p class="summary-text" style="color: #000;font-size: 16px;font-weight: 400;letter-spacing: -1px;margin: 0px;">${(data.list.subTotal).toFixed(2)}</p>
                            <!-- <span class="summary-small-text pull-right"></span> -->
                        </div>
                    </div>
                </div> 
                   <div class="summary-block" style="">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block;text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px;">Tax</h5>
                               </div>
                               <div class="summary-price" style="display: inline-block; float: right; ">
                                   <p class="summary-text" style="color: #000;font-size: 16px;font-weight: 400;letter-spacing: -1px;margin: 0px;">${(data.list.tax).toFixed(2)}</p>
                                   <!-- <span class="summary-small-text pull-right"></span> -->
                               </div>
                           </div>
                       </div>
                       <div class="summary-block">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block; text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px; color: #1c1e22; margin: 0px; padding:5px 0px;">Total</h5>
                               </div>
                               <div class="summary-price" style="display: inline-block; float: right; ">
                                   <p class="summary-text" style="color: #000; font-size: 16px; font-weight: 400; letter-spacing: -1px; margin: 0px; ">${(data.list.totalPrice).toFixed(2)}</p>
                                   <!-- <span class="summary-small-text pull-right"></span> -->
                               </div>
                           </div>
                       </div>
                       <div class="summary-block" style="">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block;text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px;">Order Payment's :</h5>
                               </div>
                               <table class="table table-printer-price" style="width: 100%; padding-top: 5px; padding-bottom: 5px;border-collapse: separate;
                               border-spacing: 0px; margin-top: 10px;">
                                <tbody>
                                  ${data.paymentType && data.paymentType.map((item, index) =>
      `<tr key=${index}>
                                         <!-- <td style=" text-align: left; border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.order_id}</td> -->
                                         <td style="text-align: left;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.payment_type}</td>
                                         <td style="text-align: right;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${parseFloat(item.payment_amount).toFixed(2)}</td>
                                      </tr>`
    )}
                               </tbody> 
                           </table>
                           </div>
                       </div>
                   </div>
                 
               </div>
                </div>
                </div>
                <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden;">---------------------------------------------------</div>
                <div class="panel-footer" style="padding: 5px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center;color: #000;text-transform: capitalize;">
                 <p style="margin: 0px;">
                   Thank You For Shopping With Us<br>
                </p>
                <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden;">-----------------------------------------------</div>
                 <p style="margin: 0px;">             
                   <a href="javascript:void(0)" style="padding: 5px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center;color: #000;text-transform: capitalize; display: inline-block; margin: 2px 0px;">
                     ${data.site_name}</a></br>
                      Printed With Oliver POS
                 </p><br><br>
                </div>
           </div>
       </div>`


  );
  mywindow.document.write('</body style="text-align: center;></html>');

  mywindow.print();
  mywindow.close();

  return true;
}




