import React from 'react';
import { connect } from 'react-redux';
import { ProductAttribute } from '../ShopView/components/ProductAttribute';
import { cartProductActions } from '../_actions/cartProduct.action'
import { history } from '../_helpers';
import { Markup } from 'interweave';

class VariationProductPopupModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            variationStockQunatity:'',
            variationTitle:'',
            variationImage:'',
            variationCombination: [],
            variationoptionArray: {},
            optionArray:[],
            CombinationArray:[]
           // cartStockQty=0
        }
        this.incrementDefaultQuantity = this.incrementDefaultQuantity.bind(this);
        this.setDefaultQuantity = this.setDefaultQuantity.bind(this);
        this.decrementDefaultQuantity = this.decrementDefaultQuantity.bind(this);
        this.addVariationProductToCart = this.addVariationProductToCart.bind(this);

        this.optionClick = this.optionClick.bind(this);

      //  console.log("Render")



    }

    
    incrementDefaultQuantity() {
        if (this.state.variationDefaultQunatity) {
            //------
            var product=this.state.getVariationProductData
                //  this.state.variationStockQunatity
               
            //--------
            let qty = parseInt(this.state.variationDefaultQunatity);

// console.log("ManagingStock",product.ManagingStock);
// console.log("product.StockQuantity",product.StockQuantity);
// console.log("variationStockQunatity",this.state.variationStockQunatity)
// console.log("qty",qty,this.state.variationId);

// //--------cart Qty------------------------
//         let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []  
//         var cartQty=0;    
//         if( cartItemList && cartItemList.length>0)  {
//         cartItemList.map(item=>{
//             if(this.state.variationId==item.variation_id){
//                 console.log("match",this.state.variationId,item.variation_id);
//                 cartQty=  item.quantity;
//             }
//         })
//     }
//     //---------------------------------
    if( (product.StockStatus==null || product.StockStatus== 'instock')  
            &&( product.ManagingStock==false || (product.ManagingStock==true && qty<  this.state.variationStockQunatity)) )
           { 
               qty++;
              // this.setState({variationStockQunatity: this.state.variationStockQunatity-1});
           }


            if(qty>this.state.variationStockQunatity)
            qty=this.state.variationStockQunatity;

            this.setDefaultQuantity(qty);
        }

    }
    decrementDefaultQuantity() {
        if (this.state.variationDefaultQunatity && this.state.variationDefaultQunatity > 1) {
            let qty = parseInt(this.state.variationDefaultQunatity);
            qty--;
            this.setDefaultQuantity(qty);
        }
    }

    setDefaultQuantity(qty) {
        this.setState({
            variationDefaultQunatity: qty,
        });
    }

    addVariationProductToCart() {
        //console.log("this.state.variationId",this.state.variationId)
        var data = {
            line_item_id: 0,
            quantity: this.state.variationDefaultQunatity,
            Title: this.state.variationTitle,
            Price: parseInt(this.state.variationDefaultQunatity) * parseFloat(this.state.variationPrice),
            product_id: this.state.variationParentId,
            variation_id: this.state.variationId,
            isTaxable: this.state.variationIsTaxable,
        }
   
        var qty=0;
        var product=this.state.getVariationProductData
        let cartItemListHolder = [];
        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
        
        // cartItemList.map(item=>{
        //     if(product.WPID==item.product_id){
        //         qty=  item.quantity;
        //     }
        // })
        // console.log("qty", qty);
        // console.log("variationDefaultQunatity", this.state.variationDefaultQunatity);
        qty= qty+this.state.variationDefaultQunatity;
    //     console.log("qty", qty);
    //    console.log("ManagingStock",product.ManagingStock)
    //    console.log("variationStockQunatity",this.state.variationStockQunatity )
        if(this.state.variationStockQunatity==0 || this.state.variationDefaultQunatity==0)
        {
           // alert("stcock quantity is empty"); 
            $('#outOfStockModal').modal('show')
            return;
        }
        else if( (product.StockStatus==null || product.StockStatus== 'instock')  && 
                (product.ManagingStock==false || (product.ManagingStock==true && qty<=this.state.variationStockQunatity )) ) {
        
            cartItemList.push(data)
            this.props.dispatch(cartProductActions.addtoCartProduct(cartItemList)); // this.state.cartproductlist
                    

            this.forceUpdate()
            $(".close").trigger("click");
              history.go()
           // window.location="/shopview";
            }
            else{
                 //alert("stcock Quantity exceed");
                 $('#outOfStockModal').modal('show')
            }
     
// if((product.ManagingStock==true && qty<=product.StockQuantity)){
        this.setState({         
            variationDefaultQunatity: 1  
          
        })
//     }
        this.forceUpdate()
        $(".close").trigger("click");
    }

    optionClick(option, attribute) {
     
console.log("option",option,"attribute",attribute);
        let attributeLenght = this.getAttributeLenght();
        this.state.variationoptionArray[attribute] = option;
        let variationOptionclick = (Object.keys(this.state.variationoptionArray)).length;
        console.log("variationOptionclick",variationOptionclick);

        if (attributeLenght == variationOptionclick) {
            console.log("variationoptionArray", this.state.variationoptionArray);

            this.searchvariationProduct(this.state.variationoptionArray);
          
        }
    }

    searchvariationProduct(combinations) {
        console.log("combinations",combinations);

        let combination = new Array();
        for (var key in combinations) {
            if (combinations.hasOwnProperty(key)) {
                combination.push(combinations[key]);
                this.setState({ variationCombination: combination  })
            }
        }
        let variations = this.state.getVariationProductData.Variations;
        let rCombination = combination.join("~");
        console.log("rCombination",rCombination);

        let lCombination = (combination.reverse()).join("~");
        console.log("lCombination",lCombination);

        var found = variations.find(function (element) {
         
            if (element.combination.toLowerCase() == rCombination.toLowerCase() || element.combination.toLowerCase() == lCombination.toLowerCase() ||
            element.combination.toUpperCase() == rCombination.toUpperCase() || element.combination.toUpperCase() == lCombination.toUpperCase() ) {
              
                return element;
            }

        });

     console.log("found",found);

        if (typeof found !== 'undefined') {

            let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
            var qty=0;
            cartItemList.map(item=>{
                if(found.WPID==item.variation_id){
                    qty=  item.quantity;
                }
            })
         
            this.setState({
                variationTitle: found.Title,
                variationId: found.WPID,
                variationParentId: found.ParentId,
                variationPrice: found.Price,
                variationStockQunatity: (found.StockStatus==null || found.StockStatus== 'instock') && found.ManagingStock==false ?"Unlimited": found.StockQuantity -qty,
                variationImage: (found.ProductImage == null) ? this.state.variationImage : found.ProductImage,
                variationIsTaxable: found.Taxable,
                variationDefaultQunatity:1
            });
          
            $("#add_variation_product_btn").css({ "cursor": "pointer", "pointer-events": "auto" });
        }

    }

    getAttributeLenght() {
        return this.props.getVariationProductData.ProductAttributes.length;
    }
    optionClickValue(option,attribute){

     console.log("option",option,"attribute",attribute);

     console.log("found CombinationArray", this.state.CombinationArray);  
    //  var found = this.state.CombinationArray && this.state.CombinationArray.find(function(element) {        
    //        return element;
    //   });
    //   const result = this.state.CombinationArray && this.state.CombinationArray.find( comp =>
    //     comp === option  );      

    
    

    }
    componentWillReceiveProps(nextPros){
        this.state.CombinationArray=[]
        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []  
        var qty=0;    
        if( cartItemList && cartItemList.length>0)  {
        cartItemList.map(item=>{
            if(nextPros.getVariationProductData && nextPros.getVariationProductData.WPID==item.variation_id){
                qty=  item.quantity;
            }
        })
    }
        if(nextPros.getVariationProductData!=null){
                  this.setState({
                    getVariationProductData: nextPros.getVariationProductData,
                    hasVariationProductData: true,
                    loadProductAttributeComponent: true,
                    variationOptionclick: 0,
                    variationTitle: nextPros.getVariationProductData.Title ? nextPros.getVariationProductData.Title  : '',
                    variationId: 0,
                    variationPrice: nextPros.getVariationProductData.Price ? nextPros.getVariationProductData.Price :  '',
                    variationStockQunatity: 
                    (nextPros.getVariationProductData.StockStatus==null || nextPros.getVariationProductData.StockStatus== 'instock') && nextPros.getVariationProductData.ManagingStock==false ? "Unlimited": (typeof nextPros.getVariationProductData.StockQuantity!= 'undefined') && nextPros.getVariationProductData.StockQuantity!= '' ? nextPros.getVariationProductData.StockQuantity-qty: '1',
                    variationImage: nextPros.getVariationProductData.ProductImage ? nextPros.getVariationProductData.ProductImage  : '',
                    variationDefaultQunatity: 1 ? 1 : nextPros.getVariationProductData ? nextPros.getVariationProductData.DefaultQunatity : ''
        
               });
  
               console.log("nextPros.getVariationProductData",nextPros.getVariationProductData.ProductAttributes)
                 var newArray=[]
                nextPros.getVariationProductData.ProductAttributes &&  nextPros.getVariationProductData.ProductAttributes.map((optAttri,index)=>{
                   
                this.state.optionArray[ optAttri.Slug ] = optAttri.Option;
                  
                   console.log("this.state.optionArray",this.state.optionArray);
               })
               nextPros.getVariationProductData.Variations &&  nextPros.getVariationProductData.Variations.map((com,index)=>{
                
                this.state.CombinationArray=com.combination
                this.setState({CombinationArray: this.state.CombinationArray})
               console.log("this.state.CombinationArray",this.state.CombinationArray);
            })
        }
         
        
    }
  
    handleChange(e) {
      
    }

    render() {
      const { getVariationProductData,hasVariationProductData } = this.props
     // console.log("variationProductPopup",getVariationProductData,hasVariationProductData);
      var img=this.state.variationImage?this.state.variationImage.split('/'):'';
    //  console.log("rr12",img)
     // console.log("rr13",img[8])

        return (
            <div id="variationProductPopup" tabIndex="-1" className="modal modal-wide fade full_height_modal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" >
                                <img src="assets/img/delete-icon.png" />
                            </button>
                            {/* <h4 className="modal-title">{hasVariationProductData ? getVariationProductData.Title : ''}</h4> */}
                            <h4 className="modal-title">{hasVariationProductData ? <Markup content={this.state.variationTitle}></Markup>  : ''}</h4>
                        </div>
                        <div className="modal-body overflowscroll ButtonRadius" id="scroll_mdl_body">
                            <div className="row">
                                <div className="col-md-4 col-xs-4 text-center">
                                    <img src={hasVariationProductData ? this.state.variationImage ? img[8] == 'placeholder.png'  ?'':this.state.variationImage : '':''} onError={(e)=>{e.target.onerror = null; e.target.src="assets/img/placeholder.png"}}  id="prdImg" style={{ width: '100%', height: 200, borderRadius: 8, marginLeft: 20 }} />
                                </div>
                                <div className="col-md-8 col-xs-8">
                                    <div className="col-md-2">
                                        <div className="modal_wide_field">
                                            <input type="text" value={hasVariationProductData ?  this.state.variationStockQunatity==0?0:this.state.variationDefaultQunatity: ''} className="form-control h_eight70" onChange={this.handleChange.bind(this)} />
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-xs-4">
                                        <div className="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div className="btn-group" role="group">
                                                <a onClick={this.incrementDefaultQuantity} className="btn btn-default bd-r h_eight70 btn-plus">
                                                    <button className="icon icon-plus button"></button>
                                                </a>
                                            </div>
                                            <div className="btn-group" role="group">
                                                <a onClick={this.decrementDefaultQuantity} className="btn btn-default h_eight70 btn-plus bl-0">
                                                    <button className="icon icon-minus button"></button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-xs-6">
                                        <div className="input-group">
                                            <div className="input-group-btn w-50 btn-left">
                                                <button type="button" className="btn btn-default br-0 h_eight70">In Stock</button>
                                            </div>
                                            <div className="modal_wide_field">
                                                <input type="text" className="form-control h_eight70 borderRightBottomRadius" value={hasVariationProductData ? this.state.variationStockQunatity!='Unlimited'? this.state.variationStockQunatity:this.state.variationStockQunatity : 1} onChange={this.handleChange.bind(this)} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12 col-xs-12 mt-4">
                                        <div className="input-group">
                                            <div className="input-group-btn w-50 btn-left">
                                                <button type="button" className="btn btn-default bd-r h_eight70">Price</button>
                                            </div>
                                            <div className="modal_wide_field">
                                                <input type="text" className="form-control bl-0 h_eight70 borderRightBottomRadius" value={hasVariationProductData ? this.state.variationPrice : 0} onChange={this.handleChange.bind(this)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ProductAttribute attribute={hasVariationProductData ? getVariationProductData.ProductAttributes : null} optionClick={this.optionClickValue} />


                            </div>
                        </div>

                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-block btn-primary checkout-items buttonProductVariation" id="add_variation_product_btn" style={{ cursor: "no-drop", pointerEvents: "none" }} data-variation-id={hasVariationProductData ? this.state.variationId : 0} onClick={this.addVariationProductToCart.bind(this)}>Add Product</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



function mapStateToProps(state) {
    const { categorylist, productlist, attributelist } = state;

    return {
        categorylist: categorylist,
        productlist: productlist,
        attributelist: attributelist
    };
}

const connectedVariationProductPopupModal = connect(mapStateToProps)(VariationProductPopupModal);
export { connectedVariationProductPopupModal as VariationProductPopupModal };