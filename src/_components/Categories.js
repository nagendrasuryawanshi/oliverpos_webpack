import React from 'react';
import { connect } from 'react-redux';
import { categoriesActions } from '../_actions/categories.action'
import { SubCategories, LoadingModal } from '.'
import { history } from '../_helpers';
import { openDb, deleteDb } from 'idb';
import {encode_UDid,get_UDid} from '../ALL_localstorage'

class Categories extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(categoriesActions.refresh());
        this.state = {
            active: false,
            isLoading: true,
            productlist: [], //JSON.parse(  localStorage.getItem("Productlist-"+localStorage.getItem('UDID')) ),   //JSON.parse(localStorage.getItem('Productlist')),
            subcategory_list: [],
            loading:false
        }
       
            //--------------------------------------------------------------
            //let decodedString = localStorage.getItem('UDID');
           // var decod=  window.atob(decodedString);
            var udid= get_UDid('UDID');
            console.log("productall",udid);
          //  var udid= localStorage.getItem('UDID');

            const dbPromise = openDb('ProductDB', 1, upgradeDB => {
                upgradeDB.createObjectStore(udid);
            });

            const idbKeyval = {
                async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
                },
                
            };

            idbKeyval.get('ProductList').then(val => 
                {
                    this.setState({ productlist: val });              
                }
                ); 
            //--------------------------------------------------------------

        const { dispatch } = this.props;
        dispatch(categoriesActions.getAll());
    }

    loadSubCategory(subcategorylist) {
      //  console.log("loadSubCategory",subcategorylist);
         
       if(subcategorylist.Subcategories != ''){  

        this.setState({
            active: true,
        })
        this.state.subcategory_list.push(subcategorylist.Subcategories)
        // this.state.active=true;
        this.state.subcategory_list = subcategorylist.Subcategories;
        }else{
           var  Code=subcategorylist.Code
           var  catg_Code = Code.replace (/-/g, " ");
                    this.setState({
                        active: true                     
                    })
                    this.state.productlist &&  this.state.productlist.map(prod => {
                prod.Categories.split(',').map(categ =>{                  
                  var  prod_Code = categ.replace (/-/g, " "); 
                if(prod_Code.toUpperCase() == catg_Code.toUpperCase() || prod_Code.toLowerCase() == catg_Code.toLowerCase() ){
                      this.state.subcategory_list.push(prod)
                      console.log("this.state.subcategory_list",  this.state.subcategory_list);
                    // this.state.active=true;
                  //  this.state.subcategory_list = prod;
                   }

                })
          
            })

       } 

    }
    componentWillMount(){
    //  setTimeout(() => {
            this.setState({
                loading: true
              })          
        // }, 500); 

   setTimeout(function () {
                setHeightDesktop();
            }, 500);    
        

       

    }

    render() {
        const { active, subcategory_list } = this.state;
        const { categorylist } = this.props;
   //     console.log("categorylist",categorylist);

        return (
            <div>

                {active == false ?

                    <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                        { this.state.loading == false ? !this.props.categorylist || this.props.categorylist.length == 0   ?<LoadingModal/>:'':''} 

                        <div className="items pt-3">
                            <div className="item-heading text-center">Library</div>
                            <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                                <div className="searchDiv" style={{ display: 'none' }}>
                                    <input type="search" className="form-control nameSearch" placeholder="Search Category / Scan" />
                                </div>

                                <div className="pl-1 pr-4 previews_setting  pointer">
                                    <a href="/listview" className="back-button " id="mainBack">
                                        <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                        <span>Back</span>
                                    </a>
                                </div>

                                <table className="table ShopProductTable  table-striped table-hover table-borderless paddignI mb-0 font">
                                    {/* <colgroup>
                                                                <col style={{width:'*'}}/>
                                                                    <col style={{width:40}}/>
                                                            </colgroup>                                                    */}

                                    <tbody>
                                        {
                                            categorylist ? //&& categorylist.lenght>0
                                                (categorylist.map((item, index) => {
                                                    console.log("catItem",item,"item.Subcategories",item.Subcategories);
                                                    return (
                                                        item.Subcategories && item.Subcategories.length > 0 ?
                                                            <tr  className="pointer" key={index} onClick={() => this.loadSubCategory(item)}>
                                                                <td>{item.Value ? item.Value : 'N/A'}</td>
                                                                <td className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                            </tr>
                                                            :
                                                            <tr  className="pointer" key={index}  onClick={() => this.loadSubCategory(item)} >
                                                                <td>{item.Value ? item.Value : 'N/A'}</td>
                                                                <td className="text-right"><a> <img src="assets/img/next.png" /></a></td>

                                                            </tr>
                                                    )
                                                })
                                                )
                                                : <tr data-toggle="modal"><td style={{textAlign:'center'}}>No Data Found</td><td></td></tr>
                                        }

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    : <SubCategories subcategorylist={subcategory_list}  productData={this.props.productData} />

                }

            </div>

        )
    }

}


function mapStateToProps(state) {
    const { categorylist } = state.categorylist;

    return {
        categorylist
    };
}

const connectedList = connect(mapStateToProps)(Categories);
export { connectedList as Categories };