import React from 'react';
import { connect } from 'react-redux';
import { refundActions } from '../RefundPage/actions/refund.action';
import { GetRoundCash } from '../CheckoutPage/Checkout';
import { checkoutActions } from '../CheckoutPage/actions/checkout.action';
import { LoadingModal } from './LoadingModal';
import { get_UDid } from '../ALL_localstorage'



class Calulator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refundingAmount: 0,
            refundPayments: 0,
            emptyPaymentFlag: true,
            refundingPaidAmount: 0,
            cash_round: 0,
            single_Order_list: (typeof localStorage.getItem("getorder") !== 'undefined') ? JSON.parse(localStorage.getItem("getorder")) : null,
            refundingCashAmount: 0,
            paidAmountStatus: false,
            refunding_amount: 0,
            paymentTypeName: (typeof localStorage.getItem('PAYMENT_TYPE_NAME') !== 'undefined') ? JSON.parse(localStorage.getItem('PAYMENT_TYPE_NAME')) : null,
            globalPayments: '',
            orderId: 0,
            globalStatus: false,
            isPaymentStart: false,
            new_status: false
        };

        this.setRefundPayment = this.setRefundPayment.bind(this);
        this.calcInp = this.calcInp.bind(this);
        this.cashPayment = this.cashPayment.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const re = new RegExp('^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
        if (e.target.value === '' || re.test(e.target.value)) {
            const { value } = e.target;
            this.setState({
                refunding_amount: value,
                paidAmountStatus: true
            })
        }
    }

    componentDidMount() {
        this.props.onRef(this)

    }

    componentWillUnmount() {
        this.props.onRef(null)
    }

    cashPayment(paymentType) {
        const { single_Order_list } = this.state;
        let order_id = single_Order_list.order_id
        let store_credit = single_Order_list.orderCustomerInfo && single_Order_list.orderCustomerInfo.store_credit;
        //  console.log("cashPayment", paymentType, store_credit ,order_id , this.state.refundingAmount)
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        if (paymentType == 'cash') {
            let CashRound = parseFloat(GetRoundCash(cash_rounding, this.state.refundingAmount))
            this.setState({
                cash_round: CashRound,
                refundingAmount: this.state.refundingAmount + CashRound
            })
            this.setRefundPayment(order_id, paymentType)
        } else {
            this.setState({
                cash_round: 0,
                refundingAmount: this.state.refundingAmount - this.state.cash_round
            })
            // if (store_credit < this.state.refundingAmount) {
            //     this.props.min_max_add(`Store Credit Balance is $${parseFloat(store_credit).toFixed(2)}`)
            // } else {
            //     // alert(" sufficiant ");
            //     this.setRefundPayment(order_id, paymentType)
            // }
            this.setRefundPayment(order_id, paymentType)

        }
    }

    setRefundPayment(order_id, paymentType) {
        this.setState({
            refunding_amount: 0,
            globalStatus: false
        })
        let paymentAmount = 0;
        if (typeof paymentType !== 'undefined') {
            if (this.state.paidAmountStatus == true) {
                paymentAmount = this.state.refunding_amount;
            } else {
                paymentAmount = this.state.refunding_amount !== 0 ? this.state.refunding_amount : this.state.refundingAmount;
            }

            let paymentAmount2 = this.state.refunding_amount !== 0 ? this.state.refunding_amount : this.state.refundingCashAmount;
            //console.log("inside" , paymentAmount , paymentAmount2)
            //console.log("paymentAmount", paymentAmount, parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2))
            // console.log("paymentAmount2", paymentAmount,  parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2))
            // console.log("inside other => " , parseFloat(paymentAmount) , this.state.refundingAmount , this.state.refundingPaidAmount)
            if (paymentType !== 'cash') {
                this.setState({ paidAmountStatus: false })
                //console.log("inside other")


                if (paymentAmount <= 0) {
                    this.props.min_max_add(`Paying amount should be greater than $0.00`);

                } else if (parseFloat(paymentAmount) > parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2)) {

                    if (parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2) == 0) {

                        this.props.min_max_add('Please select any order items.')

                    } else {

                        this.props.min_max_add('Paying amount should not be greater than total amount.')
                    }

                } else {
                    this.props.addPayment(order_id, paymentType, paymentAmount);
                    this.setState({
                        emptyPaymentFlag: true,
                    });
                    return true;
                }

            } else {
                //console.log("inside cash")
                // console.log("inside cash => " , paymentAmount2 , this.state.refundingCashAmount , this.state.refundingPaidAmount)
                if (paymentAmount2 <= 0) {
                    this.props.min_max_add('Paying amount should be greater than $0.00')

                } else if (paymentAmount2 > parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2)) {

                    if (parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2) == 0) {
                        this.props.min_max_add('Please select any order items.')

                    } else {
                        this.props.addPayment(order_id, paymentType, paymentAmount2);
                    }
                } else {
                    this.props.addPayment(order_id, paymentType, paymentAmount2);
                    this.setState({
                        emptyPaymentFlag: true,
                    });
                    return true;
                }
            }

        } else {
            this.props.min_max_add(`Please select a valid payment mode.`)
        }

        return false;
    }

    setRefundAmount(amount) {
        //alert()
        //console.log("amount", amount)
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        let CashRound = parseFloat(GetRoundCash(cash_rounding, amount))
        this.setState({
            refundingAmount: amount,
            refundingCashAmount: amount + CashRound,
            // refunding_amount  : amount
            // paidAmountStatus : amount == 0  && paidAmountStatus == true ? false : true
        });
    }

    setrefundingPaidAmount(amount) {
        //console.log("paid amount" , amount)
        this.setState({
            refundingPaidAmount: amount,
            refunding_amount: this.state.refundingAmount - amount
        });
    }

    SetValue(eleT, val) {
        eleT.html(eleT.html().replace(' ', '') + val + ' ');
        this.setState({ refunding_amount: this.state.refunding_amount + val })
    }

    calcInp(input) {
        var elemJ = jQuery('#refund_calc_output');
        if (this.state.emptyPaymentFlag || elemJ.val() == "0") {
            elemJ.val('');
            this.setState({
                emptyPaymentFlag: false,
            });
        }

        switch (input) {
            case 1:
                this.SetValue(elemJ, '1');
                break;
            case 2:
                this.SetValue(elemJ, '2');
                break;
            case 3:
                this.SetValue(elemJ, '3');
                break;
            case 4:
                this.SetValue(elemJ, '4');
                break;
            case 5:
                this.SetValue(elemJ, '5');
                break;
            case 6:
                this.SetValue(elemJ, '6');
                break;
            case 7:
                this.SetValue(elemJ, '7');
                break;
            case 8:
                this.SetValue(elemJ, '8');
                break;
            case 9:
                this.SetValue(elemJ, '9');
                break;
            case 0:
                this.SetValue(elemJ, '0');
                break;
            case '.':
                if (elemJ.val() == "") {
                    this.SetValue(elemJ, '0.');
                } else {
                    if (elemJ.val().includes(".")) {

                    } else {
                        this.SetValue(elemJ, '.');
                    }
                }
                break;
            //alert('n');
            // default code block
        }
    }

    rmvInp() {
        let str = $('#refund_calc_output').val();
        str = str.slice(0, -1);
        //str = str.substring(0, str.length - 2);
        $('#refund_calc_output').val(str + ' ');
        if (str == "" || str == " ") {
            $('#refund_calc_output').val('0');
        } else { }
        this.setState({
            refunding_amount: str,
            paidAmountStatus: true
        })
    }


    calcInp2(input) {
        var elemJ = jQuery('#calc_output_cash');
        if (this.state.emptyPaymentFlag || elemJ.val() == "0") {
            elemJ.val('');
            this.setState({
                emptyPaymentFlag: false,
            });
        }

        switch (input) {
            case 1:
                this.SetValue(elemJ, '1');
                break;
            case 2:
                this.SetValue(elemJ, '2');
                break;
            case 3:
                this.SetValue(elemJ, '3');
                break;
            case 4:
                this.SetValue(elemJ, '4');
                break;
            case 5:
                this.SetValue(elemJ, '5');
                break;
            case 6:
                this.SetValue(elemJ, '6');
                break;
            case 7:
                this.SetValue(elemJ, '7');
                break;
            case 8:
                this.SetValue(elemJ, '8');
                break;
            case 9:
                this.SetValue(elemJ, '9');
                break;
            case 0:
                this.SetValue(elemJ, '0');
                break;
            case '.':
                if (elemJ.val() == "") {
                    this.SetValue(elemJ, '0.');
                } else {
                    if (elemJ.val().includes(".")) {

                    } else {
                        this.SetValue(elemJ, '.');
                    }
                }
                break;
            //alert('n');
            // default code block
        }
    }

    rmvInp2() {
        let str = $('#calc_output_cash').val();
        str = str.slice(0, -1);
        // str = str.substring(0, str.length - 2);
        $('#calc_output_cash').val(str + ' ');
        if (str == "" || str == " ") {
            $('#calc_output_cash').val('0');
        } else { }
        this.setState({
            refunding_amount: str,
            paidAmountStatus: true
        })
    }

    hideTab(type) {
        console.log("paidAmountStatus 21" , this.state.paidAmountStatus)
        console.log("this.state.refunding_amount" , this.state.refunding_amount)
        if (type == 'cash') {
            this.setState({ paidAmountStatus: false })
        } else {
            this.setState({ paidAmountStatus: true })
        }

        // if (this.state.refunding_amount == '') {
        //     this.setState({ paidAmountStatus: false })
        // }

        this.setState({ new_status:false })
        $('.accordion_close').click()
    }

    handleFocus = (event) => {
        event.target.select();
    };

    globalPayments(order_id, paycode) {
        this.setState({
            refunding_amount: 0,
            globalPayments: paycode,
            orderId: order_id,
            globalStatus: true,
            isPaymentStart: true
        })
        let paymentAmount = 0;
        if (this.state.paidAmountStatus == true) {
            paymentAmount = this.state.refunding_amount;
        } else {
            paymentAmount = this.state.refunding_amount !== 0 ? this.state.refunding_amount : this.state.refundingAmount;
        }
        //    let decodedString = localStorage.getItem('UDID');
        //    var decod=  window.atob(decodedString);
        var UID = get_UDid('UDID');
        this.setState({
            refunding_amount: paymentAmount,
            paidAmountStatus: true
        })
        // console.log("this.props" , this.props)
        this.props.dispatch(checkoutActions.getMakePayment(UID, localStorage.getItem('register'), paycode, paymentAmount))
        //this.setRefundPayment(order_id , paycode)
    }

    componentWillReceiveProps(nextProp) {
        //  console.log("PaymentNextProp",nextProp);
        if (nextProp.global_payment) {
            //   console.log("123" ,this.state.globalPayments , this.state.orderId)
            if (this.state.globalStatus === true) {
                this.setState({ isPaymentStart: false })
                if (nextProp.global_payment.IsSuccess === true) {
                    this.setRefundPayment(this.state.orderId, this.state.globalPayments)
                }
            }
        }

    }

    changeStatus(status) {
        console.log("paidAmountStatus 0" , this.state.paidAmountStatus)
        this.setState({ 
            new_status: status,
            paidAmountStatus:false
          })
    }

    render() {
        const { single_Order_list, paidAmountStatus, paymentTypeName, new_status } = this.state;
        let getorder = single_Order_list;
        console.log("this.state", new_status)
        //const { paymentTypeName } = this.props;
        // console.log("refunding_amount", this.state.refunding_amount , this.props.global_payment)
        return (
            <div className="col-lg-5 col-md-5 col-sm-6 col-xs-12 pt-4 plr-8">
                <div className="block__box white-background round-8 full_height overflowscroll text-center pl-3 pr-3">
                    <h2>Refund Amount</h2>
                    <div className="wrapper_accordion">
                    {new_status !== 'refund_amount_true' ?
                    <div className="gray-background mb-2 round-8 d-none overflowHidden overflow-0">
                            <div className="section">
                                <div className=" " data-isopen="">
                                    <div className="pointer">
                                        <div className="box-flex center-center">
                                            <div className="col-sm-9" style={{ marginLeft: '16%' }}>
                                                <div className="block__box_text_price">
                                                    {paidAmountStatus == true ?
                                                        <input placeholder="0.00" id="my-input" value={this.state.refunding_amount} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline" autoComplete='off' />
                                                        :
                                                        <input placeholder="0.00" id="my-input" autoFocus="autoFocus" value={parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2)} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline" onFocus={this.handleFocus} />
                                                    }
                                                </div>
                                              </div>
                                            <div className="col-sm-3">
                                                <div className="block__box_action_description park_avoid" >
                                                    <img  onClick={() => { this.changeStatus('refund_amount_true'); }} src="../assets/img/calculator_minimiser.svg" className="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                         :
                        <div className="gray-background mb-2 round-8 d-none overflowHidden overflow-0">
                            <div className="section">
                                <div className="">
                                    <div className="box-flex center-center park_table_row">
                                        <div className="col-sm-3">
                                            <div className="block__box_text_hint">
                                                <img src="../assets/img/back_payment.svg" onClick={() => this.rmvInp()} />
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="block__box_text_price">
                                                {paidAmountStatus == true ?
                                                    <input placeholder="0.00" id="refund_calc_output" value={this.state.refunding_amount} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline" autoComplete='off' />
                                                    :
                                                    <input placeholder={parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2)} id="refund_calc_output" autoFocus="autoFocus" value={this.state.refunding_amount == 0 ? '' : parseFloat(this.state.refunding_amount).toFixed(2)} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline placeholder-color" onFocus={this.handleFocus} />
                                                }
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="block__box_action_description" href="#paymentReset" data-toggle="collapse">
                                                <img onClick={() => { this.changeStatus(false); }} src="../assets/img/calculator_minimiser_colored.svg" className="gray-minisizer-icon" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="panel-body p-0">
                                        <div className="box__block_calculator">
                                            <table className="table table-responsive">
                                                <tbody>
                                                    <tr className="park_table_row">
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline" onClick={() => this.calcInp(1)} value="1" placeholder="1" />
                                                        </td>
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(2)} value="2" placeholder="2" />
                                                        </td>
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(3)} value="3" placeholder="3" />
                                                        </td>
                                                    </tr>
                                                    <tr className="park_table_row">
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(4)} value="4" placeholder="4" />
                                                        </td>
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(5)} value="5" placeholder="5" />
                                                        </td>
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(6)} value="6" placeholder="6" />
                                                        </td>
                                                    </tr>
                                                    <tr className="park_table_row">
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(7)} value="7" placeholder="7" />
                                                        </td>
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(8)} value="8" placeholder="8" />
                                                        </td>
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(9)} value="9" placeholder="9" />
                                                        </td>
                                                    </tr>
                                                    <tr className="park_table_row">
                                                        <td>
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp('.')} value="." placeholder="," />
                                                        </td>
                                                        <td colSpan="2" className="border-left-0">
                                                            <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp(0)} value="0" placeholder="0" />
                                                        </td>
                                                    </tr>
                                                    <tr className="park_table_row" onClick={() => this.hideTab()}>
                                                        <td colSpan="3" >
                                                            <input type="button" className="secondry-theme-background border-0 color-4b no-outline btn-enter " value="Enter" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     }


                        {(typeof paymentTypeName !== 'undefined') && paymentTypeName !== null ?
                            paymentTypeName.map((pay_name, index) => {
                                 return (
                                    <div key={index}>
                                        {this.state.isPaymentStart === true ? <LoadingModal /> : ""}
                                        {pay_name.Code == 'global_payments' ?
                                            (pay_name.HasTerminal == true && pay_name.TerminalCount > 0) ?

                                                <div className="white-background box-flex-shadow box-flex-border mb-2 round-8 d-none overflowHidden overflow-0">
                                                    {/*   onClick={() => this.pay_amount(pay_name.Code)}*/}
                                                    <div className="section">
                                                        <div className="accordion_header" data-isopen="true" >
                                                            <div className="pointer"    >
                                                                <div style={{ borderColor: `${pay_name.ColorCode}` }} className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                                                    <div className="box-flex-text-heading">
                                                                        <h2 onClick={() => this.globalPayments(single_Order_list.order_id, pay_name.Code)}>
                                                                            {pay_name.Name}
                                                                        </h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="expandable_accordion">
                                                            <div className="border-color overflowHidden">
                                                                <div className="box__block_caption theme-background b-0 round-top-corner" data-title="Global Payments"  >
                                                                    <img src="../assets/img/cross_white.svg" className="accordion_close" />
                                                                </div>
                                                                {/* <!-- <div className="" id="cash-toggle"> --> */}
                                                                <div className="overflowscroll global-body">
                                                                    <div className="clearfix center-center d-column global_body">
                                                                        <div className="line-height-30 mb-3">
                                                                            <p className="fs16 color-4b">status</p>
                                                                            <p className="fs24 theme-color">{this.props.global_payment ? this.props.global_payment.Message : "Waiting On Terminal"}</p>
                                                                        </div>
                                                                        <div className="row w-75">
                                                                            <div className="col-sm-4 p-0">
                                                                                <img src="assets/img/ic_gloabal_close.svg" width="50px" />
                                                                                <p><small>Cancel Payment</small></p>
                                                                            </div>
                                                                            <div className="col-sm-4 p-0">
                                                                                <img onClick={() => this.globalPayments(single_Order_list.order_id, pay_name.Code)} src="assets/img/ic_gloabal_refresh.svg" width="50px" />
                                                                                <p><small>Resend Amount</small></p>
                                                                            </div>
                                                                            <div className="col-sm-4 p-0">
                                                                                <img src="assets/img/ic_gloabal_done.svg" width="50px" />
                                                                                <p><small>Manual Accept</small></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {/* <!-- </div> --> */}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                :
                                                null
                                            :

                                            pay_name.Code == 'cash' ?
                                                new_status !== `${pay_name.Code}_true` ?
                                                    <button onClick={() => { this.changeStatus(`${pay_name.Code}_true`); }} className="white-background box-flex-shadow box-flex-border mb-2 round-8 pointer d-none overflowHidden no-outline w-100 p-0 overflow-0">
                                                        <div className="section">
                                                            <div className="" data-isopen="">
                                                                <div className="pointer">

                                                                    <div style={{ borderColor: `${pay_name.ColorCode}` }} id="others" value="other" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-others border-dynamic">
                                                                        <div className="box-flex-text-heading">
                                                                            <h2>{pay_name.Name}</h2>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    :
                                                    <div className="white-background box-flex-shadow box-flex-border mb-2 round-8 d-none overflowHidden" >
                                                        <div className="section">
                                                            <div className="">
                                                                <div className="box__block_caption secondry-theme-background b-0 round-top-corner" data-title="Cash">
                                                                    <img onClick={() => this.changeStatus(false)} src="../assets/img/cross_white.svg" className="" />
                                                                </div>
                                                                {/* <!-- sdffffs --> */}
                                                                <div className="box__block_calculator" >
                                                                    <table className="table table-responsive table-layout-fixed">
                                                                        <tbody>
                                                                            <tr className="cash-row">
                                                                                <td className="b-0">
                                                                                    <img src="../assets/img/back_payment.svg" className="ic_table" onClick={() => this.rmvInp2()} />
                                                                                </td>
                                                                                <td colSpan="2" className="border-left-0 b-0">
                                                                                    {paidAmountStatus == true ?
                                                                                        <input placeholder="0.00" id="calc_output_cash" value={this.state.refunding_amount} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline " autoComplete='off' />
                                                                                        :
                                                                                        <input placeholder={parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2)} id="calc_output_cash" autoFocus="autoFocus" value={this.state.refunding_amount == 0 ? '' : parseFloat(this.state.refunding_amount).toFixed(2)} onChange={this.handleChange} type="text" className="gray-background border-0 color-4b text-center w-100 p-0 no-outline placeholder-color" onFocus={this.handleFocus} />
                                                                                    }
                                                                                </td>
                                                                            </tr>
                                                                            <tr className="cash-row">

                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline" onClick={() => this.calcInp2(1)} value="1" placeholder="1" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(2)} value="2" placeholder="2" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(3)} value="3" placeholder="3" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr className="cash-row">
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(4)} value="4" placeholder="4" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(5)} value="5" placeholder="5" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(6)} value="6" placeholder="6" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr className="cash-row">
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(7)} value="7" placeholder="7" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(8)} value="8" placeholder="8" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(9)} value="9" placeholder="9" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr className="cash-row">
                                                                                <td>
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2('.')} value="." placeholder="." />
                                                                                </td>
                                                                                <td colSpan="2" className="border-left-0">
                                                                                    <input type="button" className="transparent border-0 color-4b no-outline " onClick={() => this.calcInp2(0)} value="0" placeholder="0" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr className="cash-row" onClick={() => { this.hideTab("cash"); this.cashPayment("cash") }}>
                                                                                <td colSpan="3">
                                                                                    <input type="button" className="secondry-theme-background border-0 color-4b no-outline btn-enter " value="Enter" />
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                {/* <!-- fsadfsfsafs --> */}
                                                            </div>

                                                        </div>
                                                    </div>
                                                : pay_name.Code == 'other' ?
                                                    <button onClick={() => { this.setRefundPayment(single_Order_list.order_id, pay_name.Code); }} className="white-background box-flex-shadow box-flex-border mb-2 round-8 pointer d-none overflowHidden no-outline w-100 p-0 overflow-0">
                                                        <div className="section">
                                                            <div className="accordion_header" data-isopen="">
                                                                <div className="pointer">

                                                                    <div style={{ borderColor: `${pay_name.ColorCode}` }} id="others" value="other" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-others border-dynamic">
                                                                        <div className="box-flex-text-heading">
                                                                            <h2>{pay_name.Name}</h2>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    :
                                                    pay_name.Code == 'card' ?
                                                    new_status !== `${pay_name.Code}_true` ?
                                                    <button onClick={() => { this.changeStatus(`${pay_name.Code}_true`); }} className="white-background box-flex-shadow box-flex-border mb-2 round-8 pointer d-none overflowHidden no-outline w-100 p-0 overflow-0">
                                                        <div className="section">
                                                            <div className="" data-isopen="">
                                                                <div className="pointer">

                                                                    <div style={{ borderColor: `${pay_name.ColorCode}` }} id="others" value="other" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-others border-dynamic">
                                                                        <div className="box-flex-text-heading">
                                                                            <h2>{pay_name.Name}</h2>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </button>
                                                    :
                                                        <div className="white-background box-flex-shadow box-flex-border mb-2 round-8 d-none overflowHidden overflow-0">
                                                            <div className="section">
                                                                <div className="">
                                                                    <div className="box__block_caption giftcard-background b-0 round-top-corner" data-title="Giftcard">
                                                                        <img onClick={() => this.changeStatus(false)} src="../assets/img/cross_white.svg" className="accordion_close" />
                                                                    </div>
                                                                    <div className="overflowscroll giftcard_body" id="">
                                                                        <div className="p-3 clearfix">
                                                                            <div className="gray-background round-8 mb-3 overflowHidden">
                                                                                <div className="d-flex d-column box-flex line-height-30">
                                                                                    <div className="col-sm-12">
                                                                                        <div className="block__box_text_hint capital">
                                                                                            Amount On Card
                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-sm-12">
                                                                                        <div className="block__box_text_price">123098123909</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div className="w-100-block button_with_checkbox mb-2" title="">
                                                                                <input id="10" name="radio-group" type="radio" />
                                                                                <label htmlFor="10" className="label_select_button">Check Value</label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="line-height-30">
                                                                            <p className="fs16 color-4b">Amount On Card</p>
                                                                            <p className="fs24 theme-color">250.00</p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="box__block_button giftcard-background b-0 round-bottom-corner">
                                                                        <button type="button" className="btn transparent color-white fs24 w-100 no-outline pt-4 pb-4">Add Payment</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        :
                                                        pay_name.Code == 'debit_/_credit_card' ?
                                                            <button onClick={() => { this.setRefundPayment(single_Order_list.order_id, pay_name.Code); }} className="white-background box-flex-shadow box-flex-border mb-2 round-8 pointer d-none overflowHidden no-outline w-100 p-0 overflow-0">
                                                                <div className="section">
                                                                    <div className="accordion_header" data-isopen="">
                                                                        <div className="pointer">

                                                                            <div style={{ borderColor: `${pay_name.ColorCode}` }} id="debit_/_credit_card" value="debit_/_credit_card" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-others border-dynamic">
                                                                                <div className="box-flex-text-heading">
                                                                                    <h2>{pay_name.Name}</h2>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </button>
                                                            :
                                                            pay_name.Code == 'stripe' ?

                                                            new_status !== `${pay_name.Code}_true` ?
                                                            <button onClick={() => { this.changeStatus(`${pay_name.Code}_true`); }} className="white-background box-flex-shadow box-flex-border mb-2 round-8 pointer d-none overflowHidden no-outline w-100 p-0 overflow-0">
                                                                <div className="section">
                                                                    <div className="" data-isopen="">
                                                                        <div className="pointer">
        
                                                                            <div style={{ borderColor: `${pay_name.ColorCode}` }} id="others" value="other" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-others border-dynamic">
                                                                                <div className="box-flex-text-heading">
                                                                                    <h2>{pay_name.Name}</h2>
                                                                                </div>
                                                                            </div>
        
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </button>
                                                            :
                                                                <div className="white-background box-flex-shadow box-flex-border mb-2 round-8 mb-3 d-none overflowHidden overflow-0">
                                                                    <div className="section">
                                                                         <div className="">
                                                                            <div className="box__block_caption stripe-background b-0 round-top-corner" data-title="Stripe">
                                                                                <img onClick={() => { this.changeStatus(false); }}  src="../assets/img/cross_white.svg" className="accordion_close" />
                                                                            </div>
                                                                            {/* <!-- sdffffs --> */}
                                                                            <div className="overflowscroll stripe_body">
                                                                                <div className="p-3 clearfix">
                                                                                    <div className="gray-background round-8 mb-3 overflowHidden">
                                                                                        <div className="d-flex d-column box-flex line-height-30">
                                                                                            <div className="col-sm-12">
                                                                                                <div className="block__box_text_hint capital">
                                                                                                    Card Number
                                                                                         </div>
                                                                                            </div>
                                                                                            <div className="col-sm-12">
                                                                                                <div className="block__box_text_price">4324-2342-5477-4564</div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div className="gray-background round-8 mb-3 overflowHidden">
                                                                                        <div className="d-flex d-column box-flex line-height-30">
                                                                                            <div className="col-sm-12">
                                                                                                <div className="block__box_text_hint capital">
                                                                                                    Date
                                                                                         </div>
                                                                                            </div>
                                                                                            <div className="col-sm-12">
                                                                                                <div className="block__box_text_price">09-20</div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div className="gray-background round-8 mb-3 overflowHidden">
                                                                                        <div className="d-flex d-column box-flex line-height-30">
                                                                                            <div className="col-sm-12">
                                                                                                <div className="block__box_text_hint capital">
                                                                                                    CVV
                                                                                        </div>
                                                                                            </div>
                                                                                            <div className="col-sm-12">
                                                                                                <div className="block__box_text_price">781</div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            {/* <!-- fsadfsfsafs --> */}
                                                                            <div className="box__block_button stripe-background b-0 round-bottom-corner">
                                                                                <button type="button" className="btn transparent color-white fs24 w-100 no-outline pt-4 pb-4 uppercase">Add Payment</button>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                :
                                                                <button onClick={() => { this.setRefundPayment(single_Order_list.order_id, pay_name.Code); }} className="white-background box-flex-shadow box-flex-border mb-2 round-8 pointer d-none overflowHidden no-outline w-100 p-0 overflow-0">
                                                                    <div className="section">
                                                                        <div className="accordion_header" data-isopen="">
                                                                            <div className="pointer">

                                                                                <div style={{ borderColor: `${pay_name.ColorCode}` }} id={pay_name.Code} value={pay_name.Code} name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-others border-dynamic">
                                                                                    <div className="box-flex-text-heading">
                                                                                        <h2>{pay_name.Name}</h2>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </button>
                                        }
                                    </div>
                                )
                            })
                            : ''
                        }

                        {(typeof getorder.orderCustomerInfo !== 'undefined') && getorder.orderCustomerInfo !== null ?
                            <button onClick={() => { this.cashPayment('store-credit'); }} className="white-background box-flex-shadow box-flex-border mt-2 round-8 no-outline w-100 p-0 overflow-0">
                                <div style={{ borderColor: '#46A9D4' }} id="store-credit" value="store-credit" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                    <div className="box-flex-text-heading">
                                        <h2>Store-Credit</h2>
                                    </div>
                                </div>
                            </button>
                            :
                            null
                            // <button disabled className="white-background box-flex-shadow box-flex-border mt-2 round-8 no-outline w-100 p-0">
                            //     <div id="store-credit" value="store-credit" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-global">
                            //         <div className="box-flex-text-heading">
                            //             <h2>Store-Credit</h2>
                            //         </div>
                            //     </div>
                            // </button>
                        }


                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { refundlist, cash_rounding, global_payment } = state;
    return {
        refundlist,
        cash_rounding: cash_rounding.items,
        // paymentTypeName: paymentTypeName.items
        global_payment: global_payment.items
    };
}

const connectedRefundViewThird = connect(mapStateToProps, refundActions)(Calulator);
export { connectedRefundViewThird as Calulator };