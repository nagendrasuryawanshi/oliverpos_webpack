import React from 'react';
import { connect } from 'react-redux';


class CommonHeaderFirst extends React.Component {
    constructor(props){
        super(props);
        this.state={
            void_Sale:false
        }
    }

    componentDidMount() {
        // const page  = this.props;
        let VOID_SALE = localStorage.getItem("VOID_SALE")
        if ((typeof VOID_SALE !== 'undefined') && VOID_SALE !== null) {
            //console.log("VOID_SALE")
            this.setState({ void_Sale: true })
        }
    }

    popup_OpenClose(){
      if(this.state.void_Sale == true){
         $('#popup_void_sale').modal('show')
      }else{
        window.location ='/shopview'
      }
    }

    gotoResister(){
     
          window.location ='/shopview'
     
      }
    render() {

        const { match, history } = this.props;
        return (
            <nav className="navbar navbar-default" id="colorFullHeader">
                <div className="col-lg-3 col-sm-4 col-xs-4 pl-0 cart_header_overlap cart_left">
                    {/* Check Out */}
                    {match.path == '/checkout' ?
                        <div className="cart_header" >
                            <a href="javascript:void(0)" className="ch_icon">
                                <button className="button icon icon-backarrow-head" onClick={() =>this.popup_OpenClose()}></button>
                            </a>
                            <div className="ch_heading">
                                &nbsp; {this.state.void_Sale == false?'Add more items':'Void Sale'} 
                          </div>
                           <div className="dropdown ch_icon">
                                 <a className="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                    <button className="button icon icon-plus-white"></button>
                                </a>
                                {this.state.void_Sale == false?
                                <ul className="dropdown-menu custom-drpbox dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                    <li role="presentation"><a role="menuitem" tabIndex="-1" data-toggle="modal" data-target="#addnotehere">Add Notes</a></li>
                                    <li role="presentation"><a role="menuitem" tabIndex="-1" data-toggle="modal" data-target="#popup_oliver_add_fee">Add Fee</a></li>
                                </ul>
                                :null}
                            </div>
                        </div>
                        : null}
                    {match.path == '/refund' ?
                        <div className="cart_header">
                            <a href="/activity" className="ch_icon">
                                <button className="button icon icon-backarrow-head"></button>
                            </a>
                            <div className="ch_heading">
                                &nbsp;
                            Go Back
                        </div>
                           {/* <div className="dropdown ch_icon">
                                 <a className="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                    <button className="button icon icon-plus-white"></button>
                                </a>
                                <ul className="dropdown-menu custom-drpbox dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                    <li role="presentation"><a role="menuitem" tabIndex="-1" data-toggle="modal" data-target="#addnotehere">Add Notes</a></li>
                                    <li role="presentation"><a role="menuitem" tabIndex="-1" data-toggle="modal" data-target="#popup_oliver_add_fee">Add Fee</a></li>
                                </ul>
                            </div> */}
                        </div>
                        : null}
                </div>
                <div className="col-xs-offset-4 col-md-offset-4 col-sm-offset-4 col-lg-offset-3 col-lg-9 col-sm-8 col-xs-8 p-0">
                    <div className="container-fluid">
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-right">
                                <li className="nav_logo">
                                    <a  onClick={this.gotoResister}>Main Register</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedCommonHeaderFirst = connect(mapStateToProps)(CommonHeaderFirst);
export { connectedCommonHeaderFirst as CommonHeaderFirst };