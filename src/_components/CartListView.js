import React from 'react';
import { connect } from 'react-redux';
import { default as NumberFormat } from 'react-number-format'
import { cartProductActions } from '../_actions/cartProduct.action'
import { history } from '../_helpers';
import { checkoutActions } from '../CheckoutPage/actions/checkout.action';
import { Markup } from 'interweave';
import { getDiscountAmount, getInclusiveTax } from '../_components'
import Config from '../Config'
import moment from 'moment';
import { openDb } from 'idb';
import { get_UDid } from '../ALL_localstorage'
import { getTaxAllProduct } from './'


class CartListView extends React.Component {
    constructor(props) {
        super(props);
        // const {cartproductlist}= this.props
        this.state = {
            //  cartItem: cartproductlist,   //localStorage.getItem("CARD_PRODUCT_LIST") ? localStorage.getItem("CARD_PRODUCT_LIST") : [],
            discountCalculated: 0.0,
            discountAmount: 0,
            discountType: "",
            subTotal: 0.00,
            totalAmount: 0.00,

            // http://app.oliverpos.com/api/ShopSetting/GetRate
            isTaxPercet: false,
            taxRate: "",
            taxAmount: 0.0,
            locaUrl: history.location.pathname,
            addcust: [],
            TaxId: null,
            showTaxStaus: 'Tax',
            productlist: [],
            updateProductStatus:false

        }
        // var udid = get_UDid('UDID');
        // const dbPromise = openDb('ProductDB', 1, upgradeDB => {
        //     upgradeDB.createObjectStore(udid);
        // });

        // const idbKeyval = {
        //     async get(key) {
        //         const db = await dbPromise;
        //         return db.transaction(udid).objectStore(udid).get(key);
        //     },

        // };

        // idbKeyval.get('ProductList').then(val => {
        //     if( !val || val.length==0 || val==null || val==""){
        //         //console.error('Console.save: No data')
        //      this.setState({ productlist:[] });                
        //     }else{
        //       //  console.log('productlist1111',val)
        //       this.setState({ productlist: getTaxAllProduct(val) });
        //     }
        // }
        // );
    }



    checkout(ListItem) {
        //let discountIs = getDiscountAmount(ListItem);
        let discountIs = 0;
        //  ListItem.map(Item => {
        //     if (Item.discount_amount !== 0) {
        //         if (Item.incl_tax !== 0) {
        //             let incl_tax = getInclusiveTax(Item.old_price)
        //             discountIs += incl_tax
        //         } else {
        discountIs = this.state.discountCalculated
        //         }
        //     }
        // })
        //alert(discountIs)
        //console.log("discountIs", discountIs)
        let order_id = (typeof localStorage.getItem("CHECKLIST") !== 'undefined') ? JSON.parse(localStorage.getItem("CHECKLIST")) : null;
        // console.log("checkout 1" , order_id)
        const { dispatch } = this.props;
        const { addcust, TaxId, taxRate } = this.state;
        const CheckoutList = {
            ListItem: ListItem,
            customerDetail: addcust,
            totalPrice: this.state.totalAmount,
            discountCalculated: discountIs ? discountIs : 0,
            tax: this.state.taxAmount,
            subTotal: this.state.subTotal,
            TaxId: TaxId,
            TaxRate: taxRate,
            order_id: order_id !== null ? order_id.order_id : 0,
            showTaxStaus: this.state.showTaxStaus
        }
        //  console.log("checkout 1" , order_id)
        if (ListItem.length == 0) {
            $('#checkout1').modal('show');
        } else {
            this.setState({updateProductStatus:true})
            localStorage.setItem("CHECKLIST", JSON.stringify(CheckoutList))
            dispatch(checkoutActions.checkItemList(CheckoutList))
        }
    }

    addCustomer() {

        var param = this.state.locaUrl;
        var url = '/customerview';
        history.push(url, param)
        window.location = '/customerview'

    }

    deleteAddCust() {
        localStorage.removeItem('AdCusDetail')
        //localStorage.setItem('AdCusDetail', null)
        this.setState({ addcust: [] })
    }

    componentDidMount() {
        localStorage.removeItem('temp_paid_amount')
        let AdCusDetail = localStorage.getItem('AdCusDetail');
        if (AdCusDetail != null) {
            this.setState({ addcust: JSON.parse(AdCusDetail) })
        } else {
            this.setState({ addcust: [] })
        }
        setTimeout(function () {

            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 1000);


    }

    deleteProduct(item) {
        // localStorage.removeItem("PRODUCT")
        var product = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [];//
        /// var product = this.props.cartproductlist;// JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST"));//
        // console.log("deleteProduct list", product)
        // console.log("deleteProduct single", item )

        var i = 0;
        var index;
        for (i = 0; i < product.length; i++) {
            if ((typeof item.product_id !== 'undefined') && item.product_id !== null) {
                if (item.variation_id !== 0) {

                    if (product[i].variation_id == item.variation_id)
                        index = i;
                }
                else {

                    if (product[i].product_id == item.product_id)
                        index = i;
                }

            } else {
                if (product[i].Title == item.Title) {
                    index = i;
                }
            }
        }
        product.splice(index, 1);
        // console.log("item3" ,product)
        if (product.length == 0) {
            localStorage.removeItem("CART");
            localStorage.removeItem("PRODUCT")
            localStorage.removeItem("SINGLE_PRODUCT")
            localStorage.removeItem("CARD_PRODUCT_LIST");
            const { dispatch } = this.props;
            dispatch(cartProductActions.addtoCartProduct(null));
            dispatch(cartProductActions.singleProductDiscount())
            dispatch(cartProductActions.showSelectedProduct(null));
            dispatch(cartProductActions.addInventoryQuantity(null));
        } else {
            const { dispatch } = this.props;
            dispatch(cartProductActions.addtoCartProduct(product));
            dispatch(cartProductActions.showSelectedProduct(null));
            dispatch(cartProductActions.addInventoryQuantity(null));
            //dispatch(cartProductActions.singleProductDiscount(item))
        }
    }

    componentWillReceiveProps(nextProps) {
        var taxratelist;
        if ((typeof localStorage.getItem('TAXT_RATE_LIST') !== 'undefined') && localStorage.getItem('TAXT_RATE_LIST') !== null) {

            taxratelist = JSON.parse(localStorage.getItem('TAXT_RATE_LIST'))
            //console.log("taxratelist",taxratelist)

        } else {
            taxratelist = this.props.taxratelist && this.props.taxratelist.taxratelist;
        }

        var _subtotal = 0.0;
        var _total = 0.0;
        var _taxAmount = 0.0;
        var _taxRate = 0.0;
        var _totalDiscountedAmount = 0.0;
        var _customFee = 0.0;
        var _exclTax = 0;
        var _inclTax = 0;

        if (typeof (taxratelist) !== 'undefined' && (taxratelist) !== null) {

            if (typeof (taxratelist) !== 'undefined') {

                if (taxratelist.TaxRate.indexOf("%") >= -1) {
                    this.setState({
                        TaxId: taxratelist.TaxId,
                        taxRate: taxratelist.TaxRate
                    })

                    var _taxratelist = "";
                    if (taxratelist != undefined) {
                        _taxratelist = taxratelist.TaxRate.toString();
                        this.state.taxRate = parseFloat(_taxratelist.substring(-1, _taxratelist.length - 1));
                    }

                }
            }
        }
        _taxRate = this.state.taxRate;
        nextProps.cartproductlist && nextProps.cartproductlist.map((item, index) => {
            // console.log("DISCOUNT LIST" , item)
            if (item.Price) {
                _subtotal += parseFloat((typeof item.discount_amount !== "undefined") && item.discount_amount !== 0 ? item.Price - item.discount_amount : item.Price);
                _totalDiscountedAmount += parseFloat((typeof item.discount_amount !== "undefined") && item.discount_amount !== 0 ? parseFloat(item.discount_amount) : 0);

                if (item.product_id) {//donothing
                    _exclTax += item.excl_tax ? item.excl_tax : 0,
                        _inclTax += item.incl_tax ? item.incl_tax : 0
                }
                else {
                    _customFee += item.Price;
                }

            }
        })

        if (_taxRate) {
            // _taxAmount = ( (_subtotal-_customFee) * _taxRate) / 100.0;
            _taxAmount = _exclTax + _inclTax;

        }

        //--------------------------------------------

        // _total = _subtotal + _taxAmount;
        _total = _subtotal + _exclTax;

        this.setState({
            subTotal: _subtotal,
            totalAmount: _total,// parseFloat(_subtotal) - parseFloat(nextProps.discountAmount),           
            discountAmount: nextProps.discountAmount,
            discountType: nextProps.discountType,
            taxAmount: _taxAmount, //(( parseFloat(_subtotal) - parseFloat(nextProps.discountAmount))% parseFloat(this.state.taxRate))*100.0           
            discountCalculated: _totalDiscountedAmount > 0 ? _totalDiscountedAmount : 0,
            showTaxStaus: (_inclTax !== 0) ? 'Incl. Tax' : 'Tax'

        })

        this.props.onDiscountAmountChange(0, 'Number')
       if(nextProps.checkout_list && this.state.updateProductStatus == true){
           var checkProductUpdate ;
           var IsExist = false
           checkProductUpdate =  nextProps.checkout_list.find(item=>item.success == false);
           if(checkProductUpdate){
             // console.log("is success true", checkProductUpdate)
              IsExist = false;
              $('#checkout1').modal('show')
           }else{
              IsExist = true;
             // console.log("is success false")
           }
           //console.log("IsExist", IsExist)
           if(IsExist === true){
                localStorage.removeItem("oliver_order_payments")
                window.location = '/checkout';
           }
           this.setState({
              updateProductStatus:false   
           })
       }

    }

    singlProductDiscount(item, selectedIndex) {
        this.getProductFromIndexDB();
        setTimeout(() => {
            // console.log("singlProductDiscount", item)
            let product = this.state.productlist.find(prd => prd.WPID == item.product_id);
            if (item.variation_id !== 0) {
                var variationProdect = this.state.productlist.filter(item => {
                    return (item.ParentId === product.WPID && (item.ManagingStock == false || (item.ManagingStock == true && item.StockQuantity > -1)))
                })
                product['Variations'] = variationProdect
            }
            // variation_id
            // this.props.showPopuponcartlistView(product, item)


            if (product.Type !== 'variable') {
                product['after_discount'] = item.after_discount;
                product['cart_after_discount'] = item.cart_after_discount;
                product['cart_discount_amount'] = item.cart_discount_amount;
                product['discount_amount'] = item.discount_amount;
                product['discount_type'] = item.discount_type;
                product['excl_tax'] = item.excl_tax;
                product['incl_tax'] = item.incl_tax;
                product['new_product_discount_amount'] = item.new_product_discount_amount;
                product['old_price'] = item.old_price;
                product['product_after_discount'] = item.product_after_discount;
                product['product_discount_amount'] = item.product_discount_amount;
                item['ProductAttributes'] = product.ProductAttributes;
                item['combination'] = product.combination;
                item['StockQuantity'] = product.StockQuantity;
                item['StockStatus'] = product.StockStatus;
                item['InStock'] = product.InStock;
                item['IsTicket'] = product.IsTicket;
                item['ManagingStock'] = product.ManagingStock;
                item['ParentId'] = product.ParentId;
                item['WPID'] = product.WPID;
                item['selectedIndex'] = selectedIndex;
            } else {

                product.Variations && product.Variations.map(vartion => {
                    if (vartion.WPID == item.variation_id) {
                        vartion['after_discount'] = item.after_discount;
                        vartion['cart_after_discount'] = item.cart_after_discount;
                        vartion['cart_discount_amount'] = item.cart_discount_amount;
                        vartion['discount_amount'] = item.discount_amount;
                        vartion['discount_type'] = item.discount_type;
                        vartion['excl_tax'] = item.excl_tax;
                        vartion['incl_tax'] = item.incl_tax;
                        vartion['new_product_discount_amount'] = item.new_product_discount_amount;
                        vartion['old_price'] = item.old_price;
                        vartion['product_after_discount'] = item.product_after_discount;
                        vartion['product_discount_amount'] = item.product_discount_amount;
                        item['ProductAttributes'] = vartion.ProductAttributes;
                        item['combination'] = vartion.combination;
                        item['StockQuantity'] = vartion.StockQuantity;
                        item['StockStatus'] = vartion.StockStatus;
                        item['InStock'] = vartion.InStock;
                        item['IsTicket'] = vartion.IsTicket;
                        item['ManagingStock'] = vartion.ManagingStock;
                        item['ParentId'] = vartion.ParentId;
                        item['WPID'] = vartion.WPID;
                        item['selectedIndex'] = selectedIndex;
                    }
                })

            }

            console.log("UpdatedStockQuantity", item)

            //console.log("product", product)
            const { dispatch } = this.props;
            dispatch(cartProductActions.showSelectedProduct(item));
            this.props.showPopuponcartlistView(product, item)
        }, 500);
    }

    getProductFromIndexDB() {
        var udid = get_UDid('UDID');
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },

        };

        idbKeyval.get('ProductList').then(val => {
            if (!val || val.length == 0 || val == null || val == "") {
                //console.error('Console.save: No data')
                this.setState({ productlist: [] });
            } else {
                // console.log('productlist1111',val)
                this.setState({ productlist: getTaxAllProduct(val) });
            }
        }
        );
    }

    cardProductDiscount() {
        let ListItem = this.props.cartproductlist ? this.props.cartproductlist : [];

        if (ListItem.length !== 0) {
            $("#popup_discount").find('#txtdis').val(0)
            $('#popup_discount').modal('show');
            var data = {
                card: 'card',
            }
            localStorage.removeItem("PRODUCT")
            localStorage.removeItem("SINGLE_PRODUCT")
            this.props.dispatch(cartProductActions.selectedProductDis(data))
        } else {
            $('#checkout1').modal('show')
        }

    }

    tickit_Details(status, item) {
        //console.log("item123", item);
        if (status == 'create') {
            $('#tickitDetails').modal('show')
            this.props.ticketDetail(status, item)
        }
        else if (status == 'edit') {
            //console.log("item1234", item);
            $('#tickitDetails').modal('show')

            this.props.ticketDetail(status, item)

        }

    }

    render() {
        var totalPrice = 0;
        const { addcust } = this.state;
        let ListItem = this.props.cartproductlist ? this.props.cartproductlist : [];
        let Addcust = localStorage.getItem('AdCusDetail') ? JSON.parse(localStorage.getItem('AdCusDetail')) : [];
        //console.log("this.state.discountCalculated" , this.state.discountCalculated);

        return (


            <div className="col-lg-3 col-sm-4 col-xs-4 pr-0  plr-8">
                <div className="panel panel-default panel-right-side bb-0 r0 bg-white">
                    {Addcust && Addcust.Content != null ?
                        <div className="panel-heading bg-white">
                            {Addcust.Content.FirstName ? Addcust.Content.FirstName : Addcust.Content.Email}{" "} {Addcust.Content.LastName ? Addcust.Content.LastName : null}
                            <span className="pull-right">
                                <img src="assets/img/delete-32.png" className="pointer" onClick={() => this.deleteAddCust()} />
                            </span>
                        </div>
                        : <div className="panel-heading bg-white">
                            Add Customer
                            <span className="pull-right" onClick={(e) => this.addCustomer()}>
                                <img src="assets/img/add_29.png" className="pointer" />
                            </span>
                        </div>
                    }
                    <div className="panel-body p-0 overflowscroll bg-white" id="cart_product_list">
                        <div className="table-responsive">
                            <table className="table CartProductTable">
                                <tbody>
                                    {ListItem && ListItem.map((item, index) => {
                                        //  console.log("item",item,"index",index);

                                        //ListItem.length > 0 &&
                                        var rowclass = item.ticket_status && item.ticket_status == true ? "no-border-ticket" : "";

                                        return (
                                            <tr className="" key={index}>
                                                <td className="p-0">
                                                    <table className="table CartProductTable no-border">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" onClick={() => this.singlProductDiscount(item, index)}>{item.quantity ? item.quantity : 1}</td>
                                                                <td align="left" onClick={() => this.singlProductDiscount(item, index)}><Markup content={item.Title} /> {/* <span className="comman_subtitle">Red</span> */}</td>
                                                                {(typeof item.product_id !== 'undefined') ?
                                                                    <td align="right" onClick={() => this.singlProductDiscount(item, index)}>
                                                                        <span>{parseFloat(item.discount_amount) !== 0.00 ? <NumberFormat value={item.Price - item.discount_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> : null}</span>

                                                                        <NumberFormat className={parseFloat(item.discount_amount) == 0.00 ? '' : 'comman_delete'} value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                                    </td>
                                                                    :
                                                                    <td align="right">
                                                                        <NumberFormat value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                                    </td>
                                                                }
                                                                <td className="pr-0" align="right">
                                                                    <img onClick={() => this.deleteProduct(item)} src="assets/img/c_delete.png" />
                                                                </td>
                                                            </tr>
                                                            {/* {item.ticket_status == true  && item.ticket_info == '' ?  */}
                                                            {/* process.env.ENVIRONMENT == 'devtickera' &&  */}
                                                            {item.ticket_status == true ?
                                                                <tr>
                                                                    <td colSpan="4" className="pr-2">

                                                                        {item.ticket_status == true && (item.ticket_info === '' || item.ticket_info.length === 0) ?
                                                                            <div className="w-100-block button_with_checkbox p-0">

                                                                                <input type="radio" id={`add-details${index}`} name="radio-group" />
                                                                                <label htmlFor={`add-details${index}`} className="label_select_button" onClick={() => this.tickit_Details('create', item)} >
                                                                                    Add <span className="hide_small">Details</span></label>
                                                                            </div>
                                                                            :
                                                                            <div className="w-100-block button_with_checkbox p-0 ">
                                                                                <input type="radio" id={`add-details${index}`} name="radio-group" />
                                                                                <label htmlFor={`add-details${index}`} className="label_select_button" onClick={() => this.tickit_Details('edit', item)}>change <span className="hide_small"> Details</span></label>
                                                                            </div>

                                                                        }
                                                                    </td>
                                                                </tr>
                                                                // :process.env.ENVIRONMENT == 'devtickera' && item.ticket_status == true && item.ticket_cart_display_status && item.ticket_cart_display_status == 'expired' ?
                                                                //      <tr>
                                                                //      <td colSpan="4" className="pr-2">
                                                                //        <div className="w-100-block button_with_checkbox p-0">

                                                                //              <p>Ticket has expired</p>
                                                                //             </div>
                                                                //      </td></tr>
                                                                : <tr />
                                                            }
                                                            {/* //         :
                                                    //         item.ticket_status == true  && item.ticket_info !== '' ?
                                                    
                                                    //         <tr>
                                                    //             <td colSpan="2">
                                                    //                 <div className="w-100-block button_with_checkbox p-0 ">
                                                    //                     <input type="radio" id={`add-details${index}`} name="radio-group" />
                                                    //                     <label htmlFor={`add-details${index}`} className="label_select_button" onClick={() => this.tickit_Details('edit', item)}>change <span className="hide_small"> Details</span></label>
                                                    //                 </div>
                                                    //             </td>
                                                    //             <td colSpan="2" className="pr-2 w-50">
                                                    //                 <div className="w-100-block button_with_checkbox p-0">
                                                    //                     <input type="radio"  id={`add-details${index}`} name="radio-group" />
                                                    //                     <label htmlFor={`add-details${index}`} className="label_select_button" onClick={() => this.tickit_Details('create', item)} >Add <span className="hide_small">Details</span></label>
                                                    //                 </div>
                                                    //             </td>
                                                    //         </tr> :
                                                    // <tr></tr>
                                                   // } */}
                                                        </tbody>
                                                    </table>
                                                </td>


                                            </tr>

                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="panel-footer p-0 bg-white">
                        <div className="table-calculate-price">
                            <table className="table ShopViewCalculator">
                                <tbody>
                                    <tr>
                                        <th className="">Sub-Total</th>
                                        <td align="right" className="">
                                            <span className="value pull-right">
                                                {

                                                    ListItem && ListItem.length > 0 && ListItem.map((item, index) => {
                                                        totalPrice += item.Price
                                                    })
                                                }
                                                <NumberFormat value={this.state.subTotal} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className='w-50'> {this.state.showTaxStaus}:
                                        <span className="value pull-right">
                                                <NumberFormat value={this.state.taxAmount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </span>
                                        </th>
                                        <th className="bl-1" align="right">
                                            Discount:
                                            <span className="value pull-right pointer" style={{ color: '#46A9D4' }} data-toggle="modal" onClick={() => this.cardProductDiscount()}>
                                                {
                                                    (this.state.discountCalculated > 0) ? (
                                                        <NumberFormat value={this.state.discountCalculated} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />) : 'ADD'
                                                }
                                            </span>

                                        </th>

                                    </tr>
                                    <tr>
                                        <th colSpan="2" className="p-0">
                                            {/* <button className="btn btn-block btn-primary checkout-items" onClick="window.location.href = 'checkout.html';"> */}
                                            <button className="btn btn-block btn-primary checkout-items" onClick={() => this.checkout(ListItem)}>
                                                <span className="pull-left">
                                                    Checkout
                                                  </span>
                                                <span className="pull-right">
                                                    <NumberFormat value={this.state.totalAmount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                </span>
                                            </button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}




function mapStateToProps(state) {
    const { cartproductlist, checkout_list, taxratelist, selecteditem } = state;
    return {
        cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],
        checkout_list: checkout_list.items,
        taxratelist: taxratelist,
        selecteditem: selecteditem
    };
}

const connectedCartListView = connect(mapStateToProps)(CartListView);
export { connectedCartListView as CartListView };