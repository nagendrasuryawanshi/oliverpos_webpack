import { customerConstants } from '../constants/customer.constants'
import { customerService } from '../services/customer.service';
import { alertActions } from '../../_actions';
import { history } from '../../_helpers';


export const customerActions = {
    getAll,
    getPage,
    save,
    getDetail,
    Delete,
    filteredList,
    removeStoreCredit,
    getCountry,
    getState

};

function getPage(UID, PageSize, PageNumber) {

    return dispatch => {
        dispatch(request());

        customerService.getPage(UID, PageSize, PageNumber)
            .then(
                customerlist => {
                    //    var CustList=  localStorage.getItem("CustomerList")? localStorage.getItem("CustomerList"):[];
                    //    console.log("Action",CustList )
                    //   // console.log("customerlist",customerlist )
                    //    console.log("localstorage",localStorage.getItem("CustomerList"))
                    //    CustList=[...CustList,... customerlist.Content.Records]

                    //    localStorage.setItem("CustomerList",CustList);

                    //     console.log("customerlistAction",localStorage.getItem("CustomerList"));
                    //console.log("customerlist=>1", customerlist)
                      
                    if( customerlist.Content && customerlist.Content.Records && customerlist.Content.Records[0])
                    {
                     
                    localStorage.setItem("CUSTOMER_ID", customerlist.Content && customerlist.Content.Records && customerlist.Content.Records[0].Id)
                   
                    }  

                    
                    // if(Detail){

                    
                    dispatch(success(customerlist))
                    //customerActions
                    // }
                }
                ,
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: customerConstants.GETALL_REQUEST } }
    function success(customerlist) { return { type: customerConstants.GETALL_SUCCESS, customerlist } }
    function failure(error) { return { type: customerConstants.GETALL_FAILURE, error } }
}

function getCountry(){
        return dispatch => {
            dispatch(request());
    
            customerService.getCountryList()
                .then(
                    countrylist => {
                        dispatch(success(countrylist))
                    //  console.log("customerlist=>1", countrylist.Content)
                     localStorage.setItem('countrylist',JSON.stringify(countrylist.Content))   
                    // console.log("customerlist=>12",JSON.parse(localStorage.getItem('countrylist')))
                    },
                    error => dispatch(failure(error.toString()))
                );
        };
        function request() { return { type: customerConstants.GETALL_COUNTRY_REQUEST } }
        function success(countrylist) { return { type: customerConstants.GETALL__COUNTRY_SUCCESS, countrylist } }
        function failure(error) { return { type: customerConstants.GETALL_COUNTRY_FAILURE, error } } 
 }
  function getState(){
    return dispatch => {

        customerService.getStateList()
            .then(
                statelist => {
                   
                //  console.log("customerlist=>1", countrylist.Content)
                 localStorage.setItem('statelist',JSON.stringify(statelist.Content))   
                 //console.log("customerlist=>12",JSON.parse(localStorage.getItem('statelist')))
                },
                error => ( console.log("error",error))
            );
    };
}   
function getAll(UID) {
    return dispatch => {
        dispatch(request());

        customerService.getAll(UID)
            .then(
                customerlist => {
                    dispatch(success(customerlist))
                    console.log("customerlist=>1", customerlist)
                    localStorage.setItem("CUSTOMER_ID", customerlist.Content && customerlist.Content[0].Id)

                },
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: customerConstants.GETALL_REQUEST } }
    function success(customerlist) { return { type: customerConstants.GETALL_SUCCESS, customerlist } }
    function failure(error) { return { type: customerConstants.GETALL_FAILURE, error } }
}

function getDetail(ID, UID) {
    
    return dispatch => {
        dispatch(request());

        customerService.getDetail(ID, UID)
            .then(
                single_cutomer_list => {
                   

    
                    dispatch(success(single_cutomer_list))
                },
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: customerConstants.GET_DETAIL_REQUEST } }
    function success(single_cutomer_list) { return { type: customerConstants.GET_DETAIL_SUCCESS, single_cutomer_list } }
    function failure(error) { return { type: customerConstants.GET_DETAIL_FAILURE, error } }
}

function save(customer , status) {
    //console.log("customer",customer)
    return dispatch => {
        dispatch(request(customer));
        if( (!customer.Email) || customer.Email==""){
            dispatch(failure("Email can't be emplty"));;
        }else{
        customerService.save(customer)
            .then(
                customer => {

                    dispatch(success(customer));
                    // history.push('/customerview');
                     console.log("Success Customer List",customer)
                     localStorage.setItem("CUSTOMER_ID",customer.Content.Id);
                     if(status == 'create'){
                     }else if((typeof localStorage.getItem('CHECKLIST') =='undefined' ) ||  localStorage.getItem('CHECKLIST') == null){
                        //nothing
                     }else{

                          localStorage.setItem('AdCusDetail', JSON.stringify(customer))
                    
                    
                     }

                     var CheckoutList;
                     let checkList = (typeof localStorage.getItem('CHECKLIST') !=='undefined') ? JSON.parse(localStorage.getItem('CHECKLIST')):null;
                    if(checkList!==null){
                       let orderCustomerInfo = customer && customer.Content;
                       let customerDetail  = []
                       if(status !== 'create'){
 
                             customerDetail = {
                                
                                Content: {
                                    AccountBalance: orderCustomerInfo ? orderCustomerInfo.AccountBalance : 0,
                                    City: orderCustomerInfo ? orderCustomerInfo.City : '',
                                    Email: orderCustomerInfo ? orderCustomerInfo.Email : '',
                                    FirstName: orderCustomerInfo ? orderCustomerInfo.FirstName : '',
                                    Id: orderCustomerInfo ? orderCustomerInfo.Id : '',
                                    LastName: orderCustomerInfo ? orderCustomerInfo.LastName : '',
                                    Notes: orderCustomerInfo ? orderCustomerInfo.notes : '',
                                    Phone: orderCustomerInfo ? orderCustomerInfo.Contact : 0,
                                    Pin: orderCustomerInfo ? orderCustomerInfo.Pin : '',
                                    Pincode: orderCustomerInfo ? orderCustomerInfo.Pincode : '',
                                    StoreCredit: orderCustomerInfo ? orderCustomerInfo.store_credit : 0,
                                    StreetAddress: orderCustomerInfo ? orderCustomerInfo.StreetAddress : '',
                                    //UID: orderCustomerInfo ? orderCustomerInfo.UID : 0
                                }
                             }
                         }
                        CheckoutList = {
                            ListItem: checkList.ListItem,
                            customerDetail: customerDetail,
                            totalPrice: checkList.totalPrice,
                            discountCalculated: checkList.discountCalculated,
                            tax: checkList.tax,
                            subTotal: checkList.subTotal,
                            TaxId: checkList.TaxId,
                            status: checkList.status,
                            order_id: checkList.order_id,
                            order_date: checkList.order_date
                        }
                        //console.log("CheckoutList123",CheckoutList)

                        //setTimeout(function() { 
                        localStorage.setItem("CHECKLIST", JSON.stringify(CheckoutList))
                        //  },500)
                    }
                    setTimeout(function () {
                       // history.go()
                        location.reload();
                        dispatch(alertActions.success('save custmoer detail successfully'));
                    }, 500)
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    };

    function request() { return { type: customerConstants.INSERT_REQUEST } }
    function success(customer) { return { type: customerConstants.INSERT_SUCCESS, customer } }
    function failure(error) { return { type: customerConstants.INSERT_FAILURE, error } }
}

function Delete(ID, UID) {
    return dispatch => {
        dispatch(request());

        customerService.Delete(ID, UID)
            .then(
                customer => {
                    console.log("customerdelete",customer);
                    localStorage.removeItem("CUSTOMER_ID");
                    localStorage.removeItem("AdCusDetail")
                   getDetail(null,UID);
                   setTimeout(function () {
                    // history.go()
                     location.reload();
                     dispatch(success());
                    
                    
                    }, 2000)  
                   
                   // window.location='/CustomerView';

                },
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: customerConstants.DELETE_REQUEST } }
    function success(customer) { return { type: customerConstants.DELETE_SUCCESS, customer } }
    function failure(error) { return { type: customerConstants.DELETE_FAILURE, error } }
}

////client Side
// function filteredList(filteredList){
//     return dispatch => {
//          dispatch(success(filteredList))
//          };

//        function success(filteredList) { return { type: customerConstants.GET_FILTER_SUCCESS, filteredList } }

// }

////--server Side-----------
function filteredList(udid, pagesize, filtere) {
    return dispatch => {
        dispatch(request());

        customerService.filteredList(udid, pagesize, filtere)
            .then(
                filteredList => {
                    dispatch(success(filteredList));
                },
                error => {
                    dispatch(failure(error.toString()));
                });
    }


    function success(filteredList) { return { type: customerConstants.GET_FILTER_SUCCESS, filteredList } }
    function request() { return { type: customerConstants.GET_FILTER_REQUEST } }
    function failure(error) { return { type: customerConstants.GET_FILTER_FAILER, error } }
}

//-------------------

function removeStoreCredit(UDID, Id, amount) {
    return dispatch => {
        dispatch(request(UDID, Id, amount));

        customerService.removeStoreCredit(UDID, Id, amount)
            .then(
                remove_customer => {
                    dispatch(success(remove_customer));
                    //window.localtion = '/checkout';
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request() { return { type: customerConstants.REMOVE_STORE_CREDIT_REQUEST } }
    function success(remove_customer) { return { type: customerConstants.REMOVE_STORE_CREDIT_SUCCESS, remove_customer } }
    function failure(error) { return { type: customerConstants.REMOVE_STORE_CREDIT_FAILURE, error } }
}


