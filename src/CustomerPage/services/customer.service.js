import Config from '../../Config'
import { encode_UDid, get_UDid } from '../../ALL_localstorage'

export const customerService = {
    getAll,
    getPage,
    save,
    getDetail,
    Delete,
    removeStoreCredit,
    filteredList,
   getCountryList,
   getStateList
};

const API_URL=Config.key.OP_API_URL

function getPage(uid,pageSize,pageNumber) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
     };    
    return fetch(`${API_URL}/ShopCustomer/GetPage?Udid=${uid}&&pageSize=${pageSize}&pageNumber=${pageNumber}`, requestOptions).then(handleResponse)
    .then(result => {
        //console.log("result" , result)
       
        //console.log("Detail", getDetail(result.Content.Records[0].Id,uid));
        let new_list = result && result.Content;
       // console.log("action.customerlist" , new_list.Records[0].Id)
        localStorage.setItem("CUSTOMER_ID", new_list.Records[0].Id)
       return result; 
    });
}

function getAll(uid) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
     };
    return fetch(`${API_URL}/ShopCustomer/GetAll?Udid=${uid}`, requestOptions).then(handleResponse);

}
    function getCountryList(){
        var UID = get_UDid('UDID');
 
        const requestOptions = {
            method: 'GET',
            headers: {
                "access-control-allow-origin": "*",
                "access-control-allow-credentials": "true", 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
            } 
            ,mode: 'cors'
         };
        return fetch(`${API_URL}/ShopData/GetCountries?udid=${UID}`, requestOptions).then(handleResponse)
        .then(countrylist => {
            console.log("countryList" , countrylist)
    
            return countrylist;
        })
    }
  function getStateList(){
    var UID = get_UDid('UDID');
 
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
     };
    return fetch(`${API_URL}/ShopData/GetStates?udid=${UID}`, requestOptions).then(handleResponse)
    .then(statelist => {
        console.log("statelist" , statelist)

        return statelist;
    })



   }

function getDetail(id,uid) {
  
    console.log("service",id,uid);
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
     };
    return fetch(`${API_URL}/ShopCustomer/GetDetail?Udid=${uid}&id=${id}`, requestOptions).then(handleResponse)
    .then(singleList => {
        console.log("singleList" , singleList)

        return singleList;
    })
}

function save(customer) {
    const requestOptions = {
        method: 'POST',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors',
        body: JSON.stringify(customer)
    };

    return fetch(`${API_URL}/ShopCustomer/Save`, requestOptions).then(handleResponse);
}

function filteredList(udid,pageSize,filtervalue) {
    const requestOptions = {
        method: 'POST',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors',
        body: JSON.stringify({ "UDID":udid,
                "pageSize":pageSize,
                "pageNumber":1,
                "isSearch" : true,
                "searchVal" :filtervalue,
                })
    };
console.log('filterValue',filtervalue,udid)
    return fetch(`${API_URL}/ShopCustomer/GetPage`, requestOptions).then(handleResponse);
}

function Delete(id,uid) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
     };
    return fetch(`${API_URL}/ShopCustomer/Delete?Udid=${uid}&id=${id}`, requestOptions).then(handleResponse);
}

function removeStoreCredit(uid,id,amount){
    const requestOptions = {
        method: 'GET',
     };
    return fetch(`${API_URL}/ShopCustomer/RemoveFromStoreCredit?Udid=${uid}&Id=${id}&point=${amount}`, requestOptions).then(handleResponse);
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}