import React from 'react';
import { history } from '../../_helpers';
import { activityActions } from '../../ActivityPage/actions/activity.action';
import moment from 'moment';
import Pagination from './Pagination';
import Config from '../../Config'
function ActivityPage(props, id, uid) {
    localStorage.setItem("CUSTOMER_TO_ACTVITY",id);
    // props.dispatch(activityActions.getDetail(id, uid));
    //history.push('/activity');
    window.location = '/activity';

}


export const CustomerViewTable = (props) => {
    return (
        <table className="table customerViewShortingTable layout-fixed">
            <tbody>
                {
                    (typeof props.details !== "undefined" && props.details.length > 0) ? (
                        props.pageOfItems && props.pageOfItems.map((item, index) =>

                            <tr key={index} onClick={() => ActivityPage(props.Props, item.Id, props.UDID)}>
                                <td className="status_shorting">{moment(item.OrderTime).format(Config.key.DATE_FORMAT)}</td>
                                <td className="status_shorting">{item.Status}</td>
                                <td className="status_shorting">{item.Location}</td>
                                <td className="amount_shorting">{item.Amount}</td>
                                <td><img src="assets/img/next.png" className="width14" /></td>
                            </tr>
                        )
                    ) : (
                            <tr>
                                <td style={{ borderBottom: 0, paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, textAlign: 'unset' }} >
                                    <p className="alert text-white" style={{ textAlign: "center" }}>No activity found</p>
                                </td>
                            </tr>
                        )
                }
                {
                    (typeof props.details !== "undefined" && props.details.length > 0) ? (
                        <tr>
                            <th colSpan="5" className="bgcolor1 p-0">
                                <Pagination items={props.details} onChangePage={props.onChangePage} />
                            </th>
                        </tr>
                    ) : <tr><td></td></tr>
                }
            </tbody>
        </table>




    )

}
