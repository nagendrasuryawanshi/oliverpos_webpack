import React from 'react';


export const CustomerViewFirst = (props) => {

    return (
        <tr className={props.className} onClick={props.onClick} id={props.FirstName}>
            <td>
                
                {props.FirstName?props.FirstName:props.Email}{" "}{props.LastName?props.LastName:''}
                <span className="comman_subtitle color_black">{props.PhoneNumber}</span>
            </td>
            <td>
                <a data-toggle="modal" href="#tallModal">
                    <img src="assets/img/next.png" />
                </a>
            </td>
        </tr>
    );
}
