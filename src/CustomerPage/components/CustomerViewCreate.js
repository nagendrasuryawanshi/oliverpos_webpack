import React from 'react';
import { Markup } from 'interweave';

export const unescapeHTML=(html) =>   {

        var escapeEl = document.createElement('textarea');
        escapeEl.innerHTML = html;
        return escapeEl.textContent;
    }

export const CustomerViewCreate = (props) =>{
    console.log("CustomerViewCreate",props);

    return(
        <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                    <img src="assets/img/delete-icon.png" />
                </button>
                <h4 className="modal-title">Add New Customer</h4>
            </div>
            <div className="modal-body overflowscroll" id="scroll_mdl_body">
                <form name="form" className="clearfix form_editinfo">
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_fname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    First Name
                               </div>
                                <input className="form-control" name="cust_fname" value={props.cust_fname} placeholder="First Name" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_fname &&
                                <div className="help-block">First Name is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_lname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Last Name
                                 </div>
                                <input className="form-control" name="cust_lname" value={props.cust_lname} placeholder="Last Name" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_lname &&
                                <div className="help-block">Last Name is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_email ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Email
                                 </div>
                                <input className="form-control" name="cust_email" value={props.cust_email} placeholder="Email" type="email" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_email &&
                                 <div className="help-block">{props.emailValid == false ? 'email is required':' '}</div>
                               }
                           {props.emailValid == 'email is Invalid' &&
                                 <div className="help-block" style={{color:'#a94442'}}>{props.emailValid}</div>
                               }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_phone_number ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Phone Number
                                 </div>
                                <input maxLength={10} className="form-control" name="cust_phone_number" value={props.cust_phone_number} placeholder="Phone Number" type="tel" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_phone_number &&
                                <div className="help-block">Phone Number is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.cust_street_address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                Address Line 1&nbsp;
                                  </div>
                                <input className="form-control" name="cust_street_address" value={props.cust_street_address} placeholder="Address Line 1" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.Street_Address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Address Line 2&nbsp;
                                  </div>
                                <input className="form-control" name="Street_Address" value={props.Street_Address} placeholder="Address Line 2" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                   
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.cust_street_address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                Country&nbsp;
                                  </div>
                                  <select className="form-control"  name="country"
                                       // placeholder="Select"
                                     //  id={this.props.id}
                                       // value={this.state.name?this.state.name :this.props.displayvalue}
                                       onChange={props.onChangeList}
                                      >        
                                        <option value=''>select</option>  
                                        {props.getCountryList && props.getCountryList.map((item,index) =>
                                         {
                                          

                                            return ( 
                                            <option key={index} value={item.Code}>
                                            {/* {item.Name} */}
                                            {item.Name.replace(/[^a-zA-Z]/g, '')}
                                            
                                           {/* <Markup  noHtml='true' content={item.Name}/> */}
                                            
                                             </option>
                                          
                                          )})} 
                                         {/* <option value="Stading" >Stading</option>
                                         <option value="Shitting">Shitting</option>
                                        <option value="1 Year or 6 Month below">1 Year or 6 Month below</option> */}
                                    </select>     
                               </div>
                            {/* {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.cust_street_address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                State&nbsp;
                                  </div>
                                  <select className="form-control"   name="state"  disabled={!props.getState}
                                       // placeholder="Select"
                                     //  id={this.props.id}
                                       // value={this.state.name?this.state.name :this.props.displayvalue}

                                           onChange={props.onChangeStateList}
                                      >        
                                        <option value=''>select</option>  
                                        {props.getState && props.getState.map((item,index) =>
                                         {
                                            // console.log("item",item.name);
                                            return ( 
                                            <option key={index} value={item.Code} >
                                            
                                            {item.Name.replace(/[^a-zA-Z]/g, '')}
                                             </option>
                                          
                                          )})} 
                                         {/* <option value="Stading" >Stading</option>
                                         <option value="Shitting">Shitting</option>
                                        <option value="1 Year or 6 Month below">1 Year or 6 Month below</option> */}
                                    </select>     
                               </div>
                            {/* {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_city ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    City&nbsp;&nbsp;
                                   </div>
                                <input className="form-control" name="cust_city" value={props.cust_city} placeholder="City" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_city &&
                                <div className="help-block">City Name is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_postcode ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                ZIP/Postal Code
                                 </div>
                                <input className="form-control" name="cust_postcode" value={props.cust_postcode} placeholder="Post Code" type="text" onChange={props.onChange} />
                            </div>
                            {props.custmerPin == 'not accept' &&
                                <div className="help-block" style={{color:'#a94442'}}>not accept 0</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.cust_note ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Notes
                                 </div>
                                <textarea className="form-control" name="cust_note" type="text" value={props.cust_note} placeholder="Notes" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_note &&
                                <div className="help-block">Notes is required</div>
                            } */}
                        </div>
                    </div>
                </form>
            </div>
            <div className="modal-footer p-0">
                <button onClick={props.onClick} type="button" className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
            </div>
        </div>
    </div>
    )
}


