import React from 'react';

export const CustomerViewEdit = (props) => {
    console.log("CustomerProps",props)
 //   console.log("CustomerProps.country.country_name",props.country_name);

return(
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                    <img src="assets/img/delete-icon.png" />
                </button>
                <h4 className="modal-title">Edit Information</h4>
            </div>
            <div className="modal-body overflowscroll" id="scroll_mdl_body">
                <form className="clearfix form_editinfo">
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_fname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    First Name
                                               </div>
                                <input className="form-control" value={props.user_fname} id="user_fname" name="user_fname" type="text" placeholder="First Name" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.user_fname &&
                                <div className="help-block">First Name is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_lname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Last Name
                                                </div>
                                <input className="form-control" value={props.user_lname} id="user_lname" name="user_lname" type="text" placeholder="Last Name" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.user_lname &&
                                <div className="help-block">Last Name is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_email ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Email
                                                 </div>
                                <input className="form-control" value={props.user_email} id="user_email" placeholder="Email"  name="user_email" type="text" disabled />
                            </div>
                            {props.submitted && !props.user_email &&
                                <div className="help-block">Email is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_contact ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Phone Number
                                                </div>
                                <input maxLength={10} className="form-control" value={props.user_contact} id="user_contact"  placeholder="Phone Number" name="user_contact" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.user_contact &&
                                <div className="help-block">Contact Number is required</div>
                            } */}
                        </div>
                    </div>
                  
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.user_address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                Address Line 1&nbsp;
                                                </div>
                                <input className="form-control" value={props.user_address} id="user_address" name="user_address" placeholder="Address Line 1" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.user_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.Street_Address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Address Line 2&nbsp;
                                  </div>
                                <input className="form-control" name="Street_Address" value={props.Street_Address}  placeholder="Address Line 2" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.country_name ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                Country&nbsp;
                                  </div>
                                  <select className="form-control"  name="country"
                                       // placeholder="Select"
                                     //  id={this.props.id}
                                     value={props.country_name?props.country_name.replace(/[^a-zA-Z]/g, ''):''}
                                      onChange={props.onChangeList}
                                >        
                                        <option value=''>select</option>  
                                        {props.getCountryList && props.getCountryList.map((item,index) =>
                                         {
                                           //  console.log("item",props.country_name);
                                            return ( 
                                            <option key={index} value={item.Code}>
                                            {item.Name.replace(/[^a-zA-Z]/g, '')}
                                             </option>
                                          
                                          )})} 
                                         {/* <option value="Stading" >Stading</option>
                                         <option value="Shitting">Shitting</option>
                                        <option value="1 Year or 6 Month below">1 Year or 6 Month below</option> */}
                                    </select>     
                               </div>
                            {/* {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.cust_street_address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                State&nbsp;
                                  </div>
                                  <select className="form-control"   name="state"
                                        //placeholder="Select"
                                     //  id={this.props.id}
                                       // value={props.getState ==''?props.state_name.Code :props.edit_state_name ?props.edit_state_name:''}

                                           onChange={props.onChangeStateList}
                                      >        
                                        <option value="">{props.getState !==''?'select':props.state_name.replace(/[^a-zA-Z]/g, '')}</option>  
                                        {props.getState && props.getState.map((item,index) =>
                                         {
                                            // console.log("item",item.name);
                                            return ( 
                                            <option key={index} value={item.Code} >
                                           {item.Name.replace(/[^a-zA-Z]/g, '')}
                                             </option>
                                          
                                          )})} 
                                         {/* <option value="Stading" >Stading</option>
                                         <option value="Shitting">Shitting</option>
                                        <option value="1 Year or 6 Month below">1 Year or 6 Month below</option> */}
                                    </select>     
                               </div>
                            {/* {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_city ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    City&nbsp;&nbsp;
                                                 </div>
                                <input className="form-control" value={props.user_city} id="user_city" placeholder="City" name="user_city" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.user_city &&
                                <div className="help-block">City Name is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_pcode ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                ZIP/Postal Code
                                                </div>
                                <input className="form-control" value={props.user_pcode} id="user_pcode" name="user_pcode" placeholder="ZIP/Postal Code" type="text" onChange={props.onChange} />
                            </div>
                            {/* {props.submitted && !props.user_pcode &&
                                <div className="help-block">Post Code is required</div>
                            } */}
                        </div>
                    </div>
                    <div className="col-sm-12">
                        {/* <div className={'form-group' + (submitted && !user_note ? ' has-error' : '')}> */}
                        <div className='form-group'>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Notes
                                                </div>
                                <textarea className="form-control" name="user_note" type="text" value={props.user_note}  placeholder="Notes" onChange={props.onChange}>{props.user_note}</textarea>
                            </div>
                            {/* {submitted && !user_note &&
                                                <div className="help-block">Notes is required</div>
                                            } */}
                        </div>
                    </div>
                </form>
            </div>
            <div className="modal-footer p-0">
                <button type="button" onClick={props.onClick} className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
            </div>
        </div>
    </div>
)
}