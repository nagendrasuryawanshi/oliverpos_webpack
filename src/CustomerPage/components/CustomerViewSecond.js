import React from 'react';
import { CustomerViewTable } from './CustomerViewTable';

function handleChange(e) {
}
  
export const CustomerViewSecond = (props) => {
       console.log("CustomerViewSecond",props,"props.Props",props.Props.single_cutomer_list); 
        var full_address=props && props.Props && props.Props.single_cutomer_list ?props.Props.single_cutomer_list.Content:''
        var stateName=props.state_name?props.state_name.replace(/[^a-zA-Z]/g, ''):''
        var countryName=props.selectedCountryName?props.selectedCountryName:''
       console.log("full_address",full_address,"props",props,props && full_address ? full_address.StreetAddress+'pr' +' '+ full_address.StreetAddress2+'pri' +' '+  full_address.City+'priy' 
                       +' '+ full_address.Pincode+'priya' +' '+stateName +' '+ countryName: props.Address); 
        return (
            <div className="col-sm-9 col-xs-7 pt-4">
                <div className="panel panel-custmer">
                    <div className="panel-default bg-white">
                        <div className="panel-heading text-center customer_name font18 visible2 panel-heading_xs">
                             {props.FirstName?props.FirstName:'Customer'}{" "}{props.LastName?props.LastName:'Name'} <a data-toggle="collapse" data-target="#view" className="edit-information edit-information-overright  visible-xs visible-sm pointer collapsed" style={{ width: 220 }} aria-expanded="false">More Details</a>
                        </div>
                        <div className="panel-body customer_history p-0 bg-white">
                            <div className="col-sm-12">
                                <div className="row">
                                    <div className="col-sm-12 col-lg-8 plr-8">
                                        <div className="customer-detail visible-xs visible-sm">
                                            <div id="view" className="collapse">
                                                <div className="form-group mb-2">
                                                    <div className="col-sm-6 pl-0 mb-2 plr-8">
                                                        <label htmlFor="">Phone:</label>
                                                        <div className="col-sm-12 p-0">
                                                            <p>{props.PhoneNumber}</p>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6 pl-0 mb-2 plr-8">
                                                        <label htmlFor="">Email:</label>
                                                        <div className="col-sm-12 p-0">
                                                            <p>{props.Email}</p>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                                <div className="form-group clearfix mb-2">
                                                    <div className="col-sm-6 pl-0 mb-3 plr-8">
                                                        <label htmlFor="">Address:</label>
                                                        <div className="col-sm-12 p-0">
                                                            <p>{props.Address}</p>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6 pl-0 mb-2 plr-8">
                                                        <label htmlFor="">Notes:</label>
                                                        <div className="col-sm-12 p-0">
                                                            <p>{props.Notes}</p>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <form className="customer-detail hidden-sm hidden-xs">
                                            <div className="form-group clearfix mb-2 A_B_height">
                                                <div className="col-sm-4 pl-0 mb-2 plr-8">
                                                    <label htmlFor="">Phone:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p>{props.PhoneNumber}</p>
                                                    </div>
                                                </div>
                                                <div className="col-sm-4 pl-0 mb-2 plr-8">
                                                    <label htmlFor="">Email:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p style={{textTransform:'none'}}>{props.Email}</p>
                                                    </div>
                                                </div>
                                                <div className="col-sm-4 pl-0 mb-2 plr-8">
                                                    <label htmlFor="">Account Balance:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p>{props.AccountBalance==""?0:props.AccountBalance}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group clearfix mb-2 S_C_height">
                                                <div className="col-sm-4 pl-0 mb-2 plr-8">
                                                    <label htmlFor="">Address:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p>{props && full_address ?full_address.StreetAddress  +' '+ full_address.StreetAddress2 +' '+  full_address.City 
                                                         +' '+ full_address.Pincode +' '+stateName +' '+ countryName: props.Address}</p>
                                                    </div>
                                                </div>
                                                <div className="col-sm-4 pl-0 mb-2 plr-8">
                                                    <label htmlFor="">Notes:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p>{props.Notes}</p>
                                                    </div>
                                                </div>
                                                <div className="col-sm-4 pl-0 mb-2 plr-8">
                                                    <label htmlFor="">Store Credit:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p>{props.StoreCredit==""?0 :props.StoreCredit}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    {/* <div className="col-xs-6 col-sm-7 col-lg-3 plr-8">
                                        <form className="customer-acc_details">
                                            <div className="form-group clearfix mb-3" id="A_B_height">
                                                <label htmlFor="">Account Balance:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p>{props.AccountBalance}</p>
                                                </div>

                                            </div>
                                            <div className="form-group clearfix mb-2" id="S_C_height">
                                                <label htmlFor="">Store Credit:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p>{props.StoreCredit}</p>
                                                </div>

                                            </div>
                                        </form>
                                    </div> */}
                                    <div className="col-xs-6 col-sm-5 col-lg-4 bl-1 plr-8">
                                        <form action="#" className="w-250-px pl-3">
                                            <div className="w-100-block button_with_checkbox" onClick={props.onClick}>
                                                <input type="radio" id="test1" name="radio-group" checked="" onChange={handleChange.bind(this)} />
                                                <label htmlFor="test1" className="label_select_button">Add Customer <span className="hide_small">To Sale</span></label>
                                            </div>
                                            <div className="w-100-block text-center mt-1 mb-3 notes">
                                                <div className="line-copy"></div>
                                            </div>
                                            <div className="w-100-block button_with_checkbox">
                                                <input type="radio" id="test2" name="radio-group" data-toggle="modal" href="#edit-information"  onChange={handleChange.bind(this)} />
                                                <label htmlFor="test2" className="label_select_button">Edit <span className="hide_small">Details</span></label>
                                            </div>
                                            {/* onClick={()=>props.ID?DeleteCustomer(props.ID,props.Props,props.UDID):''} */}
                                            <div className="w-100-block button_with_checkbox">
                                                <input type="radio" id="test3" name="radio-group"  data-toggle="modal" href="#delete-information"  onChange={handleChange.bind(this)}/>
                                                <label htmlFor="test3" className="label_select_button">Delete <span className="hide_small">Customer</span></label>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer bd-0 p-0 bg-white">
                        <div className="table_head">
                            <table className="table customerViewShortingTable layout-fixed">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th className="status_shorting">Status</th>
                                        <th className="status_shorting">Location</th>
                                        <th className="amount_shorting">Amount</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div className="overflowscroll window-header-cname_chistory">
                        <CustomerViewTable pageOfItems ={props.pageOfItems} details={props.Details?props.Details:[]} Props={props.Props}  onChangePage={props.onChangePage} UDID={props.UDID}/>
                        {/* <table className="table customerViewShortingTable layout-fixed">
                                  <tbody>
                                    {props.pageOfItems && props.pageOfItems.map((item,index) =>
                                   
                                      <CustomerViewTable
                                       Key={index}
                                       Props={props.Props}  
                                       Date={item.OrderTime}
                                       Status={item.Status}
                                       Location={item.Location}
                                       Amount={item.Amount}
                                       UDID={props.UDID}
                                       ID={item.Id}
                                     />
                                      
                                    
                                    )}
                                  
                                     {props.Details && !props.Details.length > 0 ?( 
                                     <p className="alert alert-danger text-white">Order List Not Available</p>
                                     )
                                     :
                                     <tr><th colSpan="5" className="bgcolor1 p-0"><Pagination  items={props.Details?props.Details:[]} onChangePage={props.onChangePage} /></th></tr>
                                   
                                    }
                                </tbody>
                            </table> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    
}

