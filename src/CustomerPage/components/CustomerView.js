import React from 'react';
import { connect } from 'react-redux';
import { NavbarPage, CommonHeaderTwo, CommonHeaderThree } from '../../_components';
import { CustomerViewFirst } from './CustomerViewFirst';
import { CustomerViewSecond } from './CustomerViewSecond';
import { customerActions } from '../actions/customer.action';
import { CustomerViewEdit } from './CustomerViewEdit';
import { CustomerViewCreate } from './CustomerViewCreate';
import { LoadingModal } from '../../_components';
import { history } from '../../_helpers';
import { browserHistory } from 'react-router'
import { EmailAlreadyexistmodal } from './EmailAlreadyexistmodal';
import Config from '../../Config'
import { encode_UDid, get_UDid } from '../../ALL_localstorage'

class CustomerView extends React.Component {

    constructor(props) {
        super(props);
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID = get_UDid('UDID');
        this.state = {
            cust_fname: '',
            cust_lname: '',
            submitted: false,
            cust_email: '',
            cust_phone_number: '',
            cust_street_address: '',
            cust_city: '',
            cust_postcode: '',
            cust_note: '',
            user_address: '',
            user_city: '',
            user_contact: '',
            user_email: '',
            user_fname: '',
            user_lname: '',
            user_pcode: '',
            user_note: '',
            active: history.location.state ? 0 : '',
            activeFilter: false,
            UDID: UID,
            Cust_ID: '',
            user_store_credit: '',
            pageOfItems: [],
            search: '',
            backUrl: history.location.state,
            emailValid: false,
            loading: false,
            //-----Customer paging----------
            TotalRecords: 0,
            PageSize: Config.key.CUSTOMER_PAGE_SIZE,
            PageNumber: 0,
            isSearch: false,
            customerList: [],
            ID: '',
            FirstName: '',
            LastName: '',
            Email: '',
            PhoneNumber: '',
            Address: '',
            Notes: '',
            StoreCredit: '',
            AccountBalance: '',
            Details: '',
            onClick: '',
            singleStatus: true,
            custmerPin:'',
            getCountryList:localStorage.getItem('countrylist')?JSON.parse(localStorage.getItem('countrylist')):'',
            getStateList:localStorage.getItem('statelist')?JSON.parse(localStorage.getItem('statelist')):'',
            state_name:'',
            country_name:'',
            Street_Address:''
            //localStorage.getItem('countrylist')?JSON.parse(localStorage.getItem('countrylist')):''
            





            //-----------------------

        };
        this.handleChange = this.handleChange.bind(this);
        this.onChangeStateList=this.onChangeStateList.bind(this);
        this.handleChangeList=this.handleChangeList.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.filterCustomer = this.filterCustomer.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.loadMore(1);
        
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    getExistingCustomerEmail(email) {
        var Exist = false;
        this.state.customerList && this.state.customerList.map(cust => {
            if (cust.Email == email)
                Exist = true;
        })
        return Exist;
    }

    handleChange(e) {
        let emailValid = this.state.emailValid;
        let custmerPin =this.state.custmerPin;

        const { name, value } = e.target;
        console.log("handleChange",name , value,value[0]);
        let pin
        switch (name) {             
            case 'cust_email':
                emailValid = value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
            case 'cust_postcode':           
            custmerPin=value[0];
            pin=value.match(/^([1-9]|10)$/)                 
            default:
                break;
        }
        if (emailValid && emailValid.length == 4) {
            this.setState({ emailValid: '' })
        } else if (emailValid == null) {
            this.setState({ emailValid: 'email is Invalid' })

        }
        if(custmerPin && custmerPin < 1 ){
            console.log("custmerPin",custmerPin)
         this.setState({custmerPin:'not accept'})
        }else if(pin || pin == '' ) {
            this.setState({custmerPin:''})
        }else if( custmerPin == null || custmerPin == ' ' ){
            console.log("custmerPin1",custmerPin)

            this.setState({custmerPin:''})

        }

        this.setState({ [name]: value });
    }



    handleSubmit() {
        this.setState({ submitted: true,
            loading:false 
        });
        const { cust_city, cust_email, cust_fname, cust_lname,
            cust_note, cust_phone_number, cust_postcode, cust_street_address,
            UDID,
            user_address, user_city, user_contact, user_email, user_fname,
            user_lname, user_pcode, user_note, Cust_ID, user_store_credit, emailValid,Street_Address,country_name,state_name,
        } = this.state;

        const { dispatch } = this.props;
        if (!(Cust_ID) || Cust_ID == null || Cust_ID == "") {
            var userExist = false;
            userExist = this.getExistingCustomerEmail(cust_email)
            if (userExist == true) {
                $('#emailalreadyexist').modal('show')

            }
            else {
                if (
                    cust_email && cust_email !== "" && emailValid == ''
                ) {
                 //   console.log("Street_Address",Street_Address,"cust_street_address",cust_street_address)
   
                    const save = {
                        Id: "",
                        FirstName: cust_fname,
                        LastName: cust_lname,
                        Contact: cust_phone_number,
                        startAmount: 0,
                        Email: cust_email,
                        udid: UDID,
                        notes: cust_note,
                        StreetAddress: cust_street_address,
                        Pincode: cust_postcode,
                        City: cust_city,
                        Country :country_name,
                        State:state_name,
                        StreetAddress2:Street_Address,
                       


                    }
                    //console.log("save",save)
                    dispatch(customerActions.save(save, 'create'));
                    $(".close").click();
                }
            }
        }
        else if (Cust_ID && Cust_ID != "") {
             console.log("user_address",user_address,"Street_Address",Street_Address,"Country",country_name,"state_name",state_name)

            const update = {
                Id: Cust_ID,
                FirstName: user_fname,
                LastName: user_lname,
                Contact: user_contact,
                startAmount: 0,
                Email: user_email,
                udid: UDID,
                notes: user_note,
                StreetAddress: typeof user_address == 'undefined'?'':user_address,
                Pincode: user_pcode,
                City: user_city,
                Country :country_name,
                State:state_name,
                StreetAddress2:Street_Address,
            }
          //  console.log("update",update)

           dispatch(customerActions.save(update, 'update'));

        }

      //  e.preventDefault();

    }


    componentDidMount() {
        setTimeout(function () {
            setHeightDesktop();
        }, 1000);

        //  this.props.dispatch(customerActions.getCountry())
        //  this.props.dispatch(customerActions.getState())

        let CUSTOMER_ID = localStorage.getItem("CUSTOMER_ID");
        if (typeof CUSTOMER_ID !== 'undefined' && CUSTOMER_ID !== null) {
            this.setState({ active: 0 })
            var UID = get_UDid('UDID');
            this.props.dispatch(customerActions.getDetail(CUSTOMER_ID, UID));
        }

    }

    activeClass(item, index) {
        const { dispatch } = this.props;
        var udid = get_UDid('UDID');
      //  console.log("item123",item)
        this.setState({
            active: index,
            user_address: item.CustomerAddress ? item.CustomerAddress : item.StreetAddress ? item.StreetAddress : "",
            user_contact: item.PhoneNumber ? item.PhoneNumber : item.Contact ? item.Contact : "",
            user_city: item.City ? item.City : '',
            user_email: item.Email ? item.Email : '',
            user_fname: item.FirstName ? item.FirstName : '',
            user_lname: item.LastName ? item.LastName : '',
            user_pcode: item.Pincode ? item.Pincode : '',
            user_note: item.Notes ? item.Notes : '',
            Cust_ID: item.Id ? item.Id : '',
            user_store_credit: item.store_credit ? item.store_credit : '',
            Street_Address:item.StreetAddress2?item.StreetAddress2:'',
            country_name:item.country?item.country:''
          
        })


        dispatch(customerActions.getDetail(item.Id, udid));
        this.loadMore(1);

    }

    deleteCustomer() {
        this.setState({ submitted: true,loading:false });
        const { UDID, Cust_ID } = this.state;
        const { dispatch } = this.props;
        dispatch(customerActions.Delete(Cust_ID, UDID));


    }

    goBack(single_cutomer_list) {
        if (single_cutomer_list != null && this.state.backUrl) {
            if ((typeof single_cutomer_list.Email !== 'undefined') && single_cutomer_list.Email !== null) {
                let Content = single_cutomer_list
                let data = {
                    Content
                }
                localStorage.setItem('AdCusDetail', JSON.stringify(data))
            } else {
                localStorage.setItem('AdCusDetail', JSON.stringify(single_cutomer_list))
            }
            localStorage.getItem('AdCusDetail');
            let list = ((typeof localStorage.getItem('CHECKLIST') !== 'undefined') && localStorage.getItem('CHECKLIST') !== null) ? JSON.parse(localStorage.getItem('CHECKLIST')) : null;
            if (list != null) {
                const CheckoutList = {
                    ListItem: list.ListItem,
                    customerDetail: single_cutomer_list,
                    totalPrice: list.totalPrice,
                    discountCalculated: list.discountCalculated,
                    tax: list.tax,
                    subTotal: list.subTotal,
                    TaxId: list.TaxId,
                    TaxRate: list.TaxRate,
                    showTaxStaus: list.showTaxStaus,
                }
                localStorage.setItem('CHECKLIST', JSON.stringify(CheckoutList))

            }

            window.location = this.state.backUrl;

        } else {
            if (single_cutomer_list !== null) {
                if ((typeof single_cutomer_list.Email !== 'undefined') && single_cutomer_list.Email !== null) {
                    let Content = single_cutomer_list
                    let data = {
                        Content
                    }
                    localStorage.setItem('AdCusDetail', JSON.stringify(data))
                } else {
                    localStorage.setItem('AdCusDetail', JSON.stringify(single_cutomer_list))
                }

                window.location = '/shopview';
            }
        }


        if (single_cutomer_list == null) {
            $('#edit-information').modal('show')
        }

        //  window.location.href = '/shopview'
    }

    filterCustomer(e) {
        let filtered = [];
        const { value } = e.target;
        // if(value.length>=2)
        // {
        this.setState({ search: value })
        ///////Client side search----------------------------------
        // const customerlist = this.props.customerlist && this.props.customerlist.Content;

        // customerlist.map((item) => {
        //     if (String(item.FirstName).toUpperCase().toString().indexOf(String(value).toUpperCase()) != -1 || String(item.FirstName).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1
        //         || String(item.LastName).toUpperCase().toString().indexOf(String(value).toUpperCase()) != -1 || String(item.LastName).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1
        //         || String(item.Email).toUpperCase().toString().indexOf(String(value).toUpperCase()) != -1 || String(item.Email).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1
        //         || String(item.Contact) == String(value)
        //     ) {
        //         filtered.push(item)
        //         this.setState({ activeFilter: true })
        //     }
        // })
        // if (filtered.length == 0) {
        //     this.setState({ activeFilter: false })
        // }
        //   this.props.dispatch(customerActions.filteredList(filtered))
        ///---------------------------------

        //---------Searver side search-----------------------------
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID = get_UDid('UDID');
        this.props.dispatch(customerActions.filteredList(UID, this.state.PageSize, value))
        // console.log("Filterring", this.props);
        if (filtered.length == 0) {
            this.setState({ activeFilter: false })
        }

        //------------------------------------------------
        // }
    }

    clearInput() {
        this.setState({
            search: '',
            activeFilter: false
        })

    }

    componentWillMount() {
        setTimeout(() => {
            this.setState({
                loading: true
            })

        }, 3000);

    }


    componentDidUpdate(nextProp, prevState) {
        if (typeof nextProp.single_cutomer_list == 'undefined' && prevState.Cust_ID == '') {

            if (prevState.customerList && prevState.customerList.length > 0) {
                this.setState({
                    active: 0,
                    user_address: prevState.customerList[0].StreetAddress ? prevState.customerList[0].StreetAddress : prevState.customerList[0].CustomerAddress,
                    user_city: prevState.customerList[0].City,
                    user_contact: prevState.customerList[0].Contact ? prevState.customerList[0].Contact : prevState.customerList[0].PhoneNumber,
                    user_email: prevState.customerList[0].Email,
                    user_fname: prevState.customerList[0].FirstName,
                    user_lname: prevState.customerList[0].LastName,
                    user_pcode: prevState.customerList[0].cust_pin ? prevState.customerList[0].cust_pin : prevState.customerList[0].Pincode,
                    user_note: prevState.customerList[0].Notes,
                    Cust_ID: prevState.customerList[0].Id,
                    user_store_credit: prevState.customerList[0].store_credit,
                    country_name:prevState.customerList[0].country?prevState.customerList[0].country:''


                })

            }
        }

    }


    componentWillReceiveProps(nextProp) {
         //console.log("componentwillrecieve",nextProp,"nextProp.single_cutomer_list",nextProp.single_cutomer_list);
        if (nextProp.single_cutomer_list && nextProp.single_cutomer_list !== '' && nextProp.single_cutomer_list !== 'undefined') {

            this.setState({ singleStatus: false })

        }

        if (this.state.PageNumber == 1) {
            localStorage.setItem("CustomerList", []);
        }

        if (nextProp.customerlist && nextProp.customerlist.Content && nextProp.customerlist.Content.PageNumber) {
            let CUSTOMER_ID = localStorage.getItem("CUSTOMER_ID");
            if (this.state.singleStatus == true) {

                this.props.dispatch(customerActions.getDetail(CUSTOMER_ID, this.state.UDID));
            }
            this.setState({
                PageNumber: nextProp.customerlist.Content.PageNumber,
                TotalRecords: nextProp.customerlist.Content.TotalRecords
            });
        }

        var custList = [];
        if (localStorage.getItem("CustomerList")) {
            var custListState = [];
            var custListState1 = localStorage.getItem("CustomerList");

            custListState = JSON.parse(custListState1);
            custListState.map(element => {
                var ItemExit = false;
                custList.map(item => {
                    if (item.Id == element.Id) {
                        ItemExit = true;
                    }
                })
                if (ItemExit == false)
                    custList.push(element);
            });
        }

        if (nextProp.customerlist && nextProp.customerlist.Content && nextProp.customerlist.Content.Records != 'undefined') {

            var newArrList = [];
            nextProp.customerlist.Content.Records && nextProp.customerlist.Content.Records.forEach(element => {
                var ItemExit = false;
                custList.map(item => {
                    if (item.Id == element.Id) {
                        ItemExit = true;
                    }
                })
                if (ItemExit == false)

                    custList.push(element);
            });

            this.state.customerList = custList;
            if (this.state.customerList.length > 0) {
             //   console.log("this.state.customerList",this.state.customerList);
                this.setState({
                    ID: this.state.customerList.length > 0 ? this.state.customerList[0].Id : '',
                    FirstName: this.state.customerList.length > 0 ? this.state.customerList[0].FirstName : '',
                    LastName: this.state.customerList.length > 0 ? this.state.customerList[0].LastName : '',
                    Email: this.state.customerList.length > 0 ? this.state.customerList[0].Email : '',
                    PhoneNumber: this.state.customerList.length > 0 ? this.state.customerList[0].PhoneNumber : '',
                    Address: this.state.customerList.length > 0 ? this.state.customerList[0].StreetAddress : '',
                    Notes: this.state.customerList.length > 0 ? this.state.customerList[0].Notes : '',
                    StoreCredit: this.state.customerList.length > 0 ? this.state.customerList[0].StoreCredit : '',
                    AccountBalance: this.state.customerList.length > 0 ? this.state.customerList[0].AccountBalance : '',
                    Details: nextProp.single_cutomer_list ? nextProp.single_cutomer_list.Content.orders : [],
                    Street_Address:this.state.customerList.length > 0 ?this.state.customerList[0].StreetAddress2:'',
                    country_name:nextProp.single_cutomer_list ?nextProp.single_cutomer_list.Content.Country:''

                    


                })
                var statename=nextProp.single_cutomer_list ?nextProp.single_cutomer_list.Content.State:''
                this.getCountryAndStateName(statename,this.state.country_name)

            } else {
                this.setState({
                    ID: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Id : this.state.customerList.length > 0 ? this.state.customerList[0].Id : '',
                    FirstName: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.FirstName : this.state.customerList.length > 0 ? this.state.customerList[0].FirstName : '',
                    LastName: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.LastName : this.state.customerList.length > 0 ? this.state.customerList[0].LastName : '',
                    Email: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Email : this.state.customerList.length > 0 ? this.state.customerList[0].Email : '',
                    PhoneNumber: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Phone : this.state.customerList.length > 0 ? this.state.customerList[0].PhoneNumber : '',
                    Address: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.StreetAddress : this.state.customerList.length > 0 ? this.state.customerList[0].StreetAddress : '',
                    Notes: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Notes : this.state.customerList.length > 0 ? this.state.customerList[0].Notes : '',
                    StoreCredit: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.StoreCredit : this.state.customerList.length > 0 ? this.state.customerList[0].StoreCredit : '',
                    AccountBalance: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.AccountBalance : this.state.customerList.length > 0 ? this.state.customerList[0].AccountBalance : '',
                    Details: nextProp.single_cutomer_list ? nextProp.single_cutomer_list.Content.orders : [],
                    Street_Address:(typeof nextProp.single_cutomer_list !== 'undefined') ?nextProp.single_cutomer_list.Content.StreetAddress2: this.state.customerList.length > 0 ?  this.state.customerList[0].StreetAddress2:'',
                    country_name:(typeof nextProp.single_cutomer_list !== 'undefined')?nextProp.single_cutomer_list.Content.Country:this.state.customerList.length > 0? this.state.customerList[0].Country:'',


                })
                var statename=nextProp.single_cutomer_list ?nextProp.single_cutomer_list.Content.State:''
                this.getCountryAndStateName(statename,this.state.country_name)
              

            }
        }

        localStorage.setItem("CustomerList", JSON.stringify(custList));

        if (nextProp.filteredList && nextProp.filteredList.Content && nextProp.filteredList.Content.Records != 'undefined') {
            this.setState({ activeFilter: true })
        }

        this.setState({
            ID: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Id : this.state.customerList.length > 0 ? this.state.customerList[0].Id : '',
            FirstName: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.FirstName : this.state.customerList.length > 0 ? this.state.customerList[0].FirstName : '',
            LastName: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.LastName : this.state.customerList.length > 0 ? this.state.customerList[0].LastName : '',
            Email: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Email : this.state.customerList.length > 0 ? this.state.customerList[0].Email : '',
            PhoneNumber: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Phone : this.state.customerList.length > 0 ? this.state.customerList[0].PhoneNumber : '',
            Address: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.StreetAddress : this.state.customerList.length > 0 ? this.state.customerList[0].StreetAddress : '',
            Notes: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.Notes : this.state.customerList.length > 0 ? this.state.customerList[0].Notes : '',
            StoreCredit: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.StoreCredit : this.state.customerList.length > 0 ? this.state.customerList[0].StoreCredit : '',
            AccountBalance: (typeof nextProp.single_cutomer_list !== 'undefined') ? nextProp.single_cutomer_list.Content.AccountBalance : this.state.customerList.length > 0 ? this.state.customerList[0].AccountBalance : '',
            Details: nextProp.single_cutomer_list ? nextProp.single_cutomer_list.Content.orders : [],
            Street_Address:(typeof nextProp.single_cutomer_list !== 'undefined') ?nextProp.single_cutomer_list.Content.StreetAddress2: this.state.customerList.length > 0 ?  this.state.customerList[0].StreetAddress2:'',
            country_name:(typeof nextProp.single_cutomer_list !== 'undefined')?nextProp.single_cutomer_list.Content.Country:this.state.customerList.length > 0? this.state.customerList[0].Country:'',


        })
                var statename=nextProp.single_cutomer_list ?nextProp.single_cutomer_list.Content.State:''
                this.getCountryAndStateName(statename,this.state.country_name)
    }

    createCustomer() {
        this.setState({
            active: '',
            user_address: '',
            user_city: '',
            user_contact: '',
            user_email: '',
            user_fname: '',
            user_lname: '',
            user_pcode: '',
            user_note: '',
            Cust_ID: '',
            user_store_credit: '',
            Street_Address:'',
            country_name:'',
            state_name:''
        })
        $('#create-customer').modal('show')
      //  e.preventDefault();
    }

    loadMore(number) {

        if (this.state.TotalRecords == 0 || (this.state.TotalRecords / this.state.PageSize) >= (this.state.PageNumber)
            || ((this.state.PageSize * (this.state.PageNumber + number)) > 0)  //condition for go reevious
        ) {

            this.state.PageNumber = this.state.PageNumber + number;
            var UID = get_UDid('UDID');
            this.props.dispatch(customerActions.getPage(UID, this.state.PageSize, this.state.PageNumber));


        }



    }

    getCountryAndStateName(stateCode,countryCode){
      
        var stat_name=''
        var count_name=''
        var finalStatelist=[]
        // if(this.props && this.props.single_cutomer_list !=='' && (typeof  this.props.single_cutomer_list !== 'undefined')){
            console.log("call this function",stateCode,countryCode);
            this.state.getCountryList && this.state.getCountryList.find(function(element){
             //   console.log("element12",element)
                if(element.Code ==countryCode ){
   
                       count_name=element
   
                     }  
                 })
            this.setState({selectedCountryName:count_name.Name})
          //  var select_country_code=this.props.single_cutomer_list.Content.Country
          //  var select_state_code=name
          //  this.setState({country_name:select_country_code})

            this.state.getStateList && this.state.getStateList.find(function (element) {
              //  console.log("element13",element)

                if(element.Code ==stateCode ){
                 //   console.log("element.Code1",element.Code,"select_state_code",select_state_code,"element.Name1",element.Name)    

                    stat_name=element

                // console.log("stat_name",stat_name);
                  }
                }
                
            )
            this.setState({state_name:stat_name.Name?stat_name.Name:stateCode})

        // }
      
      

    }

    handleChangeList(e){
        var finalStatelist=[];

        console.log("countryList124345",e.target.value,this.state.getStateList);
        this.state.getStateList && this.state.getStateList.find(function (element) {
          //    console.log("element",element)
            if(element.Country == e.target.value ){
                finalStatelist.push(element)
              }
            }
            
        )
     this.setState({viewStateList:finalStatelist,country_name:e.target.value})      
//console.log("finalStatelist",finalStatelist)

    }
    onChangeStateList(e){
        console.log("stateinfo",e.target.value)
    this.setState({state_name:e.target.value,
  // edit_state_name:e.target.value,     
    })

    }
    render() {
       // console.log("")
      // console.log("this.state",this.state.state_name);

        const customerlist = this.state.customerList;
        const { single_cutomer_list, filteredList } = this.props;
        const {
            cust_city,
            cust_email,
            cust_fname,
            cust_lname,
            cust_note,
            cust_phone_number,
            cust_postcode,
            cust_street_address,
            submitted,
            user_address,
            user_city,
            user_contact,
            user_email,
            user_fname,
            user_lname,
            user_pcode,
            user_note,
            active,
            UDID,
            Cust_ID,
            activeFilter,
            pageOfItems,
            search,
            emailValid,
            Street_Address
        } = this.state;
        return (

            <div>
                <div className="wrapper">
                    <div className="overlay"></div>
                    <NavbarPage {...this.props} />
                    <div id="content">
                        {this.state.loading == false ? !this.props.customerlist.Content && !this.props.single_cutomer_list || this.state.submitted == true? <LoadingModal /> : '' : ''}

                        {this.state.backUrl ?
                            <CommonHeaderThree />
                            : <CommonHeaderTwo {...this.props} />
                        }
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <div className="col-sm-3 col-xs-5 p-0">
                                    <div className="items">
                                        <div className="panel panel-default panel-product-list p-0 bb-0 bor-customer">
                                            <div className="searchDiv relDiv">
                                                <input type="text" onChange={this.filterCustomer} value={search} className="form-control nameSearch search_customer_input" placeholder="Search by Name, Email or Phone" id="searchUser" data-column-index="0" />
                                                <svg onClick={() => this.clearInput()} className="search_customer_input2" width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64">
                                                    <g>
                                                        <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div className="overflowscroll" id="header-search-button">
                                                <table className="table customerViewTable table-hover" id="customer-list">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="35" />
                                                    </colgroup>
                                                    <tbody>
                                                        {activeFilter == true &&

                                                            filteredList && filteredList.Content.Records.map((item, index) => {
                                                                return (
                                                                    <CustomerViewFirst
                                                                        key={index}
                                                                        onClick={() => this.activeClass(item, index)}
                                                                        FirstName={item.FirstName}
                                                                        LastName={item.LastName}
                                                                        PhoneNumber={item.Contact}
                                                                        Email={item.Email}
                                                                        className={active == index ? 'customer_active' : null}
                                                                    />
                                                                )
                                                            })}

                                                        {activeFilter == false &&
                                                            customerlist &&
                                                            customerlist.map((item, index) => {
                                                                return (
                                                                    <CustomerViewFirst
                                                                        key={index}
                                                                        onClick={() => this.activeClass(item, index)}
                                                                        FirstName={item.FirstName}
                                                                        LastName={item.LastName}
                                                                        PhoneNumber={item.Contact}
                                                                        Email={item.Email}
                                                                        className={active == index ? 'customer_active' : null}
                                                                    />
                                                                )
                                                            })}



                                                    </tbody>
                                                </table>
                                                {/* <div className="clearfix" id="divloadmore">
                                                            <div className="searchDiv relDiv">
                                                                <input className="form-control nameSearch bb-0"  id="tileback" value="Load more...     >>" readOnly={true} data-toggle="modal"  onClick={() => this.loadMore()}  /> 
                                                            </div>                                                           
                                                        </div>  */}

                                                {(this.state.TotalRecords > (this.state.PageNumber * this.state.PageSize)) && activeFilter == false ?
                                                    <div className="createnewcustomer mb-2">
                                                        <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" onClick={() => this.loadMore(1)}>Load More</button>
                                                    </div>
                                                    : <div></div>
                                                }
                                            </div>
                                            {/* <div className="createnewcustomer" >
                                                <div className="btn-group btn-group-justified">
                                                    <div className="btn-group">
                                                        
                                                        {  (this.state.PageSize * (this.state.PageNumber-1))>0  ?
                                                              <button type="button" className="btn btn-primary total_checkout bg-blue pl-0 pr-0"  onClick={() =>this.loadMore(-1)} title="Previous">
                                                                    <img  src="assets/img/left-arrow.svg" style={{width:'25px'}} />                                                           
                                                            </button> 
                                                        : <div className="btn btn-primary total_checkout bg-blue pl-0 pr-0"></div>
                                                    }
                                                    </div>
                                                    <div className="btn-group" style={{width: '11px'}}>
                                                        <button type="button" className="btn btn-primary total_checkout bg-blue ellipsis pl-0 pr-0" data-toggle="modal" onClick={()=>this.createCustomer()} >CREATE NEW CUSTOMER</button>
                                                    </div>

                                                    <div className="btn-group">
                                                    { (this.state.TotalRecords  >( this.state.PageNumber * this.state.PageSize) ) ?
                                                         <button type="button" className="btn btn-primary total_checkout bg-blue pl-0 pr-0" onClick={() => this.loadMore(1)} title="Load more">
                                                             <img  src="assets/img/right-arrow.svg" style={{width:'25px'}} />
                                                            </button> 
                                                     : <div className="btn btn-primary total_checkout bg-blue pl-0 pr-0"></div>
                                                    }
                                                       
                                                    </div>
                                                </div>
                                            </div> */}
                                            <div className="createnewcustomer" >
                                                <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" data-toggle="modal" onClick={() => this.createCustomer()}  >CREATE NEW CUSTOMER</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <CustomerViewSecond
                                    onChangePage={this.onChangePage}
                                    pageOfItems={pageOfItems}
                                    Props={this.props}
                                    UDID={UDID}
                                    ID={this.state.ID}
                                    onClick={() => { this.goBack((typeof single_cutomer_list !== 'undefined') ? single_cutomer_list : customerlist[0]) }}
                                    FirstName={this.state.FirstName}
                                    LastName={this.state.LastName}
                                    Email={this.state.Email}
                                    PhoneNumber={this.state.user_contact}
                                    Address={this.state.Address}
                                    Notes={this.state.Notes}
                                    StoreCredit={this.state.StoreCredit}
                                    AccountBalance={this.state.AccountBalance}
                                    Details={single_cutomer_list ? single_cutomer_list.Content.orders : []}
                                    state_name={this.state.state_name}
                                    selectedCountryName={this.state.selectedCountryName}
                                    
                                />
                            </div>

                        </div>
                    </div>
                </div>


                <div className="modal fade modal-wide in sidebar-minus" id="registeropenclose1" role="dialog">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header" style={{ display: 'none' }} >
                                <button type="button" className="close" data-dismiss="modal">×</button>
                                <h4 className="modal-title">Modal Header</h4>
                            </div>
                            <div className="modal-body p-0">
                                <div className="panel panel-custmer b-0">
                                    <div className="panel-default">
                                        <div className="panel-heading text-center customer_name font18 visible2">
                                            Close Register
                                        </div>
                                        <div className="panel-body customer_history p-0">
                                            <div className="col-sm-12">
                                                <div className="row">
                                                    <div className="col-lg-9 col-sm-8 plr-8">
                                                        <form className="customer-detail">
                                                            <div className="form-group">
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Register:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Register 1</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Date &amp; Time:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p className="text-blue">15. January 12:57PM</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">User:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Mike Kellerman</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-4 bl-1 plr-8">
                                                        <form action="#" className="w-100 pl-3">
                                                            <div className="w-100-block button_with_checkbox">
                                                                <input type="radio" id="p-report" name="radio-group" />
                                                                <label htmlFor="p-report" className="label_select_button">Print Report</label>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="panel-footer bt-0 p-0">
                                            <div className="table_head">
                                                <table className="table table-border table-hover borderless c_change mb-0 tbl-bb-1 tbl-layout-fixe">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                    </colgroup>
                                                    <tbody>
                                                        <tr>
                                                            <th className="bt-0 pb-0 bb-1">Item</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Expected</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Actual</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Difference</th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="">
                                                <table className="table table-borderless tbl-bb-1 font tbl-layout-fixe pt-3 pb-3">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                    </colgroup>
                                                    <tbody>
                                                        <tr>
                                                            <td>Cash In Tiller</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Debit/Credit Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gift Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Others</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div className="col-lg-offset-7 col-lg-5 col-sm-offset-6 col-sm-6 col-xs-offset-4 col-xs-8 p-0">
                                                    <table className="table table-borderless tbl-bb-1 mb-0 font pt-1 pb-1">
                                                        <tbody>
                                                            <tr>
                                                                <td className="pl-0">Total Difference</td>
                                                                <td className="text-right">10.00</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div className="col-sm-12">
                                                    <div className="register-closenote">
                                                        <textarea cols="12" rows="4" className="form-control" placeholder="Enter notes here"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">SAVE &amp; UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>

                {Cust_ID ? (
                    <div id="edit-information" className="modal modal-wide fade full_height_one">

                        <CustomerViewEdit
                            onChange={this.handleChange}
                            onClick={() => this.handleSubmit()}
                            user_address={user_address ? user_address : cust_street_address ? cust_street_address : ""}
                            user_city={user_city ? user_city : ''}
                            user_contact={user_contact ? user_contact : cust_phone_number ? cust_phone_number : ""}
                            user_fname={user_fname ? user_fname : ''}
                            user_lname={user_lname ? user_lname : ''}
                            user_email={user_email ? user_email : ''}
                            user_note={user_note ? user_note : ''}
                            user_pcode={user_pcode ? user_pcode : ''}
                            submitted={submitted}
                            getCountryList={this.state.getCountryList}
                            getState={this.state.viewStateList?this.state.viewStateList:''}
                            Street_Address={this.state.Street_Address?this.state.Street_Address:''}
                            country_name={this.state.country_name?this.state.country_name:''}
                            state_name={this.state.state_name?this.state.state_name:''}
                            onChangeList={this.handleChangeList}
                            onChangeStateList={this.onChangeStateList}
                           // edit_state_name={this.state.edit_state_name}


                        />

                    </div>
                ) :

                    <div id="edit-information" className="modal modal-wide modal-wide1 fade">
                        <div className="modal-dialog" id="dialog-midle-align">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                        <img src="assets/img/delete-icon.png" />
                                    </button>
                                    <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                                </div>
                                <div className="modal-body p-0">
                                    <h3 id="epos_error_model_message" className="popup_payment_error_msg">Please Choose Customer !</h3>
                                </div>
                                <div className="modal-footer p-0">
                                    <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>

                }


                <div id="delete-information" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Alert Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                {Cust_ID ? (
                                    <h3 id="epos_error_model_message" className="popup_payment_error_msg">Are you sure you want to delete this Customer?</h3>
                                )
                                    :
                                    <h3 id="epos_error_model_message" className="popup_payment_error_msg">Please Choose Customer !</h3>
                                }
                            </div>
                            <div className="modal-footer p-0">
                                {Cust_ID ? (
                                    <button type="button" onClick={() => this.deleteCustomer()} className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                                ) :
                                    <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div id="create-customer" className="modal modal-wide fade full_height_one">

                    <CustomerViewCreate
                        onChange={this.handleChange}
                        onClick={() => this.handleSubmit()}
                        cust_street_address={cust_street_address ? cust_street_address : ""}
                        cust_city={cust_city ? cust_city : ''}
                        cust_phone_number={cust_phone_number ? cust_phone_number : ""}
                        cust_fname={cust_fname ? cust_fname : ''}
                        cust_lname={cust_lname ? cust_lname : ''}
                        cust_email={cust_email ? cust_email : ''}
                        cust_note={cust_note ? cust_note : ''}
                        cust_postcode={cust_postcode && cust_postcode[0] != 0   ? cust_postcode : ''}
                        submitted={submitted}
                        emailValid={emailValid}
                        Cust_ID=""
                        Street_Address={Street_Address}
                        custmerPin={this.state.custmerPin}
                        getCountryList={this.state.getCountryList}
                        getState={this.state.viewStateList?this.state.viewStateList:''}
                        onChangeList={this.handleChangeList}
                        onChangeStateList={this.onChangeStateList}
                    />
                </div>
                < EmailAlreadyexistmodal />
            </div>
        )
    }
}

function mapStateToProps(state) {
    //console.log("state",state);
    const { single_cutomer_list, customerlist, filteredList } = state;

    return {
        single_cutomer_list: single_cutomer_list.items,
        customerlist: customerlist.items ? customerlist.items : customerlist,
        filteredList: filteredList.items,
    };
}

const connectedCustomerView = connect(mapStateToProps)(CustomerView);
export { connectedCustomerView as CustomerView };