import { customerConstants  } from '../constants/customer.constants';

export function customerlist(state = {}, action) {
    switch (action.type) {
        case customerConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case customerConstants.GETALL_SUCCESS:
            return {
                ...state,
                items: action.customerlist
            };
       case customerConstants.GETALL_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}
export function countrylist(state = {}, action) {
    console.log("countrylist",action);
    switch (action.type) {
        case customerConstants.GETALL_COUNTRY_REQUEST:
            return {
                loading: true
            };
        case customerConstants.GETALL__COUNTRY_SUCCESS:
            return {
                ...state,
                items: action
            };
       case customerConstants.GETALL_COUNTRY_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}
export function single_cutomer_list(state = {}, action) {
    switch (action.type) {
        case customerConstants.GET_DETAIL_REQUEST:
            return {
                loading: true
            };
        case customerConstants.GET_DETAIL_SUCCESS:
            return {
                ...state,
                items: action.single_cutomer_list
            };
        // case customerConstants.INSERT_SUCCESS:
        //     return {
        //         ...state,
        //         items: action.customer
        //     };
        // case customerConstants.INSERT_REQUEST:
        //     return {
        //         loading: true
        //     };
        // case customerConstants.INSERT_FAILURE:
        //     return {
        //         error: action.error
        //     };
        case customerConstants.GET_DETAIL_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}

export function filteredList(state = {}, action){
    switch (action.type) {
        case customerConstants.GET_FILTER_REQUEST:
            return {
                loading: true
            };
        case customerConstants.GET_FILTER_SUCCESS:
            return {
                ...state,
                items: action.filteredList
            };
        case customerConstants.GET_FILTER_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}


export function store_credit(state = {}, action){
    switch (action.type) {
        case customerConstants.REMOVE_STORE_CREDIT_REQUEST:
            return {
                loading: true
            };
        case customerConstants.REMOVE_STORE_CREDIT_SUCCESS:
            return {
                ...state,
                items: action.remove_customer
            };
        case customerConstants.REMOVE_STORE_CREDIT_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}