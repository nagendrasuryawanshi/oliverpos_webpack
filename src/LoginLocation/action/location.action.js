import { locationConstants } from "../constants/location.constants";
import { locationService } from "../services/location.service";
import { history } from '../../_helpers/history';
import {encode_UDid,get_UDid} from '../../ALL_localstorage'

export const locationActions = {
  
      getAll,
 
  };

  function getAll() {
    // let decodedString = localStorage.getItem('UDID');
    // var decod=  window.atob(decodedString);
    var UDID= get_UDid('UDID');
    console.log("checkstatus",UDID)
    return dispatch => {
      dispatch(request());
      locationService
        .getAll(UDID)
        .then(
          locations => {dispatch(success(locations))
           //If only one location is avilable then no need to select ------------------------------------------            
              console.log("UserLocations",locations);
              if(locations)
              {
                  if(locations.length===1)
                  {
                      localStorage.setItem('Location', locations[0].Id);
                      localStorage.setItem('LocationName', locations[0].Name);
                     // let getudid = localStorage.getItem('UDID');
                      // let decodedString = localStorage.getItem('UDID');
                      // var decod=  window.atob(decodedString);
                      var getudid= get_UDid('UDID');
                      localStorage.setItem(`last_login_location_id_${getudid}`, locations[0].Id);
                      localStorage.setItem(`last_login_location_name_${getudid}`, locations[0].Name);

                    history.push('/choose_registration');
                  }
              }
          //--------------------------------------------------------------------------------------------------------
          
          },
  
          error => dispatch(failure(error.toString()))
        );
    };
  
    function request() {
      return { type: locationConstants.GETALL_REQUEST };
    }
    function success(locations) {
      return { type: locationConstants.GETALL_SUCCESS, locations };
    }
    function failure(error) {
      return { type: locationConstants.GETALL_FAILURE, error };
    }
  }