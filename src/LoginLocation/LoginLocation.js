import React from 'react';
import { connect } from 'react-redux';
import { locationActions } from './action/location.action';
import { history } from "../_helpers";
import { LoadingModal } from '../_components/LoadingModal';
import Cookies from 'universal-cookie';
import { get_UDid } from '../ALL_localstorage'

const cookies = new Cookies();

class LoginLocation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            check: true,
        };


    }
    handleSubmit(item) {
        localStorage.setItem('Location', item.Id);
        localStorage.setItem('LocationName', item.Name);
        //   this.setState({ check: value })
        //  let decodedString = localStorage.getItem('UDID');
        //  var decod=  window.atob(decodedString);
        var getudid = get_UDid('UDID');
        //let getudid = localStorage.getItem('UDID');
        localStorage.setItem(`last_login_location_id_${getudid}`, item.Id);
        localStorage.setItem(`last_login_location_name_${getudid}`, item.Name);
        //Check IndexDb Exists-------------
        //checkAndCreatIndexDb();
        // this.props.dispatch(idbProductActions.createProductDB());
        //----------------------------------   
        history.push('/choose_registration');
        // history.push('/loginpin');

    }
    windowLocation(location) {
        window.location = location
    }
    componentDidMount() {
        this.props.dispatch(locationActions.getAll());
        if (this.props.locations && Array.isArray(this.props.locations)) {
            // $(".chooseregisterLinks").niceScroll({ styler: "fb", cursorcolor: "#2BB6E2", cursorwidth: '3', cursorborderradius: '10px', background: '#d5d5d5', spacebarenabled: false, cursorborder: '', scrollspeed: 60, cursorfixedheight: 70 });
        }
    }
    componentWillReceiveProps(next) {
        if (next.locations && Array.isArray(next.locations)) {
            setTimeout(function () {
                $(".chooseregisterLinks").niceScroll({ styler: "fb", cursorcolor: "#2BB6E2", cursorwidth: '3', cursorborderradius: '10px', background: '#d5d5d5', spacebarenabled: false, cursorborder: '', scrollspeed: 60, cursorfixedheight: 70 });
            }, 500)
        }
    }

    checkStatus(value) {
        this.setState({ check: value })
    }

    render() {
        var UserLocations = this.props.locations ? this.props.locations : localStorage.getItem('UserLocations');
        if (!(Array.isArray(UserLocations))) { UserLocations = null; }
        const { check } = this.state;
        return (
            <div className="bgimg-1">
                {!UserLocations ? <LoadingModal /> : ''}
                <div className="content_main_wapper">
                    <a href="/site_link" className="aonboardingbackicon">
                        <button className="button onboardingbackicon" onClick={() => this.windowLocation('/site_link')}>&nbsp;</button>&nbsp; Go Back
       </a>

                    <div className="onboarding-loginBox">
                        <div className="login-form">
                            <div className="onboarding-pg-heading">
                                <h1>Choose Your Location</h1>
                                <p>
                                    You’ve set up these locations. Choose the Location you’re working from right now. </p>
                            </div>
                            <form className="onboarding-login-field" action="#">
                                <ul className="list-group chooseregisterLinks m-b-20" style={{ height: 230 }}>

                                    {UserLocations && UserLocations.length > 0 ? UserLocations.map((item, index) => {
                                        return (
                                            <li key={index} className="list-group-item" onClick={() => this.handleSubmit(item)} >
                                                <a href="#">
                                                    <span className="wordWarp" >{item.Name}</span>
                                                    <img src="assets/img/green-arrow.png" /></a>
                                            </li>
                                        )
                                    }) : <li className="list-group-item" ><a href="#">No Location Found</a></li>
                                    }
                                </ul>
                                <div className="checkBox-custom m-b-20">
                                    <div className="checkbox">
                                        <label className="customcheckbox">Remember location
                                        <input type="checkbox" onClick={() => this.checkStatus(!check)} checked="checked" readOnly={true} />
                                            <span className={'checkmark ' + (check == true ? ' checkUncheck' : ' ')}></span>

                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="powered-by-oliver">
                        <a href="javascript:void(0)">Powered by Oliver POS</a>
                    </div>

                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { locations } = state.locations;
    return {
        locations
    };
}

const connectedLoginLocation = connect(mapStateToProps)(LoginLocation);
export { connectedLoginLocation as LoginLocation };