import Config from '../../Config'
import { get_UDid } from '../../ALL_localstorage'

export const checkoutService = {
    getAll,
    save,
    orderToVoid,
    getCashRounding,
    getPaymentTypeName,
    getMakePayment,
    orderToCancelSale,
    getOrderReceiptDetail,
    updatePoducts
};

const API_URL = Config.key.OP_API_URL

function getPaymentTypeName(udid, registerId) {
    //     let decodedString = localStorage.getItem('UDID');
    //   var decod=  window.atob(decodedString);
    var UDID = get_UDid('UDID');
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'
    };

    return fetch(`${API_URL}/ShopPaymentType/GetAll?udid=${udid}&registerId=${registerId}`, requestOptions).then(handleResponse);
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'
    };

    return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
}

function orderToVoid(order_id, udid) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'
    };

    return fetch(`${API_URL}/ShopOrders/VoidSale?udid=${udid}&OrderId=${order_id}`, requestOptions).then(handleResponse);
}

function orderToCancelSale(order_id, udid) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'
    };

    return fetch(`${API_URL}/ShopOrders/CancelledSale?udid=${udid}&OrderId=${order_id}`, requestOptions).then(handleResponse);
}

function save(shop_order) {
    console.log("shop_order", shop_order);
    const requestOptions = {
        method: 'POST',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors',
        body: JSON.stringify(shop_order)
    };
    //console.log("shop_order",shop_order);
    return fetch(`${API_URL}/ShopOrders/Save`, requestOptions).then(handleResponse)
        .then(shop_order => {
            console.log("shop_order", shop_order);

            return shop_order;
        });
}

function getOrderReceiptDetail() {

    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',

            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'

    };


    var UDID = get_UDid('UDID');

    return fetch(`${API_URL}/ShopSetting/GetReceiptSetting?udid=${UDID}`, requestOptions)
        .then(handleResponse)
        .then(reciept => {
            var reciepts = reciept.Content


            return reciepts;


        });


}

function getCashRounding(udid) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'
    };

    return fetch(`${API_URL}/ShopSetting/GetCashRounding?udid=${udid}`, requestOptions).then(handleResponse)
        .then(cashRes => {
            return cashRes;
        });



}

function getMakePayment(udid, registerId, paycode, amount) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'
    };

    //  return fetch(`${API_URL}/ShopPaymentType/GetMakePaymentTest?udid=${udid}&registerId=${registerId}&paycode=${paycode}&command=sale&amount=${amount}&type=Cancelled`, requestOptions).then(handleResponse);
    //return fetch(`${API_URL}/ShopPaymentType/GetMakePaymentTest?udid=${udid}&registerId=${registerId}&paycode=${paycode}&command=sale&amount=${amount}&type=Approved`, requestOptions).then(handleResponse);

    return fetch(`${API_URL}/ShopPaymentType/GetMakePayment?udid=${udid}&registerId=${registerId}&paycode=${paycode}&command=sale&amount=${amount}`, requestOptions).then(handleResponse);
}

function updatePoducts(cartlist) {
    var items = [];
    var data;
    var udid = get_UDid('UDID');
    if (cartlist != null) {
        cartlist && cartlist.map(value => {
            items.push({
                ProductId: value.variation_id == 0 ? value.product_id : value.variation_id,
                Quantity: value.quantity,
                possition: 0,
                success: false
            })
        })
    }
   // console.log("items :", items)
    data = {
        udid: udid,
        productinfos: items
    }
    //console.log("data :", data)
    const requestOptions = {
        method: 'POST',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors',
        body: JSON.stringify(data)
    };
    return fetch(`${API_URL}/ShopData/CheckStockofProducts`, requestOptions).then(handleResponse)
        .then(result => {
            return result
            //console.log("result", result)
        })
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

function TicketEvent(udid, eventarray) {
    //  alert("s");

    // console.log("eventid",udid,eventarray);

    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true",
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        }
        , mode: 'cors'
    };

    return fetch(`${API_URL}/ShopTickera/GetEventByEventId?udid=${udid}&event_id=${eventarray}`, requestOptions).then(handleResponse)
        .then(events => {

            /// Ticket_event.push(events)
            //  console.log("Ticket_event",Ticket_event);

            // if(Ticket_event.length == eventarray.length ){
            //        console.log("barbar ho gaye",Ticket_event.length,eventarray.length,Ticket_event)
            //     return Ticket_event
            //   }
            return events;
        })

    //    })


}
