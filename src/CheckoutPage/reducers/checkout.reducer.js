import { checkoutConstants } from '../constants/checkout.constants';

export function checkoutlist(state = {}, action) {
    switch (action.type) {
        case checkoutConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case checkoutConstants.GETALL_SUCCESS:
            return {
                items: action.checkoutlist
            };
        case checkoutConstants.GETALL_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}

export function checkout_list(state = {}, action) {
    switch (action.type) {
        // case checkoutConstants.PRODUCT_ADD_REQUEST:
        //     return {
        //         loading: true
        //     };
        case checkoutConstants.GET_CHECKOUT_LIST_SUCCESS:
            return {
                items: action.checkout_list
            };

        default:
            return state
    }
}

   export function tick_event(state = {}, action){
    switch (action.type) {
        case checkoutConstants.TICKET_EVENT_REQUEST:
            return {
                loading: true
            };
        case checkoutConstants.TICKET_EVENT_SUCCESS:
            return {
                items: action.tick_event
            };
        case checkoutConstants.TICKET_EVENT_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
            
      }

export function shop_order(state = {}, action) {
    switch (action.type) {
        case checkoutConstants.INSERT_REQUEST:
            return {
                loading: true
            };
        case checkoutConstants.INSERT_SUCCESS:
            return {
                items: action.shop_order
            };
        case checkoutConstants.INSERT_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}

export function cash_rounding(state = {}, action) {
    switch (action.type) {
        case checkoutConstants.CASH_ROUNDING_REQUEST:
            return {
                loading: true
            };
        case checkoutConstants.CASH_ROUNDING_SUCCESS:
            return {
                items: action.cash_rounding
            };
        case checkoutConstants.CASH_ROUNDING_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}


export function paymentTypeName(state = {}, action){
    switch (action.type) {
        case checkoutConstants.GET_PAYMENT_TYPE_REQUEST:
            return {
                loading: true
            };
        case checkoutConstants.GET_PAYMENT_TYPE_SUCCESS:
            return {
                items: action.paymentTypeName
            };
        case checkoutConstants.GET_PAYMENT_TYPE_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}


export function global_payment(state = {}, action){
    switch (action.type) {
        case checkoutConstants.GLOBAL_PAYMENTS_REQUEST:
            return {
                items: action.global_payment
            };
        case checkoutConstants.GLOBAL_PAYMENTS_SUCCESS:
            return {
                items: action.global_payment
            };
        case checkoutConstants.GLOBAL_PAYMENTS_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}

