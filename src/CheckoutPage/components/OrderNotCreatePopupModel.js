import React from 'react';


export const OrderNotCreatePopupModel = (props) => {
    return(
      <div id="ordernotSuccesModal" tabIndex="-1" className="modal modal-wide fade ">
      <div className="modal-dialog">
           <div className="modal-content">
           <div className="modal-header">
           <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
              <img src="../assets/img/delete-icon.png" />
            </button>
          <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
      </div>
      <div className="modal-body p-0">
          <h3 id="epos_error_model_message" className="popup_payment_error_msg">Sorry! Something Went Wrong Please Try Again</h3>
      </div>
      <div className="modal-footer p-0">
          <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true" onClick={()=>props.statusUpdate(true)} >OK</button>
      </div>
           </div>
       </div>
   </div>
    );
}