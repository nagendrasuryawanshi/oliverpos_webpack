
import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { isConditionalExpression } from 'typescript';
import Config from '../../Config'

export const TicketEventPrint = {
    EventPrintElem,
    
  };

  function EventPrintElem(data,orderList, manager, register, address, site_name,ServedBy,inovice_Id){
   // console.log("printDate",data)
 //   console.log("tickera Reciept",localStorage.getItem('tickera_reciept')?JSON.parse(localStorage.getItem('tickera_reciept')):"") 
    let ticket_reciept=localStorage.getItem('orderreciept') ?JSON.parse(localStorage.getItem('orderreciept')):"";
    let checkList = localStorage.getItem('CHECKLIST') ?JSON.parse(localStorage.getItem('CHECKLIST')):"";
    //let inovice_Id = localStorage.getItem("ORDER_ID") && JSON.parse(localStorage.getItem("ORDER_ID")).Content;
    var payments = JSON.parse(localStorage.getItem("oliver_order_payments"));
    // function orderDetail(pay){
    //   orderList && orderList.map((item, index) =>{
    //   //  console.log("item.type",item.type);
    //      return item.payment_type;
    //   }
    //   )
    // }
  //  ${manager.shop_name?manager.shop_name:manager}  
  var gmtDateTime = ticket_reciept.ShowTime == true && ticket_reciept.ModifiedOn  ? (moment.utc(ticket_reciept.ModifiedOn)).local().format(Config.key.DATETIME_FORMAT)
        : (moment.utc(ticket_reciept.ModifiedOn)).local().format(Config.key.DATE_FORMAT)

   let baseurl=ticket_reciept.CompanyLogo?Config.key.RECIEPT_IMAGE_DOMAIN+ticket_reciept.CompanyLogo:null

   let barcode_image=ticket_reciept.AddBarcode == true ?  Config.key.RECIEPT_IMAGE_DOMAIN+"/Content/img/ic_barcode.svg":null

   // console.log("manager.shop_name ",manager ,"baseurl",baseurl,"EventPrintElem",data,"checkList",checkList,"address",address,"manager", manager,"register", register,"address", address,"site_name", site_name,"payments",payments,"orderList",orderList,ServedBy,inovice_Id);
    var mywindow = window.open('#', 'my div', 'width=600');
    var payments_type="";
    orderList && orderList.map((item, index) =>{
     //  console.log("item2112",item.type,item.payment_type,payments_type);    

       var type= (item.payment_type && item.payment_type !=null && item.payment_type !="") ?item.payment_type:item.type;
       //console.log("type",type);    
       payments_type += (payments_type != "" ? "," : "") +type;

    })
   // console.log("payments_type",payments_type);
    mywindow.document.write(`<html><head><meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title  align="left"></title>
    
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
          <style>
     @media screen {
          @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
          }

    body {
            font-family: "Lato", "Lucida Grande", "Lucida Sans Unicode", Tahoma, Sans-Serif;
          }

        }
        .demo-print {
          /*background-color: #edeff4;*/
         // padding: 15px 20px;
  
          border-top-left-radius: 5px;
          font-size: 18px;
          line-height: 25px;
          width: 420px;
          margin: auto;
          height: 99.7%;
       
      }
      .demo-print-body {
          position: relative;
          // padding-bottom: 25px;
          background: #fff;
          border-bottom: 0px;
      }
      .demo-print-content {
          padding: 22px;
          border: 1px solid #cecece;
         
          border-top-right-radius: 4px;
          border-top-left-radius: 4px;
      }
     
      .print-logo {
        border-radius: 5px;
        border: 1px solid #cacaca;
        text-align: center; 
        height: 200px;
        overflow: hidden;
        display: flex;
        width: 100%;
        justify-content: center;
        align-items: center;
        padding: 10px 0px;
    }
    .print-logo img {
      height: 100%;
    }
    .print-logo h4 {
        color: #979797;
        font-weight: 600; 
        padding: 0px;
        margin: 0px;
    }
      .list {
          color: #979797;
          margin: 10px 0px;
      }
      .demo-hint-text {
          border-top: 1px solid #979797;
          text-align: center;
          padding: 8px 0px 0px;
          margin-top: 0px;
          margin-bottom: 5px;
          color: #979797;
      }
      .printer-text {
          color: #979797;
          font-size: 18px;
          font-weight: 300;
          text-align: center;
      }
      .printer-text .shop-url {
          font-weight: 800
      }
      .adm-text-area{
          color: #535353;
          font-size: 20px;
          font-weight: 400;
          line-height: 24px;
          border-radius: 5px;
          border: 1px solid #cacaca;
      }
      .adm-text-area::placeholder { opacity: 0.5;}
      .printer-table table {
          margin-bottom: 0px;
          width: 100%;
          border-collapse: collapse;
      }
      .printer-table table tr td {
          padding: 0px;
          color: #979797;
          border-color: #979797 !important;
          font-weight: normal;
      }
      .printer-table table tr td:first-child {
          padding-right: 10px;
      }
      .font-bold {
          font-weight: bold !important;
      }
      .printer-table table tbody tr td,
      .printer-table table tfoot tr td {
          border-top: 0px;
      }
      .printer-table table tbody tr:first-child td {
          border-top: 1px solid #979797;
      }
      .printer-table table tfoot tr:first-child td,
      .printer-table table tfoot tr:last-child td {
          border-top: 1px solid #979797;
      }
      .printer-table table tfoot tr td:first-child {
          padding-left: 20px !important;;
      }
      .printer-table table tfoot tr td:last-child,
      .printer-table table tbody tr td:last-child {
        text-align: right !important;
    }
      .print-ticket-info {
          margin-top: 10px;
          margin-bottom: 5px;
      }
      .printer-table table tfoot tr:first-child td .print-ticket-info
      {
          margin-bottom: 10px;
      }
      .print-ticket-info p {
        padding: 1px
      }
      .shop-url {
        margin-top: 0px;
        margin-bottom: 5px;
      }
      .list p {
        margin: 0px;
        margin-bottom: 3px;
    }
    .print-ticket-info p {
      margin: 0px;
  }

      
    </style></title>`);
  /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
  mywindow.document.write(`</head><body>`);
  mywindow.document.write(`
  <!-- open bracket --> 
  
   ${  data && data.map((item, index)  =>{  
     return( 
        ` 
    <div class="demo-print" id="page-sidebar-right">
       <div class="demo-print-body">
     
      
          <div class="demo-print-content">
         
            <!-- open bracket -->
                
            <div class="print-logo">               
            ${ baseurl ? ` <img src=${baseurl}   />`:'' }

            ${!baseurl && manager.shop_name ?`<h4>  ${ manager.shop_name}</h4>`: ''}
                  
              
              </div>
           
            <div class="list">
             
              <p>${ticket_reciept?ticket_reciept.DateText:'Date'} : ${gmtDateTime}</p>
              <p>${ticket_reciept?ticket_reciept.Sale:'Sale#'} : ${inovice_Id} </p>
              ${ticket_reciept.ShowServedBy == true ?
                `<p>${ticket_reciept?ticket_reciept.ServedBy:'Served By'} : ${ manager.display_name}</p>`
                :''}
              <p>${ticket_reciept?ticket_reciept.TaxIdText:'Tax ID'} :${ticket_reciept?ticket_reciept.TaxId:'0'} </p>
           
             
              <p>${ticket_reciept?ticket_reciept.PaidWith:'Paid With'} : ${payments_type?payments_type:''}</p>
          
            </div>
            <h4 class="demo-hint-text">
            </h4>
            <div class="printer-table">

              <table class="table table-border">

                <tbody>
           
                  <tr>
                    <td>
                      <div class="print-ticket-info">
                        <p>${item.event_detail.title}</p>
                        <p>First Name: ${item.first_name}</p> 
                        <p>Last Name: ${item.last_name}</p>
                        <p>${ item && item.event_detail?"Start Date: "+ moment(item.event_detail.event_start_date_time).format('MMMM D ,YYYY'):""}</p>
                        <p>${item && item.event_detail?"End Date: "+ moment(item.event_detail.event_end_date_time).format('MMMM D ,YYYY'):""}</p>
                         <p>${item && item.event_detail?"Address: "+ item.event_detail.event_location:""}</p>

                      </div>
                    </td>
                    ${orderList && orderList.map((item, index) =>{
                      //  console.log("item.type",item.type);
                      return(
                   
                    `<td class="font-bold">${item.payment_amount?item.payment_amount:item.amount}</td> `
                      )
                  }
                )  }
                  </tr>
                 
                </tbody>
             
              </table>
            </div>
            <h4 class="demo-hint-text">
            </h4>
            <div class="printer-text">
            <div class="printer-barcode">
                  <img src=${barcode_image} />

               </div>
              <p class="shop-url">
              ${site_name?site_name:''}
                <address>
                 ${address.Address1?address.Address1:address} ${address.Address2?address.Address2:''} 
                 ${address.Zip?address.Zip:''} ${address.City?address.City:''} ${address.CountryName?address.CountryName:''}
                </address>
              </p>
             
            </div>
           
            <!-- close bracket -->
         
          </div>
         
         </div> 
    </div>
    `)}
    )
  }  
    <!-- close bracket -->     
    `)
  mywindow.document.write('</body></html>');

  mywindow.print();
  mywindow.close();

  return true;
    

  }
    