import React from 'react';
import { connect } from 'react-redux';
import { GetRoundCash } from '../Checkout';
import { checkoutActions } from '../actions/checkout.action';
import { LoadingModal } from '../../_components';
import { get_UDid } from '../../ALL_localstorage';
import { CardPayment, CashPayment, GlobalPayment, OtherPayment, NormalKeypad, StripePayment } from '../../_components/PaymentComponents';


class CheckoutViewThird extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            payingAmount: 0,
            emptyPaymentFlag: true,
            checkList: null,
            paidAmount: '',
            CashRound: '',
            paymentType: '',
            paidAmountStatus: false,
            isFirstKeyPressed: true,
            paymentTypeName: (typeof localStorage.getItem('PAYMENT_TYPE_NAME') !== 'undefined') ? JSON.parse(localStorage.getItem('PAYMENT_TYPE_NAME')) : null,
            isPaymentStart: false,
            globalPayments: '',
            closingTab: false,
            activeDisplay:false,
        }
        this.addPayment = this.addPayment.bind(this);

    }

    activeDisplay(st){
        this.setState({activeDisplay:st})
    }

    componentDidMount() {

        this.props.onRef(this);

    }

    componentWillUnmount() {
        this.props.onRef(null)
    }

    setPartialPayment(paymentType, paymentAmount) {

        // console.log("amount" , paymentType , paymentAmount)
        // $( ".enter-order-amount" ).attr("value", "");  
        this.setState({paidAmount: ''})
        let checkList = this.props.checkList;
        let getPayments = (typeof JSON.parse(localStorage.getItem("oliver_order_payments")) !== "undefined") ? JSON.parse(localStorage.getItem("oliver_order_payments")) : [];
        let paid_amount = 0;

        if (getPayments !== null) {
            getPayments.forEach(paid_payments => {
                //console.log("paid_payments" , paid_payments)
                paid_amount += parseFloat(paid_payments.payment_amount);
            });
        }
        // console.log("paid amount" , paid_amount , parseFloat(checkList.totalPrice) , parseFloat(paid_amount + parseFloat(paymentAmount)) )
        if (typeof paymentType !== 'undefined') {

            if (parseFloat(paymentAmount) <= 0) {

                this.props.extraPayAmount("Paying amount should be greater than 0.")

            } else if (parseFloat(checkList.totalPrice).toFixed(2) >= parseFloat(parseFloat(paid_amount + parseFloat(paymentAmount))) && paymentType !== 'cash') {

                this.props.setOrderPartialPayments(parseFloat(paymentAmount), paymentType);

            } else if (paymentType == 'cash') {
                 this.props.setOrderPartialPayments(parseFloat(paymentAmount), paymentType);

            } else if (parseFloat(parseFloat(checkList.totalPrice)) < parseFloat(parseFloat(paid_amount + parseFloat(paymentAmount)))) {
               this.props.extraPayAmount("Amount entered cannot exceed the total purchase price")
            }
        } else {
            this.props.extraPayAmount("Please select a valid payment mode.")
        }
    }

    getRemainingPrice() {
        let checkList = JSON.parse(localStorage.getItem("CHECKLIST"));
        let paid_amount = 0;

        let getPayments = (typeof JSON.parse(localStorage.getItem("oliver_order_payments")) !== "undefined") ? JSON.parse(localStorage.getItem("oliver_order_payments")) : [];

        if (getPayments !== null) {
            getPayments.forEach(paid_payments => {
                paid_amount += parseFloat(paid_payments.payment_amount);
            });
        }
        if (checkList.totalPrice >= paid_amount) {
            return (parseFloat(checkList.totalPrice) - parseFloat(paid_amount));

        }
    }

    getRemainingPriceForCash() {
        let checkList = JSON.parse(localStorage.getItem("CHECKLIST"));
        let paid_amount = 0;

        let getPayments = (typeof JSON.parse(localStorage.getItem("oliver_order_payments")) !== "undefined") ? JSON.parse(localStorage.getItem("oliver_order_payments")) : [];

        if (getPayments !== null) {
            getPayments.forEach(paid_payments => {
                paid_amount += parseFloat(paid_payments.payment_amount);
            });
        }
        let cash_rounding = this.props.cash_rounding.items && this.props.cash_rounding.items.Content
        //console.log("cash_rounding", cash_rounding)
        let totalPrice = parseFloat(checkList.totalPrice)
        this.state.CashRound = parseFloat(GetRoundCash(cash_rounding, totalPrice - paid_amount))
        let new_total_price = (totalPrice - paid_amount) + parseFloat(GetRoundCash(cash_rounding, totalPrice))
        return new_total_price;
    }

    parkOrder(status) {
        this.props.orderPopup(status);
    }

    normapNUM(val) {
        console.log("normapNUM", val)
        this.setState({
            paidAmount: val
        })
    }

    addPayment(type) {
        let checkList = this.props.checkList;
        let amount = $('#calc_output').text();
        if (checkList.totalPrice >= amount) {
            this.props.addPayment(type, amount);
        }

    }

    pay_amount(paymentType) {
        const { paidAmount , closingTab } = this.state;
        let payment_amount = paidAmount;
        this.props.paymentType(paymentType);
        console.log("payment_amount" , payment_amount , paymentType, closingTab)
        //console.log("payment_amount 0" , typeof payment_amount)
        const getRemainingPrice = this.getRemainingPrice();
        const getRemainingPriceForCash = this.getRemainingPriceForCash();
        if(paymentType == 'cash'){
        if (!payment_amount){
                payment_amount = parseFloat(getRemainingPriceForCash).toFixed(2)
            }
        }else{
            if (!payment_amount){
                payment_amount =  parseFloat(getRemainingPrice).toFixed(2)
            }
        }
       
       // console.log("payment_amount", payment_amount)
        this.setState({ paymentType: paymentType})

        if (payment_amount) {
            if (paymentType !== 'cash' && paymentType !== 'global_payments') {
                //console.log("other")
                 if(closingTab == false){
                    payment_amount = parseFloat(getRemainingPrice).toFixed(2)
                    this.setPartialPayment(paymentType, payment_amount)
                 }else{
                      this.setPartialPayment(paymentType, payment_amount)
                 }
                
               
            }

            if (paymentType == 'global_payments') {
               /// console.log("global_payments")
                this.setState({
                    isPaymentStart: true
                })

                this.globalPayments(paymentType, payment_amount)
            }

            if (paymentType == 'cash') {
                //console.log("cash")
                if(getRemainingPriceForCash > 0){
                    this.setPartialPayment(paymentType, payment_amount)
                }else if(getRemainingPriceForCash == 0 && payment_amount > 0){
                    this.props.extraPayAmount("Amount entered cannot exceed the total purchase price.")  
                }else{
                    this.props.extraPayAmount("Paying amount should be greater than 0.") 
                }

            }
        }

    }

    handleFocus = (event) => {
        event.target.select();
    };

    globalPayments(paycode, amount) {
        // console.log("globalPayments" , paycode , amount)
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID = get_UDid('UDID');
        this.setState({
            paidAmount: amount,
            globalPayments: paycode
        })
        // localStorage.removeItem('GLOBAL_PAYMENT_RESPONSE')
        this.props.dispatch(checkoutActions.getMakePayment(UID, localStorage.getItem('register'), paycode, amount))

    }

    componentWillReceiveProps(nextProp) {
        // console.log("PaymentNextProp",nextProp);
        if (nextProp.global_payment) {
            this.setState({ isPaymentStart: false })
            //  console.log("123" ,this.state.globalPayments , this.state.paidAmount)
            if (nextProp.global_payment.IsSuccess === true) {

                this.setPartialPayment(this.state.globalPayments, this.state.paidAmount)
            }
        }

    }

    closingTab(st) {
        //console.log("closingTab", st)
        this.setState({ closingTab: st })
    }

    handleFocus = (event) => {
        event.target.select();
    };


    render() {
        const { global_payment } = this.props;
        const getRemainingPrice = this.getRemainingPrice();
        const getRemainingPriceForCash = this.getRemainingPriceForCash();
        const { paidAmount, paidAmountStatus, paymentTypeName ,activeDisplay} = this.state;
       //console.log("activeDisplay" , activeDisplay)
        return (
            <div className="col-lg-5 col-md-5 col-sm-6 col-xs-12 pt-4 plr-8">
                <div className="block__box white-background round-8 full_height overflowscroll text-center pl-3 pr-3">
                    <h2>Amount Tendered</h2>

                    <div className="wrapper_accordion">
                        <NormalKeypad
                            paidAmount={paidAmount}
                            parkOrder={() => { this.parkOrder("park_sale"); this.props.paymentType('park_sale'); }}
                            paidAmountStatus={paidAmountStatus}
                            placeholder={(typeof getRemainingPrice == 'undefined') ? '0.00' : parseFloat(getRemainingPrice).toFixed(2)}
                            normapNUM={(text) => this.normapNUM(text)}
                            closing_Tab={this.state.closingTab}
                            closingTab={(text) => this.closingTab(text)}
                            handleFocus={this.handleFocus}
                            activeDisplay={(text) => this.activeDisplay(text)}
                            styles={activeDisplay == false || activeDisplay == 'undefined_true' ?'':'none'}
                        />

                        {(typeof paymentTypeName !== 'undefined') && paymentTypeName !== null ?
                            paymentTypeName.map((pay_name, index) => {
                                return (
                                    <div key={index}>
                                        {this.state.isPaymentStart && (!global_payment || !global_payment) ? <LoadingModal /> : ""}
                                        {pay_name.Code == 'global_payments' ?
                                            (pay_name.HasTerminal == true && pay_name.TerminalCount > 0) ?
                                                <GlobalPayment
                                                    color={pay_name.ColorCode}
                                                    Name={pay_name.Name}
                                                    code={pay_name.Code}
                                                    pay_amount={(text) => this.pay_amount(text)}
                                                    msg={this.props.global_payment ? this.props.global_payment.Message : "Waiting On Terminal"}
                                                    activeDisplay={(text) => this.activeDisplay(text)}
                                                    styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                />
                                                :
                                                null
                                            :
                                            pay_name.Code == 'cash' ?

                                                <CashPayment
                                                    color={pay_name.ColorCode}
                                                    Name={pay_name.Name}
                                                    paidAmounts={paidAmount}
                                                    placeholder={parseFloat(getRemainingPriceForCash.toFixed(2))}
                                                    pay_amount={(text) => this.pay_amount(text)}
                                                    code={pay_name.Code}
                                                    paidAmountStatus={paidAmountStatus}
                                                    normapNUM={(text) => this.normapNUM(text)}
                                                    cash_rounding={this.props.cash_rounding}
                                                    closingTab={(text) => this.closingTab(text)}
                                                    handleFocus={this.handleFocus}
                                                    activeDisplay={(text) => this.activeDisplay(text)}
                                                    styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                />
                                                :
                                                // pay_name.Code == 'card' ?
                                                //     <CardPayment
                                                //         color={pay_name.ColorCode}
                                                //         Name={pay_name.Name}
                                                //         code={pay_name.Code}
                                                //         activeDisplay={(text) => this.activeDisplay(text)}
                                                //         styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                //     />
                                                //     :
                                                    pay_name.Code == 'stripe' ?
                                                        <StripePayment
                                                            color={pay_name.ColorCode}
                                                            Name={pay_name.Name}
                                                            code={pay_name.Code}
                                                            pay_amount={(text) => this.pay_amount(text)}
                                                            activeDisplay={(text) => this.activeDisplay(text)}
                                                            styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                        />
                                                        :
                                                        <OtherPayment
                                                            color={pay_name.ColorCode}
                                                            Name={pay_name.Name}
                                                            code={pay_name.Code}
                                                            pay_amount={(text) => this.pay_amount(text)}
                                                            closingTab={(text) => this.closingTab(text)}
                                                            styles={activeDisplay == false ? '' : 'none'}
                                                        />
                                        }
                                    </div>
                                )
                            })
                            : ''
                        }
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { checkoutlist, cash_rounding } = state;
    return {
        checkoutlist,
        cash_rounding,
        //paymentTypeName: paymentTypeName.items
    };
}
const connectedCheckoutViewThird = connect(mapStateToProps)(CheckoutViewThird);
export { connectedCheckoutViewThird as CheckoutViewThird };

