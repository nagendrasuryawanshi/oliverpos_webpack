import React from 'react';
import { connect } from 'react-redux';
import { checkoutActions } from '../actions/checkout.action';
import { CommonHeaderFirst } from '../../_components';
import { CheckoutViewFirst } from './CheckoutViewFirst';
import { CheckoutViewSecond } from './CheckoutViewSecond';
import { CheckoutViewThird } from './CheckoutViewThird';
import { customerActions } from '../../CustomerPage/actions/customer.action';
import { cartProductActions } from '../../_actions';
import { default as NumberFormat } from 'react-number-format';
import { LoadingModal, CustomerAddFee, CustomerNote } from '../../_components'
import { GetRoundCash } from '../Checkout'
import {encode_UDid,get_UDid} from '../../ALL_localstorage'
import { OrderNotCreatePopupModel } from './OrderNotCreatePopupModel';
import {  getCheckoutList , getDiscountAmount } from '../../_components'


class CheckoutView extends React.Component {
    constructor(props) {
        // let decodedString = localStorage.getItem('UDID');
        // var decod = window.atob(decodedString);
        var UID = get_UDid('UDID');
        super(props);
        this.state = {
            user_fname: '',
            user_lname: '',
            user_address: '',
            user_city: '',
            user_contact: '',
            user_notes: '',
            user_email: '',
            user_pcode: '',
            user_id: '',
            UDID: UID,
            paying_type: '',
            paying_amount: '',
            checkList: null,
            add_note: null,
            paid_amount: null,
            location_id: null,
            lay_Away: null,
            final_place_order: null,
            store_credit: 0,
            // order_status : "pending",
            set_order_notes: new Array(),
            set_calculator_remaining_amount: 0,
            isShowLoader: false,
            msg: '',
            cash_payment: 0,
            edit_status: false,
            change_amount: 0,
            after_payment_is: 0,
            cash_round: 0,
            total_price: 0
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getPayment = this.getPayment.bind(this);
        this.setPayment = this.setPayment.bind(this);
        this.layAwayPayment = this.layAwayPayment.bind(this);
        this.setOrderPartialPayments = this.setOrderPartialPayments.bind(this);
        this.createOrder = this.createOrder.bind(this);
        this.openOrderPopup = this.openOrderPopup.bind(this);
        this.setCalculatorRemainingprice = this.setCalculatorRemainingprice.bind(this);
        this.extraPayAmount = this.extraPayAmount.bind(this);
        this.closeExtraPayModal = this.closeExtraPayModal.bind(this);
        this.edit_status = this.edit_status.bind(this);
        this.paymentType = this.paymentType.bind(this);
        // this.props.dispatch(checkoutActions.getPaymentTypeName(localStorage.getItem('UDID') , localStorage.getItem('register')))
    }


    paymentType(type) {
        let totalPrice = this.state.checkList && this.state.checkList.totalPrice;
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        let cash_round = parseFloat(GetRoundCash(cash_rounding, totalPrice))
        if (type == 'cash') {
             //console.log("round of 2 " , cash_round)
             this.state.cash_round = cash_round;
            this.setState({ cash_round: cash_round
               // total_price: parseFloat(totalPrice + cash_round).toFixed(2)
            })
        } else {
             // console.log("rounf tot 2" , totalPrice)
            this.state.cash_round = 0;
            this.setState({
                cash_round: 0,
                total_price: 0
            })
        }

    }

    edit_status(status) {

        this.setState({ edit_status: status })
    }

    getPayment(type, amount) {
        this.setState({
            paying_type: type,
            paying_amount: amount ? amount.replace(",", "") : "",
        });
    }

    layAwayPayment(type, amount) {
        this.setState({
            lay_Away: type,
            store_credit: amount
        })


        if (type == 'lay_away') {
            $('.addnoctehere').modal();
        }


    }

    componentWillMount() {
        let checkList = localStorage.getItem('CHECKLIST');
        let location_id = localStorage.getItem('Location');
        let AdCusDetail = localStorage.getItem('AdCusDetail');
        //console.log("checkList",checkList);
        //  let decodedString = localStorage.getItem('UDID');
        //var decod=  window.atob(decodedString);
        // var UID= decod;
        //const UID = localStorage.getItem('UDID');
      
        this.props.dispatch(checkoutActions.cashRounding(this.state.UDID));

        this.setState({
            checkList: JSON.parse(checkList),
            location_id: location_id,
        })
        // setTimeout(() => {
        //     this.setState({
        //         isShowLoader: false
        //     })

        // }, 3000);
    }

    componentDidMount() {
        setTimeout(function () {
            setHeightDesktop();
            // autoFocus();
        }, 1000);
        let VOID_SALE = localStorage.getItem("VOID_SALE")
        if ((typeof VOID_SALE !== 'undefined') && VOID_SALE !== null) {
            //  console.log("VOID_SALE")

        }
        let AdCusDetail = localStorage.getItem('AdCusDetail');
        const UID = this.state.UDID;
        // const UID = localStorage.getItem('UDID');
        this.setState({ UDID: UID })
        if ((typeof AdCusDetail !== 'undefined') && AdCusDetail != null) {
            let checkoutList = JSON.parse(AdCusDetail) && JSON.parse(AdCusDetail).Content;
            if (checkoutList != null) {
                this.setState({
                    user_fname: checkoutList.FirstName ? checkoutList.FirstName : '',
                    user_lname: checkoutList.LastName,
                    user_address: checkoutList.StreetAddress,
                    user_city: checkoutList.city_name,
                    user_contact: checkoutList.Phone,
                    user_notes: checkoutList.Notes,
                    user_email: checkoutList.Email,
                    user_pcode: checkoutList.Pincode,
                    user_id: checkoutList.Id,
                    store_credit: checkoutList.StoreCredit,
                    



                })
                this.state.user_id = checkoutList.Id;
            }
        }
        this.props.dispatch(checkoutActions.getOrderReceipt());

    }

    finalAdd() {
        const { checkList, after_payment_is, total_price, cash_round } = this.state;
       // console.log("cash_round here" , cash_round);
      //  console.log("after_payment_is" , after_payment_is)
        let totalPrice = total_price == 0 ? checkList && checkList.totalPrice : total_price;
        let order_id = (typeof checkList.order_id !== "undefined") ? checkList.order_id : 0;
        if (localStorage.oliver_order_payments) {
            var payments = JSON.parse(localStorage.getItem("oliver_order_payments"));
            payments.push({
                "Id": 0,
                "payment_type": 'cash',
                "payment_amount": parseFloat((totalPrice + cash_round) - after_payment_is).toFixed(2),
                "order_id": order_id,
                "description": ''
            });
            localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
        } else {

            var payments = new Array();
            payments.push({
                "Id": 0,
                "payment_type": 'cash',
                "payment_amount": parseFloat(totalPrice + cash_round).toFixed(2),
                "order_id": order_id,
                "description": ''
            });
            localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
        }

        this.orderCart.getPaymentDetails();
        this.isOrderPaymentComplete(order_id , cash_round);

    }

    // its set the order payments
    setOrderPartialPayments(paying_amount, payment_type) {
       // console.log("paying_amount", paying_amount, payment_type);
        let totalPrice = this.state.checkList && this.state.checkList.totalPrice
        let change_amount = 0;
        var payment_is = 0;
        let order_id = this.state.checkList && this.state.checkList.order_id !== 0 ? this.state.checkList && this.state.checkList.order_id : 0;
        let actual_amount = this.state.total_price == 0 ? this.state.checkList && this.state.checkList.totalPrice : this.state.total_price;
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        //console.log("cash_rounding" , cash_rounding)
        let cash_round = parseFloat(GetRoundCash(cash_rounding, totalPrice))
       // console.log("cash_round" , cash_round)
        if (typeof (Storage) !== "undefined") {

            if (localStorage.oliver_order_payments) {
                var payments = JSON.parse(localStorage.getItem("oliver_order_payments"));
                payments.map((items) => {
                    return payment_is += parseFloat(items.payment_amount);
                })
                var ret = payment_is;
                let total_pay = ret + parseFloat(paying_amount);
               // console.log("total_pay" , total_pay , actual_amount , ret , totalPrice)
                if (parseFloat(total_pay).toFixed(2) > parseFloat(totalPrice).toFixed(2) && payment_type !== 'cash') {
                    this.extraPayAmount("Amount entered cannot exceed the total purchase price")
                } else {
                    if (payment_type == 'cash') {
                        this.setState({ cash_round: cash_round })
                        paying_amount = String(paying_amount).replace(',', '');
                        if (total_pay > parseFloat(totalPrice + cash_round).toFixed(2)) {
                           // console.log("cash Condition 1", paying_amount, payment_is , cash_round);
                            change_amount = paying_amount - (actual_amount - payment_is + cash_round);
                            this.setState({
                                change_amount: change_amount,
                                cash_payment: paying_amount,
                                after_payment_is: payment_is
                            })
                            this.freezScreen()
                            $('#popup_cash_rounding').modal('show')

                        } else {
                            // console.log("cash Condition 2", paying_amount, payment_is , cash_round);
                            if (paying_amount)
                                payments.push({
                                    "Id": 0,
                                    "payment_type": payment_type,
                                    "payment_amount": String(paying_amount).replace(',', ''),
                                    "order_id": order_id,
                                    "description": ''
                                });
                            localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
                            this.orderCart.getPaymentDetails();
                            this.isOrderPaymentComplete(order_id, cash_round);
                        }

                    } else {
                        // console.log("cash Condition 2", paying_amount, paying_amount);
                        if (payment_type == 'global_payments') {
                            let g_payment = JSON.parse(localStorage.getItem('GLOBAL_PAYMENT_RESPONSE'));
                            // console.log("global_payment 1", g_payment)
                            if (g_payment !== null &&  g_payment.IsSuccess === true) {
                                let global_payments = g_payment.Content;
                                let data = `TerminalId-${global_payments.TerminalId},Authrization-${global_payments.Authrization} , RefranseCode-${global_payments.RefranseCode}`;
                                if (paying_amount && data)
                                    payments.push({
                                        "Id": 0,
                                        "payment_type": payment_type,
                                        "payment_amount": String(paying_amount).replace(',', ''),
                                        "order_id": order_id,
                                        "description": data,
                                    });
                                localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
                                this.orderCart.getPaymentDetails();
                                this.isOrderPaymentComplete(order_id, 0);
                            }

                        } else {
                            if (paying_amount)
                                payments.push({
                                    "Id": 0,
                                    "payment_type": payment_type,
                                    // "payment_amount" : paying_amount.replace(/\s+/g, ''), by shakun
                                    "payment_amount": String(paying_amount).replace(',', ''),
                                    "order_id": order_id,
                                    "description": "",
                                });
                            localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
                            this.orderCart.getPaymentDetails();
                            this.isOrderPaymentComplete(order_id, 0);
                        }
                    }
                }

            } else {
                var payments = new Array();
                if (payment_type == 'cash') {
                    //console.log("cash Condition 5" ,paying_amount , cash_round , actual_amount , actual_amount+cash_round);
                    this.setState({ cash_round: cash_round })
                    paying_amount = String(paying_amount).replace(',', '');
                    if (parseFloat(paying_amount) > parseFloat(actual_amount + cash_round).toFixed(2)) {
                       // console.log("cash Condition 3" ,paying_amount , cash_round);
                        change_amount = paying_amount - (actual_amount + cash_round);
                        this.setState({
                            change_amount: change_amount,
                            cash_payment: paying_amount,
                            after_payment_is: 0
                        })
                        this.freezScreen()
                        $('#popup_cash_rounding').modal('show')
                    } else {
                       //console.log("cash Condition 4" ,paying_amount , cash_round);
                        payments.push({
                            "Id": 0,
                            "payment_type": payment_type,
                            "payment_amount": String(paying_amount).replace(',', ''),
                            "order_id": order_id,
                            "description": "",
                        });
                        localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
                        this.orderCart.getPaymentDetails();
                        this.isOrderPaymentComplete(order_id, cash_round);
                    }

                } else {

                    if (payment_type == 'global_payments') {
                        let g_payment = JSON.parse(localStorage.getItem('GLOBAL_PAYMENT_RESPONSE'));
                        //console.log("global_payment 2", g_payment)
                        if (g_payment !== null && g_payment.IsSuccess === true) {
                            let global_payments = g_payment.Content;
                            let data = `TerminalId-${global_payments.TerminalId} , Authrization-${global_payments.Authrization},RefranseCode-${global_payments.RefranseCode}`;
                            if (paying_amount && data)
                                payments.push({
                                    "Id": 0,
                                    "payment_type": payment_type,
                                    "payment_amount": String(paying_amount).replace(',', ''),
                                    "order_id": order_id,
                                    "description": data,
                                });
                            localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
                            this.orderCart.getPaymentDetails();
                            this.isOrderPaymentComplete(order_id, 0);
                        }
                    } else {
                        payments.push({
                            "Id": 0,
                            "payment_type": payment_type,
                            "payment_amount": String(paying_amount).replace(',', ''),
                            "order_id": order_id,
                            "description": "",
                        });
                        localStorage.setItem("oliver_order_payments", JSON.stringify(payments));
                        this.orderCart.getPaymentDetails();
                        this.isOrderPaymentComplete(order_id, 0);
                    }
                }

            }

        } else {
            //alert("Your browser not support local storage");
        }


    }

    // its get the payment from checkout third view
    getOrderPartialPayments() {
        this.orderPayments.setPartialPayment();
    }

    // its check the order paymets complete or not
    isOrderPaymentComplete(order_id, cash_round1) {
        const { checkList, total_price } = this.state;
        let amountToBePaid = (typeof checkList !== 'undefined') ? parseFloat(checkList.totalPrice) : 0;
        let paidPaments = this.getOrderPayments(order_id);
        let paidAmount = 0;
        let cash_round = (cash_round1 =='undefined') ? 0 : cash_round1
        paidPaments.forEach(paid_payments => {
            paidAmount += parseFloat(paid_payments.payment_amount);
        });

      console.log("amountToBePaid" , parseFloat(amountToBePaid) ,cash_round  , parseFloat(paidAmount))
        // console.log("amountToBePaid2" , parseFloat(amountToBePaid).toFixed(2) , paidAmount , cash_round)
        // console.log("amountToBePaid 1" ,checkList.totalPrice , total_price , amountToBePaid , paidAmount , cash_round)
        // console.log("amountToBePaid 2" ,parseFloat(amountToBePaid).toFixed(2) , parseFloat(paidAmount).toFixed(2))

        //if (cash_round !== 0) {
            if (parseFloat(amountToBePaid + cash_round).toFixed(2) == parseFloat(paidAmount).toFixed(2)) {
                this.createOrder("completed");
            }
            // else{
            //     history.push('/checkout')
            //    // location.reload()
            // }
       // }
        // if (parseFloat(amountToBePaid).toFixed(2) == parseFloat(paidAmount).toFixed(2)) {
        //     this.createOrder("completed");
        // }
    }

    //its used for get the order payments
    getOrderPayments(order_id) {
        if (localStorage.oliver_order_payments) {
            let paid_amount = 0;
            var payments = new Array();

            JSON.parse(localStorage.getItem("oliver_order_payments")).forEach(paid_payments => {
                paid_amount += parseFloat(paid_payments.payment_amount);
                payments.push({
                    "Id": paid_payments.Id !== 0 ? paid_payments.Id : 0,
                    "payment_type": paid_payments.payment_type,
                    "payment_amount": paid_payments.payment_amount,
                    "order_id": order_id,
                    "description": paid_payments.description
                });
            });
            return payments;
        } else {
            //alert("Your browser not support local storage");
        }
    }

    //its used to remove the order payments
    removeOrderPayments() {
        if (localStorage.oliver_order_payments) {
            localStorage.removeItem("oliver_order_payments");
        } else {
            // alert("Your browser not support local storage");
        }
    }

    setCalculatorRemainingprice(amount) {
        this.setState({
            set_calculator_remaining_amount: amount,
        })
    }

    createOrder(status) {
        this.setState({
            isShowLoader: true,
        });
        this.setPayment(status);
    }

    openOrderPopup(status) {
        $("#park-sale-and-layaway-modal").modal("show");
        $("#park-sale-and-layaway-modal-title").text(status.replace("_", " "));
        $("#park-sale-and-layaway-modal").data("status", status);
    }

    placeParkLayAwayOrder() {
        let status = $("#park-sale-and-layaway-modal").data("status");
        let notes = this.state.set_order_notes;
        let note = $("#park-sale-and-layaway-modal-note").val();
        if (note.length === 0) {
            note = `order ${status}`;
        }

        notes.push({
            note_id: '',
            note: note
        })
        // notes.push(note)
        this.setState({
            set_order_notes: notes,
            add_note: note,
            isShowLoader: true,
        })
        this.createOrder(status);
        // }else{
        //     alert("plaese add some text!!")
        // }
        $("#park-sale-and-layaway-modal").modal("hide");
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        this.setState({ isShowLoader: false })
        e.preventDefault();
        const { UDID, user_id, user_address, user_city, user_contact, user_email, user_fname, user_lname, user_notes, user_pcode } = this.state;
        const { dispatch } = this.props;
        //if (UDID, user_id, user_address && user_city && user_contact && user_email && user_fname && user_lname && user_notes && user_pcode) {
            if (user_email) {
            const update = {
                Id: user_id,
                FirstName: user_fname,
                LastName: user_lname,
                Contact: user_contact,
                startAmount: 0,
                Email: user_email,
                udid: UDID,
                notes: user_notes,
                StreetAddress: user_address,
                Pincode: user_pcode,
                City: user_city
            }
            this.edit_status(false)
            // this.setState({
            //     isShowLoader: false,
            // });

            dispatch(customerActions.save(update));
            // console.log("this.props.single_cutomer_list",this.props.single_cutomer_list)

            $(".close").click();

        }
    }

    setPayment(get_order_status) {
        
        const { cash_round, user_address, user_city, user_contact, user_email, user_fname, user_lname, user_notes, user_pcode, paying_amount, paying_type, checkList, add_note, paid_amount, location_id, UDID, user_id, lay_Away, history } = this.state;
        //console.log("cash_round" , cash_round)
        const { dispatch } = this.props;
        let checkoutList = checkList && checkList.customerDetail && checkList.customerDetail.Content;
        let chec;
        let place_order;
        let order_payments = [];
        let complete_order_payments = [];
        let newList = [];
        let order_custom_fee = [];
        let order_notes = [];
        let totalPrice = checkList && checkList.totalPrice;
        let order_id = (typeof checkList.order_id !== "undefined") && checkList.order_id !== 0 ? checkList.order_id : 0;
        let status = get_order_status;
        let storeCredit = checkoutList ? checkoutList.StoreCredit : 0;
        let paidAmount = 0;
        let managerData = JSON.parse(localStorage.getItem('user'));
        let manager_name = '';
        var ListItem = null;
        let discountIs = 0;

        let oliver_order_payments = this.getOrderPayments(order_id);

        if (typeof oliver_order_payments !== "undefined") {
            oliver_order_payments.map(items => {
                paidAmount += parseFloat(items.payment_amount);
                order_payments.push({
                    "Id": (typeof items.Id !== "undefined") ? items.Id : 0,
                    "amount": items.payment_amount,
                    "payment_type": "order",
                    "type": items.payment_type,
                    "description": (typeof items.Id !== "undefined") ? items.description : ''
                })
            })
        }

        //console.log("Place_Order 1", checkList && checkList.ListItem)
        ListItem = checkList && checkList.ListItem ;
        discountIs = checkList && checkList.discountCalculated ? getDiscountAmount(ListItem) : 0;
        //alert(discountIs)
        getCheckoutList(ListItem)
        ListItem.map(items=>{
            if (items.product_id != null) {
                newList.push({
                    line_item_id: items.line_item_id,
                    name: items.Title,
                    product_id: (typeof items.product_id !== "undefined") ? items.product_id : items.WPID,
                    variation_id: items.variation_id,
                    quantity: items.quantity,
                    subtotal: items.subtotal?items.subtotal:items.subtotalPrice,
                    subtotal_tax: items.subtotal_tax?items.subtotal_tax:items.subtotaltax,
                    total: items.total?items.total:items.totalPrice,
                    total_tax: items.total_tax? items.total_tax:items.totaltax,
                    isTaxable: items.isTaxable,
                    ticket_info:items.ticket_info,
                    is_ticket : items.ticket_info && items.ticket_info.length>0?true :false
                })
             
            }

         // console.log("newList",newList);

            if (items.product_id == null) {
                if ((typeof items.Price !== 'undefined') && items.Price !== null) {
                    order_custom_fee.push({
                        amount: items.Price,
                        note: items.Title,
                        fee_id: ''

                    })
                } else {
                    order_notes.push({
                        note_id: '',
                        note: items.Title
                    })
                }
            }
        })

        // checkList && checkList.ListItem.map(items => {
        //     if (items.product_id != null) {
        //         newList.push({
        //             line_item_id: items.line_item_id,
        //             name: items.Title,
        //             product_id: (typeof items.product_id !== "undefined") ? items.product_id : items.WPID,
        //             variation_id: items.variation_id,
        //             quantity: items.quantity,
        //             subtotal: (typeof items.subtotalPrice !== "undefined") ? items.subtotalPrice : items.Price,
        //             subtotal_tax: (typeof items.subtotaltax !== "undefined") ? items.subtotaltax : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100),
        //             //total : (typeof items.totalPrice !== "undefined") ? items.totalPrice : items.Price,
        //             total: (typeof items.totalPrice !== "undefined") ? items.totalPrice : ((typeof items.after_discount !== "undefined") && items.after_discount !== 0 ? items.after_discount : items.Price),
        //             //total_tax : (typeof items.totaltax !== "undefined") ? items.subtotaltax : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100),
        //             total_tax: (typeof items.totaltax !== "undefined") ? items.subtotaltax : ((typeof items.after_discount !== "undefined") && items.after_discount !== 0 ? parseFloat(items.after_discount) * (parseFloat(checkList.TaxRate) / 100) : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100)),
        //             isTaxable: items.isTaxable
        //         })
        //     }
        //     if (items.product_id == null) {
        //         if ((typeof items.Price !== 'undefined') && items.Price !== null) {
        //             order_custom_fee.push({
        //                 amount: items.Price,
        //                 note: items.Title,
        //                 fee_id: ''

        //             })
        //         } else {
        //             order_notes.push({
        //                 note_id: '',
        //                 note: items.Title
        //             })
        //         }
        //     }
        // })

        if (this.state.set_order_notes.length !== 0) {
            let note = this.state.set_order_notes[0]
            order_notes.push({
                note_id: '',
                note: note.note
            })
        }



        if ( managerData && managerData.display_name !== " " && managerData.display_name !== 'undefined') {
            manager_name = managerData.display_name;
        } else {
            manager_name = managerData.user_email
        }

        var loginUser=JSON.parse(localStorage.getItem("user"));
           var CustomerDetail= JSON.parse(localStorage.getItem('AdCusDetail'));
           //console.log("CustomerDetail",CustomerDetail);
        place_order = {
            order_id: order_id,
            status: status,
            customer_note: user_notes ? user_notes : 'Add Note',
            customer_id: user_id ? user_id : 0,
            order_tax: checkList && checkList.tax ? checkList.tax : 0,
            order_total: (status == 'park_sale' || status == 'lay_away') ? checkList && checkList.totalPrice : checkList && parseFloat(checkList.totalPrice + cash_round).toFixed(2),
            //order_discount: checkList && checkList.discountCalculated ? checkList.discountCalculated : 0,
            order_discount:discountIs,
            tax_id: checkList && checkList.TaxId,
            line_items: newList,
            customer_email:CustomerDetail?CustomerDetail.Content.Email:'',
            billing_address: [{
                first_name: user_fname ? user_fname : '',
                last_name: user_lname ? user_lname : '',
                company: "",
                email: user_email ? user_email : '',
                phone: user_contact ? user_contact : '',
                address_1: loginUser ? loginUser.shop_address1: "", // user_address ? user_address : '',
                address_2: loginUser ? loginUser.shop_address2:"", // user_address ? user_address : '',
                city: loginUser ? loginUser.shop_city:"", //  user_city ? user_city : '',
                state: loginUser ? loginUser.shop_state:"", //  "",
                postcode:  loginUser ? loginUser.shop_postcode:"", //  user_pcode ? user_pcode : '',
                country: loginUser ? loginUser.shop_country_full_Name:""
            }],
            
            shipping_address: [{
                first_name: CustomerDetail ? CustomerDetail.Content.Shipping_FirstName:"", // user_fname ? user_fname : '',
                last_name: CustomerDetail ? CustomerDetail.Content.Shipping_LastName:"", //  user_lname ? user_lname : '',
                company: "",
                email:   user_email ? user_email : '',
                phone: user_contact ? user_contact : '',
                address_1:   CustomerDetail ? CustomerDetail.Content.Shipping_StreetAddress:"", // CustomerDetail. user_address ? user_address : '',
                address_2: CustomerDetail ? CustomerDetail.Content.Shipping_StreetAddress2:"", //  user_address ? user_address : '',
                city: CustomerDetail ? CustomerDetail.Content.Shipping_City:"", // user_city ? user_city : '',
                state:  CustomerDetail ? CustomerDetail.Content.Shipping_State:"", // ,
                postcode:  CustomerDetail ? CustomerDetail.Content.Shipping_Pincode:"",  // user_pcode ? user_pcode : '',
                country:  CustomerDetail ? CustomerDetail.Content.Shipping_Country:"", // 
            }],
            order_custom_fee: order_custom_fee,
            // order_notes: [add_note],
            order_notes: (typeof order_notes !== "undefined") && order_notes.length > 0 ? order_notes : [],
            order_payments: order_payments,
            order_meta: [{
                manager_id: JSON.parse(localStorage.getItem('user')).user_id,
                manager_name: manager_name,
                location_id: location_id,
                register_id: localStorage.getItem('register'),
                cash_rounding: cash_round,
                refund_cash_rounding: 0
            }],
            Udid: UDID
        }

        //console.log("Place_Order",JSON.stringify(place_order))
        //console.log("Place_Order", place_order)
      //  localStorage.setItem('placedOrderList', JSON.stringify(newList))



        if (status.includes("park_sale") || status.includes("lay_away")) {
            dispatch(checkoutActions.save(place_order, 2));
        } else {
            dispatch(checkoutActions.save(place_order, 1));
        }

    }


    cancel_VoidSale() {
        const { checkList, UDID } = this.state
        let order_id = (typeof checkList.order_id !== "undefined") ? checkList.order_id : 0;
        this.props.dispatch(checkoutActions.orderToVoid(order_id, UDID))
    }

    componentWillReceiveProps(prevProps) {
        console.log("shop_order error part" , prevProps)
        if (prevProps.err && prevProps.err.error == "") {
            this.setState({ isShowLoader: false })
            this.extraPayAmount("Service issue!, please contact to admin.");
        }
        let AdCusDetail = prevProps.single_cutomer_list && prevProps.single_cutomer_list.Content
        if ((typeof AdCusDetail !== 'undefined') && AdCusDetail != null) {
            let checkoutList = AdCusDetail;
            if (checkoutList != null) {
                this.setState({
                    user_fname: checkoutList.FirstName ? checkoutList.FirstName : '',
                    user_lname: checkoutList.LastName,
                    user_address: checkoutList.StreetAddress,
                    user_city: checkoutList.City,
                    user_contact: checkoutList.Phone,
                    user_notes: checkoutList.Notes,
                    user_email: checkoutList.Email,
                    user_pcode: checkoutList.Pincode,
                    user_id: checkoutList.Id,
                })
                this.state.user_id = checkoutList.Id;
                if (this.state.edit_status == true) {
                    $('#edit-info-checkout').modal('show');
                }
            }
        }
    }

    closeModal() {
        this.setState({ 
            isShowLoader: false,
            paidAmount:''
         })
        this.edit_status(false)


    }

    extraPayAmount(msg) {
        this.setState({ msg: msg })
        $('#exrta_pay_amount').modal('show');
    }

    closeExtraPayModal() {
        this.setState({ msg: '' })
    }

    freezScreen() {
        $('.disabled_popup_close').modal({
            backdrop: 'static',
            keyboard: false
        })
    }

    statusUpdate(text){
        console.log("text",text)
        this.setState({isShowLoader:false})
       
     }

    render() {
        const { isShowLoader, store_credit, total_price, change_amount, after_payment_is, cash_payment, checkList, paying_amount, paying_type, UDID, user_id, user_address, user_city, user_contact, user_email, user_fname, user_lname, user_notes, user_pcode , cash_round} = this.state;
        const { shop_order } = this.props;
       console.log("checkList" , isShowLoader)

        return (
            <div>
                <div className="wrapper">
                    <div id="content">
                        {isShowLoader == true ? <LoadingModal /> : ''}
                        <CommonHeaderFirst {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <CheckoutViewFirst onRef={ref => (this.orderCart = ref)} checkList={checkList} paying_amount={paying_amount} paying_type={paying_type} setCalculatorRemainingprice={this.setCalculatorRemainingprice} cash_round={cash_round} />
                                <div className="col-lg-9 col-sm-8 col-xs-8">
                                    <div className="row">
                                        <CheckoutViewSecond paymentType={this.paymentType} extraPayAmount={this.extraPayAmount} edit_status={this.edit_status} {...this.props} addPayment={this.layAwayPayment} getPayment={this.getPayment} orderPopup={this.openOrderPopup} setOrderPartialPayments={this.setOrderPartialPayments} />
                                        <CheckoutViewThird storeCredit={store_credit} paymentType={this.paymentType} extraPayAmount={this.extraPayAmount} onRef={ref => (this.orderPayments = ref)} {...this.props} checkList={checkList} addPayment={this.getPayment} setOrderPartialPayments={this.setOrderPartialPayments} orderPopup={this.openOrderPopup} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="edit-info-checkout" className="modal modal-wide fade full_height_one">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" onClick={() => this.closeModal()} data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Edit Info</h4>
                            </div>
                            <div className="modal-body overflowscroll">
                                <form className="clearfix form_editinfo">
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    First Name
                                                 </div>
                                                <input className="form-control" value={user_fname ? user_fname : ''} id="user_fname" name="user_fname" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Last Name
                                                  </div>
                                                <input className="form-control" value={user_lname ? user_lname : ''} id="user_lname" name="user_lname" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Email
                                                  </div>
                                                <input className="form-control" value={user_email ? user_email : ''} id="user_email" name="user_email" type="text" onChange={this.handleChange} disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Phone Number
                                                 </div>
                                                <input className="form-control" value={user_contact ? user_contact : ''} id="user_contact" name="user_contact" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Street Address&nbsp;
                                                 </div>
                                                <input className="form-control" value={user_address ? user_address : ''} id="user_address" name="user_address" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    City&nbsp;&nbsp;
                                                 </div>
                                                <input className="form-control" id="user_city" value={user_city ? user_city : ''} name="user_city" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Post Code
                                                </div>
                                                <input className="form-control" id="user_pcode" name="user_pcode" value={user_pcode ? user_pcode : ''} type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Notes
                                                </div>
                                                <textarea className="form-control" id="user_notes" value={user_notes ? user_notes : ''} name="user_notes" type="text" onChange={this.handleChange}>Testing</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" onClick={this.handleSubmit} className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/* this modal used for showing park sale note */}
                <div tabIndex="-1" className="modal fade in mt-5 modal-wide addnoctehere" id="park-sale-and-layaway-modal" data-status="park_sale" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header bb-0">
                                <button type="button" className="close opacity-1 mt-1" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h1 className="modal-title" id="park-sale-and-layaway-modal-title">Add Note</h1>
                            </div>
                            <div className="modal-body p-0">
                                <textarea className="form-control modal-txtArea" id="park-sale-and-layaway-modal-note" placeholder="Enter Note Here" ></textarea>
                            </div>
                            <div className="modal-footer p-0 b-0">
                                {(typeof shop_order === 'undefined') && shop_order == null && shop_order !== true ?
                                    <button onClick={() => this.placeParkLayAwayOrder()} type="button" className="btn btn-primary btn-block btn-primary-cus pt-2 pb-2">SAVE & CLOSE</button>
                                    :
                                    <button disabled type="button" className="btn btn-primary btn-block btn-primary-cus pt-2 pb-2">SAVE & CLOSE</button>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {/* this modal used for showing park sale note */}

                <div id="popup_cash_rounding" className="modal modal-wide modal-wide1 cancle_payment disabled_popup_close">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" onClick={() => this.closeModal()} className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Cash</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 className="popup_payment_error_msg" id="popup_cash_rounding_error_msg">
                                    <div className="row font18 mb-3">
                                        <div className="col-sm-6 text-left">
                                            Total
                                       <span className="pull-right">: </span>
                                        </div>
                                        <div className="col-sm-6"><NumberFormat value={total_price == 0 ? checkList && parseFloat(checkList.totalPrice + cash_round).toFixed(2) - after_payment_is : total_price - after_payment_is} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> </div>
                                    </div>
                                    <hr />
                                    {/* <div className="row font18 mb-3" style={{ display: 'none' }}>
                                        <div className="col-sm-6 text-left">Payment (Cash Rounding)
                                       <span className="pull-right">:</span>
                                        </div>
                                        <div className="col-sm-6">{0.05}</div>
                                    </div> */}
                                    <div className="row font18">
                                        <div className="col-sm-6 text-left">Payment (Cash)
                                          <span className="pull-right">:</span>
                                        </div>
                                        <div className="col-sm-6"><NumberFormat value={parseFloat(cash_payment).toFixed(2)} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></div>
                                    </div> <hr />
                                    <div className="row font18">
                                        <div className="col-sm-6 text-left">Change
                                    <span className="pull-right">:</span>
                                        </div>
                                        <div className="col-sm-6"><NumberFormat value={parseFloat(change_amount).toFixed(2)} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></div>
                                    </div>
                                    <hr />
                                    <div className="row font18">
                                        <div className="col-sm-6 text-left">Balance
                                           <span className="pull-right">:</span>
                                        </div>
                                        <div className="col-sm-6" id="balance_left"> <NumberFormat value='0.00' displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></div>
                                    </div>
                                </h3>
                            </div>
                            <div className="modal-footer" style={{ borderTop: 0 }} onClick={() => this.finalAdd()}>
                                <button type="button" className="btn btn-primary btn-block h66" style={{ height: 70 }} data-dismiss="modal" aria-hidden="true" id="popup_cash_rounding_button">Add Payment</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="StoreCredit" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="../assets/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">Store Credit Balance is $0.00
                           </h3>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div tabIndex="-1" className="modal fade in mt-5 modal-wide" id="addnotehere" role="dialog">
                    <CustomerNote />
                </div>
                <div tabIndex="-1" id="popup_oliver_add_fee" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                    <CustomerAddFee />
                </div>

                <div id="exrta_pay_amount" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="../assets/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">{this.state.msg !== '' ? this.state.msg : ''}</h3>
                            </div>
                            <div className="modal-footer p-0" onClick={this.closeExtraPayModal}>
                                <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="popup_void_sale" className="modal modal-wide modal-wide1 cancle_payment fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title"></h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 className="popup_payment_error_msg">This action will cancel the sale, do you want to proceed?</h3>
                            </div>
                            <div className="modal-footer" style={{ borderTop: 0 }}>
                                <button type="button" className="btn btn-primary btn-block h66" style={{ backgroundColor: '#d43f3a', borderRadius: 5, height: 70, borderColor: '#d43f3a' }} id="addDiscount" data-dismiss="modal" aria-hidden="true" onClick={() => this.cancel_VoidSale()}>Void Sale</button>
                            </div>
                        </div>
                    </div>
                </div>
              <OrderNotCreatePopupModel             statusUpdate={(text)=>{this.statusUpdate(text)}} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { checkoutlist, checkout_list, shop_order, single_cutomer_list, cash_rounding, global_payment } = state;
    return {
        checkoutlist,
        checkout_list: checkout_list.items,
        shop_order: shop_order.loading,
        err: shop_order,
        single_cutomer_list: single_cutomer_list.items,
        cash_rounding: cash_rounding.items,
        global_payment: global_payment.items
    };
}
const connectedCheckoutView = connect(mapStateToProps)(CheckoutView);
export { connectedCheckoutView as CheckoutView };

