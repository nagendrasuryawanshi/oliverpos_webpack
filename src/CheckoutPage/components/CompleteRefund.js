import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../../_actions'
import moment from 'moment';
import { sendMailService } from '../../_services/sendMail.service'
import { saveCustomerInOrderAction } from '../../_actions/saveCustomerInOrder.action'
import { sendMailAction } from '../../_actions/sendMail.action'
import { get_UDid } from '../../ALL_localstorage'
import { checkoutActions } from '../actions/checkout.action'
import { LoadingModal, PrintPage } from '../../_components';
import { TicketEventPrint } from '../components/TicketEventPrint'
import Config from '../../Config'
import { SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG } from 'constants';

const PrintElem = (checkList, orderList, inovice_Id, manager, register, address, site_name,order_reciept) => {
         console.log("orderList",orderList,"manager",manager,"order_reciept",order_reciept,"checkList",checkList)
    checkList.order_date ? checkList.order_date : moment().format(Config.key.DATETIME_FORMAT)

    var gmtDateTime = order_reciept.ShowTime == true && checkList.order_date ? moment.utc(checkList.order_date).local().format(Config.key.DATETIME_FORMAT) : moment().format(Config.key.DATE_FORMAT)
    var payments=""
    orderList && orderList.map((item, index) =>{
       
        payments += (payments!=""?",":"")+ item.payment_type;
    })

    let baseurl=order_reciept.CompanyLogo?Config.key.RECIEPT_IMAGE_DOMAIN+order_reciept.CompanyLogo:'';
    let barcode_image=order_reciept.AddBarcode == true ?  Config.key.RECIEPT_IMAGE_DOMAIN+"/Content/img/ic_barcode.svg":''
       console.log("baseurl",baseurl,"manager.shop_name",manager.shop_name);
    var mywindow = window.open('#', 'my div', 'width=600');
    // ${manager.shop_name?manager.shop_name:''}

    // <img src=${baseurl} width="50px" > 
    mywindow.document.write(`<html><head><meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title  align="left"></title>
    
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <style>
    @media screen {
         @font-face {
           font-family: 'Lato';
           font-style: normal;
           font-weight: 400;
           src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
         }

   body {
           font-family: "Lato", "Lucida Grande", "Lucida Sans Unicode", Tahoma, Sans-Serif;
         }

       }
       .demo-print {
         /*background-color: #edeff4;*/
        // padding: 15px 20px;
 
         border-top-left-radius: 5px;
         font-size: 18px;
         line-height: 25px;
         width: 420px;
         margin: auto;
         height: 99.7%;
      
     }
     .demo-print-body {
         position: relative;
         // padding-bottom: 25px;
         background: #fff;
         border-bottom: 0px;
     }
     .demo-print-content {
         padding: 22px;
         border: 1px solid #cecece;
        
         border-top-right-radius: 4px;
         border-top-left-radius: 4px;
     }
    
     .print-logo {
       border-radius: 5px;
       border: 1px solid #cacaca;
       text-align: center; 
       height: 200px;
       overflow: hidden;
       display: flex;
       width: 100%;
       justify-content: center;
       align-items: center;
       padding: 10px 0px;
   }
   .print-logo img {
     height: 100%;
   }
   .print-logo h4 {
       color: #979797;
       font-weight: 600; 
       padding: 0px;
       margin: 0px;
   }
     .list {
         color: #979797;
         margin: 10px 0px;
     }
     .demo-hint-text {
         border-top: 1px solid #979797;
         text-align: center;
         padding: 8px 0px 0px;
         margin-top: 0px;
         margin-bottom: 5px;
         color: #979797;
     }
     .printer-text {
         color: #979797;
         font-size: 18px;
         font-weight: 300;
         text-align: center;
     }
     .printer-text .shop-url {
         font-weight: 800
     }
     .adm-text-area{
         color: #535353;
         font-size: 20px;
         font-weight: 400;
         line-height: 24px;
         border-radius: 5px;
         border: 1px solid #cacaca;
     }
     .adm-text-area::placeholder { opacity: 0.5;}
     .printer-table table {
         margin-bottom: 0px;
         width: 100%;
         border-collapse: collapse;
     }
     .printer-table table tr td {
         padding: 0px;
         color: #979797;
         border-color: #979797 !important;
         font-weight: normal;
     }
     .printer-table table tr td:first-child {
         padding-right: 10px;
     }
     .font-bold {
         font-weight: bold !important;
     }
     .printer-table table tbody tr td,
     .printer-table table tfoot tr td {
         border-top: 0px;
     }
     .printer-table table tbody tr:first-child td {
         border-top: 1px solid #979797;
     }
     .printer-table table tfoot tr:first-child td,
     .printer-table table tfoot tr:last-child td {
         border-top: 1px solid #979797;
     }
     .printer-table table tfoot tr td:first-child {
         padding-left: 20px !important;;
     }
     .printer-table table tfoot tr td:last-child,
     .printer-table table tbody tr td:last-child {
       text-align: right !important;
   }
     .print-ticket-info {
         margin-top: 10px;
         margin-bottom: 5px;
     }
     .printer-table table tfoot tr:first-child td .print-ticket-info
     {
         margin-bottom: 10px;
     }
     .print-ticket-info p {
       padding: 1px
     }
     .shop-url {
       margin-top: 0px;
       margin-bottom: 5px;
     }
     .list p {
       margin: 0px;
       margin-bottom: 3px;
   }
   .print-ticket-info p {
     margin: 0px;
 }

     
   </style></title>`);
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write(`</head><body>`);
    mywindow.document.write(
        `
  <!-- open bracket -->
    <div class="demo-print" id="page-sidebar-right">
       
    <div class="demo-print-body">
      
          <div class="demo-print-content">
         
            <!-- open bracket -->
                
            <div class="print-logo"> 
            
            ${ baseurl ? ` <img src=${baseurl}   />`:'' }
                 
                 ${!baseurl && manager.shop_name ?`<h4>  ${ manager.shop_name}</h4>`: '' }
               
              
              </div>
           
            <div class="list">
             
              <p>${order_reciept?order_reciept.DateText:'DateTime'}:${gmtDateTime?gmtDateTime:''} </p>
              <p>${order_reciept?order_reciept.Sale:'Sale#'}:${inovice_Id?inovice_Id:''} </p>
              ${order_reciept.ShowServedBy == true ?
                `<p>${order_reciept?order_reciept.ServedBy:'Served By'}: ${manager.display_name?manager.display_name:''}</p>`
                :''}
              <p> ${order_reciept?order_reciept.TaxIdText:'Tax ID'}:${ order_reciept.TaxId? order_reciept.TaxId:''}</p>         
             
               <p>${order_reciept?order_reciept.PaidWith:'Paid With'}:${payments?payments:''}</p>
           </div>
           <h4 class="demo-hint-text">
              ${order_reciept?order_reciept.CustomText:''}
            </h4>
            <div class="printer-table">

              <table class="table table-border">
              <tbody>
                ${checkList && checkList.ListItem.map((item,index) =>
                    { 
                        return(`<tr>
                        <td ><p>${item.qunatity ? item.qunatity : index + 1}</p></td> 
                        <td> <div class="print-ticket-info">
                        <p>${item.Title?item.Title:''}
                        </p></div></td>
                        <td class="font-bold" align="right">
                        ${item.discount_amount == 0 ? item.Price.toFixed(2) : (item.Price - item.discount_amount).toFixed(2)}
                        </td></tr>`)
                    }
                )}
              </tbody>

             <tfoot>
                <tr>
                    <td colspan='2' align="left">
                        ${order_reciept.SubTotal?order_reciept.SubTotal:''}
                    </td>
                    <td style="text-align:right">
                    ${checkList.subTotal?(checkList.subTotal).toFixed(2):''}

                    </td>
                </tr>
                <tr>
                    <td colspan='2'  align="left">
                        ${order_reciept.Discount?order_reciept.Discount:''}
                    </td>
                    <td style="text-align:right">
                    ${checkList.discountCalculated? checkList.discountCalculated.toFixed(2):0.00 }  
                    </td>
                </tr>
                <tr>
                    <td colspan='2' align="left">
                        ${order_reciept.Tax?order_reciept.Tax:''}
   
                    </td>
                    <td style="text-align:right">
                    ${checkList.tax?(checkList.tax).toFixed(2):''}
                    </td>
                </tr>
                <tr class="bt-top">
                    <td colspan='2' align="left">
                      ${order_reciept.Total?order_reciept.Total:''}   
                    </td>
                    <td style="text-align:right">
                    ${checkList.totalPrice?(checkList.totalPrice).toFixed(2):''}
                    </td>
                </tr>
                </tfoot>

              </table>
            </div>
            <h4 class="demo-hint-text">
              ${order_reciept?order_reciept.Returnpolicy:''}
            </h4>
            <div class="printer-text">

           <div class="printer-barcode">
                    <img src=${barcode_image}  />

                 </div>
              <p class="shop-url">
              ${site_name?site_name:''}
                <address>
                ${address.Address1?address.Address1:''} ${address.Address2?address.Address2:''} 
                ${address.Zip?address.Zip:''} ${address.City?address.City:''} ${address.CountryName?address.CountryName:''}
                </address>
              </p>
             
            </div>
           
            <!-- close bracket -->
         
          </div>
       
         </div> 
    </div>
    <!-- close bracket -->     
    `

    );
    mywindow.document.write('</body style="text-align: center;></html>');

    mywindow.print();
    mywindow.close();

    return true;
}



class CompleteRefund extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checkList: localStorage.getItem('CHECKLIST') ? JSON.parse(localStorage.getItem('CHECKLIST')) : null,
            CancelOrder: false,
            mailsucces: null,
            tick_event_status:false,
            IsEmailExist:true,
            valiedEmail:true

        }

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount() {
        console.log(".print_receipt_on_sale_complete",JSON.parse(localStorage.getItem("user")))

        console.log("componentDidMount123");
        setTimeout(function () {

            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 2000);
        let event_array=[]
        let address;
        let site_name;
        let inovice_Id = localStorage.getItem("ORDER_ID") && JSON.parse(localStorage.getItem("ORDER_ID")).Content;
        let manager = localStorage.getItem('user') && JSON.parse(localStorage.getItem('user'));
        let register = localStorage.getItem('registerName')
        let register_id = localStorage.getItem('register')
        let location_name = localStorage.getItem('UserLocations') && JSON.parse(localStorage.getItem('UserLocations'));
        let decodedString =  localStorage.getItem('sitelist')? localStorage.getItem('sitelist'):"";

        let decod = decodedString !=="" ?window.atob(decodedString):"";
        let siteName = decod  && decod!==""? JSON.parse(decod):"";
        let udid = get_UDid('UDID');
         console.log("silte list name" , siteName )
        // console.log("uidid" , manager)
        if(siteName && siteName !==""){
        siteName && siteName.map(site => {
            if (site.UDID == udid) {
                site_name = site.StatusOpt && site.StatusOpt.instance
            }
        })
    }
            location_name && location_name.map(item => {
            if (item.Id == register_id) {
                address = item;
            }
        })
        let order_reciept=localStorage.getItem('orderreciept') ?JSON.parse(localStorage.getItem('orderreciept')):"";
        let checkList = localStorage.getItem('CHECKLIST') ?JSON.parse(localStorage.getItem('CHECKLIST')):"";
        let orderList = localStorage.getItem('oliver_order_payments') ? JSON.parse(localStorage.getItem('oliver_order_payments')):"";
        console.log("orderList",orderList)
        // let data = {
        //     list: checkList,
        //     paymentType: orderList,
        //     inovice_Id: inovice_Id,
        //     shopName: manager.shop_name,
        //     address: address.Address1 + ", " + address.City + ", " + address.CountryName + " ," + address.Zip,
        //     site_name: site_name,
        //     display_name: manager.display_name
        // }
          var checkPrintreciept=localStorage.getItem("user") && localStorage.getItem("user") !=='' ?JSON.parse(localStorage.getItem("user")).print_receipt_on_sale_complete:''

        if (checkPrintreciept && checkPrintreciept == true) {
   
        if (inovice_Id) {
            setTimeout(function () {
                //  <PrintPage 
                //      list={checkList} 
                //      paymentType={orderList} 
                //      inovice_Id={inovice_Id} 
                //      shopName={manager.shop_name}
                //      address={address.Address1+", " +address.City +", "+ address.CountryName +" ,"+address.Zip}
                //      site_name={site_name}
                //      display_name={manager.display_name}
                //     />
               
       PrintElem(checkList, orderList, inovice_Id, manager, register, address, site_name,order_reciept)
               //PrintPage.PrintElem(data)
            }, 1000)
        }
    }

        console.log("inovice_Id",checkList,orderList);
        // checkList &&  checkList.ListItem &&  checkList.ListItem.map(item=>{
        //       console.log("ListItem",item);
             
        //       if(item.ticket_status== true && item.tick_event_id ){
     
        //       this.props.dispatch(checkoutActions.Get_TicketEvent('1'));
        //     }
        // })
        if (inovice_Id) {
    
        const requestOptions = {
            method: 'GET',
            headers: {
                "access-control-allow-origin": "*",
                "access-control-allow-credentials": "true", 
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
            } 
            ,mode: 'cors'
         };
        const API_URL=Config.key.OP_API_URL

    //console.log("shop_order",shop_order);
        return fetch(`${API_URL}/ShopOrders/GetTicketDetails?udid=${udid}&OrderNumber=${inovice_Id}`, requestOptions)
         .then(response => {
  
            if (response.ok) { return response.json(); }
            throw new Error(response.statusText) 
       })
       .then(function handleData(data) {
        var EventDetailArr=[]
           data.Content && data.Content.map(event=>{
               
            var pp=JSON.parse(event)

                 EventDetailArr.push(pp)
           // console.log("eventdata1",data.Content,pp)

            //   TicketEventPrint.EventPrintElem(EventDetailArr)
           
          })
          if(data.Content &&  data.Content !=''){
            //process.env.ENVIRONMENT =='devtickera' &&  JSON.parse(localStorage.getItem("user")).print_receipt_on_sale_complete == true
            if(checkPrintreciept && checkPrintreciept == true){
            console.log("eventdata",EventDetailArr)
            var ServedBy=''
            TicketEventPrint.EventPrintElem(EventDetailArr,orderList, manager, register, address, site_name,ServedBy='',inovice_Id)
            }
          }
       
        }).catch(function handleError(error) {
            // handle errors here
            console.log("Error",error);

        })
    }
//         const requestOptions = {
//             method: 'GET',
//             headers: {
//                 "access-control-allow-origin": "*",
//                 "access-control-allow-credentials": "true", 
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json',
//                 'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
//             } 
//             ,mode: 'cors'
//          };
//              //-------------------- get Mutiple Event  Detail----------------------------------------------------
//          var eventArr=[]
       
//          checkList &&  checkList.ListItem.map(Orderitem=>{
//          console.log("Orderitem",Orderitem)           
//                 console.log("event",event)
//                 eventArr.push(  {event_id:Orderitem.tick_event_id ,ticket_info:Orderitem.ticket_info })
               
//          })

// console.log("eventArr",eventArr)

        
//              const API_URL=Config.key.OP_API_URL
//              var EventDetailArr=[]
//               for ( var i = 0; i <eventArr.length; i++) {
//                 (function (x) {
//                         var event=eventArr[i];
//                         console.log("EventInfo",event );
//                 // setTimeout(function () {
//                     if(event.event_id && event.event_id !="")
//                     {
//                     fetch(`${API_URL}/ShopTickera/GetEventByEventId?udid=${udid}&event_id=${event.event_id}`, requestOptions)
//                         .then(response => {
                          
//                             if (response.ok) { return response.json(); }
//                             throw new Error(response.statusText)  // throw an error if there's something wrong with the response

//                         })
//                         .then(function handleData(data) {
//                             console.log("eventdata",data.Content)
//                             event['event_info']=data.Content;
//                           //  var eventdata=[];

//                             EventDetailArr.push(event);
//                             console.log("eventdata",EventDetailArr)                        
//                            // EventDetailArr= [...new Set([...EventDetailArr, ...eventdata])];   
//                             console.log("EventDetailArr",EventDetailArr.length, EventDetailArr);
//                             if ( EventDetailArr.length>0 && eventArr.length== EventDetailArr.length && process.env.ENVIRONMENT =='devtickera') {                             
                           
//                                    TicketEventPrint.EventPrintElem(EventDetailArr)
//                             }
                
                           
//                         })
//                         .catch(function handleError(error) {
//                             // handle errors here
//                             console.log("Error",error);

//                         })
//                     }
//                     // }, 1000);
//                 })(i);
//             }

           
     
        // this.setState({
        //     checkList: checkList,
        //     inovice_Id: inovice_Id,
        // })

    }
    CancelSale() {
        // alert("c") 
        this.setState({ CancelOrder: true });
        let udid = get_UDid('UDID');
        let Order_Id = JSON.parse(localStorage.getItem("ORDER_ID")).Content;
        this.props.dispatch(checkoutActions.orderToCancelledSale(Order_Id, udid));



    }
    clear() {
        localStorage.removeItem('ORDER_ID');
        localStorage.removeItem('CHECKLIST');
        localStorage.removeItem('oliver_order_payments');
        localStorage.removeItem('AdCusDetail');
        const { dispatch } = this.props;
        localStorage.removeItem('CARD_PRODUCT_LIST');
        localStorage.removeItem("CART")
        localStorage.removeItem("SINGLE_PRODUCT")
        localStorage.removeItem("PRODUCT")
        dispatch(cartProductActions.addtoCartProduct(null));
        window.location = '/shopview';
    }

    sendMail() {
        $(".suctext").css("display", "block");
        this.setState({ mailsucces: null });
        let udid = get_UDid('UDID');
        let order_id = $("#order-id").val();
        let email_id = $("#customer-email").val();
      
      
        let requestData = {
            "Udid": udid,
            "OrderNo": order_id,
            "EmailTo": email_id,
        }
        console.log("requestData",requestData);
        if( !email_id  || email_id =="")
        {
            this.setState({IsEmailExist:false}) 
        }
        else{
         
           console.log("EmailValidation",/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email_id))
            this.setState({IsEmailExist:true}) 
            if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email_id)){   
                this.setState({valiedEmail:true})   

            if ($(".checkmark").hasClass("isCheck")) {
                this.props.dispatch(saveCustomerInOrderAction.saveCustomerInOrder(udid, order_id, email_id));
            }
            this.props.dispatch(sendMailAction.sendMail(requestData));
            }else
            {
               // this.state.mailsucces =false;
                this.setState({valiedEmail:false})
            }
        }
    }

    handleInputChange() {
        $(".checkmark").toggleClass("isCheck");
    }

    componentWillReceiveProps(nextprops) {

        console.log("nextprops1233",nextprops);
        // console.log("checkList deep",nextprops.sendEmail.IsSuccess );

        if ((typeof nextprops.sendEmail !== 'undefined') && nextprops.sendEmail !== '') {
            this.setState({ mailsucces: nextprops.sendEmail ? nextprops.sendEmail.IsSuccess : null
            })
         if(nextprops.sendEmail && nextprops.sendEmail.IsSuccess == true ){
             setTimeout(
                 this.clear()
                 , 1000);
        }
    }
   if(nextprops.tick_event !== '' && (typeof nextprops.tick_event !== 'undefined') )  {

     // TicketEventPrint.EventPrintElem(nextprops.tick_event)
   }
  }
    render() {
      //  const { checkList, inovice_Id } = this.state;
     let checkList= localStorage.getItem('CHECKLIST') ? JSON.parse(localStorage.getItem('CHECKLIST')) : null;
      let inovice_Id = localStorage.getItem("ORDER_ID") ? JSON.parse(localStorage.getItem("ORDER_ID")).Content:null;
        console.log("this.state.mailsucces",this.state.mailsucces, this.props)
      

        let order_reciept=localStorage.getItem('orderreciept') ?JSON.parse(localStorage.getItem('orderreciept')):"";
        let baseurl=Config.key.RECIEPT_IMAGE_DOMAIN+order_reciept.CompanyLogo;
        let barcode_image= Config.key.RECIEPT_IMAGE_DOMAIN+"/Content/img/ic_barcode.svg"

        console.log("imageURL",baseurl)
      
        return (
            <div className="bgcolor1">
            <div style={{display: 'none' }}>
                <img src={baseurl} width="50px"  /> 
                </div>
             <div style={{display: 'none' }}>
                <img src={barcode_image} width="50px"  />

            </div> 
                <div className="content_main_wapper">
                    <div className="menu">
                        <div className="aerrow pull-left arrowText" onClick={() => this.CancelSale()}>
                            <a href="javascript:void(0)">
                                <div className="icon icon-backarrow-lftWhite" ></div>
                                Cancel Sale
                        </a>
                        </div>
                        <div className="aerrow pull-right arrowText" onClick={() => this.clear()}>
                            <a href="javascript:void(0)">
                                New Sale
                            <div className="icon icon-backarrow-rgtWhite"></div>
                            </a>
                        </div>
                    </div>
                    <div className="dialog-content">

                        {this.state.CancelOrder == true ? <LoadingModal /> : null}
                        <div className="loginBox" style={{ width: 411 }}>
                            <div className="login-form">
                                <div className="login-logo">
                                    <img src="assets/img/accepted.png" className="" />
                                </div>
                                <h2 className="chooseregister optima">Sale Complete</h2>
                                <form className="login-field" action="#">
                                    <div className="input-group">
                                        <span className="input-group-addon group-addon-custom" id="basic-addon1">Email Receipt</span>
                                        <input type="hidden" id="order-id" defaultValue={(typeof inovice_Id !== "undefined") ? inovice_Id : 0} />

                                        <input type="text" defaultValue={(checkList.customerDetail && checkList.customerDetail.Content && typeof checkList.customerDetail.Content.Email !== "undefined") ? checkList.customerDetail.Content.Email : null} className="form-control form-control-custom mt-0" id="customer-email" placeholder="example@gmail.com" aria-describedby="basic-addon1" />
                                    </div>
                                    <div className="checkBox-custom">
                                        <div className="checkbox">
                                            <label className="customcheckbox">Remember Customer
                                                  <input type="checkbox" defaultChecked />
                                                <span className={`checkmark checkUncheck isCheck`} onClick={() => this.handleInputChange()}></span>
                                            </label>
                                        </div>
                                    </div>
                                    <button type="button" className="btn btn-login bgcolor2" onClick={() => this.sendMail()}>Send Email Receipt</button>
                                </form>

                                <span className="suctext" style={{ display: "none" }}>
                                    {this.state.IsEmailExist==false ? 'please enter email': 
                                    this.state.valiedEmail==false? "Invalid email address" :                                   
                                    this.state.mailsucces == null ? "Please wait..." :                                    
                                    this.state.mailsucces && this.state.mailsucces == true ? 'Mail sent successfully'
                                        : this.state.mailsucces == false ?  'Mail sending failed' : ""
                                    }
                                </span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { shop_order, sendEmail,tick_event } = state;
    return {
        shop_order: shop_order.items,
        sendEmail: sendEmail.sendEmail,
        tick_event:tick_event.items
    };
}
const connectedCompleteRefund = connect(mapStateToProps)(CompleteRefund);
export { connectedCompleteRefund as CompleteRefund };