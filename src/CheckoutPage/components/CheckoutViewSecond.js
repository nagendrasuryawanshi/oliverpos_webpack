import React from 'react';
import { connect } from 'react-redux';
import { customerActions } from '../../CustomerPage/actions/customer.action'
import { encode_UDid, get_UDid } from '../../ALL_localstorage'

class CheckoutViewSecond extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checkList: [],
        }
        this.addPayment = this.addPayment.bind(this);
    }

    componentWillMount() {
        let checkList = localStorage.getItem('CHECKLIST');
        this.setState({ checkList: JSON.parse(checkList) })

    }

    Edit_Modal(id) {
        this.props.edit_status(true)
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID = get_UDid('UDID');
        this.props.dispatch(customerActions.getDetail(id, UID));
    }

    addPayment(type, store_credit) {
        let amount = 0;
        
        let calc_output =  $('#calc_output').val();
     //   console.log("amount is", calc_output, $('#my-input').val())
        if (calc_output !== '' &&  typeof calc_output !== 'undefined') {
            amount = $('#calc_output').val();
        } else {
            amount = $('#my-input').val();
        }


       //  console.log("amount" ,amount, type, store_credit )
        if (store_credit >= amount) {
            this.props.addPayment(type, store_credit);
            this.props.getPayment(type, amount);
            this.props.setOrderPartialPayments(amount, type)
        } else if (store_credit < amount) {
            this.props.extraPayAmount(`Store Credit Balance is $${parseFloat(store_credit).toFixed(2)}`)
        } else if (type == 'lay_away') {
            this.props.getPayment(type);
        }

    }

    layAwayOrder(status) {
        this.props.orderPopup(status);
    }

    render() {
        const { single_cutomer_list } = this.props;
        const { checkList } = this.state;
        let checkoutList = (typeof single_cutomer_list !== 'undefined') && single_cutomer_list.Content !== null ? single_cutomer_list.Content : checkList && checkList.customerDetail && checkList.customerDetail.Content
        return (
            <div className="col-lg-7 col-md-7 col-sm-6 col-xs-12 pt-4 plr-8">
                <div className={checkoutList && checkoutList.Email ? "items preson_info " : "items preson_info customer_disabled"}>
                    <div className="item-heading text-center font24">Customer Information</div>

                    <div className="panel panel-default panel-product-list" style={{ borderColor: '#979797' }}>
                        <div className="singleName clearfix">
                            <h4 className="mt-0 mb-2">
                                {checkoutList ? checkoutList.FirstName : ''}{" "}{checkoutList ? checkoutList.LastName : 'Customer Info'}
                                {checkoutList ? <a className="edit-info" onClick={() => this.Edit_Modal(checkoutList.Id)}>Edit <span className="hide_small">Information</span></a> : ""}
                            </h4>
                        </div>
                        <div className="overflowscroll" id="UserInfo_checkout">
                            <div className="personal_info">
                                <strong className="lead_personal">Personal Info:</strong>
                                <div className="mt-3 mb-3">
                                    <p className="clearfix">
                                        <span className="text-muted">Email:</span>
                                        <span className="text-danger"> {checkoutList ? checkoutList.Email : ''}</span>

                                    </p>
                                    <p className="clearfix">
                                        <span className="text-muted">Address:</span>
                                        <span className="text-danger"> {checkoutList ? checkoutList.StreetAddress : ''}</span>
                                    </p>
                                    <p className="clearfix">
                                        <span className="text-muted">Phone:</span>
                                        <span className="text-danger">{checkoutList ? checkoutList.Phone : ''}</span>
                                    </p>
                                    <p className="clearfix">
                                        <span className="text-muted">Notes:</span>
                                        <span className="text-danger">{checkoutList ? checkoutList.Notes : ''}</span>
                                    </p>
                                </div>
                            </div>
                            <div className="personal_info">
                                <strong className="lead_personal">Account Info:</strong>
                                <div className="mt-3 mb-3">
                                    {/* <p className="w-50-block text-center clearfix">
                                        <span className="text-muted w-100">Account Balance</span>
                                        <span className="text-danger w-100">{checkoutList ? checkoutList.AccountBalance : ''}</span>
                                    </p> */}
                                    <p className="clearfix">
                                        <span  className="text-muted">Store Credit:</span>
                                        <span className="text-danger">{checkoutList &&  checkoutList.StoreCredit !== '' && checkoutList.StoreCredit !== 0 ?checkoutList.StoreCredit : '0'}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-footer p-0 b-0 m-0 clearfix bg-white" id="userinfo_footer">
                            {checkoutList ?
                                <div className="personal_info w-75-block float-none mx-auto">
                                    <button id="test3" name="payments-type-2" onClick={() => { this.layAwayOrder("lay_away"); this.props.paymentType('lay_away'); }} className="white-background box-flex-shadow box-flex-border mb-2 round-8 no-outline w-100 p-0 overflow-0">
                                        <div style={{ borderColor: '#46A9D4' }} className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                            <div className="box-flex-text-heading">
                                                <h2>Lay-Away</h2>
                                            </div>
                                        </div>
                                    </button>

                                    <button className="white-background box-flex-shadow box-flex-border mb-2 round-8 no-outline w-100 p-0 overflow-0">
                                        {checkoutList && checkoutList.StoreCredit !== '' && checkoutList.StoreCredit !== 0 ?
                                            <div style={{ borderColor: '#46A9D4' }} id="test4" value='store-credit' name="payments-type-2" onClick={() => { this.addPayment("store-credit", checkoutList ? checkoutList.StoreCredit : ''); this.props.paymentType('store-credit'); }} className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                                <div className="box-flex-text-heading">
                                                    <h2>Store-Credit</h2>
                                                </div>
                                            </div>
                                            :
                                            <div style={{ borderColor: '#bfbfbf' }} disabled className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                                <div className="box-flex-text-heading">
                                                    <h2>Store-Credit</h2>
                                                </div>
                                            </div>
                                        }
                                    </button>
                                </div>
                                :
                                <div className="personal_info w-75-block float-none mx-auto">
                                    <button disabled className="white-background box-flex-shadow box-flex-border mb-2 round-8 no-outline w-100 p-0 overflow-0">
                                        <div style={{ borderColor: '#bfbfbf' }} className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                            <div className="box-flex-text-heading">
                                                <h2>Lay-Away</h2>
                                            </div>
                                        </div>
                                    </button>
                                    <button className="white-background box-flex-shadow box-flex-border mb-2 round-8 no-outline w-100 p-0 overflow-0">
                                        <div style={{ borderColor: '#bfbfbf' }} disabled className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                            <div className="box-flex-text-heading">
                                                <h2>Store-Credit</h2>
                                            </div>
                                        </div>
                                    </button>
                                </div>

                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { checkoutlist, checkout_list, single_cutomer_list } = state;
    return {
        checkoutlist,
        checkout_list: checkout_list.items,
        single_cutomer_list: single_cutomer_list.items
    };
}
const connectedCheckoutViewSecond = connect(mapStateToProps)(CheckoutViewSecond);
export { connectedCheckoutViewSecond as CheckoutViewSecond };