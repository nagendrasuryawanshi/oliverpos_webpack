import React from 'react';
import { connect } from 'react-redux';
import { default as NumberFormat } from 'react-number-format';
import { history } from '../../_helpers';
import { Markup } from 'interweave';

class CheckoutViewFirst extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            totalPrice: 0,
            payments: [],
            count: 0,
            paid_amount: 0
        }

        this.getPaymentDetails = this.getPaymentDetails.bind(this);
    }

    componentDidMount() {
        this.props.onRef(this);
        this.getPaymentDetails();


    }

    componentWillReceiveProps() {

        setTimeout(function () {
            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 500);
    }

    componentWillUnmount() {
        this.props.onRef(null)
    }

    getPaymentDetails() {
        let checkList = this.props.checkList;
        let paid_amount = 0.0;
        let payments = new Array();
        this.setState({
            totalPrice: checkList.totalPrice
        })

        if (localStorage.getItem("oliver_order_payments") && checkList && checkList.totalPrice) {
            let getPayments = (typeof JSON.parse(localStorage.getItem("oliver_order_payments")) !== "undefined") ? JSON.parse(localStorage.getItem("oliver_order_payments")) : new Array();
            getPayments.forEach(paid_payments => {
                paid_amount += parseFloat(paid_payments.payment_amount);
                payments.push({
                    "payment_type": paid_payments.payment_type,
                    "payment_amount": paid_payments.payment_amount,
                });
            });
            this.setState({
                totalPrice: checkList.totalPrice - paid_amount,
                payments: payments,
                count: payments.length,
                paid_amount: paid_amount
            })
            if (checkList.totalPrice >= paid_amount) {
                this.props.setCalculatorRemainingprice(this.state.paid_amount);
            } else {
                this.props.setCalculatorRemainingprice(0);
            }
        }
    }

    AddCustomer() {
        var param = 'checkout';
        var url = '/customerview';
        history.push(url, param)
        window.location = '/customerview'

    }

    RemoveCustomer() {
        localStorage.removeItem('AdCusDetail')
        location.reload();
        let list = localStorage.getItem('CHECKLIST') ? JSON.parse(localStorage.getItem('CHECKLIST')) : null;
        if (list != null) {
            const CheckoutList = {
                ListItem: list.ListItem,
                customerDetail: null,
                totalPrice: list.totalPrice,
                discountCalculated: list.discountCalculated,
                tax: list.tax,
                subTotal: list.subTotal,
                TaxId: list.TaxId
            }
            // const CheckoutList = {
            //     ListItem: list.ListItem,
            //     customerDetail: null,
            //     totalPrice: list.totalPrice,
            //     discountCalculated: list.discountCalculated,
            //     tax: list.taxAmount,
            //     subTotal: list.subTotal,
            //     TaxId: list.tax
           // }
            localStorage.setItem('CHECKLIST', JSON.stringify(CheckoutList))
        }
    }


    render() {
        const { checkList , cash_round } = this.props;
        const { payments, count } = this.state;
        return (
            <div className="col-lg-3 col-sm-4 col-xs-4 pl-0">
                <div className="panel panel-default panel-right-side r0 br-1 bb-0">
                    {checkList && checkList.customerDetail && checkList.customerDetail.Content ?
                        <div className="panel-heading pr-0">
                            {checkList.customerDetail.Content.FirstName ? checkList.customerDetail.Content.FirstName : checkList.customerDetail.Content.Email}{" "}{checkList.customerDetail.Content.LastName ? checkList.customerDetail.Content.LastName : null}
                            <span className="pull-right pointer pl-15 pr-15" onClick={() => this.RemoveCustomer()}>
                                <img src="assets/img/delete-32.png" className="" />
                            </span>
                        </div>
                        :
                        <div className="panel-heading pr-0">
                            Add Customer
                        <span className="pull-right pointer pl-15 pr-15" onClick={() => this.AddCustomer()}>
                                <img src="assets/img/add_32.png" className="" />
                            </span>
                        </div>
                    }
                    <div className="panel-body p-0 overflowscroll" id="cart_product_list">
                        <div className="table-responsive">
                            <table className="table ListViewCartProductTable">
                                <colgroup>
                                    <col width="10" />
                                    <col width="*" />
                                    <col width="*" />
                                </colgroup>
                                <tbody>

                                    {checkList && checkList.ListItem &&
                                        checkList.ListItem.map((product, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{product.quantity?product.quantity:1}</td>
                                                    <td> <Markup content={product.Title} /> {/*{product.Title } */}
                                                        <span className="comman_subtitle">{product.color}  {product.size ? ',' + product.size : null}</span>
                                                    </td>
                                                    {(typeof product.product_id !== 'undefined') ?
                                                        <td align="right">
                                                            {/* {product.discount_amount !== 0 ?
                                                                <NumberFormat value={product.after_discount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                                : ''}
                                                            <span className={product.discount_amount !== 0 ? "comman_delete" : ''}><NumberFormat value={product.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></span> */}
                                                             <span>{parseFloat(product.discount_amount) !== 0.00 ? <NumberFormat value={product.Price - product.discount_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> : null}</span>

<NumberFormat className={parseFloat(product.discount_amount) == 0.00 ? '' : 'comman_delete'} value={product.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                        </td>
                                                        :
                                                        <td align="right">
                                                            <NumberFormat value={product.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                        </td>
                                                    }
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div className="panel-footer p-0 bg-white">
                        <div className="table-calculate-price">
                            <table className="table ShopViewCalculator">
                                <tbody>
                                    <tr>
                                        <th className="bt-0">Sub-Total</th>
                                        <td align="right" className="bt-0">
                                            <span className="value">
                                                <NumberFormat value={checkList && checkList.subTotal} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {checkList && checkList.showTaxStaus}:
                                           <span className="value pull-right">
                                                <NumberFormat value={checkList && checkList.tax} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </span>
                                        </th>
                                        <th className="bl-1" align="right">
                                            Discount:
                                           <span className="value pull-right pointer" style={{ color: '#46A9D4' }}>
                                                {checkList && checkList.discountCalculated.toFixed(2)}
                                            </span>
                                        </th>
                                    </tr>
                                    {/*  add payment */}
                                    {
                                        (this.state.count > 0) ? (
                                            <tr id="paymentTr">
                                                <th>
                                                    <div className="relDiv show_payment_box">
                                                        <span className="value pointer" style={{ color: '#46A9D4' }} id="totalPayment">
                                                            {count} Payments   </span>
                                                        <div className="absDiv">
                                                            <div className="payment_box">
                                                                <h1 className="m-0">Payments</h1>
                                                                <div className="row" id="totalPaymentSrc">
                                                                    {payments.map((item, index) => {
                                                                        return (
                                                                            <div key={index} className="col-sm-12 p-0">
                                                                                <label className="col-sm-6">{item.payment_type}</label>
                                                                                <div className="col-sm-6"><NumberFormat value={item.payment_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></div>
                                                                            </div>
                                                                        )
                                                                    })}

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <td align="right" id="paymentLeft">
                                                    <NumberFormat value={this.state.paid_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                </td>
                                            </tr>
                                        ) : null
                                    }
                                    <tr>
                                        <th colSpan="2" className="p-0">
                                            <button className="btn btn-block btn-primary total_checkout bg-blue">
                                                <span className="pull-left">
                                                    Total Left
                                                 </span>
                                                <span className="pull-right">
                                                    <NumberFormat value={this.state.totalPrice >= 0 ? (cash_round + parseFloat(this.state.totalPrice.toFixed(2))) : '0.00'} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />

                                                </span>
                                            </button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    const { checkoutlist, checkout_list } = state;
    return {
        checkoutlist,
        checkout_list: checkout_list.items
    };
}
const connectedCheckoutViewFirst = connect(mapStateToProps)(CheckoutViewFirst);
export { connectedCheckoutViewFirst as CheckoutViewFirst };