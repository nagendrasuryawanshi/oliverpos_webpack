import { checkoutConstants } from '../constants/checkout.constants'
import { checkoutService } from '../services/checkout.service';
import { alertActions } from '../../_actions'
import { idbProductActions } from '../../_actions/idbProduct.action';
import { sendMailAction } from '../../_actions/sendMail.action'
import { get_UDid } from '../../ALL_localstorage'

const tick_event = [
    {
        "id": 1,
        "content": "Holi Celebration",
        "event_end_date_time": "2019-04-30T12:11:00",
        "event_id": 46,
        "event_location": "C21 Mall, bhopal",
        "event_start_date_time": "2019-04-08T12:11:00",
        "title": "Holi Celebration",
    },
    {
        "id": 2,
        "content": "Deepawali Celebration",
        "event_end_date_time": "2019-04-30T12:12:00",
        "event_id": 49,
        "event_location": "DB Mall , Bhopal",
        "event_start_date_time": "2019-04-08T12:12:00",
        "title": "Deepawali Celebration",
    }]
export const checkoutActions = {
    getAll,
    checkItemList,
    save,
    orderToVoid,
    cashRounding,
    getPaymentTypeName,
    getMakePayment,
    orderToCancelledSale,
    Get_TicketEvent,
    getOrderReceipt
};

function getAll() {
    return dispatch => {
        dispatch(request());

        checkoutService.getAll()
            .then(
                checkoutlist => dispatch(success(checkoutlist)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: checkoutConstants.GETALL_REQUEST } }
    function success(checkoutlist) { return { type: checkoutConstants.GETALL_SUCCESS, checkoutlist } }
    function failure(error) { return { type: checkoutConstants.GETALL_FAILURE, error } }
}

function checkItemList(checkout_list) {
    let list_item = checkout_list ? checkout_list.ListItem : null;
       return dispatch => {
        checkoutService.updatePoducts(list_item).then(response => {          
            console.log("response", response)
            if(response.IsSuccess == true){
               dispatch(success(response.Content))
                // localStorage.removeItem("oliver_order_payments")
                // window.location = '/checkout';
                // history.push('/checkout')
            }else{
               dispatch(success())
            }
         })
       
        // localStorage.removeItem("oliver_order_payments")
        // window.location = '/checkout';
        //history.push('/checkout')
    };

    function success(checkout_list) { return { type: checkoutConstants.GET_CHECKOUT_LIST_SUCCESS, checkout_list } }
}

function save(shopOrder, path) {
    console.log("json data" , JSON.stringify(shopOrder) )
    let line_items = shopOrder.line_items;
    console.log("line_items" , line_items)
    return dispatch => {
        dispatch(request(shopOrder));
        checkoutService.save(shopOrder).then(
            shop_order => {
                // setTimeout(function () {
                if (shop_order.IsSuccess == true) {

                    dispatch(success(shop_order));

                    // //Send Email----------------------------------              
                    //     let udid = get_UDid('UDID');
                    //     let order_id =shop_order.Content;
                    //     let email_id = shopOrder.billing_address.length>0?shopOrder.billing_address[0].email:"";

                    //     let requestData = {
                    //         "Udid": udid,
                    //         "OrderNo": order_id,
                    //         "EmailTo": email_id,
                    //     }
                    //         console .log("emailrequestData",requestData);
                    //    dispatch(sendMailAction.sendMail(requestData));



                    //update Refund qty for product......................
                    dispatch(idbProductActions.updateOrderProductDB(line_items));
                    // },3000)
                    //dispatch(idbProductActions.updateProductDB());         
                    //----------------------------------------------------  

                    //----------------------------------------------------
                    setTimeout(function () {
                        if (path == 1) {


                            localStorage.setItem("ORDER_ID", JSON.stringify(shop_order));
                            if (JSON.parse(localStorage.getItem("user")).display_sale_refund_complete_screen == false) {
                                localStorage.removeItem('PRODUCT');
                                localStorage.removeItem("CART");
                                localStorage.removeItem('CHECKLIST');
                                localStorage.removeItem('AdCusDetail');
                                localStorage.removeItem('CARD_PRODUCT_LIST');
                                window.location = '/shopview';

                            } else {
                                window.location = '/CheckoutSuccessful';
                            }

                        } else {

                            localStorage.removeItem('CHECKLIST');
                            localStorage.removeItem('AdCusDetail');
                            localStorage.removeItem('CARD_PRODUCT_LIST');
                            window.location = '/shopview';
                        }

                    }, 4000)

                    dispatch(alertActions.success('save custmoer order successfully'));

                } else {

                    $('#ordernotSuccesModal').modal('show')


                }
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    };

    function request() { return { type: checkoutConstants.INSERT_REQUEST } }
    function success(shop_order) { return { type: checkoutConstants.INSERT_SUCCESS, shop_order } }
    function failure(error) { return { type: checkoutConstants.INSERT_FAILURE, error } }
}
function Get_TicketEvent(event_id) {
    // console.log("Passed Event Array",event_id)
    //  console.log("event_id",tick_event);     
    var udid = get_UDid('UDID');
    return dispatch => {
        dispatch(request());
        dispatch(success(tick_event));
    };
    // return dispatch => {
    //     dispatch(request(udid,event_id));
    //     checkoutService.TicketEvent(udid,event_id).then(
    //         ticket_events => {

    //             dispatch(success(tick_event));

    //         },
    error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
    }
    //  );
    // };




    function request() { return { type: checkoutConstants.TICKET_EVENT_REQUEST } }
    function success(tick_event) { return { type: checkoutConstants.TICKET_EVENT_SUCCESS, tick_event } }
    function failure(error) { return { type: checkoutConstants.TICKET_EVENT_FAILURE, error } }
}
function getOrderReceipt(){
///alert("hello")
    return dispatch => {
       
        checkoutService.getOrderReceiptDetail()
            .then(               
                orderreciept => 
               {        

                console.log("orderreciept134",orderreciept);
               localStorage.setItem('orderreciept',JSON.stringify(orderreciept))

                error => {

                }
               }
            );
    };

    // function request() { return { type: allProductConstants.PRODUCT_GETALL_REQUEST } }
    // function success(productlist) { return { type: allProductConstants.PRODUCT_GETALL_SUCCESS, productlist } }
    // function failure(error) { return { type: allProductConstants.PRODUCT_GETALL_FAILURE, error } }


    
} 

function orderToCancelledSale(order_id, udid) {
    return dispatch => {
        dispatch(request(order_id, udid));
        checkoutService.orderToCancelSale(order_id, udid).then(
            cancel_order => {
                dispatch(idbProductActions.updateProductDB());

                dispatch(success(cancel_order));
                localStorage.removeItem('AdCusDetail');
                localStorage.removeItem('CARD_PRODUCT_LIST');

                setTimeout(function () {
                    window.location = '/shopview';
                    dispatch(alertActions.success('custmoer order cancel successfully'));
                }, 5000)
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    };

    function request() { return { type: checkoutConstants.ORDER_VOID_REQUEST } }
    function success(void_order) { return { type: checkoutConstants.ORDER_VOID_SUCCESS, void_order } }
    function failure(error) { return { type: checkoutConstants.ORDER_VOID_FAILURE, error } }
}
function orderToVoid(order_id, udid) {
    return dispatch => {
        dispatch(request(order_id, udid));
        checkoutService.orderToVoid(order_id, udid).then(
            void_order => {
                dispatch(success(void_order));
                localStorage.removeItem('AdCusDetail');
                window.location = '/activity';
                dispatch(alertActions.success('custmoer order void successfully'));

            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    };

    function request() { return { type: checkoutConstants.ORDER_VOID_REQUEST } }
    function success(void_order) { return { type: checkoutConstants.ORDER_VOID_SUCCESS, void_order } }
    function failure(error) { return { type: checkoutConstants.ORDER_VOID_FAILURE, error } }
}

function cashRounding(udid) {

    return dispatch => {
        dispatch(request(udid));
        checkoutService.getCashRounding(udid).then(
            cash_rounding => {

                dispatch(success(cash_rounding));
            },
            error => {
                dispatch(failure(error.toString()));
                //   dispatch(alertActions.error(error.toString()));
            }
        );
    };

    function request() { return { type: checkoutConstants.CASH_ROUNDING_REQUEST } }
    function success(cash_rounding) { return { type: checkoutConstants.CASH_ROUNDING_SUCCESS, cash_rounding } }
    function failure(error) { return { type: checkoutConstants.CASH_ROUNDING_FAILURE, error } }
}

function getPaymentTypeName(udid, registerId) {
    // console.log("udid", udid, registerId)
    // https://app.creativemaple.ca/api/ShopPaymentType/GetAll?udid=4040246269&registerId=1
    // const data = [
    //     { Id: '1', Code: 'global_payments', color: '#46a9d4' },
    //     { Id: '2', Code: 'cash', color: 'rgba(169, 212, 125, 0.95)' },
    //     { Id: '3', Code: 'other', color: 'rgba(198, 82, 167, 0.95)' },
    //     { Id: '4', Code: 'gift-card', color: 'rgba(118, 91, 114, 0.95)' },
    //     { Id: '5', Code: 'strip', color: 'rgba(38, 98, 123, 0.95)' },
    // ]
    return dispatch => {
        dispatch(request());
        //dispatch(success(data));
        checkoutService.getPaymentTypeName(udid, registerId)
            .then(
                paymentTypeName => {
                    //    var arr=[];
                    //   arr.push(paymentTypeName.Content);
                    localStorage.setItem("PAYMENT_TYPE_NAME", JSON.stringify(paymentTypeName.Content))
                    dispatch(success(paymentTypeName.Content))
                        ,
                        error => dispatch(failure(error.toString()))
                }
            );
    };

    function request() { return { type: checkoutConstants.GET_PAYMENT_TYPE_REQUEST } }
    function success(paymentTypeName) { return { type: checkoutConstants.GET_PAYMENT_TYPE_SUCCESS, paymentTypeName } }
    function failure(error) { return { type: checkoutConstants.GET_PAYMENT_TYPE_FAILURE, error } }
}

function getMakePayment(udid, registerId, paycode, amount) {
    // console.log("api getmakpayment", udid, registerId, paycode, amount)
    return dispatch => {
        dispatch(request());

        checkoutService.getMakePayment(udid, registerId, paycode, amount).then(
            global_payment => {
                // console.log("global_payment_response",global_payment);
                setTimeout(function () {
                    localStorage.setItem('GLOBAL_PAYMENT_RESPONSE', JSON.stringify(global_payment));
                    dispatch(success(global_payment));
                }, 1000)

            },
            error => {
                dispatch(failure(error.toString()));
            }
        );
    };

    function request() { return { type: checkoutConstants.GLOBAL_PAYMENTS_REQUEST } }
    function success(global_payment) { return { type: checkoutConstants.GLOBAL_PAYMENTS_SUCCESS, global_payment } }
    function failure(error) { return { type: checkoutConstants.GLOBAL_PAYMENTS_FAILURE, error } }
    // https://app.creativemaple.ca/api/ShopPaymentType/GetMakePayment?udid=4040246198&registerId=1&paycode=global_payments&command=sale&amount=2.00
}


