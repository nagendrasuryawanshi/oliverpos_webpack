import * as React from 'react';
import { connect } from 'react-redux';
import { userActions } from './action/user.actions';
import { LoadingModal } from '../_components/LoadingModal';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(userActions.logout());

        this.state = {
            username: cookies.get('username') !== 'undefined' ? cookies.get('username') : '',
            password: cookies.get('password') !== 'undefined' ? cookies.get('password') : '',
            submitted: false,
            check: true,
            loading: false,
            fieldErr: '',
            usernamedErr: '',
            passwordErr: ''
        };
        //  console.log("this.check", this.state.check);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        localStorage.removeItem("shopstatus");

    }
    componentWillReceiveProps(nextProps) {
        // console.log("this.props",nextProps);
        if (this.props.authentication.user !== null && this.props.authentication.user !== "undefined") {
            if (this.props.authentication.loggingIn == false) {
                this.setState({
                    loading: false
                });
            }
        }
        if (nextProps.authentication && nextProps.authentication.loggedIn == false) {
            this.setState({
                loading: false
            });
        }

    }
    componentWillMount() {
        // var string = 'Hello World!';
        // // Encode the String
        // var encodedString = btoa(string);
        // console.log("encodedString",encodedString); // Outputs: "SGVsbG8gV29ybGQh"

        // // Decode the String
        // var decodedString = atob(encodedString);
        // console.log("decodedString",decodedString);
        //    console.log("this.props",this.props);
        // if (localStorage.getItem("sitelist") && localStorage.getItem('UDID') && localStorage.getItem('Location') && localStorage.getItem('register'))
        //     history.push('/loginpin');
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        if ($('#remember').attr('checked')) {
            var username = $('#username').attr("value");
            var password = $('#password').attr("value");

            if (name == "username") {
                cookies.set(name, value);
            }
            if (name == "password") {
                cookies.set(name, value);
            }
            cookies.set('remember', this.state.check);
        }

    }

    handleSubmit(e) {
        e.preventDefault();

        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            this.setState({
                submitted: true,
                loading: true,
                fieldErr: '',
                usernamedErr: '',
                passwordErr: ''
            });
            // console.log("RequestInitiat")
            dispatch(userActions.login(username, password));
        } else {

            if (username == '' && password == '') {
                this.setState({
                    fieldErr: 'Email and Password is required',
                    usernamedErr: '',
                    passwordErr: ''
                });
            } else if (username == '') {
                this.setState({
                    usernamedErr: 'Email is required',
                    passwordErr: '',
                    fieldErr: ''
                });
            } else {
                this.setState({
                    passwordErr: 'Password is required',
                    usernamedErrL: '',
                    fieldErr: ''
                });
            }
        }
    }

    checkStatus(value) {
        this.setState({ check: value })

        if (value == false) {
            var username = $('#username').attr("value");
            var password = $('#password').attr("value");
            cookies.set('username', '');
            cookies.set('password', '');
            cookies.set('remember', value);

        } else {
            var username = $('#username').attr("value");
            var password = $('#password').attr("value");
            cookies.set('username', username);
            cookies.set('password', password);
            cookies.set('remember', value);
        }
        // var remember = $.cookie('remember');
        var remem = cookies.get('remember')

        if (remem == 'true') {
            this.setState({ username: cookies.get('username') })
            this.setState({ password: cookies.get('password') })

            // autofill the fields
        }
    }

    render() {
        const { loggedIn, loginError, loggingIn } = this.props.authentication;
        const { username, password, submitted, check, loading, fieldErr, usernamedErr, passwordErr } = this.state;
        return (
            <div className="bgimg-1">
                {loggingIn && loggingIn == true || loading == true ? <LoadingModal /> : ''}
                <div className="content_main_wapper">
                    <div className="onboarding-loginBox">
                        <div className="login-form">
                            <div className="onboarding-pg-heading">
                                <h1>Login To "Register”</h1>

                                <p>Login to your Oliver POS Account <br />to open the Register</p>
                            </div>
                            <div name="form" className="onboarding-login-field" >
                                <div className={'input-group m-b-20' + (submitted && !username ? ' has-error' : '')}>
                                    <span className="input-group-addon ig-name-custom ig_ni-custom" id="basic-addon1">Email</span>
                                    <input type="text" className="form-control ig-input-custom ig_ni-custom" placeholder="example@gmail.com" id="username" aria-describedby="basic-addon1" name="username" value={username} onChange={this.handleChange} />

                                </div>
                                <div className={'input-group m-b-20' + (submitted && !password ? ' has-error' : '')}>
                                    <span className="input-group-addon ig-name-custom ig_ni-custom" id="basic-addon1">Password</span>
                                    <input type="password" className="form-control ig-input-custom ig_ni-custom" placeholder="password" id="password" aria-describedby="basic-addon1" name="password" value={password} onChange={this.handleChange} />
                                </div>

                                <div className="checkBox-custom m-b-20">
                                    <div className="checkbox">
                                        <label className="customcheckbox">Remember Me
                                        <input type="checkbox" onClick={() => this.checkStatus(!check)} checked="checked" readOnly={true} id="remember" />
                                            <span className={'checkmark ' + (check == true ? ' checkUncheck' : ' ')}></span>
                                        </label>
                                    </div>
                                </div>
                                <button type="button" className="btn btn-login bgcolor2 mt-0" onClick={this.handleSubmit}>Login</button>
                            </div>
                        </div>
                        {
                            // loggedIn && loggedIn==false?
                            <div className="validationErr">
                                {loggedIn == false ? loginError : ""}
                                {fieldErr !== '' ? fieldErr : usernamedErr !== "" ? usernamedErr : passwordErr !== '' ? passwordErr : ''}
                            </div>
                            // :
                            // submitted && !username && !password ? (
                            //     <div className="validationErr">Invalid Credentials</div>
                            // )
                            //     :

                            //     submitted && !username ? (
                            //         <div className="validationErr">Username is required</div>

                            //     )
                            //         :
                            //         submitted && !password ? (
                            //             <div className="validationErr">Password is required</div>
                            //         )
                            //             :
                            //             loggedIn == 'undefined' ? (
                            //                 <div className="validationErr">Invalid Credentials</div>
                            //             )
                            //             :
                            //                 <div className="validationErr"></div>
                        }
                    </div>
                    <div className="powered-by-oliver">
                        <a href="javascript:void(0)">Powered by Oliver POS</a>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication } = state;
    return {
        authentication
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };     