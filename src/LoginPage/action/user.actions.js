import { userConstants } from '../constants/user.constants';
import { userService } from '../services/user.service';
import { alertActions } from '../../_actions/alert.actions';
import { history } from '../../_helpers/history';
import  Cookies from 'universal-cookie'
const cookies = new Cookies();

export const userActions = {
    login,
    logout,
};

function login(username, password) {   

    return dispatch => {
        //dispatch(request({ username }));

        userService.login(username, password)
            .then(
                sitelist => { 
                    console.log("LoginResult",sitelist);
                    if(sitelist){
                    dispatch(success(sitelist));   
                    
                    //    localStorage.setItem('sitelist', JSON.stringify(sitelist));
                    var sitelis= btoa(JSON.stringify(sitelist));
                    var  encodedString =  window.btoa( localStorage.setItem('sitelist', sitelis));
                //  console.log("encodedString",encodedString); // Outputs: "SGVsbG8gV29ybGQh"
                   var decodedString = localStorage.getItem('sitelist');
                      var decod=  window.atob(decodedString);
                //  console.log("decodedString",decod); // Outputs: "SGVsbG8gV29ybGQh"

                          history.push('/site_link');
                                    
                    }
                    else
                    {
                        dispatch(failure("Invalid Email/Password"));
                        history.push('/login');
                    }
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

