import * as React from 'react';
import { connect } from 'react-redux';
import { externalLoginActions } from './action/externalLogin.actions';
import { history } from '../_helpers';

class ExternalLogin extends React.Component {
    constructor(props) {
        super(props);

        var urlParam=  this.props.location.search;

            var splParam =urlParam.replace("?","").split("&");
            var finalParam=""
            splParam.forEach(element => {
               // console.log("element",element)
               // console.log("parama ",element.substring(0,element.indexOf('=')))
               // console.log("Value ",element.substring(element.indexOf('=')+1));
                finalParam +=finalParam==""?"":"&"; 
              
                finalParam += element.substring(0,element.indexOf('='))+"="+ encodeURIComponent(element.substring(element.indexOf('=')+1));
            });
//console.log("urlParam",finalParam);
localStorage.clear();
       this.props.dispatch(externalLoginActions.externallogin(finalParam));
    }

    render() {
       
       
        return (
            <div className="bgimg-1">
                <div className="content_main_wapper">
                    <div className="onboarding-loginBox">
                        <div className="login-form">
                            <div className="onboarding-pg-heading">
                                <h1>Login</h1>
                                <p>Please wait...</p>
                            </div>
                            
                        </div>
                    
                    </div>
                    <div className="powered-by-oliver">
                        <a href="javascript:void(0)">Powered by Oliver POS</a>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication } = state;
    return {
        authentication
    };
}

const connectedExternalLogin = connect(mapStateToProps)(ExternalLogin);
export { connectedExternalLogin as ExternalLogin }; 