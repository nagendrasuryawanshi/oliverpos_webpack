import Config from '../../Config'

export const externalLoginService = {
    externallogin,
    logout,
};
const API_URL = Config.key.OP_API_URL

function externallogin(parameter) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors',
      
    };

    return fetch(`${API_URL}/Subscription/ExternalAuth?${parameter}`, requestOptions)
        .then(handleResponse)
        .then(loginRes => {      
                
            return loginRes;
        }).catch(error => {
            return error
        })
        ;
}


function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}



function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}