import { externalLoginConstants } from '../constants/externalLogin.constants';
import { externalLoginService } from '../services/externalLogin.service';
import { alertActions } from '../../_actions/alert.actions';
import { history } from '../../_helpers/history';
import {encode_UDid}  from '../../ALL_localstorage';
export const externalLoginActions = {
    externallogin,
    logout,
};

function externallogin(Parameter) {    
    return dispatch => {
        dispatch(request({ Parameter }));

        externalLoginService.externallogin(Parameter)
            .then(
                loginRes => { 
                   //console.log("ExternalLoginResult",loginRes);
                    if(loginRes){
                        if(loginRes.IsSuccess==true)
                        {
                            dispatch(success(loginRes));   
                            if(loginRes.Content.udid)
                            {
                                var locations=[]
                                locations.push(loginRes.Content.location);
                                localStorage.setItem("UserLocations",JSON.stringify(locations));
                              //  console.log("ExternalLoginLocation", JSON.parse(localStorage.getItem("UserLocations")));
                              //localStorage.setItem("UDID", loginRes.Content.udid);
                              encode_UDid(loginRes.Content.udid);
                              if(loginRes.Content.location && loginRes.Content.register)
                                {  
                                    localStorage.setItem("Location", loginRes.Content.location.Id);
                                    localStorage.setItem("LocationName",loginRes.Content.location.Name);
                                  
                                    localStorage.setItem("register", loginRes.Content.register.Id);
                                    localStorage.setItem('registerName',loginRes.Content.register.Name);
                                    
                                    history.push('/loginpin');
                                }
                              else if(loginRes.Content.locations)
                              {
                                 // console.log("userlocation1",loginRes.Content.locations);
                                localStorage.setItem('UserLocations',JSON.stringify(loginRes.Content.locations));
                                history.push('/login_location');
                              }
                              else
                              {
                                history.push('/login');
                              }
                                                      
                            }
                            else{
                                history.push('/login');
                            }
                        } else{
                            history.push('/login');
                        }
                    }
                    else
                    {
                        dispatch(failure("Invalid Email/Password"));
                        history.push('/login');
                    }
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                    history.push('/login');
                }
            );
    };

    function request(user) { return { type: externalLoginConstants.LOGIN_REQUEST, user } }
    function success(loginRes) { return { type: externalLoginConstants.LOGIN_SUCCESS, loginRes } }
    function failure(error) { return { type: externalLoginConstants.LOGIN_FAILURE, error } }
}

function logout() {
    externalLoginService.logout();
    return { type: externalLoginConstants.LOGOUT };
}

