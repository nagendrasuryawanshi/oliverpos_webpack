import React from 'react';
import { connect } from 'react-redux';
import { refundActions } from '../actions/refund.action';
import { default as NumberFormat } from 'react-number-format'
import { Markup } from 'interweave';
import {encode_UDid,get_UDid} from '../../ALL_localstorage'


class RefundViewFirst extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refund_subtotal : 0,
            refund_tax : 0,
            refund_total : 0,
            incrementClick : 0,
            single_Order_list:(typeof localStorage.getItem("getorder") !== 'undefined')?JSON.parse(localStorage.getItem("getorder")):null,
            refundPayment :localStorage.getItem("oliver_refund_order_payments")?JSON.parse(localStorage.getItem("oliver_refund_order_payments")):null
        }

        this.handleRefundingItems = this.handleRefundingItems.bind(this);
    }


    componentDidMount() {
        this.props.onRef(this)
    }

    componentWillUnmount() {
        this.props.onRef(null)
    }

    handleRefundingItems() {
        const { single_Order_list } = this.state;
        let refund_subtotal = 0;
        let refund_tax = 0;
        let refund_total = 0;
        let cash_round = single_Order_list.cash_rounding_amount?single_Order_list.cash_rounding_amount:0;
        //console.log("single_Order_list", single_Order_list)
        let total_refund_amount = single_Order_list && single_Order_list.total_amount;
        let cash_rounding_amount = single_Order_list && single_Order_list.cash_rounding_amount;
        let set_total_quantity_for_refund = 0;
        let get_total_quantity_for_refund = 0;
        let qty2 = 0;
       // console.log("total_refund_amount", total_refund_amount, cash_rounding_amount)

        single_Order_list.line_items.map(qty_is=>{
            get_total_quantity_for_refund +=  qty_is.quantity
        })
        get_total_quantity_for_refund = get_total_quantity_for_refund + single_Order_list.quantity_refunded;

        $(".refunndingItem").each(function () {
            let item_id = $(this).attr('data-id');
            let quantity = parseInt($(`#counter_show_${item_id}`).text());
            let price = parseFloat($(`#refunditem_${item_id}`).attr('data-amount'));
            let tax = parseFloat($(`#refunditem_${item_id}`).attr('data-perprdtax'));
            let qty = $(this).attr('data-quantity');
            qty2 += qty
            set_total_quantity_for_refund += quantity;

            //calculation part
            refund_subtotal += (parseFloat(price) * quantity);
            refund_tax += (parseFloat(tax) * quantity);
            //calculation part     
          //  console.log("single_Order_list" , cash_round)
        // console.log("refund_subtotal" , refund_subtotal)
            // console.log("refund_tax" , refund_tax)
        });
       // console.log("set_total_quantity_for_refund" , set_total_quantity_for_refund, get_total_quantity_for_refund, qty2)
        refund_total = (parseFloat(refund_subtotal) + parseFloat(refund_tax));
        if (set_total_quantity_for_refund == get_total_quantity_for_refund) { 
            refund_total =  parseFloat(single_Order_list.total_amount - single_Order_list.refunded_amount).toFixed(2);
        }else if(refund_total + (cash_rounding_amount) == total_refund_amount){
           // console.log("inside of rounding amount is")
            refund_total = refund_total+ (cash_rounding_amount)
        }

      //  console.log("refund_total" , refund_total)

        this.props.refundingAmount(refund_total);
        this.setState({
            refund_subtotal: refund_subtotal,
            refund_tax: refund_tax,
            refund_total: refund_total,
            // refund_total: refund_total + cash_round,
        });
    }

    toggleRefundPanel( item_id ){
        this.setState({incrementClick : 1})
        $(`#refundpanel_${item_id}`).toggle();
        $(`#refunditem_${item_id}`).toggleClass('refunndingItem');
        this.handleRefundingItems();
    }

    increaseitemRefundQty( item_id ){
        let count = parseFloat( $(`#counter_show_${item_id}`).text() );
        let maxQty = parseFloat( $(`#check_counter_${item_id}`).text() );
        if(this.state.incrementClick < maxQty){
            this.setState({incrementClick: this.state.incrementClick + 1})  
        }
        if ( count < maxQty ) {
            count++;
            $(`#counter_show_${item_id}`).text(count);
            this.handleRefundingItems();
        }

        if(this.state.incrementClick == maxQty){
            this.props.min_max_add("Refund quantity must be less than item quantity");      
        }
    }

    decreaseitemRefundQty( item_id ){
        let count = parseFloat( $(`#counter_show_${item_id}`).text() );
        this.setState({incrementClick: this.state.incrementClick - 1})  
         if ( count > 1 ) {
            count--;
            $(`#counter_show_${item_id}`).text(count);
            this.handleRefundingItems();
        }
        if(this.state.incrementClick == 1 || this.state.incrementClick <= 0 ){
           this.props.min_max_add("Refund quantity must be greater than 0.")
        }
    }

    refund(order_id, CashRound) {
        const { single_Order_list } = this.state;
        let getorder = single_Order_list;
       // console.log("getorder" , getorder)
        let managerData = localStorage.getItem('user');
        // console.log("managerData",managerData)
        let refund_subtotal = 0;
        let refund_tax = 0;
        let refund_total = 0;
        var items = new Array();
        var payments = new Array();
        let total_amount = parseFloat(getorder.total_amount - getorder.refunded_amount).toFixed(2);
        let new_last_amount_is = 0
       // console.log("total_amount",total_amount)

        $(".refunndingItem").each(function () {
            let item_id = $(this).attr('data-id');
            let quantity = parseInt($(`#counter_show_${item_id}`).text());
            let price = parseFloat($(`#refunditem_${item_id}`).attr('data-amount'));
            let tax = parseFloat($(`#refunditem_${item_id}`).attr('data-perprdtax'));

            //create the refund items array
            items.push({
                'Quantity': quantity,
                'amount': price * quantity,
                'tax': tax * quantity,
                'item_id': item_id,
            });

            //calculation part
            refund_subtotal += (parseFloat(price) * quantity);
            refund_tax += (parseFloat(tax) * quantity);
            // refund_total += ( refund_subtotal + refund_tax );
            //calculation part    
        });
        refund_total = (parseFloat(refund_subtotal) + parseFloat(refund_tax));
       // refund_total = total_amount;


        if (localStorage.oliver_refund_order_payments) {
            var date = new Date();
            let pay = JSON.parse(localStorage.getItem("oliver_refund_order_payments"));
            console.log("pay", pay)
            JSON.parse(localStorage.getItem("oliver_refund_order_payments")).forEach(paid_payments => {
                if (order_id == paid_payments.order_id) {
                    payments.push({
                        'amount': paid_payments.payment_amount,
                        'payment_type': paid_payments.payment_type,
                        'type': paid_payments.payment_type,
                        'payment_date': `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`,
                        'description': paid_payments.description
                    });
                }
            });

        }
       // console.log("payments first", payments);
        let last_length_of_payments = payments.length - 1;
        let new_amount = 0;
        
        payments.map(itemP=>{
            new_amount += parseFloat(itemP.amount)
        })
         
         //console.log("last_length_of_payments", last_length_of_payments)
        //  console.log("new_amount", new_amount);
        //  console.log("refund_total", refund_total);
        // console.log("total_amount", total_amount);
       //  console.log("refund_total", refund_total - new_amount)
        if(CashRound == 0 ){
            if(new_amount > total_amount){
               //console.log("STEP 01 ");
                let last_amount_is =  parseFloat(payments[last_length_of_payments].amount)
                new_last_amount_is = new_amount - total_amount;
               // console.log("new_last_amount_is 01 ", new_last_amount_is);
                payments[last_length_of_payments].amount = parseFloat(last_amount_is - new_last_amount_is).toFixed(2);
                refund_total = total_amount ;
            }else if(new_amount == total_amount){
               //console.log("STEP 02 ");
                let last_amount_is =  parseFloat(payments[last_length_of_payments].amount)
                payments[last_length_of_payments].amount = parseFloat(last_amount_is).toFixed(2);
                refund_total = total_amount  ;
            }else{
              // console.log("STEP 03 ");
                     let last_amount_is =  parseFloat(payments[last_length_of_payments].amount)
                     payments[last_length_of_payments].amount = parseFloat(last_amount_is).toFixed(2);
                    refund_total = new_amount ;
            }
         
        }else{
            if(new_amount > total_amount){
              // console.log("STEP 04 ");
               let last_amount_is =  parseFloat(payments[last_length_of_payments].amount)
               new_last_amount_is = (new_amount - total_amount);
             // console.log("new_last_amount_is 04 ", new_last_amount_is);
               payments[last_length_of_payments].amount = parseFloat(last_amount_is - new_last_amount_is).toFixed(2);
               // payments[last_length_of_payments].amount = total_amount;
                refund_total = total_amount ;
              }else if(new_amount == total_amount){
               // console.log("STEP 05 ");
                refund_total = total_amount ;
              }
              else{
              // console.log("STEP 06 ");
                if(new_amount + CashRound == total_amount){
                 // console.log("STEP 07 ");
                    payments[last_length_of_payments].amount = new_amount + CashRound;
                    refund_total = total_amount;
                }else if(new_amount - CashRound == total_amount){
                  // console.log("STEP 08 ");
                    payments[last_length_of_payments].amount = new_amount - CashRound;
                    refund_total = total_amount;
                }else{
                  // console.log("STEP 09 ");
                        //let last_amount_is =  parseFloat(payments[last_length_of_payments].amount)
                        //new_last_amount_is = parseFloat(new_amount - last_amount_is).toFixed(2);
                       // payments[last_length_of_payments].amount = new_last_amount_is;
                        refund_total = new_amount;
                    
                    }
                    
                }
  
        }
        // console.log("payments second", payments) 
       //  console.log("recalculate_refund_total", refund_total)
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID= get_UDid('UDID');
        let requestData = {
            'order_id': order_id,
            'refund_tax': refund_tax,
            'refund_amount': parseFloat(refund_total).toFixed(2),
            'RefundItems': items,
            'order_refund_payments': payments,
            'udid':UID,
            'customer_email': $("#hidden_customer_email_id").text(),
            'manager_id': managerData.user_id,
            'manager_email': managerData.user_email,
            'refund_cash_rounding': CashRound
        }

        //call the action function   
     // console.log("refund place order" , requestData)  
       //  console.log("refund place order" , JSON.stringify(requestData))          
      this.props.dispatch(refundActions.refundOrder(requestData));
    }


    render() {
        const { single_Order_list } = this.state;
        let getorder = single_Order_list;
        var total_amount = 0;
        //console.log("getorder" , getorder, this.props.payments)
        if(this.props.payments.length > 0) {
             this.props.payments.map(amount=>{
               total_amount += parseFloat(amount.payment_amount);
            }) 
        }
        return (
            <div className="col-lg-3 col-sm-4 col-xs-4 pl-0">
                <div className="panel panel-default panel-right-side r0 br-1 bb-0">
                    <div className="panel-heading bg-white">
                        {(typeof getorder.orderCustomerInfo !== 'undefined') && getorder.orderCustomerInfo != null ? getorder.orderCustomerInfo.customer_name : "---"}
                        <p style={{ display: "none" }} id="hidden_customer_email_id">{(typeof getorder.orderCustomerInfo !== 'undefined') && getorder.orderCustomerInfo != null ? getorder.orderCustomerInfo.customer_email : ""}</p>
                    </div>
                    <div className="panel-body p-0 overflowscroll bg-white" id="cart_product_list">
                        <div className="table-responsive">
                            <table className="table ListViewCartProductTable">
                                <colgroup>
                                    <col width="*" />
                                    <col width="70" />
                                    <col width="50" />
                                </colgroup>
                                {/* <tbody> */}

                                    {/* display all order items here */}
                                    {(typeof getorder.line_items !== 'undefined') && getorder.line_items !== null ?
                                        getorder.line_items && getorder.line_items.map((item, index) => {
                                            //console.log("Refund iTem",item)
                                            return (
                                                <tbody key={index}>
                                                    {/* <td>
                                                        <table className="table mb-0">
                                                            <tbody> */}
                                                                <tr key={item.line_item_id} id={`refunditem_${item.line_item_id}`} title={item.name} data-id={item.line_item_id} className="prdtr" price={item.total} data-amount={item.total / item.quantity} data-productid={item.product_id} data-varproductid={item.variation_id} data-orderid="6747" data-quantity={item.quantity} data-taxrate={item.total_tax} data-perprdtax={item.total_tax / item.quantity}>
                                                                    {/* <td> {item.quantity + item.quantity_refunded} </td> */}
                                                                    <td align="left">
                                                                        <Markup content={item.name} />
                                                                        <span className="comman_subtitle"></span>
                                                                    </td>
                                                                    <td align="right">
                                                                        <NumberFormat value={item.total - item.amount_refunded} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                                    </td>
                                                                    <td>
                                                                        {item.quantity - item.quantity_refunded > 0 && item.total - item.amount_refunded > 0 ?
                                                                            <div className="refund_radio_button" data-id={item.line_item_id}>
                                                                                <label className="customcheckbox">
                                                                                    <input type="checkbox" onChange={() => this.toggleRefundPanel(item.line_item_id)} />
                                                                                    <span className="checkmark"></span>

                                                                                </label>
                                                                            </div>
                                                                            : <div> <label className="customcheckbox">
                                                                                <span className="checkmark"></span>
                                                                            </label>
                                                                            </div>
                                                                        }
                                                                    </td>
                                                                </tr>

                                                                {/* display refund panel */}
                                                                <tr key={`panel_${item.line_item_id}`} id={`refundpanel_${item.line_item_id}`} style={{ display: "none" }}>
                                                                    <th colSpan="4" className="bt-0">
                                                                        <div className="btn-group btn-group-justified refund-range" role="group" aria-label="">
                                                                            <div className="btn-group" role="group">
                                                                                <button type="button" className="btn btn-default" onClick={() => this.decreaseitemRefundQty(item.line_item_id)}><img src="assets/img/minus.png" /></button>
                                                                            </div>
                                                                            <div className="btn-group" role="group">
                                                                                <button type="button" className="btn btn-default" id="countable">
                                                                                    <input className="blank_input" type="hidden" size="25" value="0" id="count" />
                                                                                    <span id={`counter_show_${item.line_item_id}`}>1</span>
                                                                                    /
                                                                <span id={`check_counter_${item.line_item_id}`}>{item.quantity + item.quantity_refunded}</span>
                                                                                </button>
                                                                            </div>
                                                                            <div className="btn-group" role="group">
                                                                                <button type="button" className="btn btn-default" onClick={() => this.increaseitemRefundQty(item.line_item_id)}><img src="assets/img/plus.png" /></button>
                                                                            </div>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                {/* display refund panel */}
                                                            {/* </tbody>
                                                        </table>
                                                    </td> */}
                                                </tbody>
                                            )
                                        })
                                        : null
                                    }

                                    {/* display all order items here */}


                                {/* </tbody> */}
                            </table>
                        </div>
                    </div>

                    <div className="panel-footer p-0 bg-white">
                        <div className="table-calculate-price">
                            <table className="table table ShopViewCalculator mb-0">
                                <tbody>
                                    <tr>
                                        <th className="bt-0">Sub-Total</th>
                                        <td align="right">
                                        <NumberFormat value={this.state.refund_subtotal} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            {/* <span className="value">{parseFloat(this.state.refund_subtotal).toFixed(2)}</span> */}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Tax:
                                            <span className="value pull-right"> {parseFloat(this.state.refund_tax).toFixed(2)} </span>
                                        </th>
                                        <th className="bl-1" align="right"> Discount:
                                            <span className="value pull-right" style={{ color: '#46A9D4' }}> N/A </span>
                                        </th>
                                    </tr>
                                    {this.props.payments.length > 0 ?
                                    <tr id="paymentTr">
                                        <th>
                                            <div className="relDiv show_payment_box">
                                                <span className="value pointer" style={{ color: '#46A9D4' }} id="totalPayment"> Payments</span>


                                                <div className="absDiv">
                                                    <div className="payment_box">
                                                        <h1 className="m-0">Refund Payments</h1>
                                                        {this.props.payments.map( (payment, index)=> {
                                                                return (
                                                                    <div key={index} className="row">
                                                                        <div className="col-sm-12">
                                                                            <label className="col-sm-6">{payment.payment_type}</label>
                                                                            <div className="col-sm-6">{payment.payment_amount}</div>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                      
                                           <th className="text-right" id="paymentLeft">
                                
                                                {parseFloat(total_amount).toFixed(2)}
                                            </th>
                                        
                                    </tr>
                                    :
                                    <tr id="paymentTr">
                                        <th>
                                            <div className="relDiv show_payment_box">
                                                <span className="value pointer" style={{ color: '#46A9D4' }} id="totalPayment"> Payments</span>


                                                <div className="absDiv">
                                                    <div className="payment_box">
                                                        <h1 className="m-0">Payments</h1>
                                                        {
                                                            getorder.order_payments.map(function (payment, index) {
                                                                return (
                                                                    <div key={index} className="row">
                                                                        <div className="col-sm-12">
                                                                            <label className="col-sm-6">{payment.type}</label>
                                                                            <div className="col-sm-6">{payment.amount}</div>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th className="text-right" id="paymentLeft">
                                            {parseFloat(getorder.total_amount).toFixed(2)}
                                        </th>
                                    </tr>}
                                    <tr>
                                        <th colSpan="2" className="p-0">
                                            <button className="btn btn-block btn-primary total_checkout">
                                                <span className="pull-left">Total Refund</span>
                                                <span className="pull-right">{parseFloat(this.state.refund_total).toFixed(2)}</span>
                                            </button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
function mapStateToProps(state) {
    const { single_Order_list, cash_rounding } = state;
    return {
        single_Order_list,
        cash_rounding: cash_rounding.items
    };
}

const connectedRefundViewFirst = connect(mapStateToProps)(RefundViewFirst);
export { connectedRefundViewFirst as RefundViewFirst };