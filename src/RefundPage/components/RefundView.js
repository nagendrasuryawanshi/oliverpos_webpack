import React from 'react';
import { connect } from 'react-redux';
import { CommonHeaderFirst } from '../../_components';
import { RefundViewFirst } from './RefundViewFirst';
import { RefundViewSecond } from './RefundViewSecond';
import { RefundViewThird } from './RefundViewThird';
import { LoadingModal } from '../../_components';
import { checkoutActions } from '../../CheckoutPage/actions/checkout.action';
import { GetRoundCash } from '../../CheckoutPage/Checkout';
import {get_UDid} from '../../ALL_localstorage'


class RefundView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            single_Order_list: (typeof localStorage.getItem("getorder") !== 'undefined') ? JSON.parse(localStorage.getItem("getorder")) : null,
        }
        this.refundAmount = React.createRef();
        //deepsagar code
        //let udid = localStorage.getItem('UDID');
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var udid= get_UDid('UDID');
        let getOrderId = this.props.location.search;
        let order_id = 0;
        if (typeof getOrderId !== 'undefined') {
            getOrderId = getOrderId.split("=");
            if (typeof getOrderId[1] !== 'undefined') {
                order_id = getOrderId[1];
            }
        }


        this.state = {
            globalUDID: udid,
            setOrderId: 0,
            refundingAmount: 0,
            isShowLoader: false,
            msg: '',
            before_paid_amount:0,
            payments:[]
        }

        this.setRefundingAmount = this.setRefundingAmount.bind(this);
        this.getRefundPayment = this.getRefundPayment.bind(this);
        this.setRefundPayment = this.setRefundPayment.bind(this);
        this.isRefundPaymentComplete = this.isRefundPaymentComplete.bind(this);
        this.setrefundingPaidAmount = this.setrefundingPaidAmount.bind(this);
        this.showLoader = this.showLoader.bind(this);
        this.min_max_add = this.min_max_add.bind(this);
        this.removeRefundPayments();
        //this.props.dispatch(checkoutActions.getPaymentTypeName())
        //deepsagar code 
    }

    min_max_add(msg) {
        this.setState({ msg: msg })
        $('#epos_error_model').modal('show')
    }


    setRefundingAmount(amount) {
        //console.log("refundingAmount" , amount)
        let cash_round = (typeof localStorage.getItem("getorder") !== 'undefined')?JSON.parse(localStorage.getItem("getorder")).cash_rounding_amount:0;
        //console.log("cash_rosingle_Order_listund" , cash_round)
        // this.refundpayments.setRefundAmount((parseFloat(amount) + parseFloat(cash_round)));
        this.refundpayments.setRefundAmount(amount);
        this.setState({
            refundingAmount: amount,
            // refundingAmount: (parseFloat(amount) + parseFloat(cash_round))
        });
    }

    getRefundPayment(order_id, type, amount) {
       // console.log("total pay" , order_id , type , amount)
        this.setState({
            paying_type: type,
            paying_amount: amount,
        });
        let paidAmount = 0;

        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        let CashRound = parseFloat(GetRoundCash(cash_rounding, this.state.refundingAmount))
         if (localStorage.oliver_refund_order_payments) {

            JSON.parse(localStorage.getItem("oliver_refund_order_payments")).forEach(paid_payments => {
                if (order_id == paid_payments.order_id) {
                    paidAmount += parseFloat(paid_payments.payment_amount);
                }
            });

        } else {
            //alert( "Your browser not support local storage" );
        }
        this.setState({before_paid_amount:paidAmount})
        if(type == 'cash'){
            //console.log("in popup cash", this.state.refundingAmount, CashRound,paidAmount, parseFloat(parseFloat(this.state.refundingAmount) + parseFloat(CashRound)).toFixed(2))
           
              if((parseFloat(parseFloat(this.state.refundingAmount) + parseFloat(CashRound)).toFixed(2) - paidAmount) < amount){
                $('#popup_cash_rounding').modal('show');
             }else{
                 this.setRefundPayment(order_id, type, amount);  
             }
            
        }else{
            this.setRefundPayment(order_id, type, amount);
        }
    }

    addPayment(order_id) {
        let payment = this.refundpayments.setRefundPayment(order_id);
    }

    setRefundPayment(order_id, paying_type, paying_amount) {
        //console.log("payment type 1" ,paying_type , order_id , paying_amount)
         // let paying_amount = payment_amount;
          let description ='';
        // let paying_amount = this.state.paying_amount;
        // let paying_type = this.state.paying_type;                
        // console.log("payment type " ,paying_type , order_id , paying_amount)
        //  if(paying_type == 'cash'){
        //     if(paying_amount > this.state.refundingAmount){
        //         paying_amount =  paying_amount - this.state.refundingAmount
        //     }
        // }
        
        if(paying_type == 'global_payments'){
            let g_payment = JSON.parse(localStorage.getItem('GLOBAL_PAYMENT_RESPONSE'));
             if (g_payment !== null && g_payment.IsSuccess === true) {
                let global_payments = g_payment.Content;
                description = `TerminalId-${global_payments.TerminalId} , Authrization-${global_payments.Authrization},RefranseCode-${global_payments.RefranseCode}`;
            }
        }

        // set order payments in localstorage
        if (typeof (Storage) !== "undefined") {
           // console.log("chech localstorage is set or not", localStorage.getItem("oliver_refund_order_payments"),localStorage.oliver_refund_order_payments)
            if (localStorage.oliver_refund_order_payments) {
              
                var payments = JSON.parse(localStorage.getItem("oliver_refund_order_payments"));
                payments.push({
                    "payment_type": paying_type,
                    "payment_amount": paying_amount,
                    "order_id": order_id,
                    "description": description
                });
                this.setState({payments:payments})
               // console.log("json 01", payments)
                localStorage.setItem("oliver_refund_order_payments", JSON.stringify(payments));
            } else {
                
                var payments = new Array();
                payments.push({
                    "payment_type": paying_type,
                    "payment_amount": paying_amount,
                    "order_id": order_id,
                    "description": description
                });
                this.setState({payments:payments})
               // console.log("json 02", payments)
                localStorage.setItem("oliver_refund_order_payments", JSON.stringify(payments));
            }
        } else {
            // alert( "Your browser not support local storage" );
        }
        // set order payments in localstorage
        this.isRefundPaymentComplete(order_id, paying_type);
        
    }

    setCashRoundedValue(CashRound){
        let order_detail =  JSON.parse(localStorage.getItem("getorder"))
        let order_id = order_detail.order_id;
        let payment_amount = 0;
        let paidAmount = 0;
        const { paying_amount , refundingAmount , before_paid_amount } = this.state;
        // console.log("setCashRoundedValue 1" , paying_amount , refundingAmount, CashRound)
        // console.log("setCashRoundedValue 2" , this.state.refundingAmount)
        // console.log("setCashRoundedValue 3" , this.state.paying_amount)
       
        if(paying_amount > ((parseFloat(refundingAmount) + parseFloat(CashRound)) - before_paid_amount)){
            payment_amount =  (parseFloat(refundingAmount) + parseFloat(CashRound)) - before_paid_amount
        }else{
            payment_amount = paying_amount;
            //payment_amount = parseFloat(refundingAmount + (paying_amount - refundingAmount)).toFixed(2);
        }
        let payment_type = 'cash';
        this.setRefundPayment(order_id , payment_type , payment_amount)
    }

    isRefundPaymentComplete(order_id, paying_type) {
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        let payments = JSON.parse(localStorage.getItem("oliver_refund_order_payments"));
        let refundAmount = this.state.refundingAmount;
        let paidAmount = 0;
        let CashRound = 0;
       // console.log("refundAmount 1" ,refundAmount)
        if (paying_type == 'cash') {
            CashRound = parseFloat(GetRoundCash(cash_rounding, parseFloat(this.state.refundingAmount)))
            refundAmount = parseFloat(refundAmount) + parseFloat(CashRound);
        }
        //get total paid amount
       // console.log("refundAmount 2" ,refundAmount)
        if (localStorage.oliver_refund_order_payments) {

            JSON.parse(localStorage.getItem("oliver_refund_order_payments")).forEach(paid_payments => {
                if (order_id == paid_payments.order_id) {
                    paidAmount += parseFloat(paid_payments.payment_amount);
                }
            });

        } else {
            //alert( "Your browser not support local storage" );
        }

        // console.log("paidAmount 1" ,paidAmount)
        // console.log("paidAmount 2" ,refundAmount , paidAmount)
        this.setrefundingPaidAmount(paidAmount);
        if (parseFloat(refundAmount).toFixed(2) == parseFloat(paidAmount).toFixed(2)) {
            this.refundcart.refund(order_id, CashRound);
            this.removeRefundPayments();

            this.showLoader()
        } else if (parseFloat(refundAmount).toFixed(2) < parseFloat(paidAmount).toFixed(2)) {
            //  alert("Can't pay more amount then refund amount.");
          //  this.removeRefundPayments();
           // history.push('/activity');
        }
    }

    showLoader() {
        this.setState({
            isShowLoader: true,
        });
    }

    setrefundingPaidAmount(amount) {
        this.refundpayments.setrefundingPaidAmount(amount);
    }

    removeRefundPayments() {
        if (localStorage.oliver_refund_order_payments) {
            localStorage.removeItem("oliver_refund_order_payments");
        }
    }

    componentWillMount() {
        setTimeout(function () {

            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();
           // autoFocus()
        }, 1000);
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var UID= get_UDid('UDID');
       // const UID = localStorage.getItem('UDID');
        this.props.dispatch(checkoutActions.cashRounding(UID));
    }

    render() {
        const { paying_amount , refundingAmount, before_paid_amount } = this.state;
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        let CashRound = parseFloat(GetRoundCash(cash_rounding, refundingAmount))
        //console.log("inside render" ,paying_amount , refundingAmount ,before_paid_amount, CashRound)
      
         return (
            <div>
                <div className="wrapper">
                    <div id="content">
                        {this.state.isShowLoader ? <LoadingModal /> : ''}
                        <CommonHeaderFirst {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <RefundViewFirst min_max_add={this.min_max_add} onRef={ref => (this.refundcart = ref)} {...this.props} refundingAmount={this.setRefundingAmount} payments={this.state.payments} />
                                <div className="col-lg-9 col-sm-8 col-xs-8">
                                    <div className="row">
                                        <RefundViewSecond {...this.props} />
                                        <RefundViewThird {...this.props} min_max_add={this.min_max_add} onRef={ref => (this.refundpayments = ref)} addPayment={this.getRefundPayment} />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="epos_error_model" className="modal modal-wide modal-wide1">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">{this.state.msg}</h3>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="popup_cash_rounding" className="modal modal-wide modal-wide1 cancle_payment fade in" >
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Refund</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 className="popup_payment_error_msg" id="popup_cash_rounding_error_msg">
                                    <div className="row font18 mb-3">
                                     <div className="col-sm-6 text-left">
                                        Total
                                      <span className="pull-right">:</span>
                                    </div>
                                    <div className="col-sm-6">
                                       {parseFloat((parseFloat(refundingAmount) + parseFloat(CashRound))- before_paid_amount).toFixed(2)}
                                    </div>
                                    </div><hr />
                                    <div className="row font18">
                                        <div className="col-sm-6 text-left">
                                            Payment (Cash)
                                          <span className="pull-right">:</span>
                                        </div>
                                        <div className="col-sm-6">{parseFloat(paying_amount).toFixed(2)}</div>
                                    </div> <hr />
                                    {(parseFloat(paying_amount -(refundingAmount - before_paid_amount)).toFixed(2) > 0 ) ?
                                    <div className="row font18">
                                        <div className="col-sm-6 text-left">
                                            Change
                                          <span className="pull-right">:</span>
                                        </div>
                                        <div className="col-sm-6">{parseFloat(paying_amount -((parseFloat(refundingAmount) + parseFloat(CashRound)) - before_paid_amount)).toFixed(2)}</div>
                                    </div>
                                     :
                                     null
                                     }
                                      {(parseFloat(paying_amount -((parseFloat(refundingAmount) + parseFloat(CashRound)) - before_paid_amount)).toFixed(2) > 0 ) ?<hr />:null}
                                    <div className="row font18">
                                        <div className="col-sm-6 text-left">
                                            Balance
                                         <span className="pull-right">:</span>
                                        </div>
                                        <div className="col-sm-6" id="balance_left">0.00</div>
                                    </div>
                                </h3>
                            </div>
                            <div className="modal-footer" style={{ borderTop: 0 }}>
                                <button type="button" className="btn btn-primary btn-block h66" style={{ backgroundColor: '#d43f3a', borderColor: '#d43f3a', borderRadius: 5, height: 70 }} data-dismiss="modal" aria-hidden="true" id="popup_cash_rounding_button" onClick={()=>this.setCashRoundedValue(CashRound)}>Add Payment</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
function mapStateToProps(state) {
    const { single_Order_list, cash_rounding } = state;
    return {
        single_Order_list,
        cash_rounding: cash_rounding.items
    };
}

const connectedRefundView = connect(mapStateToProps)(RefundView);
export { connectedRefundView as RefundView };