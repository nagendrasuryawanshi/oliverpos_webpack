import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../../_actions'
import { history } from '../../_helpers';
import { saveCustomerInOrderAction } from '../../_actions/saveCustomerInOrder.action'
import { sendMailAction } from '../../_actions/sendMail.action'
import {  checkoutActions } from '../../CheckoutPage/actions/checkout.action'
import { LoadingModal } from '../../_components'

class RefundComplete extends React.Component {
    constructor(props){
        super(props);
        this.goToShopview = this.goToShopview.bind(this);
        this.sendMail = this.sendMail.bind(this);
        this.state={
            customer_email : "",
            isLoading:false,
            mailsucces:''

        }
      }

    goToShopview(){
        localStorage.removeItem("REFUND_DATA")
        //history.push('/shopview');
        window.location = '/shopview';
    }

    CancelSale(){
        // alert("c")
        this.setState({isLoading:true})

         this.props.dispatch(checkoutActions.orderToCancelledSale(this.state.order_id,this.state.udid));
 
         
 
       }
    
    componentWillMount(){
        let data =JSON.parse(localStorage.getItem("REFUND_DATA"));
        this.setState({
            customer_email : data.customer_email,
            order_id : data.order_id,
            udid : data.udid,
        })
    }

    sendMail(){
        $(".suctext").css("display", "block");
        
        let udid = this.state.udid;
        let order_id = this.state.order_id;        
        let email_id = $("#customer-email").val();

        let requestData = {
            "Udid": udid,
            "OrderNo" : order_id,
            "EmailTo" : email_id,
        }

        if ( $(".checkmark").hasClass("isCheck") ) {
            this.props.dispatch(saveCustomerInOrderAction.saveCustomerInOrder(udid, order_id, email_id));
        }

        this.props.dispatch( sendMailAction.sendMail( requestData ) );
    }

    handleInputChange(){
        $(".checkmark").toggleClass("isCheck");
    }

   componentWillReceiveProps(nextprops){

    if( (typeof nextprops.sendEmail !== 'undefined') &&   nextprops.sendEmail !== '' ){
     //   console.log("this.props.sendEmail",this.props.sendEmail);
        
        this.setState({mailsucces:nextprops.sendEmail.IsSuccess})

         if(nextprops.sendEmail && nextprops.sendEmail.IsSuccess == true ){
            setTimeout(
                this.goToShopview()
                , 2000);

       }
    }
   }


    render() {
         
         return (
            <div className="bgcolor1">
            { this.state.isLoading == true ? <LoadingModal /> : ''}
                <div className="content_main_wapper">
                    <div className="menu">
                        <div className="aerrow pull-left arrowText" onClick={()=>this.CancelSale()}> 
                            <a href="javascript:void(0)">
                                <div className="icon icon-backarrow-lftWhite"></div>
                                Cancel Sale
                    </a>
                        </div>
                        <div className="aerrow pull-right arrowText">
                            <a href="javascript:void(0)" onClick={() => this.goToShopview()}>
                                New Sale
                        <div className="icon icon-backarrow-rgtWhite"></div>
                            </a>
                        </div>
                    </div>
                    <div className="dialog-content">
                        <div className="loginBox" style={{ width: 411 }}>
                            <div className="login-form">
                                <div className="login-logo">
                                    <img src="assets/img/accepted.png" className="" />
                                </div>
                                <h2 className="chooseregister optima">Refund Complete</h2>
                                <form className="login-field" action="#">
                                    <div className="input-group">
                                        <span className="input-group-addon group-addon-custom" id="basic-addon1">Email Receipt</span>
                                        <input type="text" id="customer-email" defaultValue={ (this.state.customer_email) ? this.state.customer_email : ""} className="form-control form-control-custom mt-0" placeholder="Customer Email" aria-describedby="basic-addon1" />
                                    </div>
                                    <div className="checkBox-custom">
                                        <div className="checkbox">
                                            <label className="customcheckbox">Remember Customer
                                                  <input type="checkbox" defaultChecked />
                                                <span className="checkmark checkUncheck isCheck" onClick={() => this.handleInputChange()}></span>
                                            </label>
                                        </div>
                                    </div>
                                    <button type="button" className="btn btn-login bgcolor2" onClick={() => this.sendMail()} >Sent Email Receipt</button>
                                </form>
                                {this.state.mailsucces &&this.state.mailsucces == true ?
                                <span className="suctext" style={{ display: "none" }}>Mail sent successfully.</span>
                                :
                                <span className="suctext" style={{ display: "none" }}>Mail sending failed.</span>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { shop_order,sendEmail } = state;
    return {
        shop_order:shop_order.items,
        sendEmail:sendEmail.sendEmail

    };
}
const connectedRefundComplete = connect(mapStateToProps)(RefundComplete);
export { connectedRefundComplete as RefundComplete };