import React from 'react';
import { connect } from 'react-redux';
import { refundActions } from '../actions/refund.action';
import { GetRoundCash } from '../../CheckoutPage/Checkout';
import { checkoutActions } from '../../CheckoutPage/actions/checkout.action';
import { LoadingModal } from '../../_components';
import { get_UDid } from '../../ALL_localstorage';
import { CardPayment, CashPayment, GlobalPayment, OtherPayment, NormalKeypad, StripePayment } from '../../_components/PaymentComponents';

class RefundViewThird extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refundingAmount: 0,
            refundPayments: 0,
            refundingPaidAmount: 0,
            cash_round: 0,
            single_Order_list: (typeof localStorage.getItem("getorder") !== 'undefined') ? JSON.parse(localStorage.getItem("getorder")) : null,
            refundingCashAmount: 0,
            paidAmountStatus: false,
            refunding_amount: 0,
            paymentTypeName: (typeof localStorage.getItem('PAYMENT_TYPE_NAME') !== 'undefined') ? JSON.parse(localStorage.getItem('PAYMENT_TYPE_NAME')) : null,
            globalPayments: '',
            orderId: 0,
            globalStatus: false,
            isPaymentStart: false,
            activeDisplay: false,
            hideShowCash:false,
            refunding_cash_amount:0
        };

        this.setRefundPayment = this.setRefundPayment.bind(this);
        this.cashPayment = this.cashPayment.bind(this);
        this.hideCashTab = this.hideCashTab.bind(this);
    }


    componentDidMount() {
        this.props.onRef(this)

    }

    componentWillUnmount() {
        this.props.onRef(null)
    }

    cashPayment(paymentType) {
        const { single_Order_list } = this.state;
        let order_id = single_Order_list.order_id
        let store_credit = single_Order_list.orderCustomerInfo && single_Order_list.orderCustomerInfo.store_credit;
       // console.log("cashPayment", paymentType, store_credit ,order_id , this.state.refundingAmount, this.state.refunding_amount )
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        if (paymentType == 'cash') {
            let CashRound = parseFloat(GetRoundCash(cash_rounding, this.state.refundingAmount))
            if( this.state.refundingAmount + CashRound  == this.state.refunding_amount ){
                this.setState({
                    cash_round: CashRound,
                    refundingAmount: this.state.refundingAmount + CashRound
                })
            }else{
                this.setState({
                    cash_round: 0,
                    refundingAmount: this.state.refundingAmount
                })
            }
           
            this.setRefundPayment(order_id, paymentType)
        } else {
            this.setState({
                cash_round: 0,
                refundingAmount: this.state.refundingAmount - this.state.cash_round
            })
            // if (store_credit < this.state.refundingAmount) {
            //     this.props.min_max_add(`Store Credit Balance is $${parseFloat(store_credit).toFixed(2)}`)
            // } else {
            //     // alert(" sufficiant ");
            //     this.setRefundPayment(order_id, paymentType)
            // }
            this.setRefundPayment(order_id, paymentType)

        }
    }

    pay_amount(code) {
        let order_id = this.state.single_Order_list && this.state.single_Order_list.order_id;
        //console.log("order_id", order_id, code, this.state.refunding_amount)
        if (code == 'global_payments') {
            this.globalPayments(order_id, code)
        } else {
            this.setRefundPayment(order_id, code)
        }

    }

    setRefundPayment(order_id, paymentType) {
        console.log("amount1", order_id, paymentType, this.state.refunding_amount , this.state.refunding_cash_amount)
        this.setState({
            globalStatus: false,
            hideShowCash:false
        })
        let paymentAmount = 0;
        if (typeof paymentType !== 'undefined') {
            // if (this.state.paidAmountStatus == true) {
            //     paymentAmount = this.state.refunding_amount;
            // } else {
                paymentAmount = this.state.refunding_amount !== 0 ? this.state.refunding_amount : this.state.refunding_cash_amount !==0 ? parseFloat(this.state.refunding_cash_amount):this.state.refundingAmount;
            //}

            let paymentAmount2 = this.state.refunding_amount !== 0 ? this.state.refunding_amount : this.state.refunding_cash_amount !==0 ? parseFloat(this.state.refunding_cash_amount).toFixed(2):this.state.refundingCashAmount;
            console.log("inside" , paymentAmount , paymentAmount2)
            //console.log("paymentAmount", paymentAmount, parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2))
            // console.log("paymentAmount2", paymentAmount,  parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2))
           //  console.log("inside other => " , parseFloat(paymentAmount) , this.state.refundingAmount , this.state.refundingPaidAmount)
            if (paymentType !== 'cash') {
                this.setState({ paidAmountStatus: false })
                this.state.paidAmountStatus = false;
                //console.log("inside other")


                if (paymentAmount <= 0) {
                    this.props.min_max_add(`Paying amount should be greater than $0.00`);

                } else if (parseFloat(paymentAmount) > parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount)) {

                    if (parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2) == 0) {

                        this.props.min_max_add('Please select any order items.')

                    } else {
                       // alert()
                        this.props.min_max_add('Paying amount should not be greater than total amount.')
                    }

                } else {
                    this.props.addPayment(order_id, paymentType, paymentAmount);
                    return true;
                }

            } else {
                //console.log("inside cash")
                // console.log("inside cash => " , paymentAmount2 , this.state.refundingCashAmount , this.state.refundingPaidAmount)
                if (paymentAmount2 <= 0) {
                    this.props.min_max_add('Paying amount should be greater than $0.00')

                } else if (paymentAmount2 > parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2)) {

                    if (parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2) == 0) {
                        this.props.min_max_add('Please select any order items.')

                    } else {
                       
                        this.props.addPayment(order_id, paymentType, paymentAmount2);
                    }
                } else {
                    this.props.addPayment(order_id, paymentType, paymentAmount2);
                    return true;
                }
            }

        } else {
            this.props.min_max_add(`Please select a valid payment mode.`)
        }

        return false;
    }

    setRefundAmount(amount) {
        //alert()
       // console.log("amount->", amount)
        let cash_rounding = this.props.cash_rounding && this.props.cash_rounding.Content;
        let CashRound = parseFloat(GetRoundCash(cash_rounding, amount))
        this.setState({
            refundingAmount: amount,
            refundingCashAmount: parseFloat(amount) + parseFloat(CashRound),
            // refunding_amount  : amount
            // paidAmountStatus : amount == 0  && paidAmountStatus == true ? false : true
        });
    }

    setrefundingPaidAmount(amount) {
       // console.log("paid amount" , amount, this.state.refundingAmount)
        this.setState({
            refundingPaidAmount: amount,
            refunding_amount: this.state.refundingAmount - amount
        });
    }

    handleFocus = (event) => {
        event.target.select();
    };

    globalPayments(order_id, paycode) {
        this.setState({
            globalPayments: paycode,
            orderId: order_id,
            globalStatus: true,
            isPaymentStart: true
        })
        let paymentAmount = 0;
        if (this.state.paidAmountStatus == true) {
            paymentAmount = this.state.refunding_amount;
        } else {
            paymentAmount = this.state.refunding_amount !== 0 ? this.state.refunding_amount : this.state.refundingAmount;
        }
        var UID = get_UDid('UDID');
        this.setState({
            refunding_amount: paymentAmount,
            paidAmountStatus: true
        })
         this.props.dispatch(checkoutActions.getMakePayment(UID, localStorage.getItem('register'), paycode, paymentAmount))
        //this.setRefundPayment(order_id , paycode)
    }

    componentWillReceiveProps(nextProp) {
        if (nextProp.global_payment) {
            if (this.state.globalStatus === true) {
                this.setState({ isPaymentStart: false })
                if (nextProp.global_payment.IsSuccess === true) {
                    this.setRefundPayment(this.state.orderId, this.state.globalPayments)
                }
            }
        }

    }

    activeDisplay(st) {
        this.setState({ activeDisplay: st })
    }

    closingTab(st) {
        this.setState({ closingTab: st })
    }

    normapNUM(val) {
        console.log("normapNUM", val , this.state.hideShowCash)
        console.log("paid amount is", this.state.refundingCashAmount , this.state.refundingAmount, this.state.refundingPaidAmount)
        // let new_amount = 0;
        // let payments = JSON.parse(localStorage.getItem("oliver_refund_order_payments"));
        // payments && payments.map(itemP=>{
        //     new_amount += parseFloat(itemP.payment_amount)
        // })
         
        if(this.state.hideShowCash == true){
            this.state.hideShowCash = false
            this.setState({
                refunding_amount: 0,
                refunding_cash_amount:parseFloat(this.state.refundingAmount) - parseFloat(this.state.refundingPaidAmount)
            })
        }else{
            this.setState({
                refunding_amount: val,
                refunding_cash_amount:0
            })
        }
       
    }

    hideCashTab(st){
        //alert('when close cash tab'+st)
        this.state.hideShowCash = st
    }

    render() {
        const { single_Order_list, paidAmountStatus, paymentTypeName, activeDisplay } = this.state;
        let getorder = single_Order_list;
        //console.log("refunding_amount", this.state.refunding_amount, this.state.refundingAmount , this.state.refundingPaidAmount)
        return (
            <div className="col-lg-5 col-md-5 col-sm-6 col-xs-12 pt-4 plr-8">
                <div className="block__box white-background round-8 full_height overflowscroll text-center pl-3 pr-3">
                    <h2>Refund Amount</h2>
                    <div className="wrapper_accordion">

                        <NormalKeypad
                            paidAmount={this.state.refunding_amount}
                            parkOrder='refund'
                            paidAmountStatus={paidAmountStatus}
                            placeholder={parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2)}
                            normapNUM={(text) => this.normapNUM(text)}
                            closing_Tab={this.state.closingTab}
                            closingTab={(text) => this.closingTab(text)}
                            handleFocus={this.handleFocus}
                            activeDisplay={(text) => this.activeDisplay(text)}
                            styles={activeDisplay == false || activeDisplay == 'undefined_true' ? '' : 'none'}
                        />



                        {(typeof paymentTypeName !== 'undefined') && paymentTypeName !== null ?
                            paymentTypeName.map((pay_name, index) => {
                                return (
                                    <div key={index}>
                                        {this.state.isPaymentStart === true ? <LoadingModal /> : ""}
                                        {pay_name.Code == 'global_payments' ?
                                            (pay_name.HasTerminal == true && pay_name.TerminalCount > 0) ?

                                                <GlobalPayment
                                                    color={pay_name.ColorCode}
                                                    Name={pay_name.Name}
                                                    code={pay_name.Code}
                                                    pay_amount={(text) => this.pay_amount(text)}
                                                    msg={this.props.global_payment ? this.props.global_payment.Message : "Waiting On Terminal"}
                                                    activeDisplay={(text) => this.activeDisplay(text)}
                                                    styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                />


                                                :
                                                null
                                            :


                                            pay_name.Code == 'cash' ?
                                                <CashPayment
                                                    type='refund'
                                                    color={pay_name.ColorCode}
                                                    Name={pay_name.Name}
                                                    paidAmount={this.state.refunding_amount}
                                                    placeholder={parseFloat(this.state.refundingCashAmount - this.state.refundingPaidAmount).toFixed(2)}
                                                    pay_amount={(text) => this.cashPayment(text)}
                                                    code={pay_name.Code}
                                                    paidAmountStatus={paidAmountStatus}
                                                    normapNUM={(text) => this.normapNUM(text)}
                                                    cash_rounding={this.props.cash_rounding}
                                                    closingTab={(text) => this.closingTab(text)}
                                                    handleFocus={this.handleFocus}
                                                    activeDisplay={(text) => this.activeDisplay(text)}
                                                    styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                    hideCashTab={this.hideCashTab}
                                                />


                                                :
                                                // pay_name.Code == 'card' ?
                                                //     <CardPayment
                                                //         color={pay_name.ColorCode}
                                                //         Name={pay_name.Name}
                                                //         code={pay_name.Code}
                                                //         activeDisplay={(text) => this.activeDisplay(text)}
                                                //         styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                //     />
                                                //     :
                                                    pay_name.Code == 'stripe' ?
                                                        <StripePayment
                                                            color={pay_name.ColorCode}
                                                            Name={pay_name.Name}
                                                            code={pay_name.Code}
                                                            pay_amount={(text) => this.pay_amount(text)}
                                                            activeDisplay={(text) => this.activeDisplay(text)}
                                                            styles={activeDisplay == false || activeDisplay == `${pay_name.Code}_true` ? '' : 'none'}
                                                        />

                                                        :
                                                        <OtherPayment
                                                            type='refund'
                                                            orderId={single_Order_list.order_id}
                                                            color={pay_name.ColorCode}
                                                            Name={pay_name.Name}
                                                            code={pay_name.Code}
                                                            pay_amount={(text) => this.pay_amount(text)}
                                                            closingTab={(text) => this.closingTab(text)}
                                                            styles={activeDisplay == false ? '' : 'none'}
                                                        />

                                        }
                                    </div>
                                )
                            })
                            : ''
                        }

                        {(typeof getorder.orderCustomerInfo !== 'undefined') && getorder.orderCustomerInfo !== null ?
                            <button onClick={() => { this.cashPayment('store-credit'); }} className="white-background box-flex-shadow box-flex-border mt-2 round-8 no-outline w-100 p-0 overflow-0">
                                <div style={{ borderColor: '#46A9D4' }} id="store-credit" value="store-credit" name="payments-type" className="d-flex box-flex box-flex-border-left box-flex-background-global border-dynamic">
                                    <div className="box-flex-text-heading">
                                        <h2>Store-Credit</h2>
                                    </div>
                                </div>
                            </button>
                            :
                            null
                        }


                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { refundlist, cash_rounding, global_payment } = state;
    return {
        refundlist,
        cash_rounding: cash_rounding.items,
        global_payment: global_payment.items
    };
}

const connectedRefundViewThird = connect(mapStateToProps, refundActions)(RefundViewThird);
export { connectedRefundViewThird as RefundViewThird };