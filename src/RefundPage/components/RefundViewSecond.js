import React from 'react';
import { connect } from 'react-redux';
import { refundActions } from '../actions/refund.action';


class RefundViewSecond extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            single_Order_list:(typeof localStorage.getItem("getorder") !== 'undefined')?JSON.parse(localStorage.getItem("getorder")):null,
        }
    }

    render() {
        
        // const { single_Order_list } = this.props;
        // let getorder = single_Order_list.items.Content;
        const { single_Order_list } = this.state;
        let getorder = single_Order_list;
        return (
            <div className="col-lg-7 col-md-7 col-sm-6 col-xs-12 pt-4 plr-8">
                <div className="items preson_info">
                    <div className="item-heading text-center font24">Customer Information</div>
                    <div className="panel panel-default panel-product-list overflowscroll" id="UserInfo_refund" style={{ borderColor: '#979797' }}>
                        <div className="singleName">
                            <h4 className="mt-0 mb-2">
                            { (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.customer_name : "---" }
                             <a className="edit-info" data-toggle="modal" href="#edit-info"> { (getorder.orderCustomerInfo != null) ? 'Edit Information' : null }</a>
                            </h4>
                        </div>
                        <div className="personal_info">
                            <strong className="lead_personal">Personal Info:</strong>
                            <div className="mt-3 mb-3">
                                <p className="clearfix">
                                    <span className="text-muted">Email:</span>
                                    <span className="text-danger"> { (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.customer_email : "---" } </span>

                                </p>
                                <p className="clearfix">
                                    <span className="text-muted">Address:</span>
                                    <span className="text-danger">{ (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.customer_address : "---" }</span>
                                </p>
                                <p className="clearfix">
                                    <span className="text-muted">Phone:</span>
                                    <span className="text-danger"> { (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.customer_phone : "---" } </span>
                                </p>
                                <p className="clearfix">
                                    <span className="text-muted">Notes:</span>
                                    <span className="text-danger">{ (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.customer_note : "---" }</span>
                                </p>
                            </div>
                        </div>
                        <div className="personal_info">
                            <strong className="lead_personal">Account Info:</strong>
                            <div className="mt-3 mb-3">
                                {/* <p className="w-50-block text-center clearfix">
                                    <span className="text-muted w-100">Account Balance</span>
                                    <span className="text-danger w-100"> 0 </span>
                                </p> */}
                                <p className="w-50-block text-center clearfix">
                                    <span className="text-muted w-100">Store Credit</span>
                                    <span className="text-danger w-100"> { (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.store_credit : "---" } </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { refundlist } = state;
    return {
        refundlist
    };
}

const connectedRefundViewSecond = connect(mapStateToProps, refundActions)(RefundViewSecond);
export { connectedRefundViewSecond as RefundViewSecond };