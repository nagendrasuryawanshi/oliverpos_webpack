import { refundConstants } from '../constant/refund.constants'
import { refundService } from '../services/refund.service';
import { history } from '../../_helpers';
import { idbProductActions } from '../../_actions/idbProduct.action'
var alert_box ;

export const refundActions = {
    refundOrder,      
};

function myFunction(text) {
    var txt = alert(text);
    //console.log("alert_box",txt)
    if(typeof txt == 'undefined'){
        window.location = 'activity';
    }
}

function refundOrder( data ) {
    return dispatch => {
        dispatch(request());

        refundService.refundOrder( data )
            .then(
                // refundOrderResponse => dispatch(success( refundOrderResponse )),
                
                refundOrderResponse =>{
                    console.log("RefundActionData",data);
                    //update Refund qty for product......................
                    refundOrderResponse.IsSuccess? dispatch(idbProductActions.updateOrderProductDB(null,data)):"";                    
                    //----------------------------------------------------
                    
                    (refundOrderResponse.IsSuccess) ?                    
                 setTimeout(function() {
                    localStorage.setItem("REFUND_DATA",JSON.stringify(data))
                   
                    if(JSON.parse( localStorage.getItem("user")).display_sale_refund_complete_screen == false){
                        window.location = 'activity';
                    }else{
                        window.location='/refund_complete';
                    }
                  
                 }, 1000)
                
               // setTimeout(function() {  history.push('/refund_complete', data); }, 5000) 
                : 
               // alert_box = confirm(refundOrderResponse.Message), 
               // console.log("alert_box",alert_box),
                 myFunction(refundOrderResponse.Message) ,

                //(typeof alert_box == 'undefined')?window.location='/activity':null,
                error => dispatch(failure(error.toString()))
           }  );
               
    };

    function request() { return { type: refundConstants.REFUND_GETALL_REQUEST } }
    function success( refundOrderResponse ) { return { type: refundConstants.REFUND_GETALL_SUCCESS, refundOrderResponse } }
    function failure( error ) { return { type: refundConstants.REFUND_GETALL_FAILURE, error } }
}



