import { pinLoginConstants  } from '../constants/pinlogin.constants';

export function pinlogin(state = {}, action) {
    switch (action.type) {
        case pinLoginConstants.PIN_LOGIN_REQUEST:
            return {
                loading: true
            };
        case pinLoginConstants.PIN_LOGIN_SUCCESS:
            return {
                loading: false,
                alert: action.logindetail
            };
        case pinLoginConstants.PIN_LOGIN_FAILURE:
       
            return {
                loading: false,
                error: action.error
            };

        default:
            return {
                loading: false,
                state
            }
    }
}

// export function register_single_list(state = {}, action) {
//     switch (action.type) {
//         case pinLoginConstants.SUBCRIPSTION_STATUS_REQUEST:
//             return {
//                 loading: true
//             };
//         case pinLoginConstants.SUBCRIPSTION_STATUS_SUCCESS:
//             return {
//                 items: action.register_single_list
//             };
//         case pinLoginConstants.SUBCRIPSTION_STATUS_FAILURE:
//             return {
//                 error: action.error
//             };

//         default:
//             return state
//     }
// }