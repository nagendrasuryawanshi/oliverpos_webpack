import Config from '../../Config'
import { authHeader } from '../../_helpers';
import { history } from '../../_helpers'
import {encode_UDid,get_UDid} from '../../ALL_localstorage'

export const pinLoginService = {
    pinLogin,
    logout,
    checkSubcriptionStatus
   
};
const API_URL=Config.key.OP_API_URL

function pinLogin(pin) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };
   // return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
  // var UDID=localStorage.getItem('UDID')
//    let decodedString = localStorage.getItem('UDID');
//    var decod=  window.atob(decodedString);
   var UDID= get_UDid('UDID');
   console.log("checkstatus",UDID)
   //Location
   return fetch(`${API_URL}/ShopAccess/VerifyPin?udid=`+UDID+'&pin='+pin, requestOptions)
        .then(handleResponse)
        .then(response => {
            // login successful if there's a jwt token in the response
        console.log("LoginResponse",response);
            if (response.Message=="Success") {    

                //var userData=btoa(JSON.stringify(response.Content))
              //  console.log("userData",userData);
                localStorage.setItem('user', JSON.stringify(response.Content)); 


                if(typeof(Storage) !== "undefined") {   
                    localStorage.setItem("check_subscription_status_datetime", new Date());               
                }

                // fetch(`${API_URL}/Subscription/CheckStatus?Udid=`+UDID, requestOptions)
                // .then(handleResponse)
                // .then(response => {
                //     console.log()
                //     if(response.Content.subscription_status != "active"){
                //         localStorage.setItem("userstatus",JSON.stringify(res));
                //         history.push('/revalidate');
                //       }  
                //   if(response.Content != "active"){
                //     history.push('/revalidate');
                //   }                    
           //})   
             //     return response.Message;
            // }
            // else
            // {
                return response.Message;
            }else{
                return response.Message;
            }
            
        }).catch(function(error) {
            console.log("LoginResponseErr",error);
            return 'Unable to login';
        })
}


        function checkSubcriptionStatus() {
           // var UDID=localStorage.getItem('UDID');
            // let decodedString = localStorage.getItem('UDID');
            // var decod=  window.atob(decodedString);
            var UDID= get_UDid('UDID');
            console.log("checkstatus",UDID)
                const requestOptions = {
                    method: 'GET', 
                    headers: {
                        "access-control-allow-origin": "*",
                        "access-control-allow-credentials": "true", 
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
                    } 
                    ,mode: 'cors'
                };
            
         return    fetch(`${API_URL}/Subscription/CheckStatus?Udid=`+UDID, requestOptions)
                    .then(handleResponse)
                    .then(response => {
                       
                        return  response;
                });
            }


function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}