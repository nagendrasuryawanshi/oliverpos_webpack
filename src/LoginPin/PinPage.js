import React from 'react';
import { connect } from 'react-redux';
import { pinLoginActions } from './action/PinLogin.action';
import { history } from '../_helpers';
import { LoadingModal } from '../_components/LoadingModal';
import { checkShopSTatusAction } from '../_actions';

class PinPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            totalSize: 0,
            txtValue: "",
            isloading: false,
            LocationName: '',
            RgisterName: '',
            notxtValue:''


        };
        this.props.dispatch(checkShopSTatusAction.getStatus());
        this.handle = this.handle.bind(this);
        this.handleBack = this.handleBack.bind(this)
        localStorage.setItem("ReloadCount", 0);
    }

    handle(e) {
        const { value } = e.target;
        // var key = e.which || e.keyCode;
        // if (key === 8) {
        //     this.resetScreen()
        // }
        // else {
        const re = new RegExp('^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$')
        if (value === '' || re.test(value)) {
            console.log("value", value)
            this.addToScreen(value)
        }
        //}
    }

    handleBack(e) {
        var key = e.which || e.keyCode;
        if (key === 8) {
            this.addToScreen('c')
            e.preventDefault();
        }
    }

    componentWillMount() {

        let decodedString = localStorage.getItem('UDID');
        var decod = window.atob(decodedString);
        var getudid = decod;
        this.setState({
            LocationName: localStorage.getItem(`last_login_location_name_${getudid}`),
            RgisterName: localStorage.getItem(`last_login_register_name_${getudid}`)
        })
        if (!getudid || !localStorage.getItem('Location') || !localStorage.getItem('register')) {
            history.push('/login');
        }
    }

    log_out() {
        localStorage.removeItem("sitelist")
        // localStorage.removeItem('UDID')
        // localStorage.removeItem('Location')
        localStorage.removeItem('CUSTOMER_ID')
        localStorage.removeItem('AdCusDetail')
        localStorage.removeItem('CARD_PRODUCT_LIST')
        // localStorage.clear();
        localStorage.removeItem("Productlist" + localStorage.getItem('UDID'));
        history.push('/login');
    }

    addToScreen(e) {
        console.log("e", typeof e, e) 
        let lenght_is = e.length - 1
        let newString = e[lenght_is];
        console.log("newString", newString)
        if (e == "c") {
            if (this.state.totalSize > 0) {
            // console.log("txt1",this.state.txtValue);
            //     var txtVal=this.state.txtValue;
            //     this.state.txtValue= txtVal.substring(0, txtVal.length - 1);
            //     console.log("txt2",this.state.txtValue);
                this.resetScreen();
            }else {
                this.state.totalSize=0;
                this.state.txtValue =''
            }
            return;
        }
        if (this.state.totalSize < 4) {
            this.state.txtValue += newString;
            this.state.totalSize += 1;
            setTimeout(function () {
                this.fillPass();
            }.bind(this), 100)

        }
        console.log("txtvalue", this.state.txtValue)

        $('#whichkey').focus()
    }

    resetScreen() {
        // console.log("txt1",this.state.txtValue);
        // console.log("l1",this.state.totalSize);
       // this.setState({ txtValue: this.state.txtValue.substring(0, this.state.txtValue.length - 1) })
        let str = this.state.txtValue;
        if(this.state.totalSize > 0){
            this.state.totalSize -= 1;
            this.state.txtValue= str.substring(0, str.length - 1) ;
        }else{
            this.state.totalSize = 0;
            this.state.txtValue = 0
        }
        // console.log("txt2",this.state.txtValue);
        // console.log("l2",this.state.totalSize);
        this.fillPass();

    }

    fillPass() {
        let i = 1;
        for (i = 1; i <= 4; i++) {

            if (this.state.totalSize >= i) {
                if (this.state.totalSize >= 4) {
                    //const { txtValue } = this.state;
                   // console.log("txtValue",this.state.txtValue)
                    const { dispatch } = this.props;
                    if (this.state.isloading == false) {
                        this.setState({ isloading: true })
                        setTimeout(function () {    //Need delay for reaset text
                            dispatch(pinLoginActions.pinLogin(this.state.txtValue));
                            this.state.txtValue="";
                            this.state.totalSize=0;
                        }.bind(this), 100)
                    }
                }
                $("#txt" + i).removeClass("bg_trasn");

            }
            else {
                $("#txt" + i).addClass("bg_trasn");
            }
        }
    }

    componentWillReceiveProps() {
        const { alert, pinlogin } = this.props;
        if ((alert && alert !== "") || (pinlogin == false)) {
            console.log('when api call')
            $("#txt1").addClass("bg_trasn");
            $("#txt2").addClass("bg_trasn");
            $("#txt3").addClass("bg_trasn");
            $("#txt4").addClass("bg_trasn");
            this.state.totalSize = 0;
            this.state.txtValue = "";
            this.setState({
                totalSize:0 ,
                txtValue:''
            })
            setTimeout(function () {
                this.setState({
                    isloading: false
                })
            }.bind(this), 4000)

        }
    }

    whichkey() {
        $('#whichkey').focus();
    }

    render() {
        const { alert } = this.props;
        return (
            <div className="bgimg-1">
                {this.state.isloading ? <LoadingModal /> : ''}
                <div className="content_main_wapper" onClick={() => this.whichkey()}>
                    <a href="javascript:void(0)" className="aonboardingbackicon" onClick={() => history.push("/login_location")}>
                        <button className="button onboardingbackicon">&nbsp;</button>&nbsp; Go Back
                 </a>
                    <div className="aerrow pull-right arrowText aonboardingbackicon right-align_absolute">
                        <a href="javascript:void(0);" >{this.state.LocationName}/{this.state.RgisterName}</a>
                    </div>
                    <div className="onboarding-loginBox">
                        <div className="login-form">
                            <div className="onboarding-pg-heading">
                                <h1>Please Enter Your PIN</h1>
                                <p>
                                    Enter the 4 digit PIN you’ve set up with Oliver POS.
                              </p>
                            </div>
                            <div>
                                <div id="calculator" style={{ width: '100%' }}>
                                    <form>
                                    {/* style={{ backgroundColor: 'transparent', color: 'transparent' }} */}
                                        <input id="whichkey" maxLength="4" type="text" style={{ backgroundColor: 'transparent', color: 'transparent' }}  value={this.state.notxtValue} autoFocus onChange={this.handle} onKeyDown={this.handleBack} className="border-0 color-4b text-center w-100 p-0 no-outline enter-order-amount placeholder-color" />
                                        <p id="demo"></p>
                                        <table>
                                            <tbody>

                                                <tr>
                                                    <td>
                                                        <input type="button" value="1" id="keys" onClick={() => this.addToScreen('1')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="2" id="keys" onClick={() => this.addToScreen('2')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="3" id="keys" onClick={() => this.addToScreen('3')} className="b_top0 b_lft0 b_rgt0 input-to-number" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="4" id="keys" onClick={() => this.addToScreen('4')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="5" id="keys" onClick={() => this.addToScreen('5')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="6" id="keys" onClick={() => this.addToScreen('6')} className="b_top0 b_lft0 b_rgt0 input-to-number" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="7" id="keys" onClick={() => this.addToScreen('7')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="8" id="keys" onClick={() => this.addToScreen('8')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="9" id="keys" onClick={() => this.addToScreen('9')} className="b_top0 b_lft0 b_rgt0 input-to-number" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="" id="keys" onClick={() => this.addToScreen('')} className="b_top0 b_bottom0 b_lft0 input-to-number" readOnly={false} />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="0" id="keys" onClick={() => this.addToScreen('0')} className="b_top0 b_lft0 bb-0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="" id="keys" onClick={() => this.addToScreen('c')} className="b_top0 b_bottom0 b_lft0 b_rgt0 cal-back input-to-number" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>

                                    <div className="clearfix"></div>

                                    <div className="HightLIghtPassword">
                                        <div className="">
                                            <input id="txt1" type="button" className="bg_trasn" />
                                            <input id="txt2" type="button" className="bg_trasn" />
                                            <input id="txt3" type="button" className="bg_trasn" />
                                            <input id="txt4" type="button" className="bg_trasn" />
                                        </div>

                                    </div>
                                    {alert ? <div className="onboarding-pg-heading"> {alert}</div> : ""}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="copy_right">
                        <a onClick={() => this.log_out()}><img src="../assets/img/logout-btn_wht.png" /></a>
                    </div>
                    <div className="powered-by-oliver">
                        <a href="javascript:void(0)">Powered by Oliver POS</a>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { alert, pinlogin } = state;

    return {
        alert: alert.message,
        pinlogin: pinlogin.loading
    };
}

const connectedPinPage = connect(mapStateToProps)(PinPage);
export { connectedPinPage as PinPage };