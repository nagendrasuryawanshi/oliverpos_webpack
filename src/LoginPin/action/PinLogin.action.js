import { pinLoginConstants } from "../constants/pinlogin.constants";
import { pinLoginService } from "../services/PinLogin.service";
 import { alertActions } from "../../_actions/alert.actions";
 import { history } from "../../_helpers";
 import moment from 'moment';
export const pinLoginActions = {
  pinLogin,
  checkSubcription  
};

// <!-- Pin Login  -->
function pinLogin(pin) {
 
  return dispatch => {
    dispatch(request());
    pinLoginService
    .checkSubcriptionStatus(1)
     .then(res=>{
     
       localStorage.setItem("check_subscription_status_datetime", new Date());   
          if(res.IsSuccess==true)
          
          {       
            console.log("userDetail",res);
   
              if(res.Content.subscription_status != "active"){
                localStorage.setItem("userstatus",JSON.stringify(res));
                console.log("userDetail",res);
                history.push('/revalidate');
              }            
            else
            {
             console.log("subscription",res)
              pinLoginService
              .pinLogin(pin)
              .then(        
                user => { 
                console.log("userPin",user); 
                    if(user=="Success")
                    {
                      //console.log("toUTCString" ,  moment.utc(new Date()).format('YYYY-MM-DD HH:mm:ss'))
                      sessionStorage.setItem("issuccess",true)
                      if(localStorage.getItem("PRODUCT_REFRESH_DATE") == null){
                         localStorage.setItem("PRODUCT_REFRESH_DATE" , moment.utc(new Date()).format('YYYY-MM-DD HH:mm:ss'))
                       }
                      dispatch(success(user));
                      dispatch(alertActions.success("Login Successfully"));
                      //history.push('/shopview')
                    // window.location='/shopview';
                     window.location='/';
                  }
                  else{
                  
                    dispatch(failure(user.toString()));
                    dispatch(alertActions.error(user.toString()));
                    
                  }
                  },
                  error => {            
                      dispatch(failure(error.toString()));
                      dispatch(alertActions.error(error.toString()));            
                  }
              //  res =>{      
              //   dispatch(success(res)),
              //   error => dispatch(failure(error.toString()))
              //   }
              );
            }
        }
        else
        {
          
          dispatch(alertActions.error(res.Message));       
        }
     }, error => {            
      dispatch(failure(error.toString()));  
      dispatch(alertActions.error(error.toString()));            
  }) 

  };

  function request() {
    return { type: pinLoginConstants.PIN_LOGIN_REQUEST };
  }
  function success(logindetail) 
  {
    return { type: pinLoginConstants.PIN_LOGIN_SUCCESS, logindetail };
  }
  function failure(error) {
    return { type: pinLoginConstants.PIN_LOGIN_FAILURE, error };
  }
}

function checkSubcription(){
  return dispatch => {
    dispatch(request());

    pinLoginService
      .checkSubcriptionStatus(2)
       .then(res=>{
         localStorage.setItem("check_subscription_status_datetime", new Date());   
            if(res.IsSuccess==true)
            {
            //  console.log("check_subscription_status_datetime",res)
                if(res.Content.subscription_status != "active"){
                  localStorage.setItem("userstatus",JSON.stringify(res));
                  history.push('/revalidate');
                }  
                
            }
            else
            {
              history.push('/login');
            }
       }) 
                   
          // },
          error => {            
              dispatch(failure(error.toString()));
              dispatch(alertActions.error(error.toString()));            
          }
       //  res =>{      
      //   dispatch(success(res)),
      //   error => dispatch(failure(error.toString()))
      //   }
      // );
  };

  function request() {
    return { type: pinLoginConstants.SUBCRIPSTION_STATUS_REQUEST };
  }
  function success(status) 
  {
    return { type: pinLoginConstants.SUBCRIPSTION_STATUS_SUCCESS, status };
  }
  function failure(error) {
    return { type: pinLoginConstants.SUBCRIPSTION_STATUS_FAILURE, error };
  }




 }

// <!-- End  -->

// // <!-- Get All Device List  -->
// function getAllDeviceList() {
//   return dispatch => {
//     dispatch(request());

//     deviceService
//       .getAllDeviceList()
//       .then(
//         device_list => dispatch(success(device_list)),

//         error => dispatch(failure(error.toString()))
//       );
//   };

  // function request() {
  //   return { type: userConstants.GETALL_DEVICE_LIST_REQUEST };
  // }
  // function success(device_list) {
  //   return { type: userConstants.GETALL_DEVICE_LIST_SUCCESS, device_list };
  // }
  // function failure(error) {
  //   return { type: userConstants.GETALL_DEVICE_LIST_FAILURE, error };
  // }
// }

// <!-- End  -->

