import React from 'react';
import { connect } from 'react-redux';
import { history } from '../../_helpers'
import { favouriteListActions } from '../action/favouritelist.actions';
import { LoadingModal } from '../../_components/LoadingModal'
import {encode_UDid,get_UDid} from '../../ALL_localstorage'
import { Markup } from 'interweave';

class FavouriteItemView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading:false
         }

        this.tileFilterData = this.tileFilterData.bind(this);
    }

    redirect() {
        history.go('/');
    }

    tileFilterData(data ,type, index,parent='') {
        console.log("data",data,"type",type,"index",index,"parent",parent)
        this.props.tileFilter(data,type,parent);

        switch (type) {
            case "inner-sub-attribute":
                $('.inner-sub-attribute a').removeAttr("style");
                $(`#${index}_sub_attribute a`).css("color", "#A9D47D");
                break;
            case "inner-sub-category":
                $('.inner-sub-category a').removeAttr("style");
                $(`#${index}_sub_category a`).css("color", "#A9D47D");
                break;
        }
    }

    RemoveFavProduct(data) {
        //const UID = localStorage.getItem('UDID');
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        const UID= get_UDid('UDID');
        const favid = data.id
        this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));


        // this.redirect()
    }
//    componentWillMount(){
      
        // setTimeout(() => {
        //     this.setState({
        //         loading: true
        //   })
        //   setHeightDesktop();

        // }, 2000);
    // }
    componentDidMount(){
        setTimeout(function () {
            setHeightDesktop();
        }, 1000);
    }

    render() {
        var arrCheck= this.props.childCategory?this.props.childCategory:  this.props.SubAttributeList?  this.props.SubAttributeList:
                this.props.childSubAttributeList?this.props.childSubAttributeList:this.props.subchildCategory?this.props.subchildCategory:[];
           console.log("arrCheck",this.props.SubAttributeList);            
        return (
            <div className="category list-unstyled overflowscroll window-header">
             {/* {this.state.loading == false ? (typeof arrCheck =='undefined' ||  arrCheck == 0 || !arrCheck)? <LoadingModal /> : '':''}   */}
                <div className="pt-3 clearfix">
                    <div className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0" onClick={() => this.redirect()}>

                        <div className="category_list bg_green">
                            <a href="javascript:void(0)">
                                Back
                   </a>
                        </div>
                    </div>

                    {
                        this.props.childCategory && this.props.childCategory.map((item, index) => {
                            return (
                                <div key={index} id={`${index}_sub_category`} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 inner-sub-category" data-attribute-id={item.attribute_id} data-id={item.id} onClick={() => this.tileFilterData(item.Code, "inner-sub-category", index)} >
                                    <div className="category_list">
                                        <a href="javascript:void(0)">                                           
                                            <Markup content={ item.Value}></Markup>
                                        </a>
                                    </div>
                                    {/* <a className="delete" href="javascript:void(0)">
                                <span aria-hidden="true">x</span>
                                <span className="sr-only">Close</span>
                            </a> */}
                                </div>
                            )
                        })
                        // loop for display favorite product
                    }
                    {
                        this.props.SubAttributeList && this.props.SubAttributeList.map((item, index) => {
                           console.log("this.props.SubAttributeList",item);
                            return (
                                <div key={index} id={`${index}_sub_attribute`} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 inner-sub-attribute" data-attribute-id={item.attribute_id} data-id={item.id} onClick={() => this.tileFilterData(item.Code, "inner-sub-attribute", index,item.taxonomy.replace("pa_", ""))}>
                                    <div className="category_list">
                                        <a href="javascript:void(0)">
                                        <Markup content={ item.Value}></Markup>
                                        </a>
                                    </div>
                                    {/* <a className="delete" href="javascript:void(0)">
                                <span aria-hidden="true">x</span>
                                <span className="sr-only">Close</span>
                            </a> */}
                                </div>
                            )
                        })
                        // loop for display favorite product
                    }
                    {this.props.childSubAttributeList && this.props.childSubAttributeList ?

                        <div className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 inner-sub-attribute">
                            <div className="category_list" onClick={() => this.tileFilterData(this.props.childSubAttributeList.attribute_slug, "inner-sub-attribute", this.props.childSubAttributeList.attribute_slug,this.props.childSubAttributeList.parent_attribute.replace("pa_", ""))} id={`${this.props.childSubAttributeList.attribute_slug}_sub_attribute`}>
                                <a href="javascript:void(0)">
                                    {this.props.childSubAttributeList.attribute_slug} / {this.props.childSubAttributeList.parent_attribute.replace("pa_", "")}
                                </a>
                            </div>
                            <a className="delete" href="javascript:void(0)" >
                                <span aria-hidden="true" onClick={() => this.RemoveFavProduct(this.props.childSubAttributeList)} ><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                <span className="sr-only">Close</span>
                            </a>
                        </div> : null


                        // loop for display favorite sub attributes
                    }
                    {this.props.subchildCategory && this.props.subchildCategory ?
                        <div className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 inner-sub-category " >
                            <div className="category_list " onClick={() => this.tileFilterData(this.props.subchildCategory.category_slug, "inner-sub-category", this.props.subchildCategory.category_slug)} id={`${this.props.subchildCategory.category_slug}_sub_category`} >
                                <a href="javascript:void(0)" >
                                    {/* {this.props.subchildCategory.name} */}
                                    <Markup content={ this.props.subchildCategory.name}></Markup>
                                </a>
                            </div>
                            <a className="delete" href="javascript:void(0)">
                                <span aria-hidden="true" onClick={() => this.RemoveFavProduct(this.props.subchildCategory)}><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                <span className="sr-only">Close</span>
                            </a>
                        </div> : null


                        // loop for display favorite sub attributes
                    }
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { refundlist } = state;
    return {
        refundlist,

    };
}
const connectedFavouriteItemView = connect(mapStateToProps)(FavouriteItemView);
export { connectedFavouriteItemView as FavouriteItemView };

