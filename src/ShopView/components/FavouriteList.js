import React from 'react';
import { connect } from 'react-redux';
import { favouriteListActions } from '../action/favouritelist.actions';
import { FavouriteItemView } from '../components/FavouriteItemView'
import { cartProductActions } from '../../_actions/cartProduct.action'
import { Markup } from 'interweave';
import { encode_UDid, get_UDid } from '../../ALL_localstorage';
import { getTaxAllProduct } from '../../_components'
import { openDb, deleteDb } from 'idb';
import {allProductActions } from '../../_actions/allProduct.action'
class FavouriteList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            item: null,
            num: '',
            UDID: '',
            activeClass: null,
            productlist: [],
            favouritesItemsProduct: [],
            ticket_Product_status:false

        }
        //console.log("ticketfieldListfavourite1",localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'',"this.state.ticket_Product_status",this.state.ticket_Product_status);

        this.ActiveList = this.ActiveList.bind(this);
        this.tileProductListFilter = this.tileProductListFilter.bind(this);
        this.checkStatus=this.checkStatus.bind(this);

        var udid = get_UDid('UDID');
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },

        };

        idbKeyval.get('ProductList').then(val => {            
           // let TaxSetting = getTaxAllProduct(val)
            //  console.log("value1233",val);
            this.state.productlist = val;
            this.setState({ productlist: val });           
        }
        );

    }

    checkStatus(){
        //-------------------------------------------
       // console.log("shopstatus",localStorage.getItem("shopstatus"));
      if(localStorage.getItem("shopstatus") && localStorage.getItem("shopstatus")!==null) {
        var sopstatus= JSON.parse(localStorage.getItem("shopstatus")?localStorage.getItem("shopstatus"):"");

        //console.log("componentDidMount",sopstatus.IsSuccess)

        if(sopstatus !=="" && sopstatus.IsSuccess==false )
        {                
            $('#PopupShopStatus').modal('show');
        }
        //---------------------------------------------
      }
}

    componentWillMount() {
        const { dispatch } = this.props;
        //  const UID = localStorage.getItem('UDID');

        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        const UID = get_UDid('UDID');

        const register_Id = localStorage.getItem('register');
       // console.log("register_Id", register_Id);
        dispatch(favouriteListActions.getAll(UID, register_Id));
    }

    tileProductListFilter(data, type, parent) {
      //  console.log("favlistTilefilter", data, type, parent)
        this.props.tileFilterData(data, type, parent)
    }
    getTicketFields(product,tick_type=null) {
       // console.log("getTicketFields",product);
        var tick_data= JSON.parse(product[0].TicketInfo)
        //var  udid=4040246357
        var   form_id=tick_data._owner_form_template
      //  console.log("ticketfield1",form_id);

       this.props.dispatch(allProductActions.ticketFormList(form_id));
        this.state.ticket_Product_status=true
       this.state.tick_type=tick_type
       this.state.ticket_Product=product
       this.setState({
           ticket_Product:product,
           ticket_Product_status:true,

          // tick_type:'simpleadd'
         })

       // }
    }
    ActiveList(item, index, type,ticketFields=null) {
       // console.log("item122322", item, "index", index, "type", type,"ticketFields",ticketFields);
        this.tileProductListFilter(item, type);
        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        const UID = get_UDid('UDID');
        this.setState({ UDID: UID })
      //  console.log("this.state.UDID", UID);
        if (item.attribute_slug) {
            this.setState({
                active: true,
                item: item,
                num: index
            })
            
          this.props.dispatch(favouriteListActions.getSubAttributes(UID, item.attribute_code));
           // this.props.dispatch(favouriteListActions.getSubAttributes(UID, item.attribute_slug));

        }
        if (item.category_id) {
            this.setState({
                active: true,
                item: item,
                num: index
                // , ListItem2:list.product_list
            })
            this.props.dispatch(favouriteListActions.getChildCategories(UID, item.category_id));

        }
        if (item.category_slug) {
            // console.log("category_slug",item);
            this.setState({
                active: true,
                item: item,
                num: index
                // , ListItem2:list.product_list
            })

        }
        if (item.parent_attribute) {
            // console.log("parent_attribute",item);
            this.setState({
                active: true,
                item: item,
                num: index
                // , ListItem2:list.product_list
            })

        }

        if (item.Product_Id?item.Product_Id:item.WPID) {

            if (item.Type == "variable") {
                var data = {
                    item: item,
                    DefaultQunatity: 1
                }
                var  getfilterProduct = this.state.productlist && this.state.productlist.filter(prodlist => {
                    // console.log("prodlist",prodlist,prodlist.WPID,item.Product_Id)
                    return prodlist.WPID == item.Product_Id
                 });

                    if(getfilterProduct[0] && getfilterProduct[0].IsTicket == true ){
                        this.getTicketFields(getfilterProduct);
                      }    
            //  console.log("getfilterProduct",getfilterProduct);   
                this.props.productData(data);

                $('#VariationPopUp').modal('show');
                localStorage.setItem('Favproduct', JSON.stringify(data))

                //this.props.dispatch(favouriteListActions.variationProdList(data));
            }
            //  const { cartproductlist } = this.props; 

            var cartlist = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []
            var qty = 0;
            cartlist.map(items => {
                if (item.Product_Id == items.product_id) {
                    qty = items.quantity;
                }
            })
            //  console.log("ProductQTY",qty);
            //  var cartlist=cartproductlist?cartproductlist:[];//this.state.cartproductlist;    
            if (item.Type !== "variable") {
                 //  console.log("item",item,"productlist",this.state.productlist,"item.Product_Id",item.Product_Id);
             //   console.log("item",item);
               var  getfilterProduct = ticketFields==null && this.state.productlist && this.state.productlist.filter(prodlist => {
                   // console.log("prodlist",prodlist,prodlist.WPID,item.Product_Id)
                   return prodlist.WPID == item.Product_Id
                });
               //  getfilterProduct=getfilterProduct == false &&  ticketFields !==null?item:''

             //   console.log("getfilterProduct", getfilterProduct,"ticketFields",ticketFields);

                if (  ticketFields==null && getfilterProduct[0] && getfilterProduct[0].IsTicket == true ) {
                   // console.log("getfilterProduct1234");
                      var tick_typ='simpleadd' 
                     
                    this.getTicketFields(getfilterProduct,tick_typ)


                }
            if(getfilterProduct[0] && getfilterProduct[0].IsTicket == false){    
                if ((item.StockStatus == null || item.StockStatus == 'instock')
                    && (item.ManagingStock == true && (parseFloat(item.Stock) <= 0 || qty >= parseFloat(item.Stock)))) {
                    $('#outOfStockModal').modal('show');
                }
                // else if(qty>= parseFloat(item.Stock))
                // {
                //     //console.log("alert");
                //     $('#outOfStockModal').modal('show');
                // }
                else {

                    var data = {
                        line_item_id: 0,
                        quantity: 1,
                        Title: item.Title,
                        Price: parseFloat(item.Price),
                        product_id: item.Product_Id,
                        variation_id: 0,
                        isTaxable: true,
                        old_price: item.old_price,
                        incl_tax: item.incl_tax,
                        excl_tax: item.excl_tax,
                        after_discount: item.after_discount ? item.after_discount : 0,
                        discount_amount: item.discount_amount ? item.discount_amount : 0,
                        cart_after_discount: item.cart_after_discount ? item.cart_after_discount : 0,
                        cart_discount_amount: item.cart_discount_amount ? item.cart_discount_amount : 0,
                        product_after_discount: item.product_after_discount ? item.product_after_discount : 0,
                        product_discount_amount: item.product_discount_amount ? item.product_discount_amount : 0,
                        ticket_status:getfilterProduct[0].IsTicket,

                    }

                    localStorage.setItem('Favproduct', null)
                    cartlist.push(data)
                    this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));
                  }

               }   else if( getfilterProduct == false && item && item.IsTicket == true && ticketFields!=null){ 
                    
                     this.setState({ticket_Product_status:false})
                   var tick_data=item && item.TicketInfo != ''?JSON.parse(item.TicketInfo):'';

               //  console.log("ticketFields loop  in",ticketFields,item);

                 if ((item.StockStatus == null || item.StockStatus == 'instock')
                 && (item.ManagingStock == true && (parseFloat(item.Stock) <= 0 || qty >= parseFloat(item.Stock)))) {
                 $('#outOfStockModal').modal('show');
                 }
                   var data = {
                    line_item_id: 0,
                    quantity: 1,
                    Title: item.Title,
                    Price: parseFloat(item.Price),
                    product_id: item.WPID,
                    variation_id: 0,
                    isTaxable: true,
                    old_price: item.old_price,
                    incl_tax: item.incl_tax,
                    excl_tax: item.excl_tax,
                    after_discount: item.after_discount ? item.after_discount : 0,
                    discount_amount: item.discount_amount ? item.discount_amount : 0,
                    cart_after_discount: item.cart_after_discount ? item.cart_after_discount : 0,
                    cart_discount_amount: item.cart_discount_amount ? item.cart_discount_amount : 0,
                    product_after_discount: item.product_after_discount ? item.product_after_discount : 0,
                    product_discount_amount: item.product_discount_amount ? item.product_discount_amount : 0,
                    tick_event_id:tick_data._event_name,
                    ticket_status:item.IsTicket,
                    product_ticket: ticketFields,
                }
                      localStorage.setItem('Favproduct', null)
                    cartlist.push(data)
                    this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));


               }

            }

        }

    }
    imgError(image) {
        //console.log("image", image)
        image.onerror = null;
        image.className = 'errorImage'
        image.src = "assets/img/placeholder.png";
        return true;
    }

    RemoveFavProduct(data) {
      //  console.log("data",data)
        if (data.category_slug) {
            //    let decodedString = localStorage.getItem('UDID');
            //    var decod=  window.atob(decodedString);
            const UID = get_UDid('UDID');
            const favid = data.id
            this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));

        }
        if (data.attribute_slug) {

            // let decodedString = localStorage.getItem('UDID');
            // var decod=  window.atob(decodedString);
            const UID = get_UDid('UDID');
            const favid = data.id
            this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));

        }
        if (data.Product_Id) {
            // let decodedString = localStorage.getItem('UDID');
            // var decod=  window.atob(decodedString);
            const UID = get_UDid('UDID');
            const favid = data.Id
            this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));
        }
        if (data.parent_attribute) {
            // let decodedString = localStorage.getItem('UDID');
            // var decod=  window.atob(decodedString);
            const UID = get_UDid('UDID');
            const favid = data.id
            this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));
        }

    }
    freezScreen() {
        $('.disabled_popup_close').modal({
            backdrop: 'static',
            keyboard: false
        })
    }
    DisplayPopUp() {
        $('#popup_cash_rounding').modal('show');
        this.freezScreen();

    }
    componentWillReceiveProps(props) {
        if($('#PopupShopStatus'))
        {
        this.checkStatus();
        }
       // console.log("propsa",this.props.ticketfield,"his.state.ticket_Product",this.state.ticket_Product);
       // console.log("ticketfieldListfavourite",localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'',"this.state.ticket_Product_status",this.state.ticket_Product_status);
  
        var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''
        var tick_data=this.state.ticket_Product_status == true? JSON.parse(this.state.ticket_Product[0].TicketInfo):''
        var   form_id=tick_data._owner_form_template
        console.log("call simple1" ,form_id)

    if(localStorage.getItem('ticket_list') && localStorage.getItem('ticket_list') !=='null' && localStorage.getItem('ticket_list') !== '' && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd' || form_id == -1 || form_id == '' && this.state.ticket_Product_status == true && this.state.tick_type == 'simpleadd'   ){  
     //   console.log("call simple")
       this.setState({ticket_Product_status:false})

     //  var  ticket_Data=localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):''
         // console.log("this.state.ticket_Product_status",this.state.ticket_Product_status,ticket_Data)
         var index=null;
         var type=null;
       this.ActiveList(this.state.ticket_Product[0], index=null,type=null,localStorage.getItem('ticket_list')?JSON.parse(localStorage.getItem('ticket_list')):'')
       }
        // const favouritesItemsProduct = favouritesItems && favouritesItems.FavProduct ? getTaxAllProduct(favouritesItems.FavProduct) : null;
        if (props.favourites) {
            const favouritesItemsProduct = props.favourites && props.favourites.FavProduct ? getTaxAllProduct(props.favourites.FavProduct) : null;

            this.state.favouritesItemsProduct = favouritesItemsProduct
            this.setState({ favouritesItemsProduct: favouritesItemsProduct })
        }

        var prdList = [];
        this.state.favouritesItemsProduct && this.state.favouritesItemsProduct.map((item, index) => {
            var getfilterTicketProduct =[] 
                if(this.state.productlist && this.state.productlist.length>0){
                this.state.productlist.filter(prodlist => prodlist.WPID == item.Product_Id);
                }
            if (getfilterTicketProduct) {
                getfilterTicketProduct && getfilterTicketProduct.map((prod, index) => {

                    //console.log("checkfavourite123",prod)

                    var isExpired = false;
                    if (prod.IsTicket && prod.IsTicket == true) {
                        // console.log("checkfavourite ticket is true",prod.IsTicket,prod)
                        var ticketInfo = JSON.parse(prod.TicketInfo);
                        // ticketInfo._ticket_checkin_availability.toLowerCase()=='range' && (ticketInfo._ticket_availability_to_date)
                        if (ticketInfo._ticket_availability.toLowerCase() == 'range' && (ticketInfo._ticket_availability_to_date)) {

                            var dt = new Date(ticketInfo._ticket_availability_to_date);
                            var expirationDate = dt.setDate(dt.getDate() + 1);

                            var currentDate = new Date();// moment.utc(new Date)
                            //   console.log("currentDate",currentDate)
                            //   console.log("expirationDate", new Date(expirationDate))
                            //   console.log("TicketDate",expirationDate,ticketInfo._ticket_availability_to_date,prod)

                            if (currentDate > expirationDate) {
                                // console.log("TicketDate",expirationDate,prod)
                                isExpired = true;
                            }
                        }

                    }
                    if (isExpired == false) {
                       // console.log("isExpired", isExpired, "prod", prod)
                        var data = {
                            Id: item.Id,
                            Image: prod.ProductImage,
                            InStock: prod.InStock,
                            ManagingStock: prod.ManagingStock,
                            Price:prod.Price,
                            Product_Id: prod.WPID,
                            Stock: prod.StockQuantity,
                            Title: prod.Title,
                            Type: prod.Type,
                            old_price:prod.old_price
                        }
                        prdList.push(data)
                    }
                })
               // console.log("getfilterTicketProduct1", prdList)
            }

        })
        if (prdList.length > 0) {
            this.state.favouritesItemsProduct = prdList;
            this.setState({ favouritesItemsProduct: prdList })
        }
       // console.log("getfilterTicketProduct2", this.state.favouritesItemsProduct)

    }
    render() {
         

        const { favourites, favouritesChildCategoryList, favouritesSubAttributeList } = this.props
        const { active, item, num, childCategoryList } = this.state;
        const favouritesItems = favourites;
        const favouritesItemsAttribute = favouritesItems && favouritesItems.FavAttribute;
        const favouritesItemsCategory = favouritesItems && favouritesItems.FavCategory
        //const favouritesItemsProduct = favouritesItems && favouritesItems.FavProduct ? getTaxAllProduct(favouritesItems.FavProduct) : null;
        const favouritesItemsSubCategory = favouritesItems && favouritesItems.FavSubCategory;
        const favouritesItemsSubAttribute = favouritesItems && favouritesItems.FavSubAttribute;

        console.log("this.state.productlist", favouritesItems)
        return (

            <div className="col-lg-8 col-md-7 col-sm-7 plr-8 pl-0">
                {active != true ?
                    <div className="category list-unstyled overflowscroll window-header">


                        <div className="pt-3 clearfix">

                            {
                                // l oop for display favorite attributes
                                favouritesItems && favouritesItemsAttribute.map((item, index) => {
                                    return (
                                        <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-attribute-id={item.attribute_id} data-id={item.id}  >
                                            <div className="category_list">
                                                <a href="javascript:void(0)" onClick={() => this.ActiveList(item, 1, "attribute")}>
                                                    {/* {item.attribute_slug > 13 ? item.attribute_slug.substring(0, 10) + "..." : item.attribute_slug} */}
                                                    {item.attribute_slug}
                                                </a>
                                            </div>
                                            <a className="delete" href="javascript:void(0)">
                                                <span aria-hidden="true" onClick={() => this.RemoveFavProduct(item)}><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                                <span className="sr-only">Close</span>
                                            </a>
                                        </div>
                                    )
                                })

                                // loop for display favorite attributes
                            }


                            {
                                // loop for display favorite sub attributes
                                favouritesItems && favouritesItemsSubAttribute.map((item, index) => {
                                    return (
                                        <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-attribute-id={item.attribute_id} data-id={item.id} data-parent-attribute={item.parent_attribute} >
                                            <div className="category_list" onClick={() => this.ActiveList(item, 3, "sub-attribute")}>
                                                <a href="javascript:void(0)">
                                                    {item.attribute_slug} / {item.parent_attribute.replace("pa_", "")}
                                                </a>
                                            </div>
                                            <a className="delete" href="javascript:void(0)">
                                                <span aria-hidden="true" onClick={() => this.RemoveFavProduct(item)}><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                                <span className="sr-only">Close</span>
                                            </a>
                                        </div>
                                    )
                                })
                                // loop for display favorite sub attributes
                            }

                            {
                                // loop for display favorite category
                                favouritesItems && favouritesItemsCategory.map((item, index) => {
                                    //console.log("FavoruriteItem", item)
                                    return (
                                        <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-category-id={item.category_id} data-id={item.id} data-category-slug={item.category_slug}  >
                                            <div className="category_list" onClick={() => this.ActiveList(item, 2, "category")}>
                                                <a href="javascript:void(0)">
                                                    {item.name}
                                                </a>
                                            </div>
                                            <a className="delete" href="javascript:void(0)">
                                                <span aria-hidden="true" onClick={() => this.RemoveFavProduct(item)}><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                                <span className="sr-only">Close</span>
                                            </a>
                                        </div>
                                    )
                                })
                                // loop for display favorite category
                            }

                            {
                                // loop for display favorite category
                                favouritesItems && favouritesItemsSubCategory.map((item, index) => {

                                    return (
                                        <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-category-id={item.category_id} data-id={item.id} data-category-slug={item.category_slug}  >
                                            <div className="category_list" onClick={() => this.ActiveList(item, 4, "sub-category")}>
                                                <a href="javascript:void(0)">
                                                    {item.name}
                                                </a>
                                            </div>
                                            <a className="delete" href="javascript:void(0)">
                                                <span aria-hidden="true" onClick={() => this.RemoveFavProduct(item)}><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                                <span className="sr-only">Close</span>
                                            </a>
                                        </div>
                                    )
                                })
                                // loop for display favorite category
                            }

                            {
                                // loop for display favorite product
                                this.state.favouritesItemsProduct && this.state.favouritesItemsProduct.map((item, index) => {
                                    var img = item.Image ? item.Image.split('/') : '';
                                   // console.log("rr12",item);
                                    //console.log("rr13",img[8])



                                    // console.log("getfilterTicketProduct",getfilterTicketProduct)       

                                    return (
                                        <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-product-id={item.Product_Id} data-id={item.Id} data-stock={item.stock} data-price={item.Price} >
                                            <div className="category_list labelAdd" onClick={() => this.ActiveList(item, 5, "product")}>
                                                <a href="javascript:void(0)">
                                                    <img src={item.Image ? img[8] == 'placeholder.png' ? '' : item.Image : ''} alt="new" onError={(e) => this.imgError(e.target)} />
                                                </a>
                                                {/* onError={(e)=>{e.target.onerror = null; e.target.src="assets/img/placeholder.png"}} */}
                                                <label className="labelTag"><Markup content={item.Title} /> </label>
                                            </div>
                                            <a className="delete" href="javascript:void(0)">
                                                <span aria-hidden="true" onClick={() => this.RemoveFavProduct(item)}><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                                <span className="sr-only">Close</span>
                                            </a>
                                        </div>
                                    )
                                })
                                // loop for display favorite product
                            }

                            <div className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 pr-0">
                                <div className="category_list labelRemove" onClick={() => this.DisplayPopUp()} data-toggle="modal">
                                    <a>
                                        <img src="assets/img/add_gry.png" />
                                    </a>
                                    <a className="delete" href="#">
                                        <span aria-hidden="true"  ><svg aria-labelledby="svg-inline--fa-title-5qXlqoFlt1IL" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" className="absolute center-a svg-inline--fa fa-times fa-w-11"><title id="svg-inline--fa-title-5qXlqoFlt1IL" className="">Close</title><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" className=""></path></svg></span>
                                        <span className="sr-only">Close</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    :
                    (
                        num == 1 ?
                            < FavouriteItemView
                                SubAttributeList={favouritesSubAttributeList.items ? favouritesSubAttributeList.items : null}
                                tileFilter={this.tileProductListFilter}
                            />
                            : num == 2 ?
                                < FavouriteItemView
                                    childCategory={favouritesChildCategoryList.items ? favouritesChildCategoryList.items : null}
                                    tileFilter={this.tileProductListFilter}
                                /> :
                                num == 3 ?
                                    < FavouriteItemView
                                        childSubAttributeList={this.state.item ? item : null}
                                        tileFilter={this.tileProductListFilter}

                                    /> :
                                    num == 4 ?
                                        < FavouriteItemView
                                            subchildCategory={this.state.item ? item : null}
                                            tileFilter={this.tileProductListFilter}
                                        /> :
                                        null
                    )
                }
            </div>
        )
    }
}


function mapStateToProps(state) {
        
        const { favourites, favouritesChildCategoryList, favouritesSubAttributeList, favProductDelete,ticketfield } = state;
    return {
        favourites: favourites.items,
        favouritesChildCategoryList,
        favouritesSubAttributeList,
        favProductDelete,
        ticketfield
    };
}

const connectedFavouriteList = connect(mapStateToProps)(FavouriteList);
export { connectedFavouriteList as FavouriteList };