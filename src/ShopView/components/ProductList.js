import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
// import { userActions } from '../_actions';


class ProductList extends React.Component {
    render() {
        return (
            <div className="col-lg-4 col-md-5 col-sm-5 p-0 plr-8">
                <div className="items pt-3">
                    <div className="item-heading text-center">Product List</div>
                    <div className="panel panel-default panel-product-list p-0 overflowscroll" id="allProductHeight">
                        <table className="table ShopProductTable">
                            <tbody>
                                {ListItem.data.forms.map((item, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>
                                                {item.name}
                                            </td>
                                            <td className="productPrice">
                                                {item.published}
                                            </td>
                                            <td>
                                                <a data-toggle="modal" href="#addProduct">
                                                    <img className="adpricon" alt="" src="assets/img/add_40.png" />
                                                </a>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}



function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedProductList = connect(mapStateToProps)(ProductList);
export { connectedProductList as ProductList };