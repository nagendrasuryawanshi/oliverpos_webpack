import React from 'react';
import { connect } from 'react-redux';
import { ProductSubAttribute } from './ProductSubAttribute';
import { array } from 'prop-types';

class ProductAttribute extends  React.Component {
          
        constructor(props) { 
            super(props);
          // alert()
            this.state = {
                getAttributes : null
            }    

            this.handleOptionClick = this.handleOptionClick.bind(this);     
            //this.handledoubleClick = this.handledoubleClick.bind(this);     
        }

        handleOptionClick(option, attribute){     
            
            this.props.optionClick(option, attribute);
        }

        // handledoubleClick()
        // {
        //    console.log("DoubleClick")
        // }

      render() {
            const ProductAttribute = this.props;    
            // console.log("ProductAttribute",ProductAttribute.attribute);   
            // console.log("selectedAttribute", this.props.selectedAttribute);   
            
           // console.log("attribute",this.props.attribute);     
            // console.log("attribute",this.props.ProductAttribute)
            // console.log("ProductOptions", this.props.attribute);     
            //this.props.filteredAttribute
             return (
              
                ProductAttribute.attribute && ProductAttribute.attribute.length > 0 ?
                 ( ProductAttribute.attribute.map((attribute, index) => 
                 { //
                 return (
                        <div  className="col-md-12 col-sm-12 text-center" key={index}>
                            <div className="line-2"></div>
                            <label className="color mb-2">{attribute.Name}</label>
                            <div className="row">
                                    {/* for getting attribute options */}
                                    <ProductSubAttribute key={ "ProductSubAttr-"+index} options={attribute.Option} parentAttribute={attribute.Slug} click={this.handleOptionClick}  filteredAttribute={attribute.Slug==this.props.selectedAttribute?[]: this.props.filteredAttribute}  productVariations={this.props.productVariations} />

                                <div className="clearfix"></div>
                            </div>
                        </div>
                 )
                    }
                 
                    )
                 ) : ""
             )
        }
    
    }
    
    
function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedShopView = connect(mapStateToProps)(ProductAttribute);
export { connectedShopView as ProductAttribute };
//export default ProductAttribute;