import React from 'react';
import { connect } from 'react-redux';
import { classicNameResolver } from 'typescript';

class ProductSubAttribute extends React.Component {

    constructor(props) {
        super(props);
    }
    // hendledoubleClick(){
    //     console.log("hendledoubleClick", this.props.filteredAttribute)
    // }
    render() {
        const ProductSubAttribute = this.props;
        console.log("ProductSubAttribute", ProductSubAttribute);
        console.log("parentAttribute",ProductSubAttribute.parentAttribute);
        console.log("this.props.filteredAttribute",this.props.filteredAttribute);
        return (
            ProductSubAttribute.options ? (
                ProductSubAttribute.options.split(',').map((option, index) => {    
                    console.log("OPTION",option);
                    
                    var isExist=  (!this.props.filteredAttribute) || this.props.filteredAttribute.length==0?true:false;

                    var newOption=option;

                    //---------Check Product Vareations Exists----------------------------------------------------------------------------
                    var isVariationExist= false;
                    this.props.productVariations && this.props.productVariations.filter(item=>{                      
                       item.combination.split("~").map(combination=>{                      
                           if(  (combination.replace(/\s/g,'-').replace(/\//g, "-").replace("'","").toLowerCase()).includes(option.replace(/\s/g,'-').replace(/\//g, "-").replace("'","").toLowerCase()))
                           isVariationExist=true;
                       })
                     
                       })  
                       console.log("isVAriationsExists",option,newOption,isVariationExist)
                      
                       //---------------------------------------------------------------------------------------------------

//Find Attribute Code----------------------------------------------
        let attribute_list = localStorage.getItem("attributelist")?  JSON.parse(localStorage.getItem("attributelist")) :null; 
           // console.log("Selected",options ,attribute)
            console.log("attribute_list", attribute_list)
            var sub_attribute;
           //var option;
            var found = attribute_list.find(function (element) {
                return element.Code.toLowerCase()==ProductSubAttribute.parentAttribute.toLowerCase()        

            })
        console.log("SubAttrFound",found  ,"option",option);
            if(found)
            {
                sub_attribute=  found.SubAttributes.find(function (element) {
                    return element.Value.toLowerCase()==option.toLowerCase()         
    
                })
            }

            newOption= sub_attribute ? sub_attribute.Value:newOption;
            console.log("newOption",newOption  ,"option",option);
//------------------------------------------------------------------


                   console.log("CheckFilter",isExist, this.props.filteredAttribute)
                   newOption=newOption.replace(/\s/g,'-').toLowerCase();
                   newOption=newOption.replace(/\//g, "-").toLowerCase();

                  // console.log("isExist",isExist,option);
                    this.props.filteredAttribute && this.props.filteredAttribute.map((item,idx)=>{
                        item.combination.split("~").map(fItem=>{
                            var fItemRes = fItem.replace(/\s/g,'-').toLowerCase();
                            fItemRes = fItemRes.replace(/\//g, "-").toLowerCase();
                                          
                                if( fItemRes===newOption)
                                { 
                                    isExist=true;  
                                  console.log("isExist",option,newOption,fItemRes)
                                }

                        })
  
                    })
                    return (
                        isVariationExist==true && isExist===true? 
                        <div className="col-sm-3" key={"subattr-"+ index}>
                            <div className="button_with_checkbox " >
                                <input type="radio" id={`option${option}`} name={`variation-option-${ProductSubAttribute.parentAttribute}`} value={option} onClick={this.props.click.bind(this, option, ProductSubAttribute.parentAttribute)}    />
                                {/* onDoubleClick={this.props.dbClick.bind(this, option, ProductSubAttribute.parentAttribute)}   */}
                                <label htmlFor={`option${option}`} className="label_select_button" title={option}>{option.length>13?option.substring(0,10)+"..." : option}</label>
                            </div>
                        </div>
                         :<div></div>
                    )
                })
            ) : ""
        )
    }

}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedShopView = connect(mapStateToProps)(ProductSubAttribute);
export { connectedShopView as ProductSubAttribute };
