import { favouriteListConstants } from '../constants/favouriteList.constants';
import { favouriteListService } from '../services/favouriteList.service';
import { alertActions } from '../../_actions/alert.actions';
import { history } from '../../_helpers/history';

export const favouriteListActions = {
    getAll,
    getChildCategories,
    getSubAttributes,
    variationProdList,
    addToFavourite,
    favProductRemove,
    getTest
    
};
function getTest(UDID, Register_id) {    
    return dispatch => {        
        dispatch(request({ UDID, Register_id })); 

        favouriteListService.getTest( )
            .then(
                favouriteList => { 
                   console.log("getTestResponse",favouriteList);                    
                    //dispatch(success(favouriteList));                  
                   // history.push('/site_link');
                },
                error => {
                    console.log("getTestResponseError",error);            
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(UDID) { 
        return {
             type: favouriteListConstants.FAVOURITE_LIST_REQUEST, UDID 
        } 
    }
    function success(favouriteList) { 
        return { 
            type: favouriteListConstants.FAVOURITE_LIST_SUCCESS, favouriteList 
        } 
    }
    function failure(error) { 
        return { 
            type: favouriteListConstants.FAVOURITE_LIST_FAILURE, error 
        } 
    }
}

function getAll(UDID, Register_id) {    
    return dispatch => {        
        dispatch(request({ UDID, Register_id })); 

        favouriteListService.getAll( UDID, Register_id )
            .then(
                favouriteList => { 
                    
                    dispatch(success(favouriteList));                  
                   // history.push('/site_link');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(UDID) { 
        return {
             type: favouriteListConstants.FAVOURITE_LIST_REQUEST, UDID 
        } 
    }
    function success(favouriteList) { 
        return { 
            type: favouriteListConstants.FAVOURITE_LIST_SUCCESS, favouriteList 
        } 
    }
    function failure(error) { 
        return { 
            type: favouriteListConstants.FAVOURITE_LIST_FAILURE, error 
        } 
    }
}

  

    function getChildCategories(UDID,CatId) {
        console.log("getChildCategories",UDID,CatId);

        return dispatch => {
            dispatch(request());    
            favouriteListService.getChildCategory( UDID, CatId )
              .then( ChildCategoryList =>{
                console.log("ChildCategoryList",ChildCategoryList) 

                   dispatch(success(ChildCategoryList))},
                    error => dispatch(failure(error.toString()))
                );
        };
    
        function request(UDID) { 
            return {
                 type: favouriteListConstants.FAVOURITE_CHILD_CATEGORY_LIST_REQUEST, UDID 
            } 
        }
        function success(ChildCategoryList ) { 
            return { 
                type: favouriteListConstants.FAVOURITE_CHILD_CATEGORY_LIST_SUCCESS, ChildCategoryList 
            } 
        }
        function failure(error) { 
            return { 
                type: favouriteListConstants.FAVOURITE_CHILD_CATEGORY_LIST_FAILURE, error 
            } 
        }
    
    }
    function getSubAttributes(UDID,Code){
        console.log("getSubAttributes",UDID,Code);
        return dispatch => {
            dispatch(request());    
          
            let attribute_list = localStorage.getItem("attributelist")?  JSON.parse(localStorage.getItem("attributelist")) :null;          
            var sub_attribute;
         
            var found = attribute_list.find(function (element) {
                return element.Code.toLowerCase()==Code.toLowerCase()  
            })       
            if(found)
            {
               sub_attribute=found.SubAttributes;
               //  sub_attribute=  found.SubAttributes.find(function (element) {
               //      return element.Value.toLowerCase()==option.toLowerCase()         
    
               //  })
            }
         
            dispatch(success(sub_attribute));
          
            // favouriteListService.getSubAttributesList( UDID,Slug )
            //   .then( SubAttributesList => {
            //       console.log("SubAttributesList",SubAttributesList) 
            //       dispatch(success(SubAttributesList))},
            //         error => dispatch(failure(error.toString()))
            //     );
        };
    
        function request(UDID) { 
            return {
                 type: favouriteListConstants.FAVOURITE_GET_SUBATTRIBUTES_LIST_REQUEST, UDID 
            } 
        }
        function success(SubAttributesList ) { 
            return { 
                type: favouriteListConstants.FAVOURITE_GET_SUBATTRIBUTES_LIST_SUCCESS, SubAttributesList 
            } 
        }
        function failure(error) { 
            return { 
                type: favouriteListConstants.FAVOURITE_GET_SUBATTRIBUTES_LIST_FAILURE, error 
            } 
        }



    }
    function addToFavourite(type,id,slug){
        console.log("actionype",type,id,slug);
        return dispatch => {
            dispatch(request());    
            favouriteListService.addToFavourite( type,id,slug )
              .then( response =>{
                 console.log("actionaddToFavourite",response);
              if(response.Message=="Success")
              {
               // dispatch(success(response))
               window.location = '/shopview'
               //history.push('/shopview');
              }
              }
              //,history.push("/shopview")
            //   ,
            //         error => dispatch(failure(error.toString()))
                );
        };
    
        function request() { 
            return {
                 type: favouriteListConstants.SAVE_LIST_REQUEST 
            } 
        }
        function success(response ) { 
            return { 
                type: favouriteListConstants.SAVE_LIST_SUCCESS, response 
            } 
        }
        function failure(error) { 
            return { 
                type: favouriteListConstants.SAVE_LIST_FAILURE, error 
            } 
        }



    }

    function variationProdList(item){
        return dispatch => {
             dispatch(success(item))
             };
    
           function success(variationProdList) { return { type: favouriteListConstants.GET_VARIATION_PRODUCT_LIST_SUCCESS, variationProdList } }
            
    }
    
    function favProductRemove(udid,favid){
        return dispatch => {
            dispatch(request());    
            favouriteListService.favProductRemove( udid,favid )
              .then(
                  favProduct => { 
                      dispatch(success(favProduct)),                 
                      window.location = '/shopview';
                  }, 
                    error => dispatch(failure(error.toString()))
                );
        };
        function request(UDID) { 
            return {
                 type: favouriteListConstants.FAVOURITE_PRODUCT_DELETE_REQUEST, UDID 
            } 
        }
        function success(favProductDelete ) { 
            return { 
                type: favouriteListConstants.FAVOURITE_PRODUCT_DELETE_SUCCESS, favProductDelete 
            } 
        }
        function failure(error) { 
            return { 
                type: favouriteListConstants.FAVOURITE_PRODUCT_DELETE_FAILURE, error 
            } 
        }
   
    }