import Config from '../../Config'
import {encode_UDid,get_UDid} from '../../ALL_localstorage'


export const favouriteListService = {
    getAll,
    getChildCategory,
    getSubAttributesList,
    addToFavourite,
    favProductRemove,
    getTest
    
};

const API_URL = Config.key.OP_API_URL

function getTest() {
   
    // var name = "posk_3dd80552ca15f4fe59bd42875d618e3ccf560bd6";
    // var password = "poss_5a3f7d8d34dc3605ea5796eda8d6dfcf6cf82254!";
    // var authString = name + ":" + password;

    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };

    // jQuery.fetch({
    // url:`${API_URL}/testauth/value`,
    // type:'GET',
    // contentType:'application/json',
    // dataType:"json",
    // headers:{"Authorization": "Basic " + btoa(authString)  },
    // success:function(resul){
    //     console.log("Result",result);
    //     return result;
    //         },
    // error:function(err){
    //     console.log("Error",err);
    // }

    // })

    // let headers = new Headers();

    // headers.append('Content-Type', 'application/json');
    // headers.append('Accept', 'application/json');
    // headers.append('Authorization', 'Basic ' + base64.encode(name + ":" +  password));
    // // headers.append('Access-Control-Allow-Origin','*');
    // // headers.append('Origin','http://localhost:3000');

    // headers.append('Access-Control-Allow-Origin'  , '*')
    // headers.append('Access-Control-Allow-Methods'  , 'POST, GET, OPTIONS, PUT, DELETE')
    // headers.append('Access-Control-Allow-Credentials', 'true')
    // headers.append('Access-Control-Max-Age'         , '86400')
    // headers.append('Access-Control-Allow-Headers'   , 'Content-Type, Authorization, X-Requested-With')

//   return  fetch(`${API_URL}/testauth/value`, {
//         mode: 'no-cors',
//         credentials: 'include',
//         method: 'GET',
//         headers: headers
//     })
//     .then(response => response.json())
//     .then(json => {console.log("APIResponse",json)
//     return json;
//         } )
//     .catch(error => {console.log('Authorization failed : ' + error.message)
//     return error;
// });


    return fetch(`${API_URL}/testauth/value`, requestOptions)
        .then(handleResponse)
        .then(response => {        
            console.log("APIGETTEST",response)   
            return response;
        });

}


function getAll(UDID, Register_id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };

    return fetch(`${API_URL}/ShopData/GetDisplayFavList?udid=` + UDID + '&Registerid=' + Register_id, requestOptions)
        .then(handleResponse)
        .then(favouriteListRes => {
            var favouriteList = favouriteListRes.Content;
            return favouriteList;
        });
}
function getChildCategory(UDID, catId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };

    return fetch(`${API_URL}/ShopSetting/GetChildCategories?udid=${UDID}&CateId=${catId}`, requestOptions)
        .then(handleResponse)
        .then(favouriteChildCatlist => {
            var favouriteChildCatlist = favouriteChildCatlist.Content;
            return favouriteChildCatlist;
        });
}

function getSubAttributesList(UDID, slug) {

    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };

    return fetch(`${API_URL}/ShopSetting/GetSubAttributes?udid=${UDID}&slug=${slug}`, requestOptions)
        .then(handleResponse)
        .then(subAttributelist => {
            var subAttributelist = subAttributelist.Content;
            return subAttributelist;
        });


}

function addToFavourite(ItemType, ItemId, ItemSlug) {
    //UserID,RegisterId,udid,

    var udid = get_UDid("UDID");
    var User = JSON.parse(localStorage.getItem('user'));
    var UserID = User.user_id;
    var RegisterId = localStorage.getItem('register');
    const requestOptions = {
        method: 'POST',
        //  mode: 'no-cors',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors',
        //{"UserID":"8041927a-aa58-4e19-b833-29c28a01531b","RegisterId":1,"udid":"4040246161","ItemId":2,"ItemType":"SubAttribute","ItemSlug":"2"}
        body: JSON.stringify({ UserID, RegisterId, udid, ItemId, ItemType, ItemSlug })
    };

    return fetch(`${API_URL}/ShopData/SaveFavorite`, requestOptions)
        .then(handleResponse)
        .then(response => {
            // addtofavourite successful if there's a jwt token in the response
            // if (user.Content[0].UDID) {
            //     // store user details and jwt token in local storage to keep user logged in between page refreshes
            //     //localStorage.setItem('user', JSON.stringify(user));
            //     const requestOptions1 = {
            //         method: 'GET',    
            //       headers: {
            //         'Accept': 'application/json',
            //         'Content-Type': 'application/json',
            //       } };
            //     return fetch(`http://app.oliverpos.com/api/ShopAccess/GetLocations?udid=`+user.Content[0].UDID,requestOptions1)
            //     .then(handleResponse)
            //     .then(location => {
            //         localStorage.setItem('user', JSON.stringify(user));
            //         localStorage.setItem('UserLocations', JSON.stringify(location.Content));

            return response;

            //     }).catch(error=>{
            //           Message:"Error- No location found"  
            //     })

            // }

        });
}

function favProductRemove(UDID, favid) {
    const requestOptions = {
        method: 'GET',
        headers: {
            "access-control-allow-origin": "*",
            "access-control-allow-credentials": "true", 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(Config.key.AUTH_KEY),
        } 
        ,mode: 'cors'
    };


    return fetch(`${API_URL}/ShopData/DeleteFavorite?Udid=${UDID}&favId=${favid}`, requestOptions)
        .then(handleResponse)
        .then(favProductdelete => {
            var favProductdelete = favProductdelete;
            return favProductdelete;
        });



}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}


