import React from 'react';
import { connect } from 'react-redux';
import { NavbarPage, CommonHeaderTwo, CustomerAddFee, CustomerNote, CommonProductPopupModal, getTaxAllProduct , SingleProductDiscountPopup , InventoryPopup, UpdateProductInventoryModal} from '../_components';
import { FavouriteList } from './components/FavouriteList';
import { CartListView } from '../_components/CartListView'
import { AllProduct } from '../_components/AllProduct';
import { history } from '../_helpers';
import { taxRateAction } from '../_actions/taxRate.action'
import { DiscountPopup } from '../_components/DiscountPopup'
import { TileModel } from '../_components/TileModel'
import { ProductOutOfStockPopupModel } from '../_components/ProductOutOfStockPopupModel';
import { ProductNotFound } from '../_components/ProductNotFound';
import { LoadingModal } from '../_components/LoadingModal';
import { Alreadyexist } from '../_components/Alreadyexist'
import { openDb } from 'idb';
import { checkoutActions } from '../CheckoutPage/actions/checkout.action'
import { get_UDid } from '../ALL_localstorage'
import { TickitDetailsPopupModal } from '../_components/TickitDetailsPopupModal/TickitDetailsPopupModal'
import { idbProductActions } from '../_actions/idbProduct.action';
import { PopupShopStatus} from '../_components/PopupShopStatus'
import { customerActions } from '../CustomerPage/actions/customer.action';


class ShopView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            getVariationProductData: null,
            hasVariationProductData: false,
            variationoptionArray: {},
            discountAmount: 0,
            discountType: "",
            variationCombination: [],
            tileFilterProductData: null,
            tileFilterProducttype: null,
            getSimpleProductData: null,
            hasSimpleProductData: false,
            loading: false,
            AllProductList: [],
            inventoryCheck:null,
            isInventoryUpdate:false,
           // updateProductIs:false
            // variationProductList:localStorage.getItem('Favproduct')
        }
        this.handleSimpleProduct = this.handleSimpleProduct.bind(this);
        this.handleProductData = this.handleProductData.bind(this);
        this.handleDiscount = this.handleDiscount.bind(this);
        this.handleTicketDetail = this.handleTicketDetail.bind(this);
        this.checkInventoryData = this.checkInventoryData.bind(this);
        this.showPopuponcartlistView = this.showPopuponcartlistView.bind(this)
        this.invetoryUpdate = this.invetoryUpdate.bind(this)


        if (!localStorage.getItem('UDID')) {
            history.push('/login');
        }

        if (!localStorage.getItem('user') ||   ! sessionStorage.getItem("issuccess") ) {
            history.push('/loginpin');
        }
        //this.addVariationProductToCart = this.addVariationProductToCart.bind(this);
        this.handletileFilterData = this.handletileFilterData.bind(this);

        // let decodedString = localStorage.getItem('UDID');
        // var decod=  window.atob(decodedString);
        var udid = get_UDid('UDID');
        // const { dispatch } = this.props;
        // dispatch(discountActions.getAll());        

        const { dispatch } = this.props;
        //----- update product qty-------------------------------------------------
        dispatch(idbProductActions.updateProductDB());
        //-------------------------------  

        dispatch(taxRateAction.getAll());
        dispatch(checkoutActions.getPaymentTypeName(udid, localStorage.getItem('register')))

        //----------Fetch All product from indexDB--------------


        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },
        };
        idbKeyval.get('ProductList').then(val => {

          //  console.log("Val",val.length)
            if( !val || val.length==0 || val==null || val==""){
                console.error('Console.save: No data')
             this.setState({ AllProductList:[] });                
            }else{
               let _productwithTax =  getTaxAllProduct(val)
               this.setState({ AllProductList: _productwithTax });                
            }
         
        }
        );

        this.props.dispatch(customerActions.getCountry())
        this.props.dispatch(customerActions.getState())
    }

    componentWillReceiveProps(recieveProps) {
     //console.log("recieveProps",recieveProps, localStorage.getItem('UPDATE_PRODUCT_LIST'))
        const { taxratelist } = recieveProps;
        if ((typeof taxratelist !== 'undefined') && taxratelist !== null) {
            localStorage.setItem('TAXT_RATE_LIST', JSON.stringify(taxratelist))
        }

        if(recieveProps.get_single_inventory_quantity){
            this.invetoryUpdate(false)
        }
        
    }

    componentWillMount() {
        localStorage.removeItem("VOID_SALE")
        if (!localStorage.getItem('user') ||   ! sessionStorage.getItem("issuccess")) {
            history.push('/loginpin');
        }
        var udid = get_UDid('UDID')
        setTimeout(() => {
            this.setState({
                loading: true
            })

        }, 500);

        setTimeout(function () {
            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 500);
       
    }

    GetSortOrder(prop) {
        return function (a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    }

    handleProductData(productData) {
       console.log("productData in shopview",productData);
        var filterdata = [];
        // var allProductList = [];
        // allProductList.push(this.state.AllProductList)
        if (productData.item) {

            //------------------
            var variationProdect = this.state.AllProductList.filter(vitem => {
                return (vitem.ParentId === productData.item.Product_Id && (vitem.ManagingStock == false || (vitem.ManagingStock == true && vitem.StockQuantity > -1)))
            })
            productData.item['Variations'] = variationProdect;
            //-------------------------------

            let filtered_data = this.state.AllProductList.sort(this.GetSortOrder("Title"));
            this.state.AllProductList && this.state.AllProductList.map(item => {
                if (item.ParentId == 0) {

                    if (productData.item.Product_Id == item.WPID) {
                        item['Variations'] = variationProdect;
                        filterdata.push(item)
                    }
                }
            })

        }
        var product = ''

        if (!productData.item) {
            product = productData;
        }

        this.setState({
            getVariationProductData: product ? product : filterdata[0],
            hasVariationProductData: true,
            loadProductAttributeComponent: true,
            variationOptionclick: 0,
            variationTitle: productData.Title ? productData.Title : productData.item ? productData.item.Title : '',
            variationId: 0,
            variationPrice: productData.Price ? productData.Price : productData.item ? productData.item.Price : '',
            variationStockQunatity: productData.StockQuantity ? productData.StockQuantity : productData.item ? productData.item.Stock : '',
            variationImage: productData.ProductImage ? productData.ProductImage : productData.item ? productData.item.Image : '',
            variationDefaultQunatity: 1 ? 1 : productData ? productData.DefaultQunatity : '',
            getSimpleProductData: null,
            hasSimpleProductData: false,

        });

    }

    handletileFilterData(data, type, parent) {

        //console.log("shopviewtilefilter",data,type,parent);
        this.setState({
            tileFilterProductData: data,
            tileFilterProducttype: type,

        });

        this.tileProductFilter.filterProductByTile(type, data, parent);
    }

    handleDiscount(amt, type) {
        if (amt) {
            this.setState({ discountAmount: parseFloat(amt), discountType: type });
        }

    }

    perCur(curEle, displayId, currency_sign) {
        if ($('#' + displayId).attr('currency') == 'num') {
            $('#' + displayId).attr('currency', 'per');
            $('#' + displayId).text('%');
            $(curEle).html(currency_sign);
        } else {
            $('#' + displayId).attr('currency', 'num');
            $('#' + displayId).html(currency_sign);
            $(curEle).text('%');
        }
    }

    handleChange(e) {

    }

    minplus() {

        if (jQuery('#spnCalcType').text() == "%")
            jQuery('#spnCalcType').text("$");
        else
            jQuery('#spnCalcType').text("%");

    }

    checkout() {
        window.location.href = 'checkout'
    }

    handleSimpleProduct(simpleProductData) {
       // console.log("simpleProductData",simpleProductData);
        this.setState({
            getSimpleProductData: simpleProductData,
            hasSimpleProductData: true,
            getVariationProductData: null,
            hasVariationProductData: false
        })


    }
    
    handleTicketDetail(status, item) {

        // console.log("handleTicketDetail",item)
        if (status == 'create') {
            this.setState({ Ticket_Detail: item })
        } else if (status == 'edit') {
            //console.log("handleTicketDetailedit",item)

            this.setState({ Ticket_Detail: item })

        }


    }

    componentDidMount() {
        setTimeout(function () {
            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 1000);   
   
    }

    checkInventoryData(productData){
     console.log("checkInventoryData", productData)
     this.setState({inventoryCheck : productData})
     this.state.inventoryCheck = productData;
    
    }

    invetoryUpdate(st){
        this.setState({isInventoryUpdate:st})
    }

    showPopuponcartlistView(product, item){
      console.log("showPopuponcartlistView", product, item)
      $('#VariationPopUp').modal('show');
      this.handleSimpleProduct(product);
    }

    closePopup(){
        this.props.dispatch(checkoutActions.checkItemList())
    }

    render() {
        const { getVariationProductData, getSimpleProductData } = this.state;
        const { hasVariationProductData, hasSimpleProductData } = this.state;
        const { checkout_list } = this.props;
        let cartProductList = localStorage.getItem("CARD_PRODUCT_LIST")? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")):null;
        let blank_quntity = []
        var new_data ;
        //console.log("checkoutList", cartProductList, checkout_list);
        if(cartProductList && checkout_list){
            checkout_list.map(itemC=>{
                   
                new_data = cartProductList.find(function (element){
                    return (element.variation_id == 0 ? element.product_id == itemC.ProductId : element.variation_id == itemC.ProductId && itemC.success == false)
                })
                //new_data = cartProductList.map(prdId=> prdId.variation_id == 0 ? prdId.product_id : prdId.variation_id == itemC.ProductId && itemC.success == true)
                //console.log("new_data", new_data) 
                if(new_data){
                 blank_quntity.push(new_data.Title)
                }
              // console.log("blank_quntity", blank_quntity)
            })
            //console.log("new_data", new_data) 
            
        }
       
        // console.log("getVariationProductData:",getVariationProductData)
        // console.log("getSimpleProductData:",getSimpleProductData)
        // console.log("hasVariationProductData:",hasVariationProductData)
        // console.log("hasSimpleProductData:",hasSimpleProductData)

        return (
            <div className="hide-overflow">
                {this.state.loading == false ? !this.props.authentication && !this.props.authentication.user && this.props.authentication.loggedIn !== true
                    && !this.state.AllProductList || !this.props.favourites.items ? <LoadingModal /> : '' : ''}
                <div className="wrapper">
                    <div className="overlay"></div>
                    <NavbarPage {...this.props} />
                    <div id="content">
                        <CommonHeaderTwo {...this.props} searchProductFilter={this.handletileFilterData} productData={this.handleProductData} />

                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">
                                    <FavouriteList productData={this.handleProductData} tileFilterData={this.handletileFilterData} />
                                    {/* <div className="col-lg-4 col-md-5 col-sm-5 p-0 plr-8">  */}
                                    {/* <ProductList/> */}
                                    {/* </div> */}
                                    <div className="col-lg-4 col-md-5 col-sm-5 p-0 plr-8">
                                        <AllProduct productData={this.handleProductData} onRef={ref => (this.tileProductFilter = ref)} simpleProductData={this.handleSimpleProduct} />
                                    </div>
                                </div>
                                <CartListView showPopuponcartlistView={this.showPopuponcartlistView} onDiscountAmountChange={this.handleDiscount} onChange={this.addFee} discountAmount={this.state.discountAmount} discountType={this.state.discountType} ticketDetail={this.handleTicketDetail} />

                            </div>
                        </div>
                    </div>
                </div>


                {/* <!-------product list popup------------> */}
                < TileModel />
                <TickitDetailsPopupModal Ticket_Detail={this.state.Ticket_Detail} />
                <ProductOutOfStockPopupModel />
                 {/* modal for simple and variation popup*/}
                <CommonProductPopupModal getQuantity={localStorage.getItem("CART_QTY")} isInventoryUpdate={this.state.isInventoryUpdate} inventoryData={this.checkInventoryData} getVariationProductData={getVariationProductData ? getVariationProductData : getSimpleProductData} hasVariationProductData={hasVariationProductData ? hasVariationProductData : hasSimpleProductData} />
                <Alreadyexist />
                <ProductNotFound />
                {/* for update invetory qunatity */}
                <InventoryPopup inventoryCheck={this.state.inventoryCheck} isInventoryUpdate={this.invetoryUpdate} />
                
                {/* modal close */}
                <DiscountPopup onDiscountAmountChange={this.handleDiscount} taxratelist={this.props.taxratelist} />
                <PopupShopStatus />
                {/* apply discount through CommonProductPopupModal */}
                <SingleProductDiscountPopup  onDiscountAmountChange={this.handleDiscount} taxratelist={this.props.taxratelist} />
                <div id="popup_CustomFee" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Add Discount (Mittens)</h4>
                            </div>
                            <div className="modal-body p-0">
                                <form className="clearfix">

                                    <div className="col-sm-7 p-0">
                                        <div className="panel-product-list" id="panelCalculatorpopUp">
                                            <div className="panel panelCalculator">
                                                <div className="panel-body p-0">
                                                    <table className="table table-bordered shopViewPopUpCalculator">
                                                        <tbody>
                                                            <tr>
                                                                <td colSpan="2" className="text-right br-1 bl-1 bt-0">
                                                                    <div className="input-group discount-input-group">
                                                                        <input type="text" id="txtdisAmount" className="form-control text-right" placeholder="1234" onChange={this.handleChange.bind(this)} value={this.state.discountAmount} text={this.state.discountAmount} aria-describedby="basic-addon1" />
                                                                        <span className="input-group-addon" id="spnCalcType" name="spnCalcType">$</span>
                                                                    </div>
                                                                </td>
                                                                <td className="text-center pointer bt-0" onClick={() => this.calcInp('c')}>

                                                                    <button type="button" className="btn btn-default calculate">
                                                                        <img width="36" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zMC4xNzEsMTYuNDE2SDAuODg4QzAuMzk4LDE2LjQxNiwwLDE2LjAyLDAsMTUuNTI5YzAtMC40OSwwLjM5OC0wLjg4OCwwLjg4OC0wLjg4OGgyOS4yODMgICAgYzAuNDksMCwwLjg4OCwwLjM5OCwwLjg4OCwwLjg4OEMzMS4wNTksMTYuMDIsMzAuNjYxLDE2LjQxNiwzMC4xNzEsMTYuNDE2eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTE2LjAxNywzMS4wNTljLTAuMjIyLDAtMC40NDUtMC4wODMtMC42MTctMC4yNUwwLjI3MSwxNi4xNjZDMC4wOTgsMTUuOTk5LDAsMTUuNzcsMCwxNS41MjkgICAgYzAtMC4yNCwwLjA5OC0wLjQ3MSwwLjI3MS0wLjYzOEwxNS40LDAuMjVjMC4zNTItMC4zNDEsMC45MTQtMC4zMzIsMS4yNTUsMC4wMmMwLjM0LDAuMzUzLDAuMzMxLDAuOTE1LTAuMDIxLDEuMjU1TDIuMTYzLDE1LjUyOSAgICBsMTQuNDcxLDE0LjAwNGMwLjM1MiwwLjM0MSwwLjM2MSwwLjkwMiwwLjAyMSwxLjI1NUMxNi40OCwzMC45NjgsMTYuMjQ5LDMxLjA1OSwxNi4wMTcsMzEuMDU5eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="></img>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1">
                                                                    <button type="button" onClick={() => this.calcInp(1)} className="btn btn-default calculate">1</button>
                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp(2)} className="btn btn-default calculate">2</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(3)} className="btn btn-default calculate">3</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1">
                                                                    <button type="button" onClick={() => this.calcInp(4)} className="btn btn-default calculate">4</button>
                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp(5)} className="btn btn-default calculate">5</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(6)} className="btn btn-default calculate">6</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1">
                                                                    <button type="button" onClick={() => this.calcInp(7)} className="btn btn-default calculate">7</button>
                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp(8)} className="btn btn-default calculate">8</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(9)} className="btn btn-default calculate">9</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1" >
                                                                    <button type="button" onClick={() => this.minplus()} className="btn btn-default calculate"> %</button>

                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp('.')} className="btn btn-default calculate">,</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(0)} className="btn btn-default calculate">0</button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">ADD DISCOUNT</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* modal close */}
                <div tabIndex="-1" className="modal fade in mt-5 modal-wide" id="addnotehere" role="dialog">
                    <CustomerNote />
                </div>
                {/* modal close */}
                <div tabIndex="-1" className="modal fade modal-wide in sidebar-minus window-height" id="registeropenclose1" role="dialog">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header" >
                                <button type="button" className="close" data-dismiss="modal">×</button>
                                <h4 className="modal-title">Modal Header</h4>
                            </div>
                            <div className="modal-body p-0">
                                <div className="panel panel-custmer b-0">
                                    <div className="panel-default">
                                        <div className="panel-heading text-center customer_name font18 visible2">
                                            Close Register
                            </div>
                                        <div className="panel-body customer_history p-0">
                                            <div className="col-sm-12">
                                                <div className="row">
                                                    <div className="col-lg-9 col-sm-8 plr-8">
                                                        <form className="customer-detail">
                                                            <div className="form-group">
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Register:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Register 1</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Date &amp; Time:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p className="">15. January 12:57PM</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">User:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Mike Kellerman</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-4 bl-1 plr-8">
                                                        <form action="#" className="w-100 pl-3">
                                                            <div className="w-100-block button_with_checkbox">
                                                                <input type="radio" id="p-report" name="radio-group" onChange={this.handleChange.bind(this)} />
                                                                <label htmlFor="p-report" className="label_select_button">Print Report</label>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="panel-footer bt-0 p-0">
                                            <div className="table_head">
                                                <table className="table table-border table-hover borderless c_change mb-0 tbl-bb-1 tbl-layout-fixe">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                    </colgroup>
                                                    <tbody>
                                                        <tr>
                                                            <th className="bt-0 pb-0 bb-1">Item</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Expected</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Actual</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Difference</th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="">
                                                <table className="table table-borderless tbl-bb-1 font tbl-layout-fixe pt-3 pb-3">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                    </colgroup>
                                                    <tbody>
                                                        <tr>
                                                            <td>Cash In Tiller</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Debit/Credit Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gift Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Others</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div className="col-lg-offset-7 col-lg-5 col-sm-offset-6 col-sm-6 col-xs-offset-4 col-xs-8 p-0">
                                                    <table className="table table-borderless tbl-bb-1 mb-0 font pt-1 pb-1">
                                                        <tbody>
                                                            <tr>
                                                                <td className="pl-0">Total Difference</td>
                                                                <td className="text-right">10.00</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div className="col-sm-12">
                                                    <div className="register-closenote">
                                                        <textarea cols="12" rows="4" className="form-control" placeholder="Enter notes here"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">SAVE &amp; UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>
                <UpdateProductInventoryModal />
                <div id="no_discount" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="../assets/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">Discount can not be more than 100% of cart amount! use 100% discount !</h3>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div tabIndex="-1" id="popup_oliver_add_fee" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                    <CustomerAddFee />
                </div>
            </div>

        );
    }
}

function mapStateToProps(state) {
    const { discountlist, productlist, taxratelist, alert, authentication, favourites, get_single_inventory_quantity, checkout_list } = state;
    return {
        discountlist,
        productlist,
        taxratelist: taxratelist.taxratelist,
        alert,
        authentication,
        favourites,
        get_single_inventory_quantity: get_single_inventory_quantity.items,
        checkout_list: checkout_list.items,
    };
}

const connectedShopView = connect(mapStateToProps)(ShopView);
export { connectedShopView as ShopView };

