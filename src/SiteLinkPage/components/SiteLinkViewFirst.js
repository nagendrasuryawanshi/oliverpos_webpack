import React from 'react';
import { connect } from 'react-redux';
import { siteActions } from '../actions/site_link.action';
import { history } from '../../_helpers/history';
import { pinLoginActions } from '../../LoginPin/action/PinLogin.action';
import { LoadingModal } from '../../_components/LoadingModal';
import { encode_UDid, get_UDid } from '../../ALL_localstorage'


class SiteLinkViewFirst extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }

    }

    componentWillMount() {

        if (localStorage.getItem('sitelist') == null || localStorage.getItem('sitelist') == 'undefined') {
            history.push('/login');
        }
    }

    componentDidMount() {

    }

    componentWillReceiveProps() {

        var UserLocations = this.props.locations ? this.props.locations : localStorage.getItem('UserLocations');
        let Sitelist = localStorage.getItem('sitelist') ? (localStorage.getItem('sitelist')) : this.props.sitelist;
        if (Sitelist && Sitelist != null) {
            setTimeout(function(){
            $(".chooseregisterLinks").niceScroll({ styler: "fb", cursorcolor: "#2BB6E2", cursorwidth: '3', cursorborderradius: '10px', background: '#d5d5d5', spacebarenabled: false, cursorborder: '', scrollspeed: 60, cursorfixedheight: 70 });
            $(".chooseregisterLinks").getNiceScroll().resize();
         },500)
        }
        // this.checkShopStatus(UDID);
    }


    checkShopStatus(UDID) {
        //-------------------------------------------
        if (localStorage.getItem("shopstatus") && localStorage.getItem("shopstatus") !== null) {
            var sopstatus = JSON.parse(localStorage.getItem("shopstatus") ? localStorage.getItem("shopstatus") : "");
            if (sopstatus !== "") //&& sopstatus.IsSuccess==false 
            {
                $('#PopupShopStatus').modal('show');
            }
            //---------------------------------------------
        }
    }


    handleSubmit(UDID) {
        //event.preventDefault()
        encode_UDid(UDID) //var UD_ID=btoa(UDID) // localStorage.setItem('UDID', UD_ID) //call get All Product for IndexDB  //this.props.dispatch(idbProductActions.createProductDB());
        //------------------------------
        var getudid = get_UDid('UDID'); // this.checkShopStatus(getudid);
        this.setState({
            loading: true
        });
        this.props.dispatch(pinLoginActions.checkSubcription());
        var UserLocations = this.props.locations ? this.props.locations : localStorage.getItem('UserLocations');
        if (UserLocations != null)
            if (localStorage.getItem(`last_login_location_name_${getudid}`) && localStorage.getItem(`last_login_location_id_${getudid}`)
                && localStorage.getItem(`last_login_register_id_${getudid}`) && localStorage.getItem(`last_login_register_name_${getudid}`)) {
                history.push('/loginpin')
            } else {
                {
                    this.setState({
                        loading: false
                    });
                    history.push('/login_location');
                }
            }
    }

    render() {

        var decodedString = localStorage.getItem('sitelist') ? localStorage.getItem('sitelist') : this.props.sitelist
        var decod = window.atob(decodedString);
        var Sitelist = JSON.parse(decod);
        if (!(Array.isArray(Sitelist))) { Sitelist = null; }
        var IsActiveExist = false;
        Sitelist && Sitelist.map((link, index) => {
            if (link.Activated == "True") {
                IsActiveExist = true;
            }
        });
        return (
            <form className="onboarding-login-field" action="index.html" >
                {!Sitelist ? <LoadingModal /> : ''}

                <ul className="list-group chooseregisterLinks" style={{ height: 230 }}>

                    {

                        Sitelist && Sitelist.length > 0 ?
                            Sitelist.map((link, index) => {

                                return (
                                    link.Activated == "True" ?
                                        <li key={index} className="list-group-item" onClick={() => this.handleSubmit(link.UDID)}>
                                            <a href="javascript:void(0)"><span className="wordWarp">{link.Host}</span>
                                                <img src="assets/img/green-arrow.png" /></a>
                                        </li>
                                        :
                                        <li key={index} className="list-group-item" title="Subscription Inactivated">
                                            <a ><span className="wordWarp" >{link.Host}</span>
                                                <img src="assets/img/lock.png" /></a>
                                        </li>
                                )
                            })
                            : <li key="1" className="list-group-item" >
                                <a href="javascript:void(0)">No Site Found
               </a>
                            </li>
                    }

                    <li className="list-group-item"></li>
                </ul>
            </form>
        )
    }

}

function mapStateToProps(state) {
    const { authentication, locations } = state;
    return {
        sitelist: authentication.user,
        locations: locations
    };

}

const connectedSiteLinkViewFirst = connect(mapStateToProps, siteActions)(SiteLinkViewFirst);
export { connectedSiteLinkViewFirst as SiteLinkViewFirst };