import React from 'react';
import { connect } from 'react-redux';
import { siteActions } from '../actions/site_link.action';
import { SiteLinkViewFirst } from './SiteLinkViewFirst';
import { history } from '../../_helpers';
import { LoadingModal } from '../../_components/LoadingModal';
import {encode_UDid,get_UDid} from '../../ALL_localstorage'
import { PopupShopStatus} from '../../_components/PopupShopStatus'
import {checkShopSTatusAction } from '../../_actions';

class SiteLinkView extends React.Component {
    constructor(props) {
        super(props);

        if(!localStorage.getItem("shopstatus"))
        {
             this.props.dispatch(checkShopSTatusAction.getStatus());   
        }   
      
    }
    componentWillMount() {
       
        console.log("this.props.sitelist",this.props.sitelist)
         var decodedString = localStorage.getItem('sitelist');
           var decod=  window.atob(decodedString);
             var sitelist= JSON.parse(decod);
             console.log("sitelist1",sitelist);
        if (this.props.sitelist || sitelist) {
            //history.push('/site_link');
            
        }
    }
    render() {
        //console.log("JSON.parse(localStorage.getItem",JSON.parse(localStorage.getItem("sitelist")));
        return (
            <div>
                <PopupShopStatus />           
            <div className="bgimg-1">

                <div className="content_main_wapper">
                      <a href="/login" className="aonboardingbackicon">
                        <button className="button onboardingbackicon">&nbsp;</button>&nbsp; Go Back
                       </a>
                    <div className="onboarding-loginBox">
                        <div className="login-form">
                            <div className="onboarding-pg-heading">
                                <h1>Select Your Site</h1>
                                <p>
                                    The sites below are registered to your Oliver POS Account.  Select the one you want to operate on this register?
                               </p>
                            </div>
                            <SiteLinkViewFirst {...this.props} />
                        </div>
                    </div>
                    <div className="powered-by-oliver">
                        <a href="javascript:void(0)">Powered by Oliver POS</a>
                    </div>
                </div>
              
            </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication } = state;
    return {
        sitelist: authentication.sitelist
    };
}

const connectedSiteLinkView = connect(mapStateToProps)(SiteLinkView);
export { connectedSiteLinkView as SiteLinkView };