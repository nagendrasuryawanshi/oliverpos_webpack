import React from 'react';
import { connect } from 'react-redux';
import { cash_reportActions } from '../actions/cash_report.action';

const cashierList = [
    {
        cashier_name:'Arno Barty',
        cashier_status:'open',
        cuurent_date:'Today',
        current_time:'1:31PM',
        cash_detail:[
            {
                description	: 'Cash Sale',
                user : 'Arno Barty',
                time : '16.54PM',
                amount	:'420.00',
                balance : '480.00'

            },
            {
                description	: 'Cash Sale',
                user : 'Arno Barty',
                time : '13.54PM',
                amount	:'420.00',
                balance : '480.00'

            }
        ]

    },
    {
        cashier_name:'Anita Boheme',
        cashier_status:'1:31PM',
        cuurent_date:'10 October 2018',
        current_time:'1:31PM',
        cash_detail:[
            {
                description	: 'Cash Sale',
                user : 'Anita Boheme',
                time : '13.54PM',
                amount	:'420.00',
                balance : '480.00'

            },
            {
                description	: 'Cash Sale',
                user : 'Anita Boheme',
                time : '13.54PM',
                amount	:'420.00',
                balance : '480.00'

            }
        ]

    }
]

class CashReportViewThird extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="panel panel-custmer bb-0" style={{ borderTopLeftRadius: 8, borderTopRightRadius: 0, borderBottomLeftRadius: 8, borderBottomRightRadius: 0 }}>
                <div className="panel-default">
                    <div className="panel-heading text-center customer_name font18 visible2">
                        Register Transactions
              </div>
                    <div className="panel-body customer_history p-0">
                        <div className="col-sm-12">
                            <div className="row">
                                <div className="col-lg-9 col-sm-8 plr-8">
                                    <form className="customer-detail">
                                        <div className="form-group m-0">
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Register:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p>Register1</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Date & Time:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p>{cashierList[0].cuurent_date} {cashierList[0].current_time}</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                <label htmlFor="">Last User:</label>
                                                <div className="col-sm-12 p-0">
                                                    <p>{cashierList[0].cashier_name}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-3 col-sm-4 bl-1 plr-8">
                                    <form action="#" className="pl-3">
                                        <div className="w-100-block button_with_checkbox p-0">
                                            <input type="radio" id="test1" name="radio-group" />
                                            <label htmlFor="test1" className="label_select_button">Print Report</label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer bt-0 p-0">
                        <div className="table_head">
                            <table className="table table-border borderless c_change mb-0 tbl-bb-1">
                                <colgroup>
                                    <col width="*" />
                                    <col width="250" />
                                    <col width="250" />
                                    <col width="250" />
                                    <col width="250" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th className="bt-0 pb-0 pl-3">Description</th>
                                        <th className="bt-0 pb-0 text-right">User</th>
                                        <th className="bt-0 pb-0 text-right">Time</th>
                                        <th className="bt-0 pb-0 text-right">Amount</th>
                                        <th className="bt-0 pb-0 text-right">Balance</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="overflowscroll cash-report">
                            <table className="table table-borderless tbl-bb-1 mb-0 font">
                                <colgroup>
                                    <col width="*" />
                                    <col width="250" />
                                    <col width="250" />
                                    <col width="250" />
                                    <col width="250" />
                                </colgroup>
                                <tbody>
                                {cashierList[0].cash_detail.map((item,index)=>{
                                    return(
                                        <tr>
                                        <td>{item.description}</td>
                                        <td className="text-right">{item.user}</td>
                                        <td className="text-right">{item.time}</td>
                                        <td className="text-right">{item.amount}</td>
                                        <td className="text-right">{item.balance}</td>
                                      </tr>
                                    )
                                })}
                                  
                                    {/* <tr>
                                        <td>Cash Sale</td>
                                        <td className="text-right">Arno Barty</td>
                                        <td className="text-right">13.54PM</td>
                                        <td className="text-right">420.00</td>
                                        <td className="text-right">480.00</td>
                                    </tr>
                                    <tr>
                                        <td>Cash Sale</td>
                                        <td className="text-right">Arno Barty</td>
                                        <td className="text-right">13.54PM</td>
                                        <td className="text-right">420.00</td>
                                        <td className="text-right">480.00</td>
                                    </tr>
                                    <tr>
                                        <td>Cash Sale</td>
                                        <td className="text-right">Arno Barty</td>
                                        <td className="text-right">13.54PM</td>
                                        <td className="text-right">420.00</td>
                                        <td className="text-right">480.00</td>
                                    </tr>
                                    <tr>
                                        <td>Cash Sale</td>
                                        <td className="text-right">Arno Barty</td>
                                        <td className="text-right">13.54PM</td>
                                        <td className="text-right">420.00</td>
                                        <td className="text-right">480.00</td>
                                    </tr> */}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    const { cash_reportlist } = state;
    return {
        cash_reportlist
    };
}

const connectedCashReportViewThird = connect(mapStateToProps, cash_reportActions)(CashReportViewThird);
export { connectedCashReportViewThird as CashReportViewThird };