import React from 'react';
import { connect } from 'react-redux';
import { cash_reportActions } from '../actions/cash_report.action';


class CashReportViewSecond extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="panel panel-custmer mb-3" id="cashblanceheight">
                <div className="panel-default">
                    <div className="panel-body pl-0 pr-0">
                        <div className="col-sm-12">
                            <div className="row">
                                <form className="customer-detail">
                                    <div className="form-group clearfix mb-0 mt-1">
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <label htmlFor=""><b>Cash Drawer Balance</b></label>
                                            <div className="col-sm-12 p-0">
                                                <p>480.00</p>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-3 pr-0 bl-1">
                                            <div className="col-lg-6 col-xs-6">
                                                <div className="w-100 button_with_checkbox p-0">
                                                    <input type="radio" id="removecash" name="radio-group" />
                                                    <label htmlFor="removecash" className="label_select_button">Remove Cash</label>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-xs-6">
                                                <div className="w-100 button_with_checkbox p-0">
                                                    <input type="radio" id="addcash" name="radio-group" />
                                                    <label htmlFor="addcash" className="label_select_button">Add Cash</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    const { cash_reportlist } = state;
    return {
        cash_reportlist
    };
}

const connectedCashReportViewSecond = connect(mapStateToProps, cash_reportActions)(CashReportViewSecond);
export { connectedCashReportViewSecond as CashReportViewSecond };