import { cash_reportConstants  } from '../constants/cash_report.constants';

export function cash_reportlist(state = {}, action) {
    switch (action.type) {
        case cash_reportConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case cash_reportConstants.GETALL_SUCCESS:
            return {
                items: action.cash_reportlist
            };
        case cash_reportConstants.GETALL_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}

