import { cash_reportConstants } from '../constants/cash_report.constants'
import { cash_reportService } from '../services/cash_report.service';


export const cash_reportActions = {
    getAll,
   
   
};

function getAll() {
    return dispatch => {
        dispatch(request());

        cash_reportService.getAll()
            .then(
                cash_reportlist => dispatch(success(cash_reportlist)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: cash_reportConstants.GETALL_REQUEST } }
    function success(cash_reportlist) { return { type: cash_reportConstants.GETALL_SUCCESS, cash_reportlist } }
    function failure(error) { return { type: cash_reportConstants.GETALL_FAILURE, error } }
}



