import { cartProductConstants } from '../_constants/cartProduct.constants';

export function cartproductlist(state = {}, action) {
    switch (action.type) {
        case cartProductConstants.PRODUCT_ADD_REQUEST:
            return {
                cartproductlist: state
            };
        case cartProductConstants.PRODUCT_ADD_SUCCESS:
            return {
                cartproductlist: action.cartproductlist
                //  [
                //     ...state.cartproductlist,
                //     action.product
                //   ] 
            };

        default:
            return state
    }
}


export function selecteditem(state = {}, action) {
    switch (action.type) {
        case cartProductConstants.SELECTED_PRODUCT_DISCOUNT_REQUEST:
            return {
                selecteditem: state
            };
        case cartProductConstants.SELECTED_PRODUCT_DISCOUNT_SUCCESS:
            return {
                selecteditem: action.selecteditem
                //  [
                //     ...state.cartproductlist,
                //     action.product
                //   ] 
            };

        default:
            return state
    }


}

export function single_product(state = {}, action){
    switch (action.type) {   

       case cartProductConstants.PRODUCT_SINGLE_ADD_SUCCESS:
           return {
               items: action.single_product
           };
       case cartProductConstants.PRODUCT_SINGLE_ADD_FAILURE:
           return {
               error: action.error
           };
       case cartProductConstants.PRODUCT_SINGLE_ADD_REQUEST:
           return {
               loading: true
           };
       default:
           return state
   }
}


export function get_single_inventory_quantity(state = {}, action){
    //console.log("get_single_inventory_quantity", action)
    switch (action.type) {   

       case cartProductConstants.GET_SINGAL_INVENTORY_SUCCESS:
           return {
               items: action.inventory_quantitiy
           };
       case cartProductConstants.GET_SINGAL_INVENTORY_FAILURE:
           return {
               error: action.error
           };
       case cartProductConstants.GET_SINGAL_INVENTORY_REQUEST:
           return {
               loading: true
           };
       default:
           return state
   }
}


export function showSelectedProduct(state = {}, action){
    //console.log("get_single_inventory_quantity", action)
    switch (action.type) {   

       case cartProductConstants.SHOW_SELECTED_PRODUCT_REQUEST:
           return {
            loading: true
           };
        case cartProductConstants.SHOW_SELECTED_PRODUCT_SUCCESS:
           return {
               items: action.showSelectedProduct
           };
       default:
           return state
   }
}
