import { taxRateConstants  } from '../_constants/taxRate.constants';

export function taxratelist(state = {}, action) {    
    switch (action.type) {        
        case taxRateConstants.GETALL_SUCCESS:
            return {
                taxratelist: action.taxratelist
            };
        case taxRateConstants.GETALL_FAILURE:
            return {
                error: action.error
            };
        case taxRateConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        default:
            return state
    }
}


export function get_tax_setting(state = {}, action){
    switch (action.type) {        
        case taxRateConstants.GET_TAXSETTING_SUCCESS:
            return {
                items: action.get_tax_setting
            };
        case taxRateConstants.GET_TAXSETTING_FAILURE:
            return {
                error: action.error
            };
        case taxRateConstants.GET_TAXSETTING_REQUEST:
            return {
                loading: true
            };
        default:
            return state
    }
}