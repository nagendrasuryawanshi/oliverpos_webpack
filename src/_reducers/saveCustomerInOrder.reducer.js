import { saveCustomerInOrderConstants } from '../_constants/saveCustomerInOrder.constants';

export function saveCustomer(state = {}, action) {    
    switch (action.type) {        
        case saveCustomerInOrderConstants.SCIO_SUCCESS:
            return {
                saveCustomer: action.saveCustomer
            };
        case saveCustomerInOrderConstants.SCIO_FAILURE:
            return {
                error: action.error
            };
        case saveCustomerInOrderConstants.SCIO_REQUEST:
            return {
                loading: true
            };
        default:
            return state
    }
}