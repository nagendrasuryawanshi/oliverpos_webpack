import { checkShopStatusConstants  } from '../_constants/checkShopStatus.constants';

export function checkshopstatus(state = {}, action) {    
    switch (action.type) {        
        case checkShopStatusConstants.SHOPSTATUS_SUCCESS:
        console.log("action.shopstatus",action.shopstatus)
            return {
                shopstatus: action.shopstatus
            };
        case checkShopStatusConstants.SHOPSTATUS_FAILURE:
        console.log("action.error",action.error)
            return {
                error: action.error
            };
        case checkShopStatusConstants.SHOPSTATUS_REQUEST:
            return {
                loading: true
            };
        default:
            return state
    }
}
