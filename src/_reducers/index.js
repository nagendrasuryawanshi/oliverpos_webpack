import { combineReducers } from 'redux';
import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { activities, single_Order_list, filteredListOrder, mail_success } from '../ActivityPage/reducers/activity.reducer';
import { customerlist, single_cutomer_list, filteredList } from '../CustomerPage/reducers/customer.reducer';
import { checkoutlist, checkout_list, shop_order ,cash_rounding ,paymentTypeName ,global_payment,tick_event } from '../CheckoutPage/reducers/checkout.reducer';
import { cash_reportlist } from '../CashReportPage/reducers/cash_report.reducer';
import { settinglist } from '../SettingPage/reducers/setting.reducer';
import { sitelist } from '../SiteLinkPage/reducers/site_link.reducer';
import { registers } from '../LoginRegisterPage/reducers/register.reducer';
import { locations } from '../LoginLocation/reducers/location.reducer';
import { productlist, attributelist, filteredProduct,ticketfield } from './allProduct.reducer';
import { sendEmail } from './sendMail.reducer'
import { cartproductlist, selecteditem, single_product, get_single_inventory_quantity, showSelectedProduct } from './cartProduct.reducer';
import { favourites, favouritesChildCategoryList, favouritesSubAttributeList, variationProductList } from '../ShopView/reducers/favouriteList.reducer';
import { categorylist } from './categoies.reducer';
import { discountlist } from './discount.reducer';
import { refundOrder } from '../RefundPage/reducers/refund.reducer';
import { externalLogin } from '../ExternalLogin/reducers/externalLogin.reducer';
import { taxratelist , get_tax_setting} from './taxrate.reducer';
import { pinlogin } from '../LoginPin/reducers/pinlogin.reducer';
import { checkshopstatus } from '../_reducers/checkShopStatus.reducer';



const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  activities,
  customerlist,
  single_cutomer_list,
  checkoutlist,
  cash_reportlist,
  settinglist,
  sitelist,
  registers,
  locations,
  productlist,
  attributelist,
  favourites,
  productlist,
  single_Order_list,
  filteredList,
  categorylist,
  cartproductlist,
  discountlist,
  filteredListOrder,
  filteredProduct,
  mail_success,
  refundOrder,
  taxratelist,
  checkout_list,
  favouritesChildCategoryList,
  favouritesSubAttributeList,
  variationProductList,
  shop_order,
  selecteditem,
  externalLogin,
  pinlogin,
  cash_rounding,
  paymentTypeName,
  global_payment,
  sendEmail,
  ticketfield,
  tick_event,
  get_tax_setting,
  single_product,
  get_single_inventory_quantity,
  checkshopstatus,
  showSelectedProduct,
//  countrylist
});

export default rootReducer;