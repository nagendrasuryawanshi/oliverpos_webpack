import React from 'react';
import { connect } from 'react-redux';
import { AllProduct } from '../../_components/AllProduct';
import { Attributes } from '../../_components/Attributes';
import { Categories } from '../../_components/Categories';
import { history } from '../../_helpers';



class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            ListItem2: [],
            item: 0,
           
        }
        console.log("history",history);

    }

    ActiveList(item) {     
        this.setState({
            active: true,
            item: item
        })

    }
          
    componentDidMount() {
        
        setTimeout(function () {
            setHeightDesktop();
        }, 1000);



    }
    componentWillReceiveProps(){     
        if(this.props.filterType==="product-search")
        {
        this.setState({
            active: true,
            item: 1
        })
    }
    }
    render() {

        const { active, ListItem2, item } = this.state;      
        return (
            <div>
                { active != true ?
                    <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                        <div className="items pt-3 ">
                            <div className="item-heading text-center">Library</div>
                            <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                                <div className="searchDiv" style={{ display: 'none' }}>
                                    <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan" />
                                </div>
                                <table className="table ShopProductTable">
                                    <colgroup>
                                        <col style={{ width: '*' }} />
                                        <col style={{ width: 40 }} />
                                    </colgroup>
                                    <tbody>
                                        <tr  className="pointer" key='1' onClick={() => this.ActiveList(1)}>
                                            <td>All Items</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                        <tr  className="pointer" key='2' onClick={() => this.ActiveList(2)}>
                                            <td>Attributes</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                        <tr  className="pointer" key='3' onClick={() => this.ActiveList(3)}>
                                            <td>Categories</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    :
                    (
                        item == 1 ?
                            <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                                <AllProduct  productData={this.props.productData} onRef={this.props.onRef} parantPage="list"  simpleProductData={this.props.simpleProductData} />
                            </div>
                            : item == 2 ?
                                <Attributes   productData={this.props.productData} onRef={this.props.onRef}   />
                                : item == 3 ?
                                    <Categories productData={this.props.productData} onRef={this.props.onRef}  />
                                    : null

                    )
                }
            </div>
        )
    }

}


function mapStateToProps(state) {
    console.log("filterType",state.filterType);
    const { registering,productlist } = state.registration;
    return {
        registering,
        productlist
    };
}

const connectedList = connect(mapStateToProps)(List);
export { connectedList as List };


