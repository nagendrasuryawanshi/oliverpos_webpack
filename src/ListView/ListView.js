import React from 'react';
import { connect } from 'react-redux';
import { CommonHeaderTwo, NavbarPage, CustomerNote, CartListView, CustomerAddFee, CommonProductPopupModal, getTaxAllProduct, SingleProductDiscountPopup, InventoryPopup, UpdateProductInventoryModal } from '../_components';
import { List } from './components/List';
import { ProductOutOfStockPopupModel } from '../_components/ProductOutOfStockPopupModel';
import { DiscountPopup } from '../_components/DiscountPopup';
import { taxRateAction } from '../_actions/taxRate.action'
import { ProductNotFound } from '../_components/ProductNotFound';
import { openDb } from 'idb';
import { get_UDid } from '../ALL_localstorage';


class ListView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hasVariationProductData: false,
            getVariationProductData: null,
            tileFilterProductData: null,
            tileFilterProducttype: null,
            discountAmount: 0,
            discountType: "",
            getSimpleProductData: [],
            hasSimpleProductData: false,
            AllProductList: [],
            inventoryCheck: null,
            isInventoryUpdate:false,

        }
        this.handleProductData = this.handleProductData.bind(this);
        this.handletileFilterData = this.handletileFilterData.bind(this);
        this.handleDiscount = this.handleDiscount.bind(this);
        this.handleSimpleProduct = this.handleSimpleProduct.bind(this);
        this.showPopuponcartlistView = this.showPopuponcartlistView.bind(this);
        this.checkInventoryData = this.checkInventoryData.bind(this);
        this.invetoryUpdate = this.invetoryUpdate.bind(this)

        var udid = get_UDid('UDID');
        const { dispatch } = this.props;
        dispatch(taxRateAction.getAll());
        const dbPromise = openDb('ProductDB', 1, upgradeDB => {
            upgradeDB.createObjectStore(udid);
        });

        const idbKeyval = {
            async get(key) {
                const db = await dbPromise;
                return db.transaction(udid).objectStore(udid).get(key);
            },
        };
        idbKeyval.get('ProductList').then(val => {
            if( !val || val.length==0 || val==null || val==""){
                console.error('Console.save: No data')
             this.setState({ AllProductList:[] });                
            }else{
            let TaxSetting = getTaxAllProduct(val)
            this.setState({ AllProductList: TaxSetting });
            }
        }
        );



    }


    componentDidMount() {
        localStorage.removeItem("VOID_SALE")
        setTimeout(function () {
            setHeightDesktop();
        }, 1000);
        // console.log("this.state.getSimpleProductData", this.state.getSimpleProductData, this.state.hasSimpleProductData);



    }

    handletileFilterData(data, type) {
        this.setState({
            tileFilterProductData: data,
            tileFilterProducttype: type,
        });
        //console.log("this.tileProductFilter",this.tileProductFilter)
        if (this.tileProductFilter) {
            this.tileProductFilter.filterProductByTile(type, data);
        }
    }

    handleProductData(productData) {
        console.log("productData in listView", productData);
        var filterdata = [];
        var allProductList = [];
        allProductList.push(this.props.productlist);
        //console.log("this.props.productlist", allProductList[0].productlist)
        //if(allProductList[0].productlist){
        if (productData.item) {
            allProductList[0].productlist.map(item => {
                if (productData.item && productData.item.Product_Id == item.WPID) {
                    filterdata.push(item)
                }
            })

        }
        // }else{
        //     alert();
        //     var variationProdect= null;
        //     if (productData) {
        //          variationProdect=  this.state.AllProductList.filter(vitem=>{ 
        //             return  (vitem.ParentId===productData.ParentId && ( vitem.ManagingStock ==false ||( vitem.ManagingStock ==true && vitem.StockQuantity>-1)))    
        //         })
        //         console.log("variationProdect 02" , variationProdect)
        //         productData.item['Variations'] = variationProdect;
        //         console.log("productData 02" , productData)
        //         this.state.AllProductList && this.state.AllProductList.map(item => {
        //             if(item.ParentId == 0){

        //             if (productData.ParentId == item.WPID) {
        //                 item['Variations'] = variationProdect;
        //                 filterdata.push(item)
        //             }
        //           }
        //         })
        //     }  
        // }

        var product = ''
        if (!productData.item) {
            product = productData;
        }

        this.setState({
            getVariationProductData: product ? product : filterdata[0],
            hasVariationProductData: true,
            loadProductAttributeComponent: true,
            variationOptionclick: 0,
            variationTitle: productData.Title ? productData.Title : productData.item ? productData.item.Title : '',
            variationId: 0,
            variationPrice: productData.Price ? productData.Price : productData.item ? productData.item.Price : '',
            variationStockQunatity: productData.StockQuantity ? productData.StockQuantity : productData.item ? productData.item.Stock : '',
            variationImage: productData.ProductImage ? productData.ProductImage : productData.item ? productData.item.Image : '',
            variationDefaultQunatity: 1 ? 1 : productData ? productData.DefaultQunatity : '',
            getSimpleProductData: null,
            handleSimpleProduct: false

        });

    }

    handleDiscount(amt, type) {
        if (amt) {
            this.setState({ discountAmount: parseFloat(amt), discountType: type });
        }

    }

    handleSimpleProduct(simpleProductData) {
        //console.log("listviewPage", simpleProductData);
        // this.state.getSimpleProductData=simpleProductData
        this.setState({
            getSimpleProductData: simpleProductData,
            hasSimpleProductData: true,
            getVariationProductData: null,
            hasVariationProductData: false
        })
        //      console.log("listviewPage123",this.state.getSimpleProductData);  

    }

    checkInventoryData(productData) {
        console.log("checkInventoryData in listview", productData)
        this.setState({ inventoryCheck: productData })
        this.state.inventoryCheck = productData;
    }

    showPopuponcartlistView(product, item) {
        console.log("showPopuponcartlistView", product, item)
        $('#VariationPopUp').modal('show');
        this.handleSimpleProduct(product);
    }

    invetoryUpdate(st){
        this.setState({isInventoryUpdate:st})
    }

    componentWillReceiveProps(recieveProps) {

           if(recieveProps.get_single_inventory_quantity){
               this.invetoryUpdate(false)
           }
           
       }


    render() {
        // console.log("LocalsorageTaxratelist", JSON.parse(localStorage.getItem("taxratelist")), JSON.stringify(localStorage.getItem("taxratelist"))) ;
        // console.log("Tax",this.props.taxratelist);
        //console.log("listViewProps",this.props);
        var taxratelist = this.props.taxratelist ? this.props.taxratelist : localStorage.getItem("taxratelist");
        const { hasSimpleProductData, getSimpleProductData } = this.state;

        const { getVariationProductData } = this.state;
        const { hasVariationProductData } = this.state;
        return (
            <div>
                <div className="wrapper">
                    <div className="overlay"></div>
                    <NavbarPage {...this.props} />
                    <div id="content">
                        <CommonHeaderTwo {...this.props} searchProductFilter={this.handletileFilterData} productData={this.handleProductData} simpleProductData={this.handleSimpleProduct} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <List productData={this.handleProductData} onRef={ref => (this.tileProductFilter = ref)} simpleProductData={this.handleSimpleProduct} filterType={this.state.tileFilterProducttype} />
                                <CartListView showPopuponcartlistView={this.showPopuponcartlistView} onDiscountAmountChange={this.handleDiscount} onChange={this.addFee} discountAmount={this.state.discountAmount} discountType={this.state.discountType} />

                            </div>
                        </div>
                    </div>
                </div>
                <div tabIndex="-1" id="popup_oliver_add_fee" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                    <CustomerAddFee />
                </div>
                <div tabIndex="-1" className="modal fade in mt-5 modal-wide" id="addnotehere" role="dialog">
                    <CustomerNote />
                </div>
                <ProductNotFound />
                {/* for update invetory qunatity */}
                <InventoryPopup inventoryCheck={this.state.inventoryCheck} isInventoryUpdate={this.invetoryUpdate} />
                <UpdateProductInventoryModal />
                <ProductOutOfStockPopupModel />
                <CommonProductPopupModal  getQuantity={localStorage.getItem("CART_QTY")} isInventoryUpdate={this.state.isInventoryUpdate} inventoryData={this.checkInventoryData} getVariationProductData={getVariationProductData ? getVariationProductData : getSimpleProductData} hasVariationProductData={hasVariationProductData ? hasVariationProductData : hasSimpleProductData} />
                <DiscountPopup onDiscountAmountChange={this.handleDiscount} taxratelist={this.props.taxratelist} />
                <SingleProductDiscountPopup onDiscountAmountChange={this.handleDiscount} taxratelist={this.props.taxratelist} />
                {/* <InventoryPopup inventoryCheck={this.state.inventoryCheck} /> */}

            </div>
        );
    }

}

function mapStateToProps(state) {
    const { discountlist, productlist, get_single_inventory_quantity, taxratelist, alert, checkout_list } = state;
    return {
        discountlist,
        productlist,
        taxratelist: taxratelist.taxratelist,
        alert,
        checkout_list: checkout_list.items,
        get_single_inventory_quantity: get_single_inventory_quantity.items,
    };
}

const connectedListView = connect(mapStateToProps)(ListView);
export { connectedListView as ListView };